function defer(method, selector) {
    if (window.jQuery) {
        if (jQuery(selector).length > 0){
            method();
        } else {
            setTimeout(function() { defer(method, selector) }, 50);
        }  
    } else {
         setTimeout(function() { defer(method, selector) }, 50);
    }    
}

defer(function(){
    $("#ta-menu-0-0").attr("href", "https://www.nespresso.com/au/en/grands-crus-coffee-range?icid=B2C_Auen_Homepage_CoffeeMegaMenu_2017_02_OurCoffeeRange_2");
},"#ta-menu-0-0");