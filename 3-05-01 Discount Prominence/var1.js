// DESKTOP

function runDesktop(){

  $(".code-promo").insertAfter(".delivery-footer");

  $(".code-promo .line-title").append('<i class="arrow down"></i>');

  $(".code-promo form").hide();

  $(".code-promo .line-title").click(function(){
    $(this).parent().find("form").slideToggle();
    $(this).find("i").toggleClass("up down");
  });

}

// MOBILE

function runMobile(){
  
  $("#activation-code-block").hide();

  $("#activation-code-form .info-bubble").before("<img class='opt-btn-toggle down' src='https://service.maxymiser.net/cm/images-eu/1/1/1/CE68B5FAF67F7B3BADFA98F0AF341D16AC37D95E1C73B3DFE2223BA4C3103778/nespresso-au/Test-3-05-01-Discount-Prominence/accordion_button_down.png' />");
 
  $(".opt-btn-toggle").click(function(){
   $("#activation-code-block").slideToggle();
    
    if($(this).hasClass("down")){
      $(this).attr("src","https://service.maxymiser.net/cm/images-eu/1/1/1/5BC22B6E39076ED17689809D4873D9E258D13A4A2F5E48A1715996FBA4934125/nespresso-au/Test-3-05-01-Discount-Prominence/accordion_button_up.png").removeClass("down").addClass("up");
    } else if ($(this).hasClass("up")){
      $(this).attr("src","https://service.maxymiser.net/cm/images-eu/1/1/1/CE68B5FAF67F7B3BADFA98F0AF341D16AC37D95E1C73B3DFE2223BA4C3103778/nespresso-au/Test-3-05-01-Discount-Prominence/accordion_button_down.png").removeClass("up").addClass("down");
    }
    
  });
}

var mobile_url = window.location.href;

if (mobile_url.indexOf("mobile") > 1){
  runMobile();
} else {
  runDesktop();
}