function defer(method, selector) {
    if (window.jQuery) {
        if (jQuery(selector).length > 0){
            method();
           
        } else {
            setTimeout(function() { defer(method, selector); }, 50);
        }  
    } else {
         setTimeout(function() { defer(method, selector); }, 50);
    }    
}

function getScript(src, callback) {
    var s = document.createElement('script');
    s.src = src;
    s.async = true;
    s.onreadystatechange = s.onload = function() {
      if (!callback.done && (!s.readyState || /loaded|complete/.test(s.readyState))) {
        callback.done = true;
        callback();
      }
    };
    document.querySelector('head').appendChild(s);
}

var variation = 2;

var plusProductList = {
    "vertuo-plus-coffee-machine-deluxe-black-c" : {
        "product": "VertuoPlus Black (Round Top)",
        "name": "Black Round Top",
        "img": "//service.maxymiser.net/cm/images-eu/1/1/1/C24059F7F680F7FBAB380345996217D77A92BECE275A5BFA2154EF05CBFE8D68/nespresso-au/Test-85-Machine-PDP-Update/VertuoplusBlack_roundtop-1.png",
        "style": 'background-color: rgb(0, 0, 0)',
        "val": 'GCB2-AU-BK-NE'
    },
    "vertuo-plus-coffee-machine-deluxe-silver-c" : {
        "product": "VertuoPlus Silver (Round Top)",
        "name": "Silver Round Top",
        "img": "//service.maxymiser.net/cm/images-eu/1/1/1/D24990BC9AB5AB28FBC58E2CB206249A6D9BABBD3C3467654FAFAB0AD3791C42/nespresso-au/Test-85-Machine-PDP-Update/VertuoplusSilver_roundtop.png",
        "style": 'background-color: rgb(204, 204, 204);',
        "val":'GCB2-AU-SI-NE'
    },
    "vertuo-plus-coffee-machine-deluxe-black-d" : {
        "product": "VertuoPlus Black (Flat Top)",
        "name": "Black Flat Top",
        "img": "//service.maxymiser.net/cm/images-eu/1/1/1/5B967462E5439916C3426DD619CF1F5A683FC611A3B683D3CD5E0385C2828921/nespresso-au/Test-85-Machine-PDP-Update/VertuoplusBlack_flattop.png",
        "style": 'background-color: rgb(0, 0, 0)',
        "val":"GDB2-AU-BK-NE"
    },
    "vertuo-plus-deluxe-titan-d-nespresso" : {
        "product": "VertuoPlus Titan (Flat Top)",
        "name": "Titan Flat Top",
        "img": "//service.maxymiser.net/cm/images-eu/1/1/1/D796B85D13E4F46931203BC905683889BEA7DAFFC16157D884953370117EC047/nespresso-au/Test-85-Machine-PDP-Update/VertuoplusSilver_flattop.png",
        "style": 'background-color: rgb(182, 175, 169);',
        "val": 'GDB2-AU-TI-NE'
    }
}

var plusProductListv2 = {
    "vertuo-plus-coffee-machine-deluxe-black-c" : {
        "product": "VertuoPlus Black (Round Top)",
        "name": "Black Round Top",
        "img": "//service.maxymiser.net/cm/images-eu/1/1/1/8AFCD17F6FE7AFC9E0402AF57796EF43EC550611FA5D3D4C1E0C413E889F4D4C/nespresso-au/Test-85-Machine-PDP-Update/blackroundtop.png",
        "style": 'background-color: rgb(0, 0, 0)',
        "val": 'GCB2-AU-BK-NE'
    },
    "vertuo-plus-coffee-machine-deluxe-silver-c" : {
        "product": "VertuoPlus Silver (Round Top)",
        "name": "Silver Round Top",
        "img": "//service.maxymiser.net/cm/images-eu/1/1/1/D5E39959209A6A83B8E87BBF36A8789F7CDA279269CC1E59964ADEE9F76831F6/nespresso-au/Test-85-Machine-PDP-Update/silverroundtop.png",
        "style": 'background-color: rgb(204, 204, 204);',
        "val":'GCB2-AU-SI-NE'
    },
    "vertuo-plus-coffee-machine-deluxe-black-d" : {
        "product": "VertuoPlus Black (Flat Top)",
        "name": "Black Flat Top",
        "img": "//service.maxymiser.net/cm/images-eu/1/1/1/7C1CDB10DD20E8A32CB6418E84A6D6E20BE33E0075C770CD3770196F9A8EA4E4/nespresso-au/Test-85-Machine-PDP-Update/blackflattop.png",
        "style": 'background-color: rgb(0, 0, 0)',
        "val":"GDB2-AU-BK-NE"
    },
    "vertuo-plus-deluxe-titan-d-nespresso" : {
        "product": "VertuoPlus Titan (Flat Top)",
        "name": "Titan Flat Top",
        "img": "//service.maxymiser.net/cm/images-eu/1/1/1/19127B0B763558E4C2858F86AB2565D37531286645B82991F2C644DD688FBFB4/nespresso-au/Test-85-Machine-PDP-Update/titanflattop.png",
        "style": 'background-color: rgb(182, 175, 169);',
        "val": 'GDB2-AU-TI-NE'
    }
}

var productList = {
    "vertuo-coffee-machine-black" : {
        "product": "Vertuo Black (Round Top)",
        "name": "Black Round Top",
        "img": "//service.maxymiser.net/cm/images-eu/1/1/1/B30A5E77D3A00664948BF1C561EDEFADC8FB6C9566A24E68675D3A1BA7098AAD/nespresso-au/Test-85-Machine-PDP-Update/Bitmap-1.png",
        "style": 'background-color: rgb(0, 0, 0)',
        "val": 'GCA1-AU-BK-NE'
    },
    "vertuo-coffee-machine-black-flat" : {
        "product": "Vertuo Black (Flat Top)",
        "name": "Black Flat Top",
        "img": "//service.maxymiser.net/cm/images-eu/1/1/1/7A9C8D5AB5B36F6D67F29B633B25D448486C3BBC9C248775388C6B4E273BFD9D/nespresso-au/Test-85-Machine-PDP-Update/Bitmap2.png",
        "style": 'background-color: rgb(0, 0, 0)',
        "val": 'GCC1-AU-BK-NE'
    }
};

var productListv2 = {
    "vertuo-coffee-machine-black" : {
        "product": "Vertuo Black (Round Top)",
        "name": "Black Round Top",
        "img": "//service.maxymiser.net/cm/images-eu/1/1/1/867FEC3B8C1EEEFF09BFFEA304862E2C8C5A17CB8DBCA5EFF7696511BF6B2C81/nespresso-au/Test-85-Machine-PDP-Update/vertuoogroundtop.png",
        "style": 'background-color: rgb(0, 0, 0)',
        "val": 'GCA1-AU-BK-NE'
    },
    "vertuo-coffee-machine-black-flat" : {
        "product": "Vertuo Black (Flat Top)",
        "name": "Black Flat Top",
        "img": "//service.maxymiser.net/cm/images-eu/1/1/1/00F3058FE3EE853C753E3249DEE1A3050D451B6584CC77A34D78E89A9214A5E3/nespresso-au/Test-85-Machine-PDP-Update/vertuoogflattop.png",
        "style": 'background-color: rgb(0, 0, 0)',
        "val": 'GCC1-AU-BK-NE'
    }
}

var vertuoPlusMachines = [
    "//service.maxymiser.net/cm/images-eu/1/1/1/19127B0B763558E4C2858F86AB2565D37531286645B82991F2C644DD688FBFB4/nespresso-au/Test-85-Machine-PDP-Update/titanflattop.png",
    "//service.maxymiser.net/cm/images-eu/1/1/1/7C1CDB10DD20E8A32CB6418E84A6D6E20BE33E0075C770CD3770196F9A8EA4E4/nespresso-au/Test-85-Machine-PDP-Update/blackflattop.png",
    "//service.maxymiser.net/cm/images-eu/1/1/1/D5E39959209A6A83B8E87BBF36A8789F7CDA279269CC1E59964ADEE9F76831F6/nespresso-au/Test-85-Machine-PDP-Update/silverroundtop.png",
    "//service.maxymiser.net/cm/images-eu/1/1/1/8AFCD17F6FE7AFC9E0402AF57796EF43EC550611FA5D3D4C1E0C413E889F4D4C/nespresso-au/Test-85-Machine-PDP-Update/blackroundtop.png",
];

var vertuoMachines = [
    "//service.maxymiser.net/cm/images-eu/1/1/1/00F3058FE3EE853C753E3249DEE1A3050D451B6584CC77A34D78E89A9214A5E3/nespresso-au/Test-85-Machine-PDP-Update/vertuoogflattop.png",
    "//service.maxymiser.net/cm/images-eu/1/1/1/867FEC3B8C1EEEFF09BFFEA304862E2C8C5A17CB8DBCA5EFF7696511BF6B2C81/nespresso-au/Test-85-Machine-PDP-Update/vertuoogroundtop.png"
];

var machineIcons = [
    "//service.maxymiser.net/cm/images-eu/1/1/1/505091CC7639EFC4438D915B7542842DA9A679B2413D4C16CDE27B3399573430/nespresso-au/Test-85-Machine-PDP-Update/autocapsule.png",
    "//service.maxymiser.net/cm/images-eu/1/1/1/02FBEEA6A122FE133100342B701B253E985A71B99638EE9AF3FDB0B039E836BD/nespresso-au/Test-85-Machine-PDP-Update/onebuttonebrew.png",
    "//service.maxymiser.net/cm/images-eu/1/1/1/546582F8224AB0417B7AAAA6585C12158DA1AAE06C3094600D5989F468D05E22/nespresso-au/Test-85-Machine-PDP-Update/moveablewatertank.png",
    "//service.maxymiser.net/cm/images-eu/1/1/1/5B8C02C2F2285A0ABC2A636CA3F9C90680D6AAEE0418713ADBA68CCE53B9EF21/nespresso-au/Test-85-Machine-PDP-Update/electricopening.png",
    "//service.maxymiser.net/cm/images-eu/1/1/1/75DF022E78D5A0BAF40216C58D59AF00706BB26C6BADB7698E07A33A4AF2334B/nespresso-au/Test-85-Machine-PDP-Update/adjustablecups.png",
    "//service.maxymiser.net/cm/images-eu/1/1/1/6EE29E42C480913619DC64E040833670D06337FC31BA4339EFD5C70FF97DBA1D/nespresso-au/Test-85-Machine-PDP-Update/heatsup25secVertuoOG.png",
    "//service.maxymiser.net/cm/images-eu/1/1/1/F92471E223F7289B8505F607AE0A1AD636AD7FB9C4FE18D6B6150C4F85A788C9/nespresso-au/Test-85-Machine-PDP-Update/onebuttonbreweingVertuoOG.png",
];

var vertuoPlusMachineLines = [
    "//service.maxymiser.net/cm/images-eu/1/1/1/82505A50DC0FAE07403DEE550978751052790498098EB497C3DBD7CE53286503/nespresso-au/Test-85-Machine-PDP-Update/autocapsuledots.png",
    "//service.maxymiser.net/cm/images-eu/1/1/1/4ED47053A3E1BB3A744CF3110257F0C7F2570B29E9CD3002D210751D8A0552E7/nespresso-au/Test-85-Machine-PDP-Update/adjustcupsdots.png",
    "//service.maxymiser.net/cm/images-eu/1/1/1/DBFE0212C22870A8CEE736011837DDFD5138191CABA605C86522704642A122DF/nespresso-au/Test-85-Machine-PDP-Update/onebuttonbrewdots.png",
    "//service.maxymiser.net/cm/images-eu/1/1/1/1C28E9463EAC25DF3255E99C8A59D87D57B318AB54F6139AC5BC67D2258D6893/nespresso-au/Test-85-Machine-PDP-Update/electricopendots.png",
    "//service.maxymiser.net/cm/images-eu/1/1/1/0DE10BE4A9E5ECB0348330DB435FC794A36674B0C4654A20E77DD5FF5B112CB2/nespresso-au/Test-85-Machine-PDP-Update/moveablewaterdots.png",
];

var vertuoMachineLines = [
    "//service.maxymiser.net/cm/images-eu/1/1/1/C5AC0F836B06479DC6120D59BF852B2661614171685FBD59DD4EBF3170A4A5D3/nespresso-au/Test-85-Machine-PDP-Update/adjustdotsogvertuo.png",
    "//service.maxymiser.net/cm/images-eu/1/1/1/C61160AD4E5FC216A93B4578EB44236776F9CC91EA797A48289A301467B68137/nespresso-au/Test-85-Machine-PDP-Update/25secondsdotsogvertuo.png",
    "//service.maxymiser.net/cm/images-eu/1/1/1/4F027134A9DF82284B226DF8B9C25B3E995FF64C3AE745A5D65119F1D1AE3B02/nespresso-au/Test-85-Machine-PDP-Update/adjustvertuodots.png",
];

var mobile = [
    "//service.maxymiser.net/cm/images-eu/1/1/1/21D7861AB4A51BDC7090A42627A5468E8B845D216645994CAAC17A3059CA1FE9/nespresso-au/Test-85-Machine-PDP-Update/mobileonebuttonbrewdots.png",
    "//service.maxymiser.net/cm/images-eu/1/1/1/DE22D1DA15271CB2A8902E2946AD07C1560E9DB96B8AAACC1E1BD167743E1864/nespresso-au/Test-85-Machine-PDP-Update/mobileautocapsuledots.png",
    "//service.maxymiser.net/cm/images-eu/1/1/1/BC299D21CF859E67D3D94076452F312BD0D14E0B4B4534EEB1324F7A793C42A7/nespresso-au/Test-85-Machine-PDP-Update/mobileadjustcupsdots.png",
    "//service.maxymiser.net/cm/images-eu/1/1/1/013DC2533913211D675DDE3292CD191CB83443D041D5E913FFD69C96A1120CAE/nespresso-au/Test-85-Machine-PDP-Update/mobilemoveablewatertankdots.png",
    "//service.maxymiser.net/cm/images-eu/1/1/1/A457DC8ED9C40309F6D4555695FE5CD63F748C3A081620DDFF4E561957D39741/nespresso-au/Test-85-Machine-PDP-Update/mobileelectricopendots.png",
];

var mobileSVG = [
    "//service.maxymiser.net/cm/images-eu/1/1/1/B1CCE0F2B48BBFCB05F8E7A1AC25641EC0E67D156921DF4CB59343C69F9C2AA5/nespresso-au/Test-85-Machine-PDP-Update/mobileelectricopendots.svg",
    "//service.maxymiser.net/cm/images-eu/1/1/1/B8C07A30AA0D71A1EF7EBC36AADA0CB6C3DF087C3D99E19FA9D0915A0F8409B9/nespresso-au/Test-85-Machine-PDP-Update/autocapsule.svg",
    "//service.maxymiser.net/cm/images-eu/1/1/1/08CF6B9CD14A631ACB43B2C15BB44DF157DDA51A9FBA3CDFBABAE3A3BD8CDC30/nespresso-au/Test-85-Machine-PDP-Update/adjustablecups.svg",
    "//service.maxymiser.net/cm/images-eu/1/1/1/381A76287D66AB038299F539496EDAC5ACD41E87CF5EC7D4C6157DBCF3A64059/nespresso-au/Test-85-Machine-PDP-Update/moveablewatertank.svg",
    "//service.maxymiser.net/cm/images-eu/1/1/1/4F425DCA22BDD4E50F1EE4398998A9DD5B353925A89E98202AF25D2F2BD99350/nespresso-au/Test-85-Machine-PDP-Update/autoonandoff.svg"
]



var button =  '<div id="AddToBagButton__button-CremaComponentId-1" class="special_add_to_cart"><button id="ta-product-details__add-to-bag-button" class="AddToBagButton AddToBagButtonLarge AddToBagButtonLarge--active" data-focus-id="AddToBagButton__button-CremaComponentId-1"><span class="AddToBagButtonLarge__basketIcon"><i aria-hidden="true" class="Glyph Glyph--basket"></i></span><span class="VisuallyHidden">You have zero of VertuoPlus Black (Round Top) in your basket. Activate to add the product</span><span class="AddToBagButtonLarge__label" aria-hidden="true">ADD TO BASKET</span><i aria-hidden="true" class="Glyph Glyph--plus AddToBagButtonLarge__plusIcon"></i></button></div>';
var popup =  '<div class="QuantitySelector" id="QuantitySelector__wrapper" role="dialog" aria-labelledby="QuantitySelector__title" aria-describedby="QuantitySelector__description"><span class="VisuallyHidden" id="QuantitySelector__title">Quantity selector</span><span class="VisuallyHidden" id="QuantitySelector__description">Choose a predefined quantity below</span><div class="QuantitySelector__container"><div class="QuantitySelector__popin QuantitySelector__popin--top"><ul class="PredefinedQuantityList"><li class="PredefinedQuantityList__quantity"><button id="ta-quantity-selector__predefined-0" data-focus-id="PredefinedQuantityList__quantity-focusable" class="PredefinedQuantityList__quantity-button"><span class="VisuallyHidden">quantityselector.a11y.removeproduct.label</span><span aria-hidden="true">0</span></button></li><li class="PredefinedQuantityList__quantity"><button id="ta-quantity-selector__predefined-1" class="PredefinedQuantityList__quantity-button"><span class="VisuallyHidden">Add 1</span><span aria-hidden="true">1</span></button></li><li class="PredefinedQuantityList__quantity"><button id="ta-quantity-selector__predefined-2" class="PredefinedQuantityList__quantity-button"><span class="VisuallyHidden">Add 2</span><span aria-hidden="true">2</span></button></li><li class="PredefinedQuantityList__quantity"><button id="ta-quantity-selector__predefined-3" class="PredefinedQuantityList__quantity-button"><span class="VisuallyHidden">Add 3</span><span aria-hidden="true">3</span></button></li><li class="PredefinedQuantityList__quantity"><button id="ta-quantity-selector__predefined-4" class="PredefinedQuantityList__quantity-button"><span class="VisuallyHidden">Add 4</span><span aria-hidden="true">4</span></button></li><li class="PredefinedQuantityList__quantity"><button id="ta-quantity-selector__predefined-5" class="PredefinedQuantityList__quantity-button"><span class="VisuallyHidden">Add 5</span><span aria-hidden="true">5</span></button></li><li class="PredefinedQuantityList__quantity"><button id="ta-quantity-selector__predefined-6" class="PredefinedQuantityList__quantity-button"><span class="VisuallyHidden">Add 6</span><span aria-hidden="true">6</span></button></li><li class="PredefinedQuantityList__quantity"><button id="ta-quantity-selector__predefined-7" class="PredefinedQuantityList__quantity-button"><span class="VisuallyHidden">Add 7</span><span aria-hidden="true">7</span></button></li><li class="PredefinedQuantityList__quantity"><button id="ta-quantity-selector__predefined-8" class="PredefinedQuantityList__quantity-button"><span class="VisuallyHidden">Add 8</span><span aria-hidden="true">8</span></button></li><li class="PredefinedQuantityList__quantity"><button id="ta-quantity-selector__predefined-9" class="PredefinedQuantityList__quantity-button"><span class="VisuallyHidden">Add 9</span><span aria-hidden="true">9</span></button></li><li class="PredefinedQuantityList__quantity"><button id="ta-quantity-selector__predefined-10" class="PredefinedQuantityList__quantity-button"><span class="VisuallyHidden">Add 10</span><span aria-hidden="true">10</span></button></li><li class="PredefinedQuantityList__quantity"><button id="ta-quantity-selector__predefined-11" class="PredefinedQuantityList__quantity-button"><span class="VisuallyHidden">Add 11</span><span aria-hidden="true">11</span></button></li><li class="PredefinedQuantityList__quantity"><button id="ta-quantity-selector__predefined-12" class="PredefinedQuantityList__quantity-button"><span class="VisuallyHidden">Add 12</span><span aria-hidden="true">12</span></button></li><li class="PredefinedQuantityList__quantity"><button id="ta-quantity-selector__predefined-13" class="PredefinedQuantityList__quantity-button"><span class="VisuallyHidden">Add 13</span><span aria-hidden="true">13</span></button></li><li class="PredefinedQuantityList__quantity"><button id="ta-quantity-selector__predefined-14" class="PredefinedQuantityList__quantity-button"><span class="VisuallyHidden">Add 14</span><span aria-hidden="true">14</span></button></li></ul><form class="QuantitySelectorCustomField__container" novalidate=""><div class="TextField QuantitySelectorCustomField__field"><div class="TextField__group TextField__group--compact"><label for="ta-quantity-selector__custom-field" class="TextField__label TextField__label--compact">\</label><input type="number" min="0" placeholder="Choose a quantity" id="ta-quantity-selector__custom-field" data-focus-id="QuantitySelectorCustomField__field" value="" class="TextField__input QuantitySelectorCustomField__input TextField__input--compact"></div></div><button id="ta-quantity-selector__custom-ok" class="QuantitySelectorCustomField__button-ok">OK</button></form></div></div></div>';
var curlist = {};

function selectColor(list, val){
    var machine = list[val];
    jQuery("#opt-color").val(val);
    var pathname = window.location.pathname.split("/");
    pathname = pathname[pathname.length - 1];
    var name = machine["product"];
    jQuery(".opt-title h3").text(name);

    //if slide is not in the current slide slot change to the corrext one.
    var pathnameSlide = getSlideNumberFromPathname(val);
    var currentSlide = jQuery(".slider-for .slick-active").attr("data-slick-index");

    jQuery(".opt-color-container .opt-color-ball").attr("style", (curlist[val]["style"]));

    if(pathnameSlide != currentSlide){
        jQuery('#opt-slider-container .slider-for').slick('slickGoTo', pathnameSlide);
    }

    jQuery(".ProductDetailsColorSwitcher__color-list a") .removeClass("opt-active-color");
    jQuery(".ProductDetailsColorSwitcher__color-list a").each(function(){
        var pathname = jQuery(this).attr("href").split("/");
        pathname = pathname[pathname.length - 1];
        if(val == pathname){
            jQuery(this).addClass("opt-active-color");
        }
    });
    console.log("select color");
}

function getSlideNumberFromPathname(pathname){
    return jQuery("div[data-value='"+pathname+"']").parent().parent().attr("data-slick-index");
}

function addQuantity(qty){
    var product = curlist[jQuery("#opt-color").val()].val;
    n_addtocart(product, qty);
}

function n_addtocart(product, qty) {
    window.CartManager.updateItem('erp.au.b2c/prod/' + product, qty);
    jQuery("body").removeClass("opt-shown");
    jQuery(".special_add_to_cart #QuantitySelector__wrapper").remove();
}

function buildVisual(data){
    jQuery(".ProductDetailsImageSlider").before('<div class="opt-left-container"></div>');
    jQuery(".opt-left-container").append('<div class="opt-title"><h3></h3></div>');
    jQuery(".ProductDetails__add-to-basket .AddToBagButton__container").append(button);
    jQuery(".special_add_to_cart #ta-product-details__add-to-bag-button").click(function(e){
        e.stopPropagation();
        console.log("running");
        if(jQuery("body").hasClass("opt-shown")){
            jQuery("body").removeClass("opt-shown");
            jQuery(".special_add_to_cart #QuantitySelector__wrapper").remove();
        } else {
            jQuery("body").addClass("opt-shown");
            jQuery(".special_add_to_cart").append(popup);
            jQuery(".special_add_to_cart #QuantitySelector__wrapper").click(function(e){
                e.preventDefault();
                e.stopPropagation();
                var target = e.target;
                if(jQuery(target).hasClass("PredefinedQuantityList__quantity-button")) {
                    var quantity = jQuery(target).attr("id").split("-");
                    quantity = quantity[quantity.length - 1];
                    addQuantity(quantity);
                } else if (jQuery(target).parent().hasClass("PredefinedQuantityList__quantity-button") ){
                   var quantity = jQuery(target).parent().attr("id").split("-");
                   quantity = quantity[quantity.length - 1];
                   addQuantity(quantity);
                } else if (jQuery(target).hasClass("QuantitySelectorCustomField__button-ok")){
                    var quantity = jQuery("#ta-quantity-selector__custom-field").val()
                    if(!quantity){
                        console.log("not a valid number");
                        jQuery("#ta-quantity-selector__custom-field").addClass("opt-error");
                    } else {
                        jQuery("#ta-quantity-selector__custom-field").removeClass("opt-error");
                        addQuantity(quantity);
                    }
                }
            });
        }
    });

    jQuery("#ta-quantity-selector__custom-field").focus(function(){
        jQuery(this).removeClass("opt-error");
    });
   

    jQuery("body").click(function(){    
        if(jQuery("body").hasClass("opt-shown")){
            jQuery("body").removeClass("opt-shown");
            jQuery(".special_add_to_cart #QuantitySelector__wrapper").remove();
        }
    });

    //update the RHS
    jQuery(".opt-color-container").remove();
    jQuery(".ProductDetails__price").after('<div class="opt-color-container"></div>');
    jQuery(".opt-color-container").append(`<span class="opt-color-ball"></span><select name="opt-color" id="opt-color"></select>`);
    
    for (var object in data){
        jQuery("#opt-color").append('<option value="'+object+'">'+data[object]["name"]+'</option>');
    }

    var pathname = window.location.pathname.split("/");
    pathname = pathname[pathname.length - 1];
    
    jQuery(".opt-color-container .opt-color-ball").attr("style", (curlist[pathname]["style"]));
    jQuery(".ProductDetailsImageSlider").append(jQuery(".ProductDetails__name"));
    jQuery("#opt-color").change(function(){
        selectColor(data, jQuery(this).val());
    });

    selectColor(data, pathname);

    //Update the LHS
    jQuery("head link[rel='stylesheet']").last().after("<link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick-theme.css' type='text/css' media='screen'>");
    jQuery("head link[rel='stylesheet']").last().after("<link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.css' type='text/css' media='screen'>");
    function getScript(src, callback) {
        var s = document.createElement('script');
        s.src = src;
        s.async = true;
        s.onreadystatechange = s.onload = function() {
          if (!callback.done && (!s.readyState || /loaded|complete/.test(s.readyState))) {
            callback.done = true;
            callback();
          }
        };
        document.querySelector('head').appendChild(s);
    }

    getScript("https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.min.js", function(){
        jQuery("div[id=opt-slider-container]").remove();
        jQuery(".opt-left-container").append('<div id="opt-slider-container"></div>');
        jQuery("#opt-slider-container").append('<div class="slider-for">');
        jQuery("#opt-slider-container").append('<div class="slider-nav">');
        for(var product in data){
            jQuery("#opt-slider-container .slider-for").append('<div data-value="'+product+'"><img src="'+data[product]["img"]+'" alt="'+data[product]["name"]+'"></div>');
            jQuery("#opt-slider-container .slider-nav").append('<div data-value="'+product+'"><img src="'+data[product]["img"]+'" alt="'+data[product]["name"]+'"></div>');
        }
        jQuery('.slider-for').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: false,
            fade: true,
            asNavFor: '.slider-nav'
        });

        if(curlist == productList) {
            jQuery('.slider-nav').slick({
                slidesToShow: 2,
                slidesToScroll: 1,
                centerMode: false,
                asNavFor: '.slider-for',
                focusOnSelect: true
            });
        } else {
            jQuery('.slider-nav').slick({
                slidesToShow: 3,
                slidesToScroll: 1,
                centerMode: true,
                asNavFor: '.slider-for',
                focusOnSelect: true
            });
        }
     

        jQuery('.slider-for').on('beforeChange', function(event, slick, currentSlide, nextSlide){
            var value = jQuery(".slider-for .slick-slide[data-slick-index='"+nextSlide+"']").find("div[data-value]").attr("data-value");
            selectColor(data, value)
        });
        if(jQuery(window).width() <= 768){
            jQuery(".ProductDetails__add-to-basket").before(jQuery("#opt-slider-container"));
        } else {
            jQuery(".opt-left-container").append(jQuery("#opt-slider-container"));
        }

        jQuery(window).resize(function(){
            if(jQuery(window).width() <= 768){
                if(jQuery(".opt-left-container #opt-slider-container").length > 0) {
                    jQuery(".ProductDetails__add-to-basket").before(jQuery("#opt-slider-container"));
                }
                
            } else {
                if(jQuery(".ProductDetails__information #opt-slider-container").length > 0) {
                    jQuery(".opt-left-container").prepend(jQuery("#opt-slider-container"));
                }
            }
        });
    });

    if(jQuery("body").hasClass("opt-85v0")){
        jQuery(".ProductDetailsBodyInformation__description:eq(0)").addClass("opt-open");

        jQuery(".ProductDetailsBodyInformation__description:eq(0)").prepend('<div class="opt-accordion-content"></div>');
        jQuery(".ProductDetailsBodyInformation__description:eq(0)").prepend('<div class="opt-accordion-header"></div>');
        jQuery(".ProductDetailsBodyInformation__description:eq(0) .opt-accordion-content").append(jQuery(".FreeHTML"));
        jQuery(".ProductDetailsBodyInformation__description:eq(0) .opt-accordion-header").append("<span>Details</span>")


        jQuery(".ProductDetailsBodyInformation__description:eq(1)").prepend('<div class="opt-accordion-content"></div>');
        jQuery(".ProductDetailsBodyInformation__description:eq(1)").prepend('<div class="opt-accordion-header"></div>');
        jQuery(".ProductDetailsBodyInformation__description:eq(1) .opt-accordion-header").append("<span>Specifications</span>");

        jQuery(".ProductDetailsBodyInformation__description:eq(1) .opt-accordion-content").append(jQuery(".ProductDetailsBodyInformation__specifications-list"));

        jQuery(".opt-accordion-header").click(function(){
            jQuery(this).parent().toggleClass("opt-open");
        })
    }
}

function postVisualUpdates(){
    jQuery(".ProductDetails__information").prepend(jQuery(".opt-title"));
    jQuery(".ProductDetails__information").after(jQuery(".opt-left-container"));
    jQuery(".opt-left-container").before(jQuery(".ProductDetails__colorSwitcher"));

    jQuery(".ProductDetailsBodyInformation__description:eq(0) .opt-accordion-content ").append(jQuery(".ProductDetailsBodyInformation__description .embed-container"));

    jQuery(".ProductDetailsColorSwitcher__color-list a").click(function(e){
        e.preventDefault();
        e.stopPropagation();
        var pathname = jQuery(this).attr("href").split("/");
        pathname = pathname[pathname.length - 1];
        selectColor(curlist, pathname);
    });
}

/** variation 3 */
var positionValue = [
    "top_middle",
    "top_top_right",
    "top_right_right",
    "right_middle",
    "bottom_right_right",
    "bottom_bottom_right",
    "bottom_middle",
    "bottom_bottom_left",
    "bottom_left_left",
    "left_middle",
    "top_left_left",
    "top_top_left"
];

var hoveri = {
    "maxHeight": 588,
    "maxWidth": 600,
    "mobileheight": 472,
    "mobileWidth": 320,
    "hoveriElements": [
        {
            "boxHeight":84,
            "boxWidth":184,
            "boxPosTop":6,
            "boxPosLeft":50,
            "positionValue":2,
            "bodyHTML": '<div class="opt-test-box"><img src="//service.maxymiser.net/cm/images-eu/1/1/1/DBFE0212C22870A8CEE736011837DDFD5138191CABA605C86522704642A122DF/nespresso-au/Test-85-Machine-PDP-Update/onebuttonbrewdots.png" alt="one button brew"></div>',
            "tailHTML":'<div class="opt-content"><div class="opt-img-wrapper"><img src="//service.maxymiser.net/cm/images-eu/1/1/1/02FBEEA6A122FE133100342B701B253E985A71B99638EE9AF3FDB0B039E836BD/nespresso-au/Test-85-Machine-PDP-Update/onebuttonebrew.png" alt></div><span>One button<br> brewing</span></div>',
           
            "mobileHeight":54,
            "mobileWidth":133,
            "mobileHTML": '<div class="opt-mobile-box"><img src="//service.maxymiser.net/cm/images-eu/1/1/1/B1CCE0F2B48BBFCB05F8E7A1AC25641EC0E67D156921DF4CB59343C69F9C2AA5/nespresso-au/Test-85-Machine-PDP-Update/mobileelectricopendots.svg" alt=""></div>',
            "mobileTailHTML":'<div class="opt-content"><div class="opt-img-wrapper"><img src="//service.maxymiser.net/cm/images-eu/1/1/1/02FBEEA6A122FE133100342B701B253E985A71B99638EE9AF3FDB0B039E836BD/nespresso-au/Test-85-Machine-PDP-Update/onebuttonebrew.png" alt></div><span>One button<br> brewing</span></div>',
            },
        {
            "boxHeight":54,
            "boxWidth":134,
            "boxPosTop":40,
            "boxPosLeft":78,
            "positionValue":1,
            "bodyHTML": `<div class="opt-test-box"><?xml version="1.0" encoding="utf-8"?>
              <svg version="1.1" id="Layer_1" class="animating" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                 viewBox="0 0 134 54" xml:space="preserve" restart="always">
              <path class="line" d="M132,52V2, M2,52H134"/>
              <path class="overlay" d="M0,52H134" fill="none" stroke-dasharray="134">
                <animate attributeType="CSS" attributeName="stroke-dashoffset" 
                       to="-134" dur="1s" begin="0s" fill="freeze"/>
              </path>
              <path class="overlay" d="M132,54V4" fill="none" stroke-dasharray="54">
               <animate id="oo"attributeType="CSS" attributeName="stroke-dashoffset" 
                       to="-54" dur="1s" begin="1s" fill="freeze"/>
              </path> 
            </svg>
            </div>`,
            "tailHTML":'<div class="opt-content"><div class="opt-img-wrapper"><img src="//service.maxymiser.net/cm/images-eu/1/1/1/75DF022E78D5A0BAF40216C58D59AF00706BB26C6BADB7698E07A33A4AF2334B/nespresso-au/Test-85-Machine-PDP-Update/adjustablecups.png" alt></div><span>Adjustable cup<br> support and different<br> cup sizes</span></div>',
            "mobileHeight":20,
            "mobileWidth":87,
            "mobileHTML": '<div class="opt-mobile-box"><img src="//service.maxymiser.net/cm/images-eu/1/1/1/B8C07A30AA0D71A1EF7EBC36AADA0CB6C3DF087C3D99E19FA9D0915A0F8409B9/nespresso-au/Test-85-Machine-PDP-Update/autocapsule.svg" alt=""></div>',
            "mobileTailHTML":'<div class="opt-content"><div class="opt-img-wrapper"><img src="//service.maxymiser.net/cm/images-eu/1/1/1/505091CC7639EFC4438D915B7542842DA9A679B2413D4C16CDE27B3399573430/nespresso-au/Test-85-Machine-PDP-Update/autocapsule.png" alt></div><span>Automatic<br> capsule ejection</span></div>',
                 
        },
        {
            "boxHeight":54,
            "boxWidth":194,
            "boxPosTop":58,
            "boxPosLeft":5,
            "positionValue":7,
            "bodyHTML": '<div class="opt-test-box"><img src="//service.maxymiser.net/cm/images-eu/1/1/1/1C28E9463EAC25DF3255E99C8A59D87D57B318AB54F6139AC5BC67D2258D6893/nespresso-au/Test-85-Machine-PDP-Update/electricopendots.png" alt="electricopening"></div>',
            "tailHTML":'<div class="opt-content"><div class="opt-img-wrapper"><img src="//service.maxymiser.net/cm/images-eu/1/1/1/5B8C02C2F2285A0ABC2A636CA3F9C90680D6AAEE0418713ADBA68CCE53B9EF21/nespresso-au/Test-85-Machine-PDP-Update/electricopening.png" alt></div><span>Electrical opening<br> and closing</span></div>',  
            "mobileHeight":54,
            "mobileWidth":33,
            "mobileHTML": '<div class="opt-mobile-box"><img src="//service.maxymiser.net/cm/images-eu/1/1/1/08CF6B9CD14A631ACB43B2C15BB44DF157DDA51A9FBA3CDFBABAE3A3BD8CDC30/nespresso-au/Test-85-Machine-PDP-Update/adjustablecups.svg" alt=""></div>',
            "mobileTailHTML":'<div class="opt-content"><div class="opt-img-wrapper"><img src="//service.maxymiser.net/cm/images-eu/1/1/1/75DF022E78D5A0BAF40216C58D59AF00706BB26C6BADB7698E07A33A4AF2334B/nespresso-au/Test-85-Machine-PDP-Update/adjustablecups.png" alt></div><span>Adjustable cup<br> support and different<br> cup sizes</span></div>',

        },
        {
            "boxHeight":4,
            "boxWidth":94,
            "boxPosTop":47,
            "boxPosLeft":-1,
            "positionValue":9,
            "bodyHTML": '<div class="opt-test-box"><img src="//service.maxymiser.net/cm/images-eu/1/1/1/0DE10BE4A9E5ECB0348330DB435FC794A36674B0C4654A20E77DD5FF5B112CB2/nespresso-au/Test-85-Machine-PDP-Update/moveablewaterdots.png" alt="moveablewatertank"></div>',
            "tailHTML":'<div class="opt-content"><div class="opt-img-wrapper"><img src="//service.maxymiser.net/cm/images-eu/1/1/1/546582F8224AB0417B7AAAA6585C12158DA1AAE06C3094600D5989F468D05E22/nespresso-au/Test-85-Machine-PDP-Update/moveablewatertank.png" alt></div><span>Moveable<br> water<br> tank</span></div>',
            "mobileHeight":54,
            "mobileWidth":183,
            "mobileHTML": '<div class="opt-mobile-box"><img src="//service.maxymiser.net/cm/images-eu/1/1/1/381A76287D66AB038299F539496EDAC5ACD41E87CF5EC7D4C6157DBCF3A64059/nespresso-au/Test-85-Machine-PDP-Update/moveablewatertank.svg" alt=""></div>',
            "mobileTailHTML":'<div class="opt-content"><div class="opt-img-wrapper"><img src="//service.maxymiser.net/cm/images-eu/1/1/1/546582F8224AB0417B7AAAA6585C12158DA1AAE06C3094600D5989F468D05E22/nespresso-au/Test-85-Machine-PDP-Update/moveablewatertank.png" alt></div><span>Moveable<br> water<br> tank</span></div>',
           },
        {
            "boxHeight":34,
            "boxWidth":134,
            "boxPosTop":13,
            "boxPosLeft":13,
            "positionValue":10,
            "bodyHTML": '<div class="opt-test-box"><img src="//service.maxymiser.net/cm/images-eu/1/1/1/82505A50DC0FAE07403DEE550978751052790498098EB497C3DBD7CE53286503/nespresso-au/Test-85-Machine-PDP-Update/autocapsuledots.png" alt="automatic"></div>',
            "tailHTML":'<div class="opt-content"><div class="opt-img-wrapper"><img src="//service.maxymiser.net/cm/images-eu/1/1/1/505091CC7639EFC4438D915B7542842DA9A679B2413D4C16CDE27B3399573430/nespresso-au/Test-85-Machine-PDP-Update/autocapsule.png" alt></div><span>Automatic<br> capsule ejection</span></div>',
            "mobileHeight":54,
            "mobileWidth":141,
            "mobileHTML": '<div class="opt-mobile-box opt-bottom"><img src="//service.maxymiser.net/cm/images-eu/1/1/1/4F425DCA22BDD4E50F1EE4398998A9DD5B353925A89E98202AF25D2F2BD99350/nespresso-au/Test-85-Machine-PDP-Update/autoonandoff.svg" alt=""></div>',
            "mobileTailHTML":'<div class="opt-content"><div class="opt-img-wrapper"><img src="//service.maxymiser.net/cm/images-eu/1/1/1/5B8C02C2F2285A0ABC2A636CA3F9C90680D6AAEE0418713ADBA68CCE53B9EF21/nespresso-au/Test-85-Machine-PDP-Update/electricopening.png" alt></div><span>Electrical opening<br> and closing</span></div>',
        }
    ]
};

var hoveri2 = {
    "maxHeight": 588,
    "maxWidth": 600,
    "mobileheight": 472,
    "mobileWidth": 320,
    "hoveriElements": [
        {
            "boxHeight":84,
            "boxWidth":184,
            "boxPosTop":6,
            "boxPosLeft":48,
            "positionValue":2,
            "bodyHTML": '<div class="opt-test-box"><img src="//service.maxymiser.net/cm/images-eu/1/1/1/DBFE0212C22870A8CEE736011837DDFD5138191CABA605C86522704642A122DF/nespresso-au/Test-85-Machine-PDP-Update/onebuttonbrewdots.png" alt="onebuttonebrew"></div>',
            "tailHTML":'<div class="opt-content"><div class="opt-img-wrapper"><img src="//service.maxymiser.net/cm/images-eu/1/1/1/02FBEEA6A122FE133100342B701B253E985A71B99638EE9AF3FDB0B039E836BD/nespresso-au/Test-85-Machine-PDP-Update/onebuttonebrew.png" alt></div><span>One button<br> brewing</span></div>',
            "mobileHeight":20,
            "mobileWidth":87,
            "mobileHTML": '<div class="opt-mobile-box "><img src="//service.maxymiser.net/cm/images-eu/1/1/1/DE22D1DA15271CB2A8902E2946AD07C1560E9DB96B8AAACC1E1BD167743E1864/nespresso-au/Test-85-Machine-PDP-Update/mobileautocapsuledots.png" alt=""></div>',
            "mobileTailHTML":'<div class="opt-content"><div class="opt-img-wrapper"><img src="//service.maxymiser.net/cm/images-eu/1/1/1/02FBEEA6A122FE133100342B701B253E985A71B99638EE9AF3FDB0B039E836BD/nespresso-au/Test-85-Machine-PDP-Update/onebuttonebrew.png" alt></div><span>One button<br> brewing</span></div>',
        },
        {
            "boxHeight":54,
            "boxWidth":134,
            "boxPosTop":44,
            "boxPosLeft":74,
            "positionValue":1,
            "bodyHTML": '<div class="opt-test-box"><img src="//service.maxymiser.net/cm/images-eu/1/1/1/4F027134A9DF82284B226DF8B9C25B3E995FF64C3AE745A5D65119F1D1AE3B02/nespresso-au/Test-85-Machine-PDP-Update/adjustvertuodots.png" alt="adjustablecups"></div>',
            "tailHTML":'<div class="opt-content"><div class="opt-img-wrapper"><img src="//service.maxymiser.net/cm/images-eu/1/1/1/75DF022E78D5A0BAF40216C58D59AF00706BB26C6BADB7698E07A33A4AF2334B/nespresso-au/Test-85-Machine-PDP-Update/adjustablecups.png" alt></div><span>Adjustable cup<br> support and<br> different cup sizes</span></div>',
            "mobileHeight":20,
            "mobileWidth":87,
            "mobileHTML": '<div class="opt-mobile-box "><img src="//service.maxymiser.net/cm/images-eu/1/1/1/DE22D1DA15271CB2A8902E2946AD07C1560E9DB96B8AAACC1E1BD167743E1864/nespresso-au/Test-85-Machine-PDP-Update/mobileautocapsuledots.png" alt=""></div>',
            "mobileTailHTML":'<div class="opt-content"><div class="opt-img-wrapper"><img src="//service.maxymiser.net/cm/images-eu/1/1/1/75DF022E78D5A0BAF40216C58D59AF00706BB26C6BADB7698E07A33A4AF2334B/nespresso-au/Test-85-Machine-PDP-Update/adjustablecups.png" alt></div><span>Electrical opening<br> and closing</span></div>',
        },
        {
            "boxHeight":54,
            "boxWidth":74,
            "boxPosTop":24,
            "boxPosLeft":2,
            "positionValue":11,
            "bodyHTML": '<div class="opt-test-box"><img src="//service.maxymiser.net/cm/images-eu/1/1/1/C61160AD4E5FC216A93B4578EB44236776F9CC91EA797A48289A301467B68137/nespresso-au/Test-85-Machine-PDP-Update/25secondsdotsogvertuo.png" alt="Electrical"></div>',
            "tailHTML": '<div class="opt-content"><div class="opt-img-wrapper"><img src="//service.maxymiser.net/cm/images-eu/1/1/1/6EE29E42C480913619DC64E040833670D06337FC31BA4339EFD5C70FF97DBA1D/nespresso-au/Test-85-Machine-PDP-Update/heatsup25secVertuoOG.png" alt></div><span>Heats up within<br> 25 seconds</span></div>',
            "mobileHeight":20,
            "mobileWidth":87,
            "mobileHTML": '<div class="opt-mobile-box"><img src="//service.maxymiser.net/cm/images-eu/1/1/1/DE22D1DA15271CB2A8902E2946AD07C1560E9DB96B8AAACC1E1BD167743E1864/nespresso-au/Test-85-Machine-PDP-Update/mobileautocapsuledots.png" alt=""></div>',
            "mobileTailHTML":'<div class="opt-content"><div class="opt-img-wrapper"><img src="//service.maxymiser.net/cm/images-eu/1/1/1/6EE29E42C480913619DC64E040833670D06337FC31BA4339EFD5C70FF97DBA1D/nespresso-au/Test-85-Machine-PDP-Update/heatsup25secVertuoOG.png" alt></div><span>Electrical opening<br> and closing</span></div>',
        }
    ]
};

jQuery.fn.Hoveri = function(config) {
    var _element = jQuery(this);
    var _config = config;
    var _hoveri;
    function init(){
        jQuery(_element).after('<div id="opt-hoveri"></div>');
        _hoveri = jQuery(_element).next();
        jQuery("#opt-hoveri").append('<div class="opt-hover-container"></div>');
        
        for(var i = 0; i < _config["hoveriElements"].length; i++){
            var hoveriElement = _config["hoveriElements"][i];
            jQuery(".opt-hover-container").append('<div class="opt-hover-element" style="top: '+hoveriElement["boxPosTop"]+'%;left:'+hoveriElement["boxPosLeft"]+'%;"></div>');
            var element = jQuery(".opt-hover-element:eq("+i+")");
            jQuery(element).append('<div class="opt-body" data-width="'+hoveriElement["boxWidth"]+'"  data-height="'+hoveriElement["boxHeight"]+'">'+hoveriElement["bodyHTML"]+'</div>');
            jQuery(element).append('<div class="opt-mobile" data-width="'+hoveriElement["mobileWidth"]+'"  data-height="'+hoveriElement["mobileHeight"]+'">'+hoveriElement["mobileHTML"]+'</div>');
            jQuery(element).find(".opt-body").append('<div class="opt-tail '+positionValue[hoveriElement["positionValue"]]+' ">'+hoveriElement["tailHTML"]+'</div>');
            jQuery(element).find(".opt-mobile").append('<div class="opt-tail">'+hoveriElement["mobileTailHTML"]+'</div>');
        }

        jQuery(".opt-hover-element").first().addClass("opt-active");

        jQuery("#opt-hoveri").append('<div class="opt-mobile-info"></div>');

        jQuery(window).resize(function(){
            resize();
        });

        setTimeout(function(){
            set();
        },500);

        jQuery(".opt-hover-element").click(function(){
            jQuery(".opt-hover-element").removeClass("opt-active");
            jQuery(this).addClass("opt-active");
            updateMobileInfo();
        })
    }

    function updateMobileInfo(){
        var activeSection = jQuery(".opt-hover-container .opt-active .opt-mobile .opt-content").html();
        jQuery("#opt-hoveri .opt-mobile-info").html(activeSection);
        jQuery(".opt-85v2 #opt-slider-container .opt-mobile-info .opt-img-wrapper").width(jQuery("#opt-hoveri .opt-hover-element .opt-img-wrapper").last().height());
        jQuery(".opt-85v2 #opt-slider-container .opt-mobile-info .opt-img-wrapper").height(jQuery("#opt-hoveri .opt-hover-element .opt-img-wrapper").last().height());
    }

    function set() {
        if(jQuery(window).width() <= 768){
            jQuery(_element).find(".slider-for").prepend(jQuery("#opt-hoveri"));
            var width = jQuery("#opt-slider-container").width();
            var scale = width / config["mobileWidth"];
            jQuery(".opt-hover-element .opt-mobile").each(function(){
                var width = jQuery(this).attr("data-width")* scale;
                var height = jQuery(this).attr("data-height")* scale; 
                jQuery(this).find(".opt-mobile-box").width(width);
                jQuery(this).find(".opt-mobile-box").height(height);
            });
            setTimeout(function(){
                jQuery("#opt-hoveri .opt-hover-element").width(jQuery("#opt-hoveri .opt-hover-element").height());
                updateMobileInfo();
            }, 100);
        } else {
            jQuery(_element).after(jQuery("#opt-hoveri"));
            var width = jQuery("#opt-slider-container").width();
            var scale = width / config["maxWidth"];
            jQuery(".opt-hover-element .opt-body").each(function(){
                var width = jQuery(this).attr("data-width")* scale;
                var height = jQuery(this).attr("data-height")* scale; 
                jQuery(this).width(width);
                jQuery(this).height(height);
            });
        }
    }
    function resize(){
        set();
    }
    init();
};

function initTest(data){
    defer(function(){
        jQuery("#opt-slider-container").Hoveri(data);
    }, "#opt-slider-container");
}

/** run variation */

defer(function(){
    defer(function(){
        if(window.location.pathname.indexOf("plus") >= 0){
            if(variation != 2){
                curlist = plusProductList;
            } else {
                curlist = plusProductListv2;
            }
          
        } else {
            if(variation != 2){
                curlist = productList;
            } else {
                curlist = productListv2;
            }
        }
        switch(variation) {
            case 0: 
                jQuery("body").addClass("opt-85"); 
                jQuery("body").addClass("opt-85v"+variation);
                jQuery(".ProductDetails__information").append(jQuery(".ProductDetailsBodyInformation__description"));
                setTimeout(function(){
                    buildVisual(curlist);
                },500);
                break;
            case 1:
                jQuery("body").addClass("opt-85"); 
                jQuery("body").addClass("opt-85v"+variation);
                jQuery(".ProductDetails__information").append(jQuery(".ProductDetailsBodyInformation__description"));
                setTimeout(function(){
                    buildVisual(curlist);
                },500);
                break;
            case 2:
                jQuery("body").addClass("opt-85"); 
                jQuery("body").addClass("opt-85v0"); 
                jQuery("body").addClass("opt-85v"+variation);
                jQuery(".ProductDetailsReferenceOrder").before(jQuery(".ProductDetailsBodyInformation__description").addClass("ResponsiveContainer"));
                setTimeout(function(){
                    buildVisual(curlist);
                    postVisualUpdates();
                    if(window.location.pathname.indexOf("plus") >= 0){
                        initTest(hoveri);
                    } else {
                        initTest(hoveri2);
                    }
                   
                },500);
                break;
            default:
                break;
        }
    }, ".ProductDetailsBodyInformation__description");
}, ".ProductDetailsImageSlider");