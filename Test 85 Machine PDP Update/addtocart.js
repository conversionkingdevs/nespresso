function addQuantity(qty, pid){
    n_addtocart(pid, qty);
}

//add button here

//clicking on body will remove the quantity selector
jQuery("body").click(function(){    
    if(jQuery("body").hasClass("opt-shown")){
        jQuery("body").removeClass("opt-shown");
        jQuery(".special_add_to_cart #QuantitySelector__wrapper").remove();
    }
});

//clicking on the add to cart add the quantity popup
jQuery(".special_add_to_cart #ta-product-details__add-to-bag-button").click(function(e){
    e.stopPropagation();

    //is the popup already showing
    if(jQuery("body").hasClass("opt-shown")){
        jQuery("body").removeClass("opt-shown");
        jQuery(".special_add_to_cart #QuantitySelector__wrapper").remove();
    } else {

        //add the popup
        jQuery("body").addClass("opt-shown");
        jQuery(".special_add_to_cart").append(popup);

        //handle clicks on quantity selector
        jQuery(".special_add_to_cart #QuantitySelector__wrapper").click(function(e){
            e.preventDefault();
            e.stopPropagation();
            var target = e.target;
            //clicking on the button text, button or entered into inout field add quantity.
            if(jQuery(target).hasClass("PredefinedQuantityList__quantity-button")) {
                var quantity = jQuery(target).attr("id").split("-");
                quantity = quantity[quantity.length - 1];
                addQuantity(quantity);
            } else if (jQuery(target).parent().hasClass("PredefinedQuantityList__quantity-button") ){
               var quantity = jQuery(target).parent().attr("id").split("-");
               quantity = quantity[quantity.length - 1];
               addQuantity(quantity);
            } else if (jQuery(target).hasClass("QuantitySelectorCustomField__button-ok")){
                var quantity = jQuery("#ta-quantity-selector__custom-field").val()
                if(!quantity){
                    console.log("not a valid number");
                    jQuery("#ta-quantity-selector__custom-field").addClass("opt-error");
                } else {
                    jQuery("#ta-quantity-selector__custom-field").removeClass("opt-error");
                    addQuantity(quantity);
                }
            }
        });
    }
});

var button =  '<div id="AddToBagButton__button-CremaComponentId-1" class="special_add_to_cart"><button id="ta-product-details__add-to-bag-button" class="AddToBagButton AddToBagButtonLarge AddToBagButtonLarge--active" data-focus-id="AddToBagButton__button-CremaComponentId-1"><span class="AddToBagButtonLarge__basketIcon"><i aria-hidden="true" class="Glyph Glyph--basket"></i></span><span class="VisuallyHidden">You have zero of VertuoPlus Black (Round Top) in your basket. Activate to add the product</span><span class="AddToBagButtonLarge__label" aria-hidden="true">ADD TO BASKET</span><i aria-hidden="true" class="Glyph Glyph--plus AddToBagButtonLarge__plusIcon"></i></button></div>';
var popup =  '<div class="QuantitySelector" id="QuantitySelector__wrapper" role="dialog" aria-labelledby="QuantitySelector__title" aria-describedby="QuantitySelector__description"><span class="VisuallyHidden" id="QuantitySelector__title">Quantity selector</span><span class="VisuallyHidden" id="QuantitySelector__description">Choose a predefined quantity below</span><div class="QuantitySelector__container"><div class="QuantitySelector__popin QuantitySelector__popin--top"><ul class="PredefinedQuantityList"><li class="PredefinedQuantityList__quantity"><button id="ta-quantity-selector__predefined-0" data-focus-id="PredefinedQuantityList__quantity-focusable" class="PredefinedQuantityList__quantity-button"><span class="VisuallyHidden">quantityselector.a11y.removeproduct.label</span><span aria-hidden="true">0</span></button></li><li class="PredefinedQuantityList__quantity"><button id="ta-quantity-selector__predefined-1" class="PredefinedQuantityList__quantity-button"><span class="VisuallyHidden">Add 1</span><span aria-hidden="true">1</span></button></li><li class="PredefinedQuantityList__quantity"><button id="ta-quantity-selector__predefined-2" class="PredefinedQuantityList__quantity-button"><span class="VisuallyHidden">Add 2</span><span aria-hidden="true">2</span></button></li><li class="PredefinedQuantityList__quantity"><button id="ta-quantity-selector__predefined-3" class="PredefinedQuantityList__quantity-button"><span class="VisuallyHidden">Add 3</span><span aria-hidden="true">3</span></button></li><li class="PredefinedQuantityList__quantity"><button id="ta-quantity-selector__predefined-4" class="PredefinedQuantityList__quantity-button"><span class="VisuallyHidden">Add 4</span><span aria-hidden="true">4</span></button></li><li class="PredefinedQuantityList__quantity"><button id="ta-quantity-selector__predefined-5" class="PredefinedQuantityList__quantity-button"><span class="VisuallyHidden">Add 5</span><span aria-hidden="true">5</span></button></li><li class="PredefinedQuantityList__quantity"><button id="ta-quantity-selector__predefined-6" class="PredefinedQuantityList__quantity-button"><span class="VisuallyHidden">Add 6</span><span aria-hidden="true">6</span></button></li><li class="PredefinedQuantityList__quantity"><button id="ta-quantity-selector__predefined-7" class="PredefinedQuantityList__quantity-button"><span class="VisuallyHidden">Add 7</span><span aria-hidden="true">7</span></button></li><li class="PredefinedQuantityList__quantity"><button id="ta-quantity-selector__predefined-8" class="PredefinedQuantityList__quantity-button"><span class="VisuallyHidden">Add 8</span><span aria-hidden="true">8</span></button></li><li class="PredefinedQuantityList__quantity"><button id="ta-quantity-selector__predefined-9" class="PredefinedQuantityList__quantity-button"><span class="VisuallyHidden">Add 9</span><span aria-hidden="true">9</span></button></li><li class="PredefinedQuantityList__quantity"><button id="ta-quantity-selector__predefined-10" class="PredefinedQuantityList__quantity-button"><span class="VisuallyHidden">Add 10</span><span aria-hidden="true">10</span></button></li><li class="PredefinedQuantityList__quantity"><button id="ta-quantity-selector__predefined-11" class="PredefinedQuantityList__quantity-button"><span class="VisuallyHidden">Add 11</span><span aria-hidden="true">11</span></button></li><li class="PredefinedQuantityList__quantity"><button id="ta-quantity-selector__predefined-12" class="PredefinedQuantityList__quantity-button"><span class="VisuallyHidden">Add 12</span><span aria-hidden="true">12</span></button></li><li class="PredefinedQuantityList__quantity"><button id="ta-quantity-selector__predefined-13" class="PredefinedQuantityList__quantity-button"><span class="VisuallyHidden">Add 13</span><span aria-hidden="true">13</span></button></li><li class="PredefinedQuantityList__quantity"><button id="ta-quantity-selector__predefined-14" class="PredefinedQuantityList__quantity-button"><span class="VisuallyHidden">Add 14</span><span aria-hidden="true">14</span></button></li></ul><form class="QuantitySelectorCustomField__container" novalidate=""><div class="TextField QuantitySelectorCustomField__field"><div class="TextField__group TextField__group--compact"><label for="ta-quantity-selector__custom-field" class="TextField__label TextField__label--compact">\</label><input type="number" min="0" placeholder="Choose a quantity" id="ta-quantity-selector__custom-field" data-focus-id="QuantitySelectorCustomField__field" value="" class="TextField__input QuantitySelectorCustomField__input TextField__input--compact"></div></div><button id="ta-quantity-selector__custom-ok" class="QuantitySelectorCustomField__button-ok">OK</button></form></div></div></div>';


