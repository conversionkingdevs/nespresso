

function defer(method, selector) {
    if (window.jQuery) {
        if (jQuery(selector).length > 0){
            method();
           
        } else {
            setTimeout(function() { defer(method, selector); }, 50);
        }  
    } else {
         setTimeout(function() { defer(method, selector); }, 50);
    }    
}

function getScript(src, callback) {
    var s = document.createElement('script');
    s.src = src;
    s.async = true;
    s.onreadystatechange = s.onload = function() {
      if (!callback.done && (!s.readyState || /loaded|complete/.test(s.readyState))) {
        callback.done = true;
        callback();
      }
    };
    document.querySelector('head').appendChild(s);
}

var variation = 0;

var plusProductList = {
    "vertuo-plus-coffee-machine-deluxe-black-c" : {
        "product": "VertuoPlus Black (Round Top)",
        "name": "Black Round Top",
        "img": "//service.maxymiser.net/cm/images-eu/1/1/1/C24059F7F680F7FBAB380345996217D77A92BECE275A5BFA2154EF05CBFE8D68/nespresso-au/Test-85-Machine-PDP-Update/VertuoplusBlack_roundtop-1.png",
        "style": 'background-color: rgb(0, 0, 0)',
        "val": 'GCB2-AU-BK-NE'
    },
    "vertuo-plus-coffee-machine-deluxe-silver-c" : {
        "product": "VertuoPlus Silver (Round Top)",
        "name": "Silver Round Top",
        "img": "//service.maxymiser.net/cm/images-eu/1/1/1/D24990BC9AB5AB28FBC58E2CB206249A6D9BABBD3C3467654FAFAB0AD3791C42/nespresso-au/Test-85-Machine-PDP-Update/VertuoplusSilver_roundtop.png",
        "style": 'background-color: rgb(204, 204, 204);',
        "val":'GCB2-AU-SI-NE'
    },
    "vertuo-plus-coffee-machine-deluxe-black-d" : {
        "product": "VertuoPlus Black (Flat Top)",
        "name": "Black Flat Top",
        "img": "//service.maxymiser.net/cm/images-eu/1/1/1/5B967462E5439916C3426DD619CF1F5A683FC611A3B683D3CD5E0385C2828921/nespresso-au/Test-85-Machine-PDP-Update/VertuoplusBlack_flattop.png",
        "style": 'background-color: rgb(0, 0, 0)',
        "val":"GDB2-AU-BK-NE"
    },
    "vertuo-plus-deluxe-titan-d-nespresso" : {
        "product": "VertuoPlus Titan (Flat Top)",
        "name": "Titan Flat Top",
        "img": "//service.maxymiser.net/cm/images-eu/1/1/1/D796B85D13E4F46931203BC905683889BEA7DAFFC16157D884953370117EC047/nespresso-au/Test-85-Machine-PDP-Update/VertuoplusSilver_flattop.png",
        "style": 'background-color: rgb(182, 175, 169);',
        "val": 'GDB2-AU-TI-NE'
    }
}

var productList = {
    "vertuo-coffee-machine-black" : {
        "product": "Vertuo Black (Round Top)",
        "name": "Black Round Top",
        "img": "//service.maxymiser.net/cm/images-eu/1/1/1/B30A5E77D3A00664948BF1C561EDEFADC8FB6C9566A24E68675D3A1BA7098AAD/nespresso-au/Test-85-Machine-PDP-Update/Bitmap-1.png",
        "style": 'background-color: rgb(0, 0, 0)',
        "val": 'GCA1-AU-BK-NE'
    },
    "vertuo-coffee-machine-black-flat" : {
        "product": "Vertuo Black (Flat Top)",
        "name": "Black Flat Top",
        "img": "//service.maxymiser.net/cm/images-eu/1/1/1/7A9C8D5AB5B36F6D67F29B633B25D448486C3BBC9C248775388C6B4E273BFD9D/nespresso-au/Test-85-Machine-PDP-Update/Bitmap2.png",
        "style": 'background-color: rgb(0, 0, 0)',
        "val": 'GCC1-AU-BK-NE'
    }
};

var button =  '<div id="AddToBagButton__button-CremaComponentId-1" class="special_add_to_cart"><button id="ta-product-details__add-to-bag-button" class="AddToBagButton AddToBagButtonLarge AddToBagButtonLarge--active" data-focus-id="AddToBagButton__button-CremaComponentId-1"><span class="AddToBagButtonLarge__basketIcon"><i aria-hidden="true" class="Glyph Glyph--basket"></i></span><span class="VisuallyHidden">You have zero of VertuoPlus Black (Round Top) in your basket. Activate to add the product</span><span class="AddToBagButtonLarge__label" aria-hidden="true">ADD TO BASKET</span><i aria-hidden="true" class="Glyph Glyph--plus AddToBagButtonLarge__plusIcon"></i></button></div>'
var popup =  '<div class="QuantitySelector" id="QuantitySelector__wrapper" role="dialog" aria-labelledby="QuantitySelector__title" aria-describedby="QuantitySelector__description"><span class="VisuallyHidden" id="QuantitySelector__title">Quantity selector</span><span class="VisuallyHidden" id="QuantitySelector__description">Choose a predefined quantity below</span><div class="QuantitySelector__container"><div class="QuantitySelector__popin QuantitySelector__popin--top"><ul class="PredefinedQuantityList"><li class="PredefinedQuantityList__quantity"><button id="ta-quantity-selector__predefined-0" data-focus-id="PredefinedQuantityList__quantity-focusable" class="PredefinedQuantityList__quantity-button"><span class="VisuallyHidden">quantityselector.a11y.removeproduct.label</span><span aria-hidden="true">0</span></button></li><li class="PredefinedQuantityList__quantity"><button id="ta-quantity-selector__predefined-1" class="PredefinedQuantityList__quantity-button"><span class="VisuallyHidden">Add 1</span><span aria-hidden="true">1</span></button></li><li class="PredefinedQuantityList__quantity"><button id="ta-quantity-selector__predefined-2" class="PredefinedQuantityList__quantity-button"><span class="VisuallyHidden">Add 2</span><span aria-hidden="true">2</span></button></li><li class="PredefinedQuantityList__quantity"><button id="ta-quantity-selector__predefined-3" class="PredefinedQuantityList__quantity-button"><span class="VisuallyHidden">Add 3</span><span aria-hidden="true">3</span></button></li><li class="PredefinedQuantityList__quantity"><button id="ta-quantity-selector__predefined-4" class="PredefinedQuantityList__quantity-button"><span class="VisuallyHidden">Add 4</span><span aria-hidden="true">4</span></button></li><li class="PredefinedQuantityList__quantity"><button id="ta-quantity-selector__predefined-5" class="PredefinedQuantityList__quantity-button"><span class="VisuallyHidden">Add 5</span><span aria-hidden="true">5</span></button></li><li class="PredefinedQuantityList__quantity"><button id="ta-quantity-selector__predefined-6" class="PredefinedQuantityList__quantity-button"><span class="VisuallyHidden">Add 6</span><span aria-hidden="true">6</span></button></li><li class="PredefinedQuantityList__quantity"><button id="ta-quantity-selector__predefined-7" class="PredefinedQuantityList__quantity-button"><span class="VisuallyHidden">Add 7</span><span aria-hidden="true">7</span></button></li><li class="PredefinedQuantityList__quantity"><button id="ta-quantity-selector__predefined-8" class="PredefinedQuantityList__quantity-button"><span class="VisuallyHidden">Add 8</span><span aria-hidden="true">8</span></button></li><li class="PredefinedQuantityList__quantity"><button id="ta-quantity-selector__predefined-9" class="PredefinedQuantityList__quantity-button"><span class="VisuallyHidden">Add 9</span><span aria-hidden="true">9</span></button></li><li class="PredefinedQuantityList__quantity"><button id="ta-quantity-selector__predefined-10" class="PredefinedQuantityList__quantity-button"><span class="VisuallyHidden">Add 10</span><span aria-hidden="true">10</span></button></li><li class="PredefinedQuantityList__quantity"><button id="ta-quantity-selector__predefined-11" class="PredefinedQuantityList__quantity-button"><span class="VisuallyHidden">Add 11</span><span aria-hidden="true">11</span></button></li><li class="PredefinedQuantityList__quantity"><button id="ta-quantity-selector__predefined-12" class="PredefinedQuantityList__quantity-button"><span class="VisuallyHidden">Add 12</span><span aria-hidden="true">12</span></button></li><li class="PredefinedQuantityList__quantity"><button id="ta-quantity-selector__predefined-13" class="PredefinedQuantityList__quantity-button"><span class="VisuallyHidden">Add 13</span><span aria-hidden="true">13</span></button></li><li class="PredefinedQuantityList__quantity"><button id="ta-quantity-selector__predefined-14" class="PredefinedQuantityList__quantity-button"><span class="VisuallyHidden">Add 14</span><span aria-hidden="true">14</span></button></li></ul><form class="QuantitySelectorCustomField__container" novalidate=""><div class="TextField QuantitySelectorCustomField__field"><div class="TextField__group TextField__group--compact"><label for="ta-quantity-selector__custom-field" class="TextField__label TextField__label--compact">\</label><input type="number" min="0" placeholder="Choose a quantity" id="ta-quantity-selector__custom-field" data-focus-id="QuantitySelectorCustomField__field" value="" class="TextField__input QuantitySelectorCustomField__input TextField__input--compact"></div></div><button id="ta-quantity-selector__custom-ok" class="QuantitySelectorCustomField__button-ok">OK</button></form></div></div></div>';


var curlist = {};

function selectColor(list, val){
    console.log(list, val);
    var machine = list[val];
    jQuery("#opt-color").val(val);
    var pathname = window.location.pathname.split("/");
    pathname = pathname[pathname.length - 1];
    var name = machine["product"];
    jQuery(".opt-title h3").text(name);

    //if slide is not in the current slide slot change to the corrext one.
    var pathnameSlide = getSlideNumberFromPathname(val);
    var currentSlide = jQuery(".slider-for .slick-active").attr("data-slick-index");

    jQuery(".opt-color-container .opt-color-ball").attr("style", (curlist[val]["style"]));

    if(pathnameSlide != currentSlide){
        console.log("going to the pathanme slide"+ pathnameSlide);
        jQuery('#opt-slider-container .slider-for').slick('slickGoTo', pathnameSlide);
    } 
}

function getSlideNumberFromPathname(pathname){
    return jQuery("div[data-value='"+pathname+"']").parent().parent().attr("data-slick-index");
}

function addQuantity(qty){
    var product = curlist[jQuery("#opt-color").val()].val;

    n_addtocart(product, qty);
}

function n_addtocart(product, qty) {
    window.CartManager.updateItem('erp.au.b2c/prod/' + product, qty);
    jQuery("body").removeClass("opt-shown");
    jQuery(".special_add_to_cart #QuantitySelector__wrapper").remove();
}

function buildVisual(data){
    jQuery(".ProductDetailsImageSlider").before('<div class="opt-left-container"></div>');
    jQuery(".opt-left-container").append('<div class="opt-title"><h3></h3></div>');
    jQuery(".ProductDetails__add-to-basket .AddToBagButton__container").append(button);
    jQuery(".special_add_to_cart #ta-product-details__add-to-bag-button").click(function(e){
        e.stopPropagation();
        console.log("running");
        if(jQuery("body").hasClass("opt-shown")){
            jQuery("body").removeClass("opt-shown");
            jQuery(".special_add_to_cart #QuantitySelector__wrapper").remove();
        } else {
            jQuery("body").addClass("opt-shown");
            jQuery(".special_add_to_cart").append(popup);
            jQuery(".special_add_to_cart #QuantitySelector__wrapper").click(function(e){
                e.preventDefault();
                e.stopPropagation();
                var target = e.target;
                if(jQuery(target).hasClass("PredefinedQuantityList__quantity-button")) {
                    var quantity = jQuery(target).attr("id").split("-");
                    quantity = quantity[quantity.length - 1];
                    addQuantity(quantity);
                } else if (jQuery(target).parent().hasClass("PredefinedQuantityList__quantity-button") ){
                   var quantity = jQuery(target).parent().attr("id").split("-");
                   quantity = quantity[quantity.length - 1];
                   addQuantity(quantity);
                } else if (jQuery(target).hasClass("QuantitySelectorCustomField__button-ok")){
                    var quantity = jQuery("#ta-quantity-selector__custom-field").val()
                    if(!quantity){
                        console.log("not a valid number");
                        jQuery("#ta-quantity-selector__custom-field").addClass("opt-error");
                    } else {
                        jQuery("#ta-quantity-selector__custom-field").removeClass("opt-error");
                        addQuantity(quantity);
                    }
                }
            });
        }
    });

    jQuery("#ta-quantity-selector__custom-field").focus(function(){
        jQuery(this).removeClass("opt-error");
    });
   

    jQuery("body").click(function(){    
        if(jQuery("body").hasClass("opt-shown")){
            jQuery("body").removeClass("opt-shown");
            jQuery(".special_add_to_cart #QuantitySelector__wrapper").remove();
        }
    });

    //update the RHS
    jQuery(".opt-color-container").remove();
    jQuery(".ProductDetails__price").after('<div class="opt-color-container"></div>');
    jQuery(".opt-color-container").append(`<span class="opt-color-ball"></span><select name="opt-color" id="opt-color"></select>`);
    
    for (var object in data){
        jQuery("#opt-color").append('<option value="'+object+'">'+data[object]["name"]+'</option>');
    }

    var pathname = window.location.pathname.split("/");
    pathname = pathname[pathname.length - 1];
    
    jQuery(".opt-color-container .opt-color-ball").attr("style", (curlist[pathname]["style"]));
    jQuery(".ProductDetailsImageSlider").append(jQuery(".ProductDetails__name"));
    jQuery("#opt-color").change(function(){
        selectColor(data, jQuery(this).val());
    });

    selectColor(data, pathname);

    //Update the LHS
    jQuery("head link[rel='stylesheet']").last().after("<link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick-theme.css' type='text/css' media='screen'>");
    jQuery("head link[rel='stylesheet']").last().after("<link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.css' type='text/css' media='screen'>");
    function getScript(src, callback) {
        var s = document.createElement('script');
        s.src = src;
        s.async = true;
        s.onreadystatechange = s.onload = function() {
          if (!callback.done && (!s.readyState || /loaded|complete/.test(s.readyState))) {
            callback.done = true;
            callback();
          }
        };
        document.querySelector('head').appendChild(s);
    }

    getScript("https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.min.js", function(){
        jQuery("div[id=opt-slider-container]").remove();
        jQuery(".opt-left-container").append('<div id="opt-slider-container"></div>');
        jQuery("#opt-slider-container").append('<div class="slider-for">');
        jQuery("#opt-slider-container").append('<div class="slider-nav">');
        for(var product in data){
            jQuery("#opt-slider-container .slider-for").append('<div data-value="'+product+'"><img src="'+data[product]["img"]+'" alt="'+data[product]["name"]+'"></div>');
            jQuery("#opt-slider-container .slider-nav").append('<div data-value="'+product+'"><img src="'+data[product]["img"]+'" alt="'+data[product]["name"]+'"></div>');
        }
        jQuery('.slider-for').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: false,
            fade: true,
            asNavFor: '.slider-nav'
        });

        if(curlist == productList) {
            jQuery('.slider-nav').slick({
                slidesToShow: 2,
                slidesToScroll: 1,
                centerMode: true,
                asNavFor: '.slider-for',
                focusOnSelect: true
            });
        } else {
            jQuery('.slider-nav').slick({
                slidesToShow: 3,
                slidesToScroll: 1,
                centerMode: true,
                asNavFor: '.slider-for',
                focusOnSelect: true
            });
        }
     

        jQuery('.slider-for').on('beforeChange', function(event, slick, currentSlide, nextSlide){
            var value = jQuery(".slider-for .slick-slide[data-slick-index='"+nextSlide+"']").find("div[data-value]").attr("data-value");
            selectColor(data, value)
        });
        if(jQuery(window).width() <= 768){
            jQuery(".ProductDetails__add-to-basket").before(jQuery("#opt-slider-container"));
        } else {
            jQuery(".opt-left-container").append(jQuery("#opt-slider-container"));
        }

        jQuery(window).resize(function(){
            if(jQuery(window).width() <= 768){
                if(jQuery(".opt-left-container #opt-slider-container").length > 0) {
                    jQuery(".ProductDetails__add-to-basket").before(jQuery("#opt-slider-container"));
                }
                
            } else {
                if(jQuery(".ProductDetails__information #opt-slider-container").length > 0) {
                    jQuery(".opt-left-container").append(jQuery("#opt-slider-container"));
                }
            }
        });
    });

    if(variation == 0){
        jQuery(".ProductDetailsBodyInformation__description:eq(0)").addClass("opt-open");

        jQuery(".ProductDetailsBodyInformation__description:eq(0)").prepend('<div class="opt-accordion-content"></div>');
        jQuery(".ProductDetailsBodyInformation__description:eq(0)").prepend('<div class="opt-accordion-header"></div>');
        jQuery(".ProductDetailsBodyInformation__description:eq(0) .opt-accordion-content").append(jQuery(".FreeHTML"));
        jQuery(".ProductDetailsBodyInformation__description:eq(0) .opt-accordion-header").append("<span>Details</span>")


        jQuery(".ProductDetailsBodyInformation__description:eq(1)").prepend('<div class="opt-accordion-content"></div>');
        jQuery(".ProductDetailsBodyInformation__description:eq(1)").prepend('<div class="opt-accordion-header"></div>');
        jQuery(".ProductDetailsBodyInformation__description:eq(1) .opt-accordion-header").append("<span>Specifications</span>");

        jQuery(".ProductDetailsBodyInformation__description:eq(1) .opt-accordion-content").append(jQuery(".ProductDetailsBodyInformation__specifications-list"));

        jQuery(".opt-accordion-header").click(function(){
            jQuery(this).parent().toggleClass("opt-open");
        })
    }
}

function buildVariation3(){

}

defer(function(){
    jQuery("body").addClass("opt-85"); 
    jQuery("body").addClass("opt-85v"+variation);
    defer(function(){
        if(window.location.pathname.indexOf("plus") >= 0){
            curlist = plusProductList;
        } else {
            curlist = productList;
        }
        switch(variation) {
            case 0: 
                jQuery(".ProductDetails__information").append(jQuery(".ProductDetailsBodyInformation__description"));
                setTimeout(function(){
                    buildVisual(curlist);
                },500);
                break;
            case 1:
                jQuery(".ProductDetails__information").append(jQuery(".ProductDetailsBodyInformation__description"));
                setTimeout(function(){
                    buildVisual(curlist);
                },500);
                break;
            case 2:

                break;
            default:

                break;
        }
      
        
    }, ".ProductDetailsBodyInformation__description");
}, ".ProductDetailsImageSlider");