var html = `
<body style="zoom: 1;">
		<div>
			<header id="top">
				<h2 class="visually-hidden">
					Header
				</h2>

				<div id="header">
					<div class="header noscroll" data-label="" id="block-8823714579516" role="banner">
						<div class="wrapper clearfix centered-on-page site-width">
							<div class="logo">
								<a href="https://www.nespresso.com/au/en/home" id="ta-header-logo"><img alt="Nespresso logo Go To Homepage" class="bi" height="33" src="/ecom/medias/sys_master/public/9948523233310/nespresso-logo-2.png" width="175"></a>
							</div>

							<div class="pull-right clearfix">
								<div class="pull-left lang">
									<h3 class="visually-hidden">
										Language and country selection
									</h3>
									<a href="/au/en/country"><img alt="You are viewing the Nespresso Australia website, click to change country" class="pull-left" src="/mosaic/_ui/img/flags/au.png"></a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</header>

			<div class="wrapper">
				<nav class="steps-nav">
					<h3 class="visually-hidden">
						Progress
					</h3>

					<ul class="unstyled">
						<li class="active">
							<span class="step">Step 1</span> <span class="title">Shopping bag summary</span> <span class="visually-hidden">Current step</span> <span class="border"></span>
						</li>

						<li class="">
							<span class="step">Step 2</span> <span class="title">Delivery setup</span> <span class="border"></span>
						</li>

						<li class="">
							<span class="step">Step 3</span> <span class="title">Payment</span> <span class="border"></span>
						</li>

						<li class="">
							<span class="step">Step 4</span> <span class="title">Order summary</span> <span class="border"></span>
						</li>

						<li class="">
							<span class="step">Step 5</span> <span class="title">Order confirmation</span>
						</li>
					</ul>
				</nav>
			</div>

			<main class="content checkout lbt-component" id="main">
				<div class="wrapper">
					<div class="content checkout">
						<div class="wrapper">
							<div class="clearfix">
								<!-- Delivery content -->

								<div class="delivery-content">
									<!--  header navigation -->

									<div class="delivery-title radius proceed-top clearfix">
										<h2>
											Shopping Bag
										</h2>
										<a class="btn button-primary button-green txt-shadow pull-right" href="/au/en/checkout?execution=e2s1&amp;_eventId_continue" id="ta-checkout-top" role="button">Proceed to checkout</a>
									</div>
									<!-- Container Shoppingbag (gradient) -->

									<div class="container-shoppingbag">
										<!-- cart content zone -->
										 <span class="visually-hidden">On this page you can check the content of your basket, update product quantities, remove products and see the total price of products in your basket. You can proceed to checkout to finalise your order.</span> <!-- cart content zone -->

										<div class="ng-scope" id="shopping-bag-with-ecotax">
											<span class="visually-hidden" id="a11y-qty-update-warning">This page will reload when you remove or update the quantity of a product.</span>
											<h3 class="visually-hidden">
												Cart content
											</h3>

											<table class="shopping-bag" id="js-shopping-bag">
												<caption class="visually-hidden">
													Shopping basket details table. Products are grouped by categories. Each product is on a separate row, and includes the unit price, quantity and total price. On each row the quantity button allows you to vary the quantity while the "remove" button allows you remove the product.
												</caption>

												<thead>
													<tr class="error-panel">
														<td colspan="4">
														</td>
													</tr>

													<tr class="shopbag-title head">
														<th class="col-a" id="article-header" scope="col">
															Article
														</th>
														<th class="col-p" id="price-header" scope="col">
															Unit price
														</th>
														<th class="col-q" id="quantity-header" scope="col">
															Quantity
														</th>
														<th class="col-t" id="total-header" scope="col">
															Total
														</th>
													</tr>
												</thead>

												<tbody class="productshere">
                                                <!-- OPTCODEHERE -->
													
												</tbody>
											</table>
										</div>
										<!-- activation code zone -->
										<div class="code-promo">
											<div class="line-title">
												<h3 class="title">
													Reward Code
												</h3>
												<span class="gift container-tooltip">Want to redeem a Reward Code? <span class="tooltip tt-coffee"></span></span>
												<p>
													<span class="gift container-tooltip">A Reward Code is a special offer code provided by <em>Nespresso</em> for you to redeem.</span>
												</p>
											</div>

											<div class="error-panel display-none" id="activation-code-bruteforce-message">
											</div>

											<form action="/au/en/checkout?execution=e2s1" data-validate="1" id="command" method="post" name="command">
												<div class="line-input clearfix">
													<label class="txt" for="activation-code-add">To use a reward code, please enter it here<br>
													<b>Your code is not applied until you click on the 'ok' button</b></label>
													<div class="submit">
														<input aria-describedby="activation-code-bruteforce-message activation-code-error-message" data-validate="" data-validate-regexp-message="Invalid reward code" data-validate-regexp-test="" id="activation-code-add" maxlength="30" name="activationCode" placeholder="Reward code" title="" type="text"> <button class="btn button-primary button-grey-shadow button-small pull-right" id="apply-activation-code" name="activateVoucher" type="submit" value="activateVoucher">ok</button>
														<div class="error-message display-none" id="activation-code-error-message">
														</div>
													</div>
												</div>
											</form>
										</div>
										<!-- total zone -->
										<!-- Totaux -->

										<div class="total-footer-zone">
											<h3 class="shopbag-title">
												Total
											</h3>

											<table class="shopping-bag-total">
												<caption>
													<span>Capsules TOTAL (250)</span>
												</caption>

												<thead>
													<tr class="shopping-bag-subtotal">
														<th class="col-articles">
															SUBTOTAL
														</th>
														<td class="col-total text-right">
															$179.00
														</td>
													</tr>
												</thead>

												<tbody>
													<tr class="shopping-bag-tax-included">
														<th class="line-tiret light">
															<span>GST (incl.)</span>
														</th>
														<td class="text-right">
															$0.00
														</td>
													</tr>
												</tbody>

												<tfoot>
													<tr class="line-total">
														<th>
															TOTAL
														</th>
														<td class="text-right">
															$179.00
														</td>
													</tr>
												</tfoot>
											</table>
										</div>
									</div>
									<!-- Container Shoppingbag (gradient) -->
									<!-- footer navigation-->

									<div class="delivery-footer clearfix">
										<p class="moar pull-left">
											<span aria-hidden="true" class="orange">←</span> <a href="/au/en/order/capsules">Back to the Online Shop</a>
										</p>
										<a class="btn button-primary button-green txt-shadow pull-right" href="/au/en/checkout?execution=e2s1&amp;_eventId_continue" id="ta-checkout-bottom" role="button">Proceed to checkout</a>
									</div>
								</div>
								<!-- Delivery content -->
								<!-- Sidebar -->

								<div class="aside push-zone">
									<h2 class="push-zone-title">
										Nespresso recommends
									</h2>

									<ul class="unstyled">
										<li style="list-style: none; display: inline">
											<div class="push-prod" data-product-position="1" data-product-section="Nespresso recommends">
												<img alt="" src="/ecom/medias/sys_master/public/10029767393310/WO-gifts-Versilo-182x180.png">
												<div class="desc clearfix">
													<div class="title">
														Versilo Dispenser
													</div>

													<p>
													</p>

													<p>
														A compact, transparent and versatile capsule dispenser (up to 40 capsules). *Gifted empty without capsules.
													</p>

													<p>
													</p>

													<p>
														<button class="button-grey-shadow btn-container btn button-primary pushprod-btn add-quantity-to-cart filled" data-added-quantity="1" data-force-page-reload="true" data-product-id="5499" data-promotion-code="691" type="button"><span class="visually-hidden"></span>
														<p>
															<span class="visually-hidden">Versilo Dispenser</span>
														</p>

														<p>
															<span class="visually-hidden"></span>
														</p>

														<p>
															<span class="visually-hidden">A compact, transparent and versatile capsule dispenser (up to 40 capsules). *Gifted empty without capsules.</span>
														</p>

														<p>
															<span class="visually-hidden"></span>
														</p>
														Add my gift</button>
													</p>
												</div>
											</div>

											<div class="push-prod" data-product-position="2" data-product-section="Nespresso recommends">
												<img alt="" src="/ecom/medias/sys_master/public/9981099769886.png">
												<div class="desc clearfix">
													<div class="title">
														Espresso &amp; Lungo Glass cups
													</div>

													<p>
													</p>

													<p>
														Set of 2 Espresso (80 ml) and 2 Lungo (150 ml) tempered glass cups and charcoal grey and coloured melamine saucers.
													</p>

													<p>
													</p>

													<p>
														<button class="button-grey-shadow btn-container btn button-primary pushprod-btn add-quantity-to-cart filled" data-added-quantity="1" data-force-page-reload="true" data-product-id="3384/4" data-promotion-code="691" type="button"><span class="visually-hidden"></span>
														<p>
															<span class="visually-hidden">Espresso &amp; Lungo Glass cups</span>
														</p>

														<p>
															<span class="visually-hidden"></span>
														</p>

														<p>
															<span class="visually-hidden">Set of 2 Espresso (80 ml) and 2 Lungo (150 ml) tempered glass cups and charcoal grey and coloured melamine saucers.</span>
														</p>

														<p>
															<span class="visually-hidden"></span>
														</p>
														Add my gift</button>
													</p>
												</div>
											</div>

											<div class="push-prod" data-product-position="3" data-product-section="Nespresso recommends">
												<img alt="" src="/ecom/medias/sys_master/public/9981107273758/WO-gifts-totem-2in1-182x180.png">
												<div class="desc clearfix">
													<div class="title">
														Totem 2 in 1
													</div>

													<p>
													</p>

													<p>
														Part of the Glass Collection of accessories, the organised melamine Totem allows you to store and display your favourite Grands Crus.
													</p>

													<p>
													</p>

													<p>
														<button class="button-grey-shadow btn-container btn button-primary pushprod-btn add-quantity-to-cart filled" data-added-quantity="1" data-force-page-reload="true" data-product-id="3394" data-promotion-code="691" type="button"><span class="visually-hidden"></span>
														<p>
															<span class="visually-hidden">Totem 2 in 1</span>
														</p>

														<p>
															<span class="visually-hidden"></span>
														</p>

														<p>
															<span class="visually-hidden">Part of the Glass Collection of accessories, the organised melamine Totem allows you to store and display your favourite Grands Crus.</span>
														</p>

														<p>
															<span class="visually-hidden"></span>
														</p>
														Add my gift</button>
													</p>
												</div>
											</div>
										</li>

										<li class="push-prod" data-product-item-id="100917" data-product-position="4" data-product-section="Nespresso recommends">
											<img alt="" src="/ecom/medias/sys_master/public/10358010839070/4-Tribute-to-Milano-sleeves-CartPush-312x250.jpg">
											<div class="desc clearfix">
												<span class="title">5 SLEEVE PACK, TRIBUTE TO MILANO</span>
												<div>
												</div>

												<p class="price">
													<span class="primary-price">$46.50</span> <span class="alternative-price"></span> <button aria-describedby="a11y-qty-update-warning" class="button-primary add-to-cart small-button-green smallbtn quantity-btn top pull-right" data-defaultcontent="" data-force-page-reload="true" data-max="9990" data-min="0" data-number-of-units="50" data-orientation="top" data-product-id="100917" data-product-name="5 SLEEVE PACK, TRIBUTE TO MILANO" data-promotion-code="" data-step="1" type="button"><span aria-hidden="true" class="quantity" data-product-id="100917"></span> <span class="visually-hidden" tabindex="-1">You have <span class="quantity">0</span> of 5 SLEEVE PACK in your basket, click on this button to <span class="label">Add to basket</span>.</span></button>
												</p>
											</div>
										</li>

										<li class="push-prod" data-product-item-id="89315" data-product-position="5" data-product-section="Nespresso recommends">
											<img alt="" src="/ecom/medias/sys_master/public/10298159104030/13-BTQ-Recycling-CartPush-312x350-v2.jpg">
											<div class="desc clearfix">
												<span class="title">Recycling Bag</span>
												<div>
												</div>

												<p class="price">
													<span class="primary-price">$0.50</span> <span class="alternative-price"></span> <button aria-describedby="a11y-qty-update-warning" class="button-primary add-to-cart small-button-green smallbtn quantity-btn top pull-right" data-defaultcontent="" data-force-page-reload="true" data-max="9990" data-min="0" data-number-of-units="1" data-orientation="top" data-product-id="89315" data-product-name="Recycling Bag" data-promotion-code="" data-step="1" type="button"><span aria-hidden="true" class="quantity" data-product-id="89315"></span> <span class="visually-hidden" tabindex="-1">You have <span class="quantity">0</span> of Recycling Bag in your basket, click on this button to <span class="label">Add to basket</span>.</span></button>
												</p>
											</div>
										</li>

										<li class="push-prod" data-product-item-id="60091-KIT" data-product-position="6" data-product-section="Nespresso recommends">
											<img alt="" src="/ecom/medias/sys_master/public/9855077416990/A-0255-cartPush-312x350.jpg">
											<div class="desc clearfix">
												<span class="title">Cailler Le Chocolat</span>
												<div>
												</div>

												<p class="price">
													<span class="primary-price">$8.50</span> <span class="alternative-price"></span> <button aria-describedby="a11y-qty-update-warning" class="button-primary add-to-cart small-button-green smallbtn quantity-btn top pull-right" data-defaultcontent="" data-force-page-reload="true" data-max="9990" data-min="0" data-number-of-units="1" data-orientation="top" data-product-id="60091-KIT" data-product-name="Cailler Le Chocolat" data-promotion-code="" data-step="1" type="button"><span aria-hidden="true" class="quantity" data-product-id="60091-KIT"></span> <span class="visually-hidden" tabindex="-1">You have <span class="quantity">0</span> of Cailler Le Chocolat in your basket, click on this button to <span class="label">Add to basket</span>.</span></button>
												</p>
											</div>
										</li>

										<li class="push-prod" data-product-item-id="5044/2" data-product-position="7" data-product-section="Nespresso recommends">
											<img alt="" src="/ecom/medias/sys_master/public/10302926618654/A-0341-Nespresso-Descaling-Kit-CartPush-312x350.jpg">
											<div class="desc clearfix">
												<span class="title">Descaling Kit</span>
												<div>
												</div>

												<p class="price">
													<span class="primary-price">$16.00</span> <span class="alternative-price"></span> <button aria-describedby="a11y-qty-update-warning" class="button-primary add-to-cart small-button-green smallbtn quantity-btn top pull-right" data-defaultcontent="" data-force-page-reload="true" data-max="9990" data-min="0" data-number-of-units="1" data-orientation="top" data-product-id="5044/2" data-product-name="Descaling Kit" data-promotion-code="" data-step="1" type="button"><span aria-hidden="true" class="quantity" data-product-id="5044/2"></span> <span class="visually-hidden" tabindex="-1">You have <span class="quantity">0</span> of Descaling Kit in your basket, click on this button to <span class="label">Add to basket</span>.</span></button>
												</p>
											</div>
										</li>
									</ul>
								</div>
								<!-- Sidebar -->
							</div>
						</div>
					</div>

					<div class="popin-page" id="potential-duplicate-order">
						<div class="content">
							<div>
								<div class="art-title">
									Warning: you have already placed an order today
								</div>

								<p class="potential-duplicate-order-warning">
									Our records show you have already placed an order from this account in the last 15 minutes. Please note that if you proceed you will be placing another order with Nespresso today.
								</p>
							</div>

							<div class="popin-separator">
							</div>

							<div class="clearfix popin-footer">
								<button class="btn button-primary button-grey-shadow pull-right small" id="potential-duplicate-order-warning-close">OK</button>
							</div>
						</div>
					</div>
				</div>
			</main>

			<footer role="contentinfo">
				<div class="footer-block ng-scope" data-ng-controller="footerBlockController" id="footer">
					<h2 class="visually-hidden">
						Footer
					</h2>

					<div class="centered-on-page site-width footer-extra clearfix" data-label="" id="block-8823717332028" role="contentinfo">
						<div class="pull-left">
							<a href="/au/en/home"><img alt="More information on Nespresso" height="19" src="/ecom/medias/sys_master/public/9948528377886/nespresso-logo-footer.png" width="96"></a> © Nestlé Nespresso S.A. 2017
						</div>

						<ul class="unstyled pull-right">
							<li class="back2top">
								<a href="#top" id="ta-back-to-top"><img alt="" class="icon" src="/mosaic/_ui/img/back2top.png">Top of the page
								<script src="https://www.nespresso.com/shared_res/soho/scripts/mosaic-personalization.js" type="text/javascript">
								</script></a>
							</li>
						</ul>

						<div class="clearfix">
						</div>

						<h3 class="visually-hidden">
							Website links
						</h3>

						<ul class="unstyled">
							<li class="">
								<a href="http://www.nestle-nespresso.com/">About Us</a>
							</li>

							<li class="">
								<a href="https://www.nespresso.com/au/en/service-customer-care">Contact</a>
							</li>

							<li class="">
								<a href="https://www.nespresso.com/au/en/legal">Legal</a>
							</li>

							<li class="">
								<a href="https://www.nespresso.com/au/en/sitemap">Sitemap</a>
							</li>

							<li class="">
								<a href="https://www.nespresso.com/au/en/storeLocator">Store Locator</a>
							</li>

							<li class="">
								<a href="https://www.nespresso.com/au/en/accessibility">Accessibility</a>
							</li>

							<li class="last">
								<a href="http://www.nestle-nespresso.com/">Media Centre</a>
							</li>
						</ul>
					</div>
				</div>
			</footer>
		</div>

		<div aria-labelledby="ui-id-4" class="ui-dialog ui-widget ui-widget-content ui-corner-all ui-front quantity-dialog-open" role="dialog" style="left: 422px; top: 180px; display: none; height: auto; width: 164px;" tabindex="-1">
			<div class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix">
				<span class="ui-dialog-title" id="ui-id-4">Update basket</span> <button aria-disabled="false" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-icon-only ui-dialog-titlebar-close" role="button" title="Close"><span class="ui-button-icon-primary ui-icon ui-icon-closethick"></span><span class="ui-button-text">Close</span></button>
			</div>

			<div class="quantity-popin display-none lbt-component ui-dialog-content ui-widget-content" id="quantity-selector" style="display: block; width: auto; min-height: 0px; max-height: none; height: auto;">
				<script type="text/javascript">
				    _Lbt.dl.components.push({
				        elementId: 'quantity-selector',
				        type: 'cart',
				        layout: 'box',
				        dynamic: 'static',
				        actions: {
				            action: 'addToCart',
				            eventName: 'cartUpdate',
				            eventType: 'click'
				        }
				    });
				</script>
				<div class="quantity-popin-inner">
					<div class="quantity-popin-content clearfix" tabindex="-1">
						<p class="visually-hidden">
							You can choose the predefined quantities below or set a custom quantity in the next input field.
						</p>

						<ul id="quantity-selector-options">
							<li class="qty-row-first qty-col-first qty-row-last">
								<button class="quantity-selection" data-quantity="0"><span class="visually-hidden">Click to remove ULTIMATE SELECTION WELCOME OFFER from your basket.</span><span aria-hidden="true">0</span></button>
							</li>

							<li class="qty-row-first qty-row-last">
								<button class="quantity-selection" data-quantity="1"><span class="visually-hidden">Click to update 1 to your basket.</span><span aria-hidden="true">1</span></button>
							</li>

							<li class="qty-row-first qty-row-last disabled">
								<button class="quantity-selection" data-quantity="2" disabled><span class="visually-hidden">Click to update 2 to your basket.</span><span aria-hidden="true">2</span></button>
							</li>

							<li class="qty-row-first qty-row-last disabled">
								<button class="quantity-selection" data-quantity="3" disabled><span class="visually-hidden">Click to update 3 to your basket.</span><span aria-hidden="true">3</span></button>
							</li>

							<li class="qty-row-first qty-row-last disabled qty-col-last">
								<button class="quantity-selection" data-quantity="4" disabled><span class="visually-hidden">Click to update 4 to your basket.</span><span aria-hidden="true">4</span></button>
							</li>
						</ul>

						<div class="separator">
						</div>

						<form action="#" id="quantity-selector-form" name="quantity-selector-form">
							<label class="other-quantity" for="quantity-selector-other">Please enter quantity</label> <input aria-describedby="quantity-selector-hint" id="quantity-selector-other" maxlength="4" placeholder="Other quantity" type="text"> <span class="visually-hidden" id="quantity-selector-hint">Other quantity</span>
							<div class="separator">
							</div>
							<button class="qty-btn-styled pull-left" id="quantity-selector-ok"><span class="qty-btn button-primary button-grey-shadow small">ok</span></button>
						</form>

						<p class="display-none" id="quantity-selector-warning" style="display: none;">
						</p>
					</div>
				</div>

				<div class="quantity-popin-bottom">
				</div>

				<div class="arrow">
				</div>
			</div>
		</div>
	</body>
`

var format = `
<tbody>
	<tr>
		<th class="shopbag-title" colspan="4" id="group-Capsules" scope="colgroup">
			Capsules (250)
		</th>
	</tr>

	<tr>
		<th class="" headers="group-Capsules article-header" id="product-8bfbea1b-b06e-46bc-b6a4-de16521b19cc">
			<p class="art-tab">
				<img alt="" height="42" src="/ecom/medias/sys_master/public/10020472553502/Welcome-Ultimate-42x42.png" width="42"> <span>ULTIMATE SELECTION WELCOME OFFER</span>
			</p>
		</th>
		<td class="" headers="group-Capsules product-8bfbea1b-b06e-46bc-b6a4-de16521b19cc price-header">
			<div class="price pull-right single-price">
				<span class="primary-price">$179.00</span>
			</div>
		</td>
		<td headers="group-Capsules product-8bfbea1b-b06e-46bc-b6a4-de16521b19cc quantity-header">
			<button aria-describedby="a11y-qty-update-warning" class="button-primary add-to-cart small-button-green smallbtn quantity-btn right filled" data-defaultcontent=" 1" data-force-page-reload="true" data-initial-quantity="1" data-max="1" data-min="0" data-number-of-units="250" data-orientation="top" data-product-id="10192" data-product-name="ULTIMATE SELECTION WELCOME OFFER" data-promotion-code="" data-step="1" type="button"><span aria-hidden="true" class="quantity" data-product-id="10192">1</span> <span class="visually-hidden" tabindex="-1">You have <span class="quantity">1</span> of ULTIMATE SELECTION WELCOME OFFER in your basket, click on this button to <span class="label">Update basket</span>.</span></button>
		</td>
		<td headers="group-Capsules product-8bfbea1b-b06e-46bc-b6a4-de16521b19cc total-header">
			<div class="price pull-left total single-price">
				<span class="primary-price">$179.00</span>
			</div>
			<button aria-describedby="a11y-qty-update-warning" class="remove-from-cart btn-close" data-amount="179.0" data-next-order="false" data-product-id="10192" data-product-name="ULTIMATE SELECTION WELCOME OFFER" data-promotion-code="" type="button"><span class="visually-hidden">Click to remove ULTIMATE SELECTION WELCOME OFFER from your basket.</span></button>
		</td>
	</tr>
    </tbody>
                `

jQuery('body').replaceAll(html)

var products = nestms.DataLayer.cart.products;

function builder () {

    // checkers
    var tablehtml = '',
        headers = [],
        capsules = 0;

// initial loop for headers
  for (var key in products) {
        if (products.hasOwnProperty(key)) {
            var prod = products[key].product,
                qty = products[key].quantity,
                name = prod.name,
                price = prod.price,
                prodId = prod.productId,
                type = prod.type;

            switch (type) { // This checks for the type so that we can add the appropriate titles
                case "capsule":
                    headers.push('capsule')
                    capsules += getItemQuantityFromName(name);
                    break;
                case "machine":
                    headers.push('machine')
                    break;
                case "accessory":
                    headers.push('accessory')
                    break;
                default: 
                    console.log('ERROR: No Type - Check Product Array')
            }

            

        }
    }
}  



function getItemQuantityFromName(name){
    if(name.indexOf("3") >= 0 || name.indexOf("Trio") >= 0 || name.indexOf("TRIO") >= 0){
        return 30;
    } else if(name.indexOf("5") >= 0){
        return 50;
    } else if(name.indexOf("10") >= 0) {
        return 100;
    } else if (name.indexOf("250") >= 0) {
        return 250;
    }
    return 1;
}

function removep () {
    jQuery('button p').remove();
}