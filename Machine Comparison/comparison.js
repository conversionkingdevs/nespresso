  function defer(method, selector) {
    if (window.jQuery) {
        if (jQuery(selector).length > 0){
            method();  
        } else {
            setTimeout(function() { defer(method, selector); }, 50);
        }  
    } else {
         setTimeout(function() { defer(method, selector); }, 50);
    }    
}

defer(function () {
  setTimeout(function () {
    jQuery('body').addClass('opt');
    /***** Products Data *****/
    var product_comparison = {
        'A3ND30-ME-GN-NE': {
            title: 'Essenza Mini Bundle',
            price: 'MYR 899.00',
            images: [
                'https://i.imgur.com/m7o5YOj.png',
                'https://i.imgur.com/oOC45Id.png',
                'https://i.imgur.com/qnIzZLi.png',
                'https://i.imgur.com/5i2HJHa.png'
            ],
            colors: [
                '#008000',
                '#ffffff',
                '#ff0000',
                '#808080'
            ],
            benefits: [
                'One-touch milk system',
                'Integrated milk function',
                'The perfect cappucino, latte or espresso',
                'The perfect cappucino, latte or espresso'
            ],
            features: [
                {
                    'image': 'https://i.imgur.com/430THxH.png',
                    'text' : 'One Touch fresh milk system'
                },
                {
                    'image': 'https://i.imgur.com/XErVQNW.png',
                    'text' : 'OFF mode after 9 min'
                },
                {
                    'image': 'https://i.imgur.com/deLmJmn.png',
                    'text' : 'Fast heat up 25 seconds'
                },
                {
                    'image': 'https://i.imgur.com/HhEPdVV.png',
                    'text' : '19 bar pressure'
                }
            ],
            drinks: [
                'Ristretto',
                'Espresso',
                'Lungo',
                'Recipes'
            ],
            info : [
                {'title': 'Water tank', 'text': '0.9 litre'},
                {'title': 'Used capsule capacity', 'text': '9 capsules'},
                {'title': 'Height', 'text': '25.8CM'},
                {'title': 'Weight', 'text': '4.5 Kilogram'},
                {'title': 'Dimensions <span>(WxDxH)</span>', 'text': '<small><strong>WxDxH:</strong> 17.3 x 32 x 25.8cm</small>'},
                {'title': 'Warranty', 'text': '2 Year'},
                {'title': 'Coffee Machine Power', 'text': '<small>220-240V / 50Hz / 1260W</small>'},
            ],
            url : 'https://www.nespresso.com/my/en/order/machines/essenza-mini-lime-green-bundle',
            pdf : 'https://www.nespresso.com/shared_res/manuals/essenza-mini/2017/UM_NESPRESSO_ESSENZA_MINI_DELONGHI_PROD_WEB_2017_02_20.pdf'
        },
        'F511-ME-BK-NE': {
            title: 'Lattissima Touch',
            price: 'MYR 1,499.00',
            images: ['https://i.imgur.com/miADiBF.png'],
            colors: [  
                '#000000'
            ],
            benefits: [
                'One-touch milk system',
                'Integrated milk function',
                'The perfect cappucino, latte or espresso'
            ],
            features: [
                {
                    'image': 'https://i.imgur.com/430THxH.png',
                    'text' : 'One Touch fresh milk system'
                },
                {
                    'image': 'https://i.imgur.com/XErVQNW.png',
                    'text' : 'OFF mode after 9 min'
                },
                {
                    'image': 'https://i.imgur.com/deLmJmn.png',
                    'text' : 'Fast heat up 25 seconds'
                },
                {
                    'image': 'https://i.imgur.com/HhEPdVV.png',
                    'text' : '19 bar pressure'
                }
            ],
            drinks: [
                'Ristretto',
                'Espresso',
                'Lungo',
                'Recipes'
            ],
            info : [
                {'title': 'Water tank', 'text': '0.9 litre'},
                {'title': 'Used capsule capacity', 'text': '9 capsules'},
                {'title': 'Height', 'text': '25.8CM'},
                {'title': 'Weight', 'text': '4.5 Kilogram'},            
                {'title': 'Dimensions <span>(WxDxH)</span>', 'text': '<small><strong>WxDxH:</strong> 17.3 x 32 x 25.8cm</small>'},
                {'title': 'Warranty', 'text': '2 Year'},
                {'title': 'Coffee Machine Power', 'text': '<small>220-240V / 50Hz / 1260W</small>'},
            ],
            url : 'https://www.nespresso.com/my/en/order/machines/lattissima-touch-glam-black-coffee-maker',
            pdf : 'https://www.nespresso.com/shared_res/manuals/lattissima_touch/www_LattissimaTouch_Z3B(EN_HK).pdf'
        },
        'F111-ME-WH-NE': {
            title: 'Lattissima One',
            price: 'MYR 1,399.00',
            images: ['https://i.imgur.com/l8HDFPg.png'],
            colors: [
                '#fff8dc'
            ],
            benefits: [
                'Innovative fresh milk system',
                'Dishwasher safe compenents',
                'Fits perfectly in any kitchen'
            ],
            features: [
                {
                    'image': 'https://i.imgur.com/430THxH.png',
                    'text' : 'One Touch fresh milk system'
                },
                {
                    'image': 'https://i.imgur.com/XErVQNW.png',
                    'text' : 'OFF mode after 9 min'
                },
                {
                    'image': 'https://i.imgur.com/deLmJmn.png',
                    'text' : 'Fast heat up 25 seconds'
                },
                {
                    'image': 'https://i.imgur.com/HhEPdVV.png',
                    'text' : '19 bar pressure'
                }
            ],
            drinks: [
                'Ristretto',
                'Espresso',
                'Lungo',
                'Recipes'
            ],
            info : [
                {'title': 'Water tank', 'text': '1 litre'},
                {'title': 'Used capsule capacity', 'text': '8 capsules'},
                {'title': 'Height', 'text': '25.6CM'},
                {'title': 'Weight', 'text': '4.2 Kilogram'},
                {'title': 'Dimensions <span>(WxDxH)</span>', 'text': '<small><strong>WxDxH:</strong> 15.4 x 32.4 x 25.8cm</small>'},
                {'title': 'Warranty', 'text': '2 Year'},
                {'title': 'Coffee Machine Power', 'text': '<small>220-240V / 50Hz / 1260W</small>'},
            ],
            url : 'https://www.nespresso.com/my/en/order/machines/lattissima-one-silky-white-coffee-machine',
            pdf : ''
        }
    };
	var adjustHeights = function() {
        jQuery("#product-comparison-list .colors").adjustHeight();
        jQuery("#product-comparison-list .benefits ul").adjustHeight();
        jQuery("#product-comparison-list .features ul").adjustHeight();
        jQuery("#product-comparison-list .drinks ul").adjustHeight();
        jQuery("#product-comparison-list .info").adjustHeight();
    };
    /**************************************
        LAYOUT
    **************************************/ 
    jQuery('<div class="add-to-compare" />').insertAfter(jQuery(".opt .ProductListElement.ProductListElement--machine .ProductListElement__name"));
    jQuery(".add-to-compare").append("<p>Compare machiens</p>");
    jQuery(".add-to-compare").append("<a href='#' class='add-to-compare-button'><span class='popup-message'>You can only compare a maximum of 3 machines at once.</span><span class='compare-dot' /><span class='compare-icon' /></a>");
    jQuery(".add-to-compare").append("<a href='#' class='add-to-compare-button-mobile'><span class='stage-1'>Select to Compare Machines</span><span class='stage-2'>Select 1 more machine to compare</span><span class='stage-3'>Compare Machines</span></a>");
    jQuery(".add-to-compare").append("<a href='#' class='compare-button DefaultLink--with-right-chevron'><span class='popup-message popup-message-1 active'>Please select 2 or 3 machines to begin comparing options.</span> <span class='popup-message popup-message-2'>Please select 1 or 2 more machines to start comparing.</span>Start comparing now</a>");

    jQuery(".opt .ProductList__container").append('<div id="comparison-info-mobile"><a href="#" class="clear">Clear All</a><a class="compare-now" href="#"><span class="stage-1">Select 1 more to compare</span><span class="stage-2">Compare machines now</span></a></div>');

    jQuery(".opt .ProductList__container").append('<div id="product-comparison" />');
    jQuery("#product-comparison").append('<div class="product-comparison-header" />');
    jQuery("#product-comparison .product-comparison-header").append('<h2>COMPARE COFFEE MACHINES</h2>');
    jQuery("#product-comparison .product-comparison-header").append('<p>Use the drop-down fields to change or add <strong>Nespresso</strong> Machines to compare</p>');
    jQuery("#product-comparison .product-comparison-header").append('<a href="#" class="product-comparison-back">Return to all machines</a>');

    jQuery("#product-comparison").append('<div class="product-comparison-filters" />');
    jQuery(".product-comparison-filters").append('<div class="filter" id="filter-1"><a href="#" class="filter-button">Please Choose Another Machine</a><ul class="filters-list"></ul></div><div class="filter" id="filter-2"><a href="#" class="filter-button">Please Choose Another Machine</a><ul class="filters-list"></ul></div><div class="filter" id="filter-3"><a href="#" class="filter-button">Please Choose Another Machine</a><ul class="filters-list"></ul></div>');
    jQuery("#product-comparison").append('<div id="product-comparison-list"><div id="product-comparison-1"><div class="placeholder"><p>Select another machine from the drop to compare</p><a href="#"></a><img src="https://i.imgur.com/rwcppzL.png" /></div></div><div id="product-comparison-2"><div class="placeholder"><p>Select another machine from the drop to compare</p><a href="#"></a><img src="https://i.imgur.com/rwcppzL.png" /></div></div><div id="product-comparison-3"><div class="placeholder"><p>Select another machine from the drop to compare</p><a href="#"></a><img src="https://i.imgur.com/rwcppzL.png" /></div></div></div>');


    var comparison_products = new Array();

    jQuery("body").on('click','.add-to-compare',function(e){
        e.preventDefault();
        e.stopPropagation();
        jQuery("#product-comparison-list > div").html('<div class="placeholder"><p>Select another machine from the drop to compare</p><a href="#"></a><img src="https://i.imgur.com/rwcppzL.png" /></div>');
        jQuery(".product-comparison-filters .filter-button").removeClass('active').text('Please Choose Another Machine');
        var $this = jQuery(this);
        if(jQuery(".add-to-compare.selected").length <= 2 || $this.hasClass('selected')){
            $this.toggleClass('selected');
            jQuery(".add-to-compare-button .popup-message").removeClass('active');
            var product_id = $this.parents('article').data('product-item-id');
            if(comparison_products.includes(product_id)){
                index = comparison_products.indexOf(product_id)
                comparison_products.splice(index, 1);
            } else {
                comparison_products.push(product_id)
            }   
        } else {
            jQuery(".add-to-compare-button .popup-message").addClass('active');
        };
        if(jQuery(".add-to-compare.selected").length > 0 && jQuery(".add-to-compare.selected").length < 2){
            jQuery(".add-to-compare .popup-message-1").removeClass('active');
            jQuery(".add-to-compare .popup-message-2").addClass('active');
            jQuery(".add-to-compare .compare-button").removeClass('active');
        } else if(jQuery(".add-to-compare.selected").length > 1) {
            jQuery(".add-to-compare .popup-message-1").removeClass('active');
            jQuery(".add-to-compare .popup-message-2").removeClass('active');
            jQuery(".add-to-compare .compare-button").addClass('active');
        } else {
            jQuery(".add-to-compare .popup-message-1").addClass('active');
            jQuery(".add-to-compare .popup-message-2").removeClass('active');
            jQuery(".add-to-compare .compare-button").removeClass('active');
        };
    });

    jQuery("body").on('click','.add-to-compare-button-mobile:not(.active)',function(e){
        e.preventDefault();
        e.stopPropagation();
        var $this = jQuery(this);
        if(jQuery(".add-to-compare.selected").length <= 1 || $this.parent().hasClass('selected')){
            $this.parent().toggleClass('selected');
            product_id = $this.parents('article').data('product-item-id');
            if(comparison_products.includes(product_id)){
                index = comparison_products.indexOf(product_id)
                comparison_products.splice(index, 1);
            } else {
                comparison_products.push(product_id)
            };
            if(jQuery(window).width() < 767){
                jQuery("#comparison-info-mobile").addClass('visible');
            };
        };
        if(jQuery(".add-to-compare.selected").length > 1){
            jQuery(".add-to-compare.selected .add-to-compare-button-mobile").addClass('active');
            jQuery("#comparison-info-mobile .compare-now").addClass('active');
        };
    });

    jQuery("body").on('click','.add-to-compare-button-mobile.active',function(e){
        e.preventDefault();
        e.stopPropagation();
        jQuery(".ProductList__content").animate({opacity:0},300,function(){
            jQuery(".ProductList__content").hide();
            comparison_products.forEach(function(product_id,parent){
                populate_content(parent+1,product_id);  
            });
            jQuery("#product-comparison").css('display','block').animate({opacity:1},300,function(){
                adjustHeights();
            });
        });
    });

    jQuery("body").on('click','.compare-button',function(e){
        e.preventDefault();
        e.stopPropagation();
        var $this = jQuery(this);
        if($this.hasClass('active')){
            jQuery(".ProductList__content").animate({opacity:0},300,function(){
                jQuery(".ProductList__content").hide();
                comparison_products.forEach(function(product_id,parent){
                    populate_content(parent+1,product_id);  
                });
                jQuery("#product-comparison").css('display','block').animate({opacity:1},300,function(){
                    adjustHeights();
                });
            });
        };
    });

    jQuery("body").on('click','#comparison-info-mobile .compare-now:not(.active)',function(e){
        e.preventDefault();
    });

    jQuery("body").on('click','#comparison-info-mobile .compare-now.active',function(e){
        e.preventDefault();
        jQuery(".ProductList__content").animate({opacity:0},300,function(){
            jQuery(".ProductList__content").hide();
            comparison_products.forEach(function(product_id,parent){
                populate_content(parent+1,product_id);  
            });
            jQuery("#product-comparison").css('display','block').animate({opacity:1},300,function(){
                adjustHeights();
            });
        });
        jQuery("#comparison-info-mobile").removeClass('visible');
        jQuery("#comparison-info-mobile .compare-now").removeClass('active');
    });

    jQuery("body").on('click','#comparison-info-mobile .clear',function(e){
        e.preventDefault();
        comparison_products = [];   
        jQuery(".add-to-compare").removeClass('selected');
        jQuery(".add-to-compare .compare-button").removeClass('active');
        jQuery("#comparison-info-mobile").removeClass('visible');
        jQuery("#comparison-info-mobile .compare-now").removeClass('active');
        jQuery(".add-to-compare-button-mobile").removeClass('active');
    });

    jQuery("body").on('click','.product-comparison-back',function(e){
        e.preventDefault();
        jQuery("#product-comparison").animate({opacity:0},300,function(){
            jQuery("#product-comparison").hide();
            jQuery(".ProductList__content").css('display','block').animate({opacity:1},300);
            comparison_products = [];   
            jQuery(".add-to-compare").removeClass('selected');
            jQuery(".add-to-compare .popup-message-1").addClass('active');
            jQuery(".add-to-compare .popup-message-2").removeClass('active');
            jQuery(".add-to-compare .compare-button").removeClass('active');
            jQuery(".add-to-compare-button-mobile").removeClass('active');
        });
    });


    $.each(product_comparison,function(id, data){
        jQuery(".filter .filters-list").append('<li><a href="#" data-id="'+id+'">'+data.title+'</a></li>');    
    });

    jQuery(".filter").on('click','.filters-list a', function(e){
        e.preventDefault();
        if(!jQuery(this).parent().hasClass('disabled')){     
            var id = jQuery(this).data('id');
            var parent = jQuery(this).parents('.filter').attr('id').replace('filter-','');
            populate_content(parent, id);        
        } else {
            e.stopPropagation();
        };
    });

    function populate_content(parent, product_id){
        var $parent = jQuery('#product-comparison-' + parent);
        var product = product_comparison[product_id];
        
        $parent.html('').append(`
            <div class="inner" data-active-id="`+product_id+`">
                <h3>`+product.title+`</h3>
                <span class="price">`+product.price+`</span>
                <div class="product-image"></div>
                <div class="colors">
                    <h5>Available Colours</h5>
                </div>
                <div class="benefits">
                    <h5>Machine Benefits</h5>
                    <ul></ul>
                    <a href="`+product.url+`" class="read-more DefaultLink--with-right-chevron">Read More</a>
                </div>
                <div class="features">
                    <h5>Key Features</h5>
                    <ul></ul>
                </div>
                <div class="drinks">
                    <h5>What Drinks Can I Prepare?</h5>
                    <ul></ul>
                </div>
                <div class="info">
                    <h5><a href="#">Additional Information</a></h5>
                    <div class="more-info"><ul></ul></div>
                </div>
                <a href="`+product.url+`" class="view-details">View Details & Buy</a>
            </div>
        `); 
        
        product.images.forEach(function(color,h){
            $parent.find(".product-image").append('<img src="'+product.images[h]+'" />');
            $parent.find(".product-image img").first().addClass('active');
        });
        jQuery(".product-image").each(function(){
            if(jQuery(this).find('.placeholder').length == 0){
                jQuery(this).append('<img class="placeholder" src="https://i.imgur.com/LJLTpW1.png" />');    
            };
        });
        
        product.colors.forEach(function(color,i){
            $parent.find(".colors").append('<span class="dot" style="background-color:'+product.colors[i]+'"><span class="inner"></span></span>');
            $parent.find(".colors .dot").first().addClass('active');
        });
        
        product.benefits.forEach(function(benefits,j){
            $parent.find(".benefits ul").append('<li>'+product.benefits[j]+'</li>');
        });
        
        product.features.forEach(function(features,k){
            $parent.find(".features ul").append('<li><img src="'+product.features[k].image+'" /><p>'+product.features[k].text+'</p></li>');
        });
        
        product.drinks.forEach(function(features,l){
            if(product.drinks[l] == 'Ristretto'){
                $parent.find(".drinks ul").append('<li><img src="https://i.imgur.com/1y76ZZM.png" /><p><strong>25ml</strong>'+product.drinks[l]+'</p></li>');   
            } else if(product.drinks[l] == 'Espresso'){
                $parent.find(".drinks ul").append('<li><img src="https://i.imgur.com/tRR7TBI.png" /><p><strong>40ml</strong>'+product.drinks[l]+'</p></li>');
            } else if(product.drinks[l] == 'Lungo'){
                $parent.find(".drinks ul").append('<li><img src="https://i.imgur.com/ZQEIjmb.png" /><p><strong>110ml</strong>'+product.drinks[l]+'</p></li>');
            } else if(product.drinks[l] == 'Recipes'){
                $parent.find(".drinks ul").append('<li><img src="https://i.imgur.com/gOpswn0.png" /><p><strong>Milk</strong>'+product.drinks[l]+'</p></li>');
            };
        });
        
        product.info.forEach(function(info,m){
            $parent.find(".info .more-info ul").append('<li><strong>'+product.info[m].title+': </strong><span>'+product.info[m].text+'</span></li>');
        });
        
        if(product.pdf != ''){
            $parent.append('<a target="_blank" href="`+product.pdf+`" class="pdf DefaultLink--with-right-chevron">Need more information? Read the User Manual</a>');
        };
        
        jQuery(".filter li").removeClass('disabled');
        jQuery("#product-comparison-list .inner").each(function(){
            jQuery(".filter").find('a[data-id="'+jQuery(this).data('active-id')+'"]').parent().addClass('disabled');
        })
        jQuery("#filter-" + parent).find('a.filter-button').text(product.title).addClass('active');
        
        adjustHeights();
    };

    jQuery(".filter-button").click(function(e){
        e.preventDefault();
        e.stopPropagation();
        var $this = jQuery(this);
        if($this.parent().hasClass('active')){
            $this.parent().removeClass('active');
        } else {
            jQuery(".filter").removeClass('active');
            $this.parent().addClass('active');
        };
    });
    jQuery("body").click(function(e){
        jQuery(".filter").removeClass('active');
    });

    jQuery("body").on('click','.info h5 a',function(e){
        e.preventDefault();
        var $this = jQuery(this);
        if($this.hasClass('active')){
            jQuery(".info h5 a").removeClass('active');
            jQuery(".more-info").css('min-height', 'initial');
            jQuery('.more-info').slideToggle('medium', function () {
                jQuery(".more-info").adjustHeight();
            });
        } else {
            jQuery(".info h5 a").addClass('active');
            jQuery(".more-info").css('min-height', 'initial');
            jQuery('.more-info').slideToggle('medium', function () {
                jQuery(".more-info").adjustHeight();
            });
        };
    });

    jQuery("body").on('click','#product-comparison-list > div .placeholder a',function(e){
        e.preventDefault();
        e.stopPropagation();
        jQuery("#filter-3 .filter-button").trigger('click');
    });

    jQuery("body").on('click','.colors .dot',function(e){
        e.preventDefault();
        var $this = jQuery(this);
        $this.parent().find('.dot').removeClass('active');
        $this.addClass('active');
        $this.parents('.inner').find('.product-image img').removeClass('active');
        $this.parents('.inner').find('.product-image img:nth-child('+$this.index()+')').addClass('active');
    });

    jQuery.fn.adjustHeight = function() {
        var maxHeightFound = 0;
        this.css('min-height','1px');
        this.each(function() {
            if( jQuery(this).outerHeight(true) > maxHeightFound ) {
                maxHeightFound = jQuery(this).outerHeight(true);
            }
        });
        this.css('min-height',maxHeightFound);
    };


    jQuery(window).resize(function () {
        adjustHeights();
    });

    if(jQuery(window).width() < 767){
        jQuery(document).scroll(function(){
            if(jQuery("#product-comparison:visible").length){
                if(jQuery(window).scrollTop() > jQuery("#product-comparison-list").offset().top - 80){
                    jQuery(".product-comparison-back").addClass('fixed');
                    jQuery(".product-comparison-filters").addClass('fixed');
                } else {
                    jQuery(".product-comparison-filters").removeClass('fixed');
                    jQuery(".product-comparison-back").removeClass('fixed');
                };
            } else {
                jQuery(".product-comparison-back").removeClass('fixed');
                jQuery(".product-comparison-filters").removeClass('fixed');            
            };
        });
    } else {
        jQuery(".product-comparison-back").removeClass('fixed');
        jQuery(".product-comparison-filters").removeClass('fixed');  
    };
  console.log('running');
  },500);
}, '.ProductListElement.ProductListElement--machine .AccessibleLink');