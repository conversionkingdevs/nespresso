function jQueryDefer(method) {
    if (window.jQuery)
        method();
    else
        setTimeout(function() {jQueryDefer(method)}, 50);
}

function defer(method, selector) {
    if (jQuery(selector).length > 0){
        method();
    } else{
        setTimeout(function() { defer(method, selector) }, 50);
    } 
}

var productList = [
    {
        "url":          "https://www.nespresso.com/au/en/order/accessories/nespresso-recycling-bag",
        "img":          '<img src="/ecom/medias/sys_master/public/10104383995934/8-BTQ-Recycling-Main-684x378.jpg" itemprop="image" width="684" height="378" alt="" class="main-media">',
        "title":        'RECYCLING BAG',
        "content":      'Custom designed resealable recycling bag for collecting and storing used capsules at home, prior to returning to Nespresso for recycling. ',
        "price":        '$0.50',
        "buttonHtml":   '<button type="button" class="button-primary button-green two-parts add-to-cart pull-right" data-product-id="89315" data-product-name="Recycling Bag" data-number-of-units="1" data-min="0" data-step="1" data-max="9990" data-orientation="top" aria-labelledby="aria-ta-add_plus_button" id="ta-add_plus_button" data-defaultcontent=" <img src=&quot;/mosaic/_ui/img/Elements/cross_plus.png&quot; alt=&quot;&quot; width=&quot;16&quot;> "> <span id="aria-ta-add_plus_button" class="visually-hidden" tabindex="-1"> You have <span class="quantity">0</span> of Recycling Bag in your basket, click on this button to <span class="label">Add to basket</span>.</span> <span aria-hidden="true" class="left-part quantity" data-product-id="89315"> <img src="/mosaic/_ui/img/Elements/cross_plus.png" alt="" width="16"> </span> <span class="label right-part" aria-hidden="true">Add to basket</span> </button>'
    },
    {
        "url":          "https://www.nespresso.com/au/en/order/accessories/capsule-recycling-bin",
        "img":          '<img src="/ecom/medias/sys_master/public/9381754699806/A-0123-main-684x378.jpg" itemprop="image" width="684" height="378" alt="" class="main-media">',
        "title":        'RECYCLING CANISTER',
        "content":      'Realized in ABS with chromium plating, this sealable container facilitates the recovery of the used capsules and their transportation to the next collection point in full safety.',
        "price":        '$29.00',
        "buttonHtml":   '<button type="button" class="button-primary button-green two-parts add-to-cart pull-right" data-product-id="3404" data-product-name="Recycling Canister" data-number-of-units="1" data-min="0" data-step="1" data-max="9990" data-orientation="top" aria-labelledby="aria-ta-add_plus_button" id="ta-add_plus_button" data-defaultcontent=" <img src=&quot;/mosaic/_ui/img/Elements/cross_plus.png&quot; alt=&quot;&quot; width=&quot;16&quot;> "> <span id="aria-ta-add_plus_button" class="visually-hidden" tabindex="-1"> You have <span class="quantity">0</span> of Recycling Canister in your basket, click on this button to <span class="label">Add to basket</span>.</span> <span aria-hidden="true" class="left-part quantity" data-product-id="3404"> <img src="/mosaic/_ui/img/Elements/cross_plus.png" alt="" width="16"> </span> <span class="label right-part" aria-hidden="true">Add to basket</span> </button>'
    },
    {
        "url":          "https://www.nespresso.com/au/en/order/accessories/recycling-postage-bag?icid=B2C_Auen_Landing_btn_2016_08_RecyclingProcess_How_satchel",
        "img":          '<img src="/ecom/medias/sys_master/public/10098807275550/8-AusPostBag-Main-684x378.jpg" itemprop="image" width="684" height="378" alt="" class="main-media">',
        "title":        'AUSTRALIA POST RECYCLING SATCHEL',
        "content":      'Pre-paid Australia Post Recycling Satchel. Use this resealable satchel to collect & return used Nespresso Aluminium capsules for recycling. Satchel holds up to 130 capsules.',
        "price":        '$1.90',
        "buttonHtml":   '<button type="button" class="button-primary button-green two-parts add-to-cart pull-right" data-product-id="89315/POST" data-product-name="Australia Post Recycling Satchel" data-number-of-units="1" data-min="0" data-step="1" data-max="9990" data-orientation="top" aria-labelledby="aria-ta-add_plus_button" id="ta-add_plus_button" data-defaultcontent=" <img src=&quot;/mosaic/_ui/img/Elements/cross_plus.png&quot; alt=&quot;&quot; width=&quot;16&quot;> "> <span id="aria-ta-add_plus_button" class="visually-hidden" tabindex="-1"> You have <span class="quantity">0</span> of Australia Post Recycling Satchel in your basket, click on this button to <span class="label">Add to basket</span>.</span> <span aria-hidden="true" class="left-part quantity" data-product-id="89315/POST"> <img src="/mosaic/_ui/img/Elements/cross_plus.png" alt="" width="16"> </span> <span class="label right-part" aria-hidden="true">Add to basket</span> </button>'
    }
];
jQueryDefer(function(){
    defer(function () {
        $("body").addClass("opt3-01-03");
        $(".opt-container").remove();
        $("#services").before(`<div class="opt-container"></div>`);
        $(".opt-container").append('<div class="opt-capsule-container"></div>');
        for(var i = 0; i < productList.length; i++){
            $(".opt-capsule-container").append('<div class="opt-capsule"></div>');
            $(".opt-capsule:eq("+i+")").append('<div class="opt-header"><a href="'+productList[i]["url"]+'">'+productList[i]["img"]+'</a></div>');
            $(".opt-capsule:eq("+i+")").append('<div class="opt-title"><p><span>'+productList[i]["title"]+'</span></p></div>');
            $(".opt-capsule:eq("+i+")").append('<div class="opt-content"><p>'+productList[i]["content"]+'</p></div>');
            $(".opt-capsule:eq("+i+")").append('<div class="opt-price"><span>'+productList[i]["price"]+'</span></div>');
            $(".opt-capsule:eq("+i+")").append('<div class="opt-button-container">'+productList[i]["buttonHtml"]+'</div>');
        }
    }, "#services");
})

