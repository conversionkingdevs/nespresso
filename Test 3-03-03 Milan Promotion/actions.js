//product added, purchase


/*configuration block begin*/
var config = {
	"actionName": "Click Action 5",
	"selectorsDefault": {},
	"selectorsAfter": {
		"element1": {
			"variant1": {
				".opt-click": {
					"value": 1,
					"attr": "Toggle Capsules List"
				}
			}
		}
	}
};
/*configuration block end*/

function attachActions(targetArray){
    for(var i in targetArray){
		if( targetArray.hasOwnProperty(i) ){
			actions.trackClicks(i, {name: config.actionName,value:targetArray[i].value,attribute:targetArray[i].attr});
		}
	}
}

attachActions(config.selectorsDefault);

campaign.events.variantJsExecuted(function(elementName, variantName){
	var selectors = (config.selectorsAfter[elementName] || {})[variantName.toLowerCase()] || [];
	attachActions(selectors);
});

/*configuration block begin*/
var config = {
	"actionName": "Click Action 7",
	"selectorsDefault": {},
	"selectorsAfter": {
		"element1": {
			"variant1": {
				".product-list-wrapper .add-to-cart": {
					"value": 1,
					"attr": "Capsule List CTA Click"
				}
			}
		}
	}
};
/*configuration block end*/

function attachActions(targetArray){
    for(var i in targetArray){
		if( targetArray.hasOwnProperty(i) ){
			actions.trackClicks(i, {name: config.actionName,value:targetArray[i].value,attribute:targetArray[i].attr});
		}
	}
}

attachActions(config.selectorsDefault);

campaign.events.variantJsExecuted(function(elementName, variantName){
	var selectors = (config.selectorsAfter[elementName] || {})[variantName.toLowerCase()] || [];
	attachActions(selectors);
});

/*configuration block begin*/
var config = {
	"actionName": "Click Action 8",
	"selectorsDefault": {},
	"selectorsAfter": {
		"element1": {
			"variant1": {
				"#ng-product-detail-content .button-primary": {
					"value": 1,
					"attr": "Milano CTA Click"
				}
			}
		}
	}
};
/*configuration block end*/

function attachActions(targetArray){
    for(var i in targetArray){
		if( targetArray.hasOwnProperty(i) ){
			actions.trackClicks(i, {name: config.actionName,value:targetArray[i].value,attribute:targetArray[i].attr});
		}
	}
}

attachActions(config.selectorsDefault);

campaign.events.variantJsExecuted(function(elementName, variantName){
	var selectors = (config.selectorsAfter[elementName] || {})[variantName.toLowerCase()] || [];
	attachActions(selectors);
});

/*configuration block begin*/
var config = {
	"actionName": "Click Action 9",
	"selectorsDefault": {},
	"selectorsAfter": {
		"element1": {
			"variant1": {
				".prod-suggest .button-primary": {
					"value": 1,
					"attr": "Suggestion CTA Click"
				}
			}
		}
	}
};
/*configuration block end*/

function attachActions(targetArray){
    for(var i in targetArray){
		if( targetArray.hasOwnProperty(i) ){
			actions.trackClicks(i, {name: config.actionName,value:targetArray[i].value,attribute:targetArray[i].attr});
		}
	}
}

attachActions(config.selectorsDefault);

campaign.events.variantJsExecuted(function(elementName, variantName){
	var selectors = (config.selectorsAfter[elementName] || {})[variantName.toLowerCase()] || [];
	attachActions(selectors);
});