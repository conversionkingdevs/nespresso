console.log("start");

$("body").addClass("opt-3");

var menuItems = {  
   "href":"",
   "title":"",
   "data":[  
      {  
         "href":"#",
         "title":"Order Coffee",
         "data":[  
            {  
               "href":"https://www.nespresso.com/mobile/nz/en/catalog#capsules",
               "title":"Order coffee capsules",
               "data":[  

               ],
               "dataPromoImgSrc":"",
               "dataPromoLink":""
            },
            {  
               "href":"https://www.nespresso.com/mobile/nz/en/products/Capsule/Intense-Connoisseur-15-Sleeve-Assortment-/p/intense-15-sleeve-assortment/capsulesAssortment",
               "title":"Discover our assortment packs",
               "data":[  

               ],
               "dataPromoImgSrc":"",
               "dataPromoLink":""
            },
            {  
               "href":"https://www.nespresso.com/mobile/nz/en/lastOrdersPage",
               "title":"Re-order",
               "data":[  

               ],
               "dataPromoImgSrc":"",
               "dataPromoLink":""
            }
         ],
         "dataPromoImgSrc":"https://www.nespresso.com/ecom/medias/sys_master/public/10311842365470/mobile-comp-000000IVWY-largePushMain26296997e-5fe9-401f-b146-cf8d480135f1-en-jpg-640Wx360H.jpg",
         "dataPromoLink":"https://www.nespresso.com/mobile/nz/en/recipes/intense/home"
      },
      {  
         "href":"#",
         "title":"Machines",
         "data":[  
            {  
               "href":"https://www.nespresso.com/mobile/nz/en/catalog#machines",
               "title":"Order a Nespresso machine",
               "data":[  

               ],
               "dataPromoImgSrc":"",
               "dataPromoLink":""
            },
            {  
               "href":"https://www.nespresso.com/mobile/nz/en/products/Machine/Range/Essenza-Mini-and-Aeroccino-3/Essenza-Mini-Breville-Intense-Grey/p/essenza-mini-breville-intense-grey",
               "title":"View our Essenza Mini Range",
               "data":[  
               ],
               "dataPromoImgSrc":"",
               "dataPromoLink":""
            }
         ],
         "dataPromoImgSrc":"/ecom/medias/sys_master/public/10286724874270/Menu-Banner-238x228-V2.jpg",
         "dataPromoLink":"https://www.nespresso.com/mobile/nz/en/news/mob-news-mothers-day-machine-promo-News"
      },
      {  
         "href":"#",
         "title":"Accessories",
         "data":[  
            {  
               "href":"https://www.nespresso.com/mobile/nz/en/catalog#accessories",
               "title":"Order accessories",
               "data":[  

               ],
               "dataPromoImgSrc":"",
               "dataPromoLink":""
            },
            {  
               "href":"https://www.nespresso.com/mobile/nz/en/catalog#accessoriesPureoriginal",
               "title":"Cups & saucers",
               "data":[  
               ],
               "dataPromoImgSrc":"",
               "dataPromoLink":""
            },
            {  
               "href":"https://www.nespresso.com/mobile/nz/en/catalog#accessoriesVieworiginal",
               "title":"Dispensers",
               "data":[  

               ],
               "dataPromoImgSrc":"",
               "dataPromoLink":""
            },
            {  
               "href":"https://www.nespresso.com/mobile/nz/en/catalog#accessoriesBaristaoriginal",
               "title":"Milk Frothers",
               "data":[  

               ],
               "dataPromoImgSrc":"",
               "dataPromoLink":""
            },
            {  
               "href":"https://www.nespresso.com/mobile/nz/en/catalog#accessoriesRevealoriginal",
               "title":"Glasses",
               "data":[  

               ],
               "dataPromoImgSrc":"",
               "dataPromoLink":""
            },
            {  
               "href":"https://www.nespresso.com/mobile/nz/en/catalog#accessoriesConfectionarySugaroriginal",
               "title":"Hot Chocolate & Sugar",
               "data":[  

               ],
               "dataPromoImgSrc":"",
               "dataPromoLink":""
            }
         ],
         "dataPromoImgSrc":"/ecom/medias/sys_master/public/10295352229918/Ristretto-Decaffeinato-Mega-Drop-Down-238x228.jpg",
         "dataPromoLink":"/mobile/nz/en/products/Capsule/Ristretto-Decaffeinato/p/ristretto-decaffeinato-coffee-pods"
      },
      {  
         "href":"#",
         "title":"Recipes",
         "data":[  
            {  
               "href":"https://www.nespresso.com/mobile/nz/en/recipes/intense/turmeric-and-honey-latte-featuring-ristretto?icid=B2C_Auen_RecipeLandingPage_RistrettoLink_2017_04_Intense_1",
               "title":"Turmeric and Honey Latte",
               "data":[  

               ],
               "dataPromoImgSrc":"",
               "dataPromoLink":""
            },
            {  
               "href":"https://www.nespresso.com/mobile/nz/en/recipes/intense/hazelnut-liqueur-affogato-featuring-ristretto-decaffeinato?icid=B2C_Auen_RecipeLandingPage_RistrettoDecafLink_2017_04_Intense_2",
               "title":"Hazelnut Liqueur Affogato",
               "data":[  

               ],
               "dataPromoImgSrc":"",
               "dataPromoLink":""
            },
            {  
               "href":"https://www.nespresso.com/mobile/nz/en/recipes/intense/coconut-latte-with-a-twist-featuring-arpeggio?icid=B2C_Auen_RecipeLandingPage_ArpeggioLink_2017_04_Intense_3",
               "title":"Coconut Latte with a Twist",
               "data":[  

               ],
               "dataPromoImgSrc":"",
               "dataPromoLink":""
            },
            {  
               "href":"https://www.nespresso.com/mobile/nz/en/recipes/intense/home?icid=B2C_Auen_MobHome_LinksList_2017_04_Intense_3",
               "title":"View All",
               "data":[  

               ],
               "dataPromoImgSrc":"",
               "dataPromoLink":""
            }
         ],
         "dataPromoImgSrc":"",
         "dataPromoLink":""
      },
      {  
         "href":"https://www.nespresso.com/mobile/nz/en/delivery-services",
         "title":"Delivery Services",
         "data":[  
            
         ],
         "dataPromoImgSrc":"",
         "dataPromoLink":""
      },
      {  
         "href":"https://www.nespresso.com/nz/en/storeLocator?icid=B2C_Auen_Homepage_OurServicesMegaMenu_2016_07_mainItem_0",
         "title":"Find Us",
         "data":[  
            {  
               "href":"https://www.nespresso.com/mobile/nz/en/findUs/choose-pickup-point-map#stores",
               "title":"Boutiques",
               "data":[  

               ],
               "dataPromoImgSrc":"",
               "dataPromoLink":""
            },
            {  
               "href":"https://www.nespresso.com/mobile/nz/en/findUs/choose-pickup-point-map#resellers",
               "title":"Machine resellers",
               "data":[  

               ],
               "dataPromoImgSrc":"",
               "dataPromoLink":""
            }
         ],
         "dataPromoImgSrc":"",
         "dataPromoLink":""
      },
      {  
         "href":"https://www.nespresso.com/mobile/nz/en/findUs/choose-pickup-point-map#recycling",
         "title":"Recycling Points",
         "data":[  
            
         ],
         "dataPromoImgSrc":"",
         "dataPromoLink":""
      },
      {  
         "href":"https://www.contact.nespresso.com/contact-us/nz/en/terminal/mobile",
         "title":"Contact Us",
         "data":[  
         ],
         "dataPromoImgSrc":"",
         "dataPromoLink":""
      },
      {  
         "href":"https://www.nespresso.com/mobile/nz/en/login/reset",
         "title":"Login",
         "data":[  
           
         ],
         "dataPromoImgSrc":"",
         "dataPromoLink":""
      }
   ],
   "dataPromoImgSrc":"",
   "dataPromoLink":""
};

function buildHTML(){
    var temp = "<div>";
    for(var i = 0; i < menuItems.data.length; i++) {
        var subData = menuItems.data[i].data;
        if(subData.length > 0) {
            temp += '<div class="opt-homePage-Item opt-parent-item">';
            temp += '<a class="opt-item opt-parent" href='+menuItems.data[i].href+'>'+menuItems.data[i].title+'</a>';
            temp += '<div class="opt-child">';  
            temp += '<a class="opt-back">'+menuItems.data[i].title+'</a>';
            for(var x = 0; x < subData.length; x++ ){  
                temp += '<a href="'+subData[x].href+'" class="opt-item">'+subData[x].title+'</a>';
            }
            if(menuItems.data[i].dataPromoLink != "" || menuItems.data[i].dataPromoImgSrc != ""){
                temp += '<a class="opt-promo"href="'+menuItems.data[i].dataPromoLink+'"><img src="'+menuItems.data[i].dataPromoImgSrc+'" alt="promo"></a>';
            }
            temp += '</div>';
        } else {
            temp += '<div class="opt-homePage-Item">';
            temp += '<a class="opt-item" href='+menuItems.data[i].href+'>'+menuItems.data[i].title+'</a>';
        }
        temp += '</div>';
    }
    temp += "</div>";
    return temp;
}

function buildHtmlRecurse(object, html){
    console.log(object.data.length);
    if(object.data.length == 0){
       return html;
    }
    for(var i = 0; i < object.data.length; i++){
        html += buildHtmlRecurse(object.data[i], html);
    }
}


//add hamburger
$("#header").append(`<a id="nav-toggle" href="#"><span></span></a>`);

$( "#nav-toggle" ).click(function(e){
    if($(this).hasClass("active")){
         $(".opt-menu-container").css("width", "0px");
    } else {
        $(".opt-menu-container").css("width", "80%");
    }
    document.querySelector( "#nav-toggle" ).classList.toggle( "active" );
    e.stopPropagation();
});

$("body").append(`<div class="opt-menu-container">
</div>`);



$(".opt-menu-container").append(buildHTML());

// ajaxItems(function(a){
//     console.log(a);
//     for(var i = 0; i < a.length; i++){
//         if(a[i] != null){
//             console.log(a[i].href);
//             console.log($(a[i]).find("img")[0].src);
//             $(".opt-promo:eq("+i+")").attr("href", a[i].href);
//             $(".opt-promo:eq("+i+") img").attr("src", $(a[i]).find("img")[0].src);
//         }
//     }
// });

// function ajaxItems(callback) {
//     $.get( "https://www.nespresso.com/mobile/nz/", function( data) {
//         var html = $.parseHTML(data);
//         data = $(html).find('.slides a');    
//         callback(data);
//     });
// }

$(".opt-menu-container").css("width", "0px");

$("body").children().not(".opt-menu-container").click(function(){
    $(".opt-menu-container").css("width", "0px");
    $( "#nav-toggle" ).removeClass("active");
    $(".opt-child").removeClass("opt-active");
    $(".opt-menu-container").removeClass("opt-active");
});

//click on a menu
$(".opt-homePage-Item.opt-parent-item").click(function(){
    console.log("clicked");
    $(".opt-child").removeClass("opt-active");
    $(this).find(".opt-child").addClass("opt-active");
    $(".opt-menu-container").addClass("opt-active");
});

$(".opt-back").click(function(e){
    $(".opt-child").removeClass("opt-active");
    setTimeout(function(){
        $(".opt-menu-container").removeClass("opt-active");
    },300);
    e.stopPropagation();
});

$(".opt-menu-container a").not(".opt-back").not(".opt-parent").click(function(){
    if($(this).attr("href").indexOf("#") >= 0){
        $(location).attr('href',$(this).attr("href"));
        window.location.reload();
    }
});

if ($("a[aria-label='Logout']").length == 1) {
  $(".opt-item[href='https://www.nespresso.com/mobile/nz/en/login/reset']").attr("href", "/mobile/nz/en/logout");
}

console.log("end");