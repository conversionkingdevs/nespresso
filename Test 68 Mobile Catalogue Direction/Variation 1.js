function defer(method, selector) {
    if (window.jQuery) {
        if (jQuery(selector).length > 0){
            method();
        } else {
            setTimeout(function() { defer(method, selector) }, 50);
        }  
    } else {
         setTimeout(function() { defer(method, selector) }, 50);
    }    
}
  
  
function start() {

/*****************
Helper Functions
*****************/
    // Stringify
    function stringify(a) {
        return JSON.stringify(a);
    }

    // Parse JSON
    function parseJson(a) {
        try {
            var json = JSON.parse(a);
            if (json) {
                return json
            }
        } catch (e) {
            return e;
        }
    }

    // Get Item
    function getStore (a, arr) {
        var item = localStorage.getItem(a);
        if (item == undefined || item == null) {
            localStorage.setItem(a, '{}');
            return;
        }
        return item;
     }

    // Store Item
     function storeItem (a, arr) {
        var initialarr = getStore(a);
        if (arr) {
            var updatedarr = parseJson(initialarr);
            updatedarr[arr[0]] = arr[1];
            var initialarr = updatedarr;
        }
            localStorage.setItem(a, stringify(initialarr));
     }

     // Check storage
     function checkObj(item, key) {
        var arr = parseJson(getStore(item));
        if (arr && typeof arr == "object") {
            try {
                if (arr[key].length) {
                    return true;
                }
            } catch (e) {
                return false;
            }
        } 
        return false;
    }

/*****************
Intensity Functions
*****************/
function getIntenseFromUrl (url, callback) {
    jQuery.get(url, function( data ) {
        var html = jQuery.parseHTML(data),
            intensity = jQuery(html).find('#product-description-title .intensity').text(),
            name = jQuery(html).find('#product-view-title h1').text(),
            arr = [name, intensity];
        if (callback) {
            callback(arr);
        } else {
            return arr;
        }
    });}

function getItem (key, itemName) {
    var arr = parseJson(getStore(key));
    return arr[itemName];
}

// Check if it is in the list
function checkList (item, callback) {
    
    var name = jQuery(item).find('.product-title').text(),
        url = jQuery(item).find('a.track-product-click').attr('href');

    if (checkObj("Intensity", name) == false) {
        getIntenseFromUrl(url, function (a) {
            storeItem("Intensity",a);
            if (callback) {
                callback(name)
            }
        });
    } else if (checkObj("Intensity", name) == true) {
        if (callback) {
            callback(name)
        }
    }
}

// Makes the intensity boxes with # of cells as 'a'
function makeBoxes(a) {
    var boxes = '';
    for (var x = 0; x < a; x++) {
        boxes += '<div class="optIntensityBox"></div>';
    }
    var html = '<p class="optGold">Intensity: </p>'+boxes;
    return html;
}

function setIntensity(item, intensity) {
    var container = jQuery(item),
        intensityEq = intensity-1;
    jQuery(container).find('.optIntensityBox:eq('+intensityEq+')').attr('opt-intense', intensity).addClass('optIntensely');
}

/*****************
Cart Area Functions
*****************/

function newVal(val, inc) {
    var returnVal = parseInt(val) + inc;
    if (returnVal < 0) {
        returnVal = 0;
    }
    return returnVal;
}

function updateInput(item, inc) {
    var input =  jQuery(item).closest('.product-container').find('.optQtyInput'),
        currVal = input.val(),
        val = newVal(currVal, inc),
        check = parseInt(jQuery(item).parents('.product-container').find('.quantity-selector').val().split(':')[0]);
    if (val != 0 &&  check != 0) {
        jQuery(item).closest('.product-container').find('.optButton').text('Update');
    }

    input.val(val);
}

function addToCart(item, callback) {
    var container = jQuery(item).closest('.product-container'),
        actSelect = container.find('select'),
        myQty = container.find('.optQtyInput').val(),
        productcode = actSelect.attr('data-product-code'),
        newAtcVal = myQty+':'+productcode;
    actSelect.val(newAtcVal);
    actSelect.change();
    if (callback) {
        callback();
    }
}

function optPlus(item) {

    var inc = 10;

    if (jQuery(item).find('.track-product-click:eq(0)').attr('data-product-section') == "Assortments") {
        inc = 1;
    }

    jQuery(item).find('#optPlus:not(.optHold)').click(function () {
        var that = this;
        jQuery(this).addClass('optHold');
        updateInput(this, inc);
        setTimeout(function () {
            jQuery(that).removeClass('optHold');
        }, 100);
    });
}

function optMinus(item) {

    var dec = -10

    if (jQuery(item).find('.track-product-click:eq(0)').attr('data-product-section') == "Assortments") {
        dec = -1;
    }

    jQuery(item).find('#optMinus:not(.optHold)').click(function () {
        var that = this;
        jQuery(this).addClass('optHold');
        updateInput(this, dec);
        setTimeout(function () {
            jQuery(that).removeClass('optHold');
        }, 100);
    });
}

function optButton (item) {
    jQuery(item).find('.optCartArea:not(.optDisable) .optButton').click(function (e) {
        e.preventDefault();
        
        var val = jQuery(this).parents('.product-container').find('.optQtyInput').val(),
            qty = jQuery(this).parents('.product-container').find('.quantity-selector').val();
        
        if (val != qty) {
            jQuery(this).text('Added!');
        }
        addToCart(this);
    });
}




function setInitialVal () {
    // var cart = nestms.DataLayer.cart;
    // for (var x in cart.products ) {
    //     var qty = cart.products[x]["quantity"];
    //     if (parseInt(qty) > 0) {
    //         jQuery('a[data-product-code="'+x+'"]').closest('.product-container').find('.optQtyInput').val(parseInt(qty));
    //     }
    // }
    jQuery('.product-container').each(function () {
      var qty = jQuery(this).find('.quantity-selector').val();
      if (qty) {
          qty = parseInt(qty.split(':')[0]);
      } else {
          qty = 'optDisable';
      }
      if (qty == 0){
        jQuery(this).find('.optQtyInput').val(qty);
      } else if (qty > 0) {
        jQuery(this).find('.optQtyInput').val(qty);
      } else if (qty == 'optDisable') {
        jQuery(this).find('.optCartArea').addClass(qty);
        jQuery(this).find('.optButton').text('Out Of Stock');

      }
    });
}

function addHref (item) {
    var href = jQuery(item).find('.track-product-click:eq(0)').attr('href');
    jQuery(item).find('.optReadMore').attr('href', href);
}

// Add Cart Area
function addCartArea (item) {
        jQuery(item).find('.track-product-click:eq(0)').after('<div class="optCartArea"></div>');
        var html = `
        <div class="optQtyCont">
            <p>QTY</p>
            <p class="optPM" id="optMinus">-</p>
            <input class="optQtyInput" readonly="readonly" value="0"></input>
            <p class="optPM" id="optPlus">+</p>
        </div>
        <div class="optButton">Add to Cart</div>
        <div class="optReadMoreCont">
            <div class="optRightArrow">></div>  
            <a href="" class="optReadMore">Read More</a>
        </div>
        `;
        jQuery(item).find('.optCartArea').append(html);
        setInitialVal();
};



/*****************
Fixed Bottom Modal
*****************/

function getQty() {
    var qty = parseInt(jQuery('.basket_number').text());
    if (Number.isNaN(qty)) {
        return false;
    } else {
        return qty;
    }
}

function getItemQuantityFromName(name){
      if(name.indexOf("Trio") >= 0 || name.indexOf("TRIO") >= 0){
          return 3 * 10;
      } else if(name.indexOf("150") >= 0){
          return 150;
      } else if(name.indexOf("250") >= 0){    
          return 250;
      } else if(name.indexOf("15") >= 0){
        return 15 * 10;
      } else if(name.indexOf("5") >= 0){
          return 5 * 10;
      } else if(name.indexOf("10") >= 0){
          return 10 * 10;
      } 
                 
      return 1;
  }
  
function getCapsuleCartQuantity(){
      var sum = 0;
      jQuery('.product-container').each(function () {
        var qty = jQuery(this).find('.quantity-selector').val();
        if (qty) {
            qty = qty.split(':')[0];
            if (qty > 0){
            var name = jQuery(this).find('.product-title').text();
            sum = sum + (getItemQuantityFromName(name) * qty);
            }
        }
      });
      return sum;
  }

function removePopup(item) {
    jQuery(item).remove();
}

function validPopUp() {
    var html = `
        <div class="opt_fixed_up_cont">
            <div class="opt_close_button" onclick="`+removePopup('.opt_fixed_up_cont')+`"></div>
            <div class="opt_content">
                <div class="opt_qty_cont">
                    <p>Total Quantity: <span id="opt_fixed_up_qty">`+getCapsuleCartQuantity()+`</span> capsules</p>
                </div>
                <a href="/mobile/au/en/cart" class="opt_proceed_button">Proceed to Checkout</a>
            </div>
        </div>
        `
    jQuery('body').append(html);
}

function invalidPopUp () {
    var html = `
        <div class="opt_fixed_up_cont opt_invalid_height">
            <div class="opt_close_button"></div>
            <div class="opt_content">
                <div class="opt_qty_cont">
                    <p>Total Quantity: <span id="opt_fixed_up_qty">`+getCapsuleCartQuantity()+`</span> capsules</p>
                </div>
                <div class="opt_invalid_cont">
                    <p class="opt_white">Don’t forget you need to add atleast 50 capsules to your cart before you checkout. All orders must be in multiples of 50 (e.g 50, 100, 150, 200...). Happy shopping!</p>
                </div>
                <a class="opt_proceed_button optInvalid">Proceed to Checkout</a>
            </div>
        </div>
        `
    jQuery('body').append(html);
}

function addModal () {
    var qty = getCapsuleCartQuantity();
    jQuery('.opt_fixed_up_cont').remove();
    if (qty) {
        var mod = qty % 50;
        if (mod != 0) {
            invalidPopUp();
        } else if (mod == 0) {
            validPopUp();
        } else {
            jQuery('.opt_fixed_up_cont').remove();
        }
    }
}

/*****************
Variation
*****************/

    jQuery('.product-container').each(function () {
        var that = this;
        checkList(this, function (a) {
            var intensity = getItem("Intensity", a);
            jQuery(that).find('.track-product-click:eq(0)').find('.left-item, .middle-item').wrapAll('<div></div>');
            if (intensity > 0) { 
                jQuery(that).find('.track-product-click:eq(0)').append('<div class="optIntenseCont">'+makeBoxes(13)+'</div>');
                setIntensity(that, intensity);
            }
        
            addCartArea(that);
            addHref(that);
            optPlus(that);
            optMinus(that);
            optButton(that);
        });
    });


jQuery('.basket_number').bind("DOMSubtreeModified",function(){
    addModal ();
    jQuery('.opt_close_button').click(function () {
        jQuery('.opt_fixed_up_cont').remove();
    });
});
}

defer(start, '.product-container .product-title');
