
//html of green button and product name on data value
var DEFAULTCAPSULE = {
  "html": '<button type="button" id="ta-add-to-cart-7686.20" aria-labelledby="aria-7686.20" class="button-primary quantity-btn-title add-to-cart two-parts button-green" data-ng-class="{\'button-green two-parts add-to-cart\': data.addToCartButton.activated, \'add-to-cart disabled two-parts\': !data.addToCartButton.activated}" data-product-id="7686.20" data-min="0" data-step="10" data-max="9990" data-orientation="" data-use-custom-third-line="true" data-force-page-reload="false" data-product-name="Tribute to Milano" data-number-of-units="" aria-disabled="false" data-defaultcontent=" <img src=&quot;/mosaic/_ui/img/Elements/cross_plus.png&quot; alt=&quot;&quot; width=&quot;16&quot;> "> <span data-ng-if="data.addToCartButton.activated" class="visually-hidden ng-binding ng-scope" id="aria-7686.20" tabindex="-1"> You have <span class="quantity">10</span> of Tribute to Milano in your basket, click on this button to <span class="label">Update basket</span>.</span> <span data-ng-if="data.addToCartButton.activated" aria-hidden="true" class="left-part quantity ng-scope" data-product-id="7686.20">10</span> <span data-ng-if="data.addToCartButton.activated" aria-hidden="true" class="label right-part ng-scope">Update basket</span> </button>',
  "name": "Tribute to Milano"
}

function jQueryDefer(method) {
    if (window.jQuery)
        try{
          console.log(nestms);
          method();
        } catch(e){
          jQueryDefer(method);
        }
    else
    setTimeout(function() { jQueryDefer(method) }, 50);
}

function defer(method, selector) {
    if (jQuery(selector).length > 0){
        method();
    } else
        setTimeout(function() { defer(method, selector) }, 50);
}

function run(){
  var prod_button,  prod_name, num_capsules, capsules_left, prod_qty,capsules_needed;
  console.log("running 3.01.01");

  function getItemQuantityFromName(name){
    if(name.indexOf("3") >= 0 || name.indexOf("Trio") >= 0 || name.indexOf("TRIO") >= 0){
        return 3 * 10;
    } else if(name.indexOf("5") >= 0){
        return 5 * 10;
    } else if(name.indexOf("10") >= 0){
        return 10 * 10;
    }
    return 1;
  }
  function getQuantityByName(value){
  var type = "nameLocale";
  for (var product in nestms.DataLayer.cart.products) {
      console.log(nestms.DataLayer.cart.products[product].product[type].toLowerCase().trim());
      if(nestms.DataLayer.cart.products[product].product[type].toLowerCase().trim() == value.toLowerCase().trim()){
        return parseInt(nestms.DataLayer.cart.products[product].quantity);
      }
  }
  return 0;
  }
  function getProductByValueAndType(value, type){
    for (var product in nestms.DataLayer.products) {
      if(nestms.DataLayer.products[product][type] == value){
        return nestms.DataLayer.products[product];
      }
    }
  }

  function getProductNameById(id){
      return nestms.DataLayer.products[id].name;
  }
  function getQuantity(){
    var quantity = 0;
    for (var product in nestms.DataLayer.cart.products) {
        switch(nestms.DataLayer.cart.products[product].product.type){
            case "capsule":
                quantity += parseInt(nestms.DataLayer.cart.products[product].quantity) * getItemQuantityFromName(nestms.DataLayer.cart.products[product].product.name);
                break;
            default:
                break;
        }
    }
    return quantity;
  }
  function addHtml(){
    jQuery("body").append("<div class='opt-overlay'></div><div class='opt-modal'><img class='opt-close' src='https://service.maxymiser.net/cm/images-eu/1/1/1/22F6BC6DD520867E7C8298379BF9ABED1804DFAE28A428E66CB0887CD2FBF0D7/nespresso-au/Test-3-01-01-How-to-Order-Direction/cross-1.png' /></div>");

    jQuery(".opt-modal").append("<h2>Review your Basket");

    jQuery(".opt-modal").append("<hr />");
    setInterval(function(){
      num_capsules = getQuantity();
      if( jQuery(".opt-modal #mini-cart-dropdown").html() != jQuery("#mini-cart-dropdown").html()){
          jQuery(".opt-modal #mini-cart-dropdown").remove();
          jQuery("#mini-cart-dropdown").clone().appendTo(".opt-modal").show();
      }
      if (num_capsules % 50 == 0) {
        jQuery(".opt-modal").addClass("enough");
         jQuery(".opt-upsell").hide();
      } else {
        jQuery(".opt-upsell").show();
      }
    },50);
    jQuery(".opt-modal .cart-details caption").hide();
    jQuery(".opt-modal").append("<div class='opt-upsell'></div>");
    jQuery(".opt-upsell").append("<h3> You'll need another <span class='opt-left'>20</span> pods to complete this order. </h3>");
    jQuery(".opt-upsell").append("<div class='upsell-area'> </div>");
    jQuery(".upsell-area").append("<p> To validate your basket, your order should consist of a minimum 50 capsules and be in multiples of {4} (i.e. 50, 100, 150, etc.) </p>");
    jQuery(".upsell-area").append("<h4> WOULD YOU LIKE <span class='opt-left'>-</span> MORE OF THESE PODS? </h4>");
    jQuery(".upsell-area").append("<div class='opt-item'> </div>");
    jQuery(".upsell-area").append("<h4> OR </h4>");
    jQuery(".upsell-area").append("<h4> WANT TO CHOOSE ANOTHER TYPE? </h4>");
    jQuery(".upsell-area").append("<button class='opt-continue'> CONTINUE SHOPPING </div>");
    jQuery(".opt-modal .cart-open").removeAttr("style");
    jQuery(".opt-close, .opt-overlay, .opt-continue").click(function(){
        jQuery(".opt-overlay").hide(); 
        jQuery(".opt-modal").hide();
        jQuery("body").css({"height":"auto","overflow":"inherit"});
    });
  }

  function updateHtml() {
    prod_button = localStorage.getItem("opt-button");
    prod_name = localStorage.getItem("opt-name");
    if(prod_button == null || prod_name == null) {
    jQuery(".opt-modal").addClass("enough");
    } else {
      num_capsules = getQuantity();
      capsules_left = 50 - (num_capsules % 50); 
      prod_qty = getItemQuantityFromName(prod_name);
      capsules_needed = Math.ceil(capsules_left / prod_qty);

      jQuery(".upsell-area .opt-item").children().remove();
      jQuery(".upsell-area .opt-item").append("<span class='item-name'>"+prod_name+"</span> <span class='item-price opt-left'></span>");
      jQuery(".upsell-area .opt-item").append(prod_button);
      $(".opt-modal .add-to-cart").attr("class","button-primary add-to-cart small-button-green smallbtn pull-right");
      $(".opt-modal .add-to-cart .quantity").text("");
      setTimeout(function(){
        $(".opt-modal .add-to-cart").click(function(){
          $("body").addClass("opt-hide-quantity");
          setTimeout(function(){
          $("#quantity-selector-other").val((getQuantityByName(prod_name)+ capsules_needed));
          $("#quantity-selector-other").change();
          $("#quantity-selector-ok").click()
            setTimeout(function(){
              $("body").removeClass("opt-hide-quantity");
            },2000);
          },200);
        });
      },200);
      $(".opt-left").first().text(capsules_left);
      $(".item-price.opt-left").text(capsules_needed);
    }
  }

  function addModal(){
    addHtml();
    updateHtml();
  }

  function AddToCartCallBack(){
  jQuery("button.add-to-cart").click(function(){
    var value = $(this).attr("data-product-id");
    var name = $(this).attr("data-product-name");
    var type = "productIdLocale";
    if(getProductByValueAndType(value, type).type == "capsule" ) {
      var prod_button_old = localStorage.getItem("opt-button");
      var prod_name_old = localStorage.getItem("opt-name");
      console.log(name);
      console.log(getItemQuantityFromName(name));
      if(getItemQuantityFromName(name) > 1){
        prod_button = DEFAULTCAPSULE["html"];
        prod_name = DEFAULTCAPSULE["name"];
      } else {
        prod_button= jQuery(this)[0].outerHTML;
        prod_name = jQuery(this).attr("data-product-name");
      }
      console.log(prod_button);
      console.log(prod_name);
      if(prod_button_old == prod_button || prod_name_old == prod_name) {
        console.log("Not Loading Item");
      } else {
        console.log("Loading Item");
        prod_button= jQuery(this)[0].outerHTML;
        prod_name = jQuery(this).attr("data-product-name");
        localStorage.setItem("opt-button",prod_button)
        localStorage.setItem("opt-name",prod_name);
          updateHtml(prod_name, prod_button);
      }
    } else {
      console.log("Not Loading Item");
    }
  });
  }

  function start(){
    defer(function(){
      setTimeout(function(){
      jQuery("body").addClass("opt3-01-01");
      jQuery(".opt-overlay").remove();
      addModal();
      jQuery(".opt-overlay").hide();
      jQuery(".opt-modal").hide();

      jQuery("#mini-cart").click(function(e){
        e.stopPropagation();
        $(".opt-modal").removeClass("enough");
        jQuery(".opt-overlay").show();
        jQuery(".opt-modal").show();
        jQuery(".opt-upsell").hide();
        updateHtml();
        e.preventDefault();
      });
      //on add to cart add product to local storage
      jQuery(".view-product").click(function(){ 
        defer(function(){
            AddToCartCallBack(); 
        }, "button.add-to-cart");
      });
      AddToCartCallBack();
      },1000);
    }, "#mini-cart");
  };
  start();
}
jQueryDefer(run);
