// card image: https://www.nespresso.com/ecom/medias/sys_master/public/10280757231646/card-nespresso-club.png


function addModal() {
  	$("body").css({"height":"100%","overflow":"hidden"});	
  
	$("body").append("<div class='opt-overlay'><div class='opt-modal'></div></div>");

	$(".opt-modal").append("<img class='opt-logo' src='https://www.nespresso.com/ecom/medias/sys_master/public/9948523233310/nespresso-logo-2.png' />");

	$(".opt-modal").append("<div class='opt-returning'></div><div class='opt-new'></div>");

	$(".opt-new").append("<p class='opt-heading' >I am a new customer </p> <p class='opt-gold'>Register and benefit from being a Nespresso Club Member</p><img src='https://www.nespresso.com/ecom/medias/sys_master/public/10280757231646/card-nespresso-club.png' />");

	$(".opt-new").append('<a href="/au/en/registration" id="registerBtn" class="btn-container  ng-isolate-scope" data-registration-checkister-url="/au/en/fastregistration" data-registration-url="/au/en/registration" data-redirect="true" data-registration-button=""><span class="btn button-primary button-green">CONTINUE</span></a>');

	$(".opt-returning").append("<p>I am a returning customer</p> <label>Email address</label> <input class='opt-username' type='text'> <label>Password</label> <input class='opt-password' type='password'> <a href='https://www.nespresso.com/au/en/forgot-password'> Password Forgotten? </a> <input id='opt-ta-header-remember-me' data-ng-model='user.rememberme' class='ng-pristine ng-valid' type='checkbox'><label for='opt-ta-header-remember-me' class='checkbox '>Remember me</label>");

	$(".opt-returning").append('<a href="#" id="signinBTN" class="btn-container  ng-isolate-scope"><span class="btn button-primary button-green">SIGN IN</span></a>');

	$(".opt-modal").append("<div class='opt-close' >X</div> ");

	$(".opt-close").click(function() {
 		$(".opt-overlay").fadeOut("slow"); 
	  	$("body").css({"height":"auto","overflow":"inherit"});
	});

	$(".opt-overlay").click(function(e){
		if (e.target !== this)
		    return;
		$(this).remove();
	});

	$("#signinBTN").click(function(e){
		e.preventDefault();
		var opt_username = $(".opt-username").val();
		var opt_password = $(".opt-password").val();

		$("#ta-header-username").val(opt_username);
		$("#ta-header-password").val(opt_password);
		
		$("#ta-header-username").change();
		$("#ta-header-password").change();

		if ($("#opt-ta-header-remember-me").is(":checked")){
			$("#ta-header-remember-me").attr("checked","true");
		} else {
			$("ta-header-remember-me").removeAttr("checked");
		}	


		$("#login-dropdown form").eq(1).submit();
	});

}

// Check modal views, show once a session

var modal_view_count = sessionStorage.getItem('opt_shown_modal');

if (!modal_view_count){
	addModal();

	modal_view_count = true;
	sessionStorage.opt_shown_modal = modal_view_count;
}