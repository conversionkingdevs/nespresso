function defer(method, selector) {
    if (window.jQuery) {
        if (jQuery(selector).length > 0){
            method();
        } else {
            setTimeout(function() { defer(method, selector); }, 50);
        }  
    } else {
         setTimeout(function() { defer(method, selector); }, 50);
    }    
}

function checkCartQuantity(value) {
    if (value == '') {
        return 0;
    } else {
        return parseInt(value);
    }
}

function removeQuantitySelector() {
    jQuery("#main").click();
}

function AddToCartFunctionality (el) {
    jQuery(el).click(function (e) {
        e.preventDefault();
        e.stopPropagation();
        var article = jQuery(this).closest('article.ProductListElement'),
            currentValue = checkCartQuantity(article.find('.AddToBagButtonLarge__basketIcon span.AddToBagButtonLarge__quantity').text()),
            selectedValue = checkCartQuantity(jQuery(this).find('span:eq(1)').text()),
            id = article.attr('data-product-item-id').toString(),
            newValue = selectedValue+currentValue;
            if (selectedValue == 0) {
                newValue = 0;
            }
            window.CartManager.updateItem('erp.au.b2c/prod/' + id, newValue);
            removeQuantitySelector();
    });
}

function addAddToCartFunctionality () {
    jQuery('.AddToBagButton__container').on('mouseenter','.QuantitySelector li',function(e){
        if (!jQuery(this).hasClass('opt_Added')) {
        jQuery(this).addClass('opt_Added');
            AddToCartFunctionality(this);
        }
    });
}

defer(function () {
    addAddToCartFunctionality();
}, '.AddToBagButton__container');