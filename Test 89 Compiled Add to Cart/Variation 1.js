function defer(method, selector) {
    if (window.jQuery) {
        if (jQuery(selector).length > 0){
            method();
        } else {
            setTimeout(function() { defer(method, selector); }, 50);
        }  
    } else {
         setTimeout(function() { defer(method, selector); }, 50);
    }    
}

function checkCartQuantity(value) {
    if (value == '') {
        return 0;
    } else {
        return parseInt(value);
    }
}

function removeQuantitySelector() {
    jQuery("#main").click();
}

function addUpdatedCartHTML (qty, product, el) {
    if (timer) {
        clearInterval(timer);
    }
    jQuery(".opt_PopupContainer").remove();
    var message = `<div class="opt_Text">`+qty+` `+product+` Capsules Added to Cart</div>`;
    if (qty == 0) {
        message = `<div class="opt_Text">`+product+` Removed From Cart</div>`;
    }
    var html =  `<div class="opt_PopupContainer" style="display:none"> `+message+` </div>`;
    jQuery(el).closest('.AddToBagButton__container').append(html);
    jQuery("opt_PopupContainer").click(function(e) {e.preventDefault();});
    jQuery('.opt_PopupContainer').fadeIn('medium');
    var timer = setTimeout(function () {
        //jQuery('.opt_PopupContainer').fadeOut('slow', function () {jQuery(this).remove();})
    },2000)
}

function AddToCartFunctionality (el) {
    jQuery(el).click(function (e) {
        e.preventDefault();
        e.stopPropagation();
        var article = jQuery(this).closest('article.ProductListElement'),
            currentValue = checkCartQuantity(article.find('.AddToBagButtonLarge__basketIcon span.AddToBagButtonLarge__quantity').text()),
            selectedValue = checkCartQuantity(jQuery(this).find('span:eq(1)').text()),
            id = article.attr('data-product-item-id').toString(),
            newValue = selectedValue+currentValue,
            productName = article.attr('data-ta-product-name');
            if (selectedValue == 0) {
                newValue = 0;
            }
            window.CartManager.updateItem('erp.au.b2c/prod/' + id, newValue);
            addUpdatedCartHTML(selectedValue, productName, el);
            removeQuantitySelector();
    });
}

function addAddToCartFunctionality () {
    jQuery('.AddToBagButton__container').on('mouseenter','.QuantitySelector li',function(e){
        if (!jQuery(this).hasClass('opt_Added')) {
        jQuery(this).addClass('opt_Added');
            AddToCartFunctionality(this);
        }
    });
}
defer(function () {
    addAddToCartFunctionality();
}, '.AddToBagButton__container');