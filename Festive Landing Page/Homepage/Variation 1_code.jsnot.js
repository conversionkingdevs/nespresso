function defer(method, selector) {
    if (window.jQuery) {
        if (jQuery(selector).length > 0){
            method();  
        } else {
            setTimeout(function() { defer(method, selector); }, 50);
        }  
    } else {
         setTimeout(function() { defer(method, selector); }, 50);
    }    
}

function initHome() {

    jQuery('.focusPushBlock__products').append('<a href="/au/en/gift-ideas" class="opt-button-cont"><p>DISCOVER MORE GREAT GIFTING IDEAS HERE</p><div class="opt-icon"></div></a>');
    jQuery('.marketingPushBlock__content').before('<div class="opt-banner opt-orange"></div>');
    jQuery('#push-group-element').html('<img src="https://www.nespresso.com/ecom/medias/sys_master/public/11969371209758/BV5K3kg.png">');
    jQuery('#push-group-element').prepend('<div class="opt-banner opt-pink"></div>');
    jQuery('.Header__top').before('<div class="opt-banner opt-green"></div>');

    function replaceSlide () {
        $slide = jQuery('.CarouselSlide[data-promotion-item-id="AU_Explorations2018"]');
        $slide.addClass('opt-festivebanner');
        $slide.find('.CarouselSlide__links a').text('DISCOVER MORE');
        $slide.find('.CarouselSlide__image-gradient').html(`<img width="996" height="410" tabindex="-1" src="https://www.nespresso.com/ecom/medias/sys_master/public/11969372389406/YusuQMt.png" role="presentation" class="opt-desktopImage ResponsiveImage CarouselSlide__image" alt=""><img src="https://www.nespresso.com/ecom/medias/sys_master/public/11969372454942/fdy5f656.png" class="CarouselSlide__image opt-mobileImage ResponsiveImage">`);
        $slide.find('.CarouselSlide__links a').attr('href', '/au/en/gift-ideas');
        $slide.find('.CarouselSlide__text').html('<p style="font-size: xx-large;">A FRENCH TOUCH<br>TO YOUR GIFT<br>INSPIRATION</p>');
    }

    function addCSS () {
        jQuery('head').append(`
            <style type="text/css">
            @font-face {
                font-family: "NespressoLucas";
                src: url("https://www.nespresso.com/shared_res/mos/free_html/br/inbetween/fonts/NespressoLucas-Regular.woff2") format("woff2"), url("https://www.nespresso.com/shared_res/mos/free_html/br/inbetween/fonts/NespressoLucas-Regular.woff") format("woff"), url("https://www.nespresso.com/shared_res/mos/free_html/br/inbetween/fonts/NespressoLucas-Regular.ttf") format("truetype");
                font-style: normal;
                font-weight: 400
            }
            @font-face {
                font-family: "NespressoLucas";
                src: url("https://www.nespresso.com/shared_res/agility/commons/fonts/NespressoLucas-Medium.woff") format("woff");
                font-style: normal;
                font-weight: 500
            }
            @font-face {
                font-family: "NespressoLucas";
                src: url("https://www.nespresso.com/shared_res/mos/free_html/br/inbetween/fonts/NespressoLucas-SemiBd.woff2") format("woff2"), url("https://www.nespresso.com/shared_res/mos/free_html/br/inbetween/fonts/NespressoLucas-SemiBd.woff") format("woff"), url("https://www.nespresso.com/shared_res/mos/free_html/br/inbetween/fonts/NespressoLucas-SemiBd.ttf") format("truetype");
                font-style: normal;
                font-weight: 600
            }
            @font-face {
                font-family: "NespressoLucas";
                src: url("https://www.nespresso.com/shared_res/mos/free_html/br/inbetween/fonts/NespressoLucas-Bold.woff2") format("woff2"), url("https://www.nespresso.com/shared_res/mos/free_html/br/inbetween/fonts/NespressoLucas-Bold.woff") format("woff"), url("https://www.nespresso.com/shared_res/mos/free_html/br/inbetween/fonts/NespressoLucas-Bold.ttf") format("truetype");
                font-style: normal;
                font-weight: 700
            }
            
            
            .box {
                outline: none !important;
                color: #BD5C81 !important;
                border: 4px solid #bd5c81 !important;;
                border-left: 0 !important;;
                border-right: 0 !important;;
            }
            
            .box .button-close {
                color: #BD5C81 ;
            }
            
            @media(min-width: 767px) {
                .box {
                    font-size: 16px;
                }
            }
            
            .opt-button-cont {
                background: #BD5C81;
                max-width: 550px;
                width: 100%;
                margin: auto;
                padding: 19px 28px;
                text-align: center;
                color: #FFFFFF;
                font-family: "Trebuchet MS";
                font-size: 20px;
                font-weight: bold;
                display: -webkit-box;
                display: -webkit-flex;
                display: -ms-flexbox;
                display: flex;
                -webkit-box-align: center;
                -webkit-align-items: center;
                -ms-flex-align: center;
                align-items: center;
                -webkit-justify-content: space-around;
                -ms-flex-pack: distribute;
                justify-content: space-around;
                border-radius: 5px;
                z-index: 100;
                position: absolute;
                -webkit-transform: translatex(-50%);
                -ms-transform: translatex(-50%);
                transform: translatex(-50%);
                left: 50%;
                bottom: 50px;
            }
            
            .opt-button-cont p {
                display: inline-block;
                line-height: 20px;
            }
            
            .opt-button-cont .opt-icon {
                width: 25px;
                height: 25px;
                background: url('https://www.nespresso.com/ecom/medias/sys_master/public/11969372192798/VvTEQHe.png') no-repeat !important;
            }
            
            .opt-banner {
                width: 100%;
                height: 38px;
                background-repeat: repeat;
                -webkit-background-size: auto 100%;
                background-size: auto 100%;
                background-position: top left;
            }
            
            .opt-orange {
                background-image: url('https://www.nespresso.com/ecom/medias/sys_master/public/11969371996190/JXb8tZa.png');
            }
            
            .opt-pink {
                background-image: url('https://www.nespresso.com/ecom/medias/sys_master/public/11969372061726/mCk6SaE.png');
            }
            
            .opt-green {
                background-image: url('https://www.nespresso.com/ecom/medias/sys_master/public/11969371799582/HpB7yzf.png');
            }
            
            .Header__top .opt-banner {
                display: none;
            }
            
            .focusPushBlock__products {
                position: relative;
            }
            
            .focusPushBlock__products .push-group-element {
                overflow: hidden;
                position: relative;
                background: none !important;
            }
            .CarouselSlide__content {
                max-width: 500px;   
            }
            .opt-festivebanner .CarouselSlide__text {
                width: 100%;   
            }
            .opt-festivebanner .CarouselSlide__text p {
                font-family: "NespressoLucas";
                font-size: 33px;
                font-weight: bold;
                letter-spacing: 9.5px;
                color: white;
            }
            
            body #main-container {
                background: #000 url('https://www.nespresso.com/ecom/medias/sys_master/public/11969372127262/stScyu4.jpg') !important;
                -webkit-background-size: 100% !important;
                background-size: 100% !important;
                background-repeat: repeat !important;
                background-position: center;
            }
            
            .focusPushBlock__products .opt-banner {
                position: absolute;
                overflow: hidden;
            }
            .focusPushBlock__products {
                width: 100%;
            }
            
            .opt-festivebanner .CarouselSlide__button {
                background: white !important;
                border: white !important;
                color: #BD5C81 !important;
                font-weight: bold;
                font-size: 16px;
                margin-top: 20px;
            }
            .opt-banner.opt-orange {
                width: 210%;   
            }
            .focusPushBlock__articles {
                overflow: hidden;   
            }
            @media screen and (min-width: 996px){
                .HeaderNavigationBar__nav {
                    margin-top: 120px !important;
                }
            }
            @media (max-width: 995px) {
                .focusPushBlock__articles .recipes {

                    padding-top: 50px;
                }
                .focusPushBlock__products img{
                    width: 100%;
                }
                .opt-button-cont {
                    margin-bottom: 30px;
                }
                .opt-festivebanner .CarouselSlide__button {
                    position: static;
                    margin-top: 10px;
                }
                .ResponsiveContainer .opt-banner {
                    display: none;
                }
                .opt-mobileImage {
                    display: initial;
                }
                .opt-desktopImage {
                    display: none
                }
            }
            
            @media (max-width: 625px) {
                .focusPushBlock__articles .recipes {

                    padding-top: 20px;
                }
                .opt-button-cont {
                    font-size: 15px;
                    width: 80%;
                    padding-top: 15px;
                    padding-bottom: 15px;
                    margin-bottom: initial;
                    bottom: 20px;
                }
            
                .opt-button-cont .opt-icon {
                    margin-left: 10px;
                }
                .opt-festivebanner .CarouselSlide__text p {
                    font-size: 26px !important;   
                }
            }
            
            @media (max-width: 450px) {
               .opt-festivebanner .CarouselSlide__text p {
                    font-size: 18px !important;   
                }
            }
            </style>`
        )
    }
    replaceSlide();
    addCSS();
}

defer(initHome, '.Carousel__slides');