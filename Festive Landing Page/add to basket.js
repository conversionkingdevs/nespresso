var assortments = [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14];
var capsules = [0,10,20,30,40,50,60,70,80,90,100,150,200,250,300];

function addtocart(product, qty) {
    window.CartManager.updateItem('erp.au.b2c/prod/' + product, qty)
}

function hidequantities(data) {
    data.parents('.optnes-quantity-group').hide();
    data.parents('.optnes-quantity-group').removeClass('optnes-active');
    jQuery('.optnes-bg').hide();
}

function addquantitybuttons() {
    jQuery('.slides li').each(function() {
        var id = jQuery(this).find('a.basket').attr('data-sku');
        var group = jQuery(this).find('a.basket').attr('data-type');
        jQuery(this).find('a.basket').after('<div class="optnes-quantity-group opt84-target"><div class="optnes-quantity-error opt84-target"><div class="optnes-quantity-text opt84-target">You can purchase this product only by multiples of 10</div><div class="optnes-quantity-text opt84-target">We have modified the quantity to <span></span></div></div><div class="optnes-quantity-list opt84-target"><div class="optnes-quantity-item"></div><div class="optnes-quantity-item"></div><div class="optnes-quantity-item"></div><div class="optnes-quantity-item"></div><div class="optnes-quantity-item"></div><div class="optnes-quantity-item"></div><div class="optnes-quantity-item"></div><div class="optnes-quantity-item"></div><div class="optnes-quantity-item"></div><div class="optnes-quantity-item"></div><div class="optnes-quantity-item"></div><div class="optnes-quantity-item"></div><div class="optnes-quantity-item"></div><div class="optnes-quantity-item"></div><div class="optnes-quantity-item"></div></div><div class="optnes-quantity-input-group opt84-target" data-id="' + id + '"><div class="optnes-quantity-input opt84-target"><label for="optnes-quantity-input-' + id + '" class="optnes-input-label opt84-target"><span class="optnes-choose opt84-target">Choose a quantity</span></label><input class="opt84-target" id="optnes-quantity-input-' + id + '" type="number" min="0"></div><button type="button" class="optnes-quantity-ok opt84-target">OK</button></div></div>');

        if (group.indexOf('accessories') > -1 || group.indexOf('machine') > -1) {
            var setquantity = assortments;
        } else {
            var setquantity = capsules;
        }

        jQuery(this).find('.optnes-quantity-item').each(function(index) {
            jQuery(this).append('<button type="button" data-value="' + setquantity[index] + '">' + setquantity[index] + '</button>');
        });
    });
}

addquantitybuttons();

jQuery('.slides li').on('click', 'a.basket', function(e) {
    e.preventDefault();
    if (window.innerWidth < 768) {
        jQuery('.optnes-bg').fadeIn();
    }
    if (jQuery(this).next('.optnes-quantity-group').hasClass('optnes-active')) {
        jQuery(this).next('.optnes-quantity-group').hide();
        jQuery(this).next('.optnes-quantity-group').removeClass('optnes-active');
    } else {
        jQuery('.optnes-quantity-group').removeClass('optnes-active');
        jQuery('.optnes-quantity-group').hide();
        jQuery(this).next('.optnes-quantity-group').show();
        jQuery(this).next('.optnes-quantity-group').addClass('optnes-active');
    }
});

jQuery('.slides li').on('click', '.optnes-quantity-item button', function() {
    var product = jQuery(this).closest('.buttons').find('a.basket').attr('data-sku');
    var qty = jQuery(this).attr('data-value');
    addtocart(product, qty);
    hidequantities(jQuery(this));
});

jQuery('.slides li').on('click', '.optnes-quantity-ok', function() {
    jQuery(this).parent().parent().find('.optnes-quantity-error').hide();
    jQuery(this).parent().parent().find('.optnes-quantity-list').show();
    var product = jQuery(this).parents('.optnes-quantity-input-group').attr('data-id');
    var qty = jQuery(this).parent().find('input').val();

    if (qty !== 0) {
        if (jQuery(this).parent().parent().find('.optnes-quantity-list .optnes-quantity-item:eq(-1) button').attr('data-value') == '300') {
            if ((qty % 10) == 0) {
                addtocart(product, qty);
                hidequantities(jQuery(this));
            } else {
                var newvalue = (Math.round(qty / 10) * 10);
                jQuery(this).parent().parent().find('.optnes-quantity-error').show();
                jQuery(this).parent().parent().find('.optnes-quantity-list').hide();
                jQuery(this).parent().parent().find('.optnes-quantity-error span').text(newvalue);
                jQuery(this).parent().find('input').val(newvalue);
            }
        } else {
                addtocart(product, qty);
                hidequantities(jQuery(this));
        }
    }
});

jQuery('.slides li').on('click', '.optnes-quantity-input', function() {
    jQuery(this).addClass('optnes-input-active');
});

jQuery('.slides li').on('keypress', '.optnes-quantity-input input', function(e) {
    if(e.which == 13) {
        jQuery(this).parent().parent().find('.optnes-quantity-ok').click();
    }
});

jQuery('.optnes-bg').click(function() {
    jQuery(this).fadeOut();
    jQuery('.optnes-quantity-group').hide();
    jQuery('.optnes-quantity-group').removeClass('optnes-active');
});

