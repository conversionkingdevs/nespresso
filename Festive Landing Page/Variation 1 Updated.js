function initLandingPage() {
    jQuery('body').addClass('opt');

    /**************************************
        HELPERS
    **************************************/ 
    jQuery.fn.adjustHeight = function() {
        let maxHeightFound = 0;
        this.css('min-height','1px');
        this.each(function() {
            if( jQuery(this).outerHeight() > maxHeightFound ) {
                maxHeightFound = jQuery(this).outerHeight();
            }
        });
        this.css('min-height', maxHeightFound);
    };

    function startAddtoCart () {
        var assortments = [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14];
        var capsules = [0,10,20,30,40,50,60,70,80,90,100,150,200,250,300];

        function addtocart(product, qty) {
            window.CartManager.updateItem('erp.au.b2c/prod/' + product, qty)
        }

        function hidequantities(data) {
            data.parents('.optnes-quantity-group').hide();
            data.parents('.optnes-quantity-group').removeClass('optnes-active');
            jQuery('.optnes-bg').hide();
        }

        function addquantitybuttons() {
            jQuery('.slides li').each(function() {
                var id = jQuery(this).find('a.basket').attr('data-sku');
                var group = jQuery(this).find('a.basket').attr('data-type');
                jQuery(this).find('a.basket').after('<div class="optnes-quantity-group opt84-target"><div class="optnes-quantity-error opt84-target"><div class="optnes-quantity-text opt84-target">You can purchase this product only by multiples of 10</div><div class="optnes-quantity-text opt84-target">We have modified the quantity to <span></span></div></div><div class="optnes-quantity-list opt84-target"><div class="optnes-quantity-item"></div><div class="optnes-quantity-item"></div><div class="optnes-quantity-item"></div><div class="optnes-quantity-item"></div><div class="optnes-quantity-item"></div><div class="optnes-quantity-item"></div><div class="optnes-quantity-item"></div><div class="optnes-quantity-item"></div><div class="optnes-quantity-item"></div><div class="optnes-quantity-item"></div><div class="optnes-quantity-item"></div><div class="optnes-quantity-item"></div><div class="optnes-quantity-item"></div><div class="optnes-quantity-item"></div><div class="optnes-quantity-item"></div></div><div class="optnes-quantity-input-group opt84-target" data-id="' + id + '"><div class="optnes-quantity-input opt84-target"><label for="optnes-quantity-input-' + id + '" class="optnes-input-label opt84-target"><span class="optnes-choose opt84-target">Choose a quantity</span></label><input class="opt84-target" id="optnes-quantity-input-' + id + '" type="number" min="0"></div><button type="button" class="optnes-quantity-ok opt84-target">OK</button></div></div>');

                if (group.indexOf('accessories') > -1 || group.indexOf('machine') > -1) {
                    var setquantity = assortments;
                } else {
                    var setquantity = capsules;
                }

                jQuery(this).find('.optnes-quantity-item').each(function(index) {
                    jQuery(this).append('<button type="button" data-value="' + setquantity[index] + '">' + setquantity[index] + '</button>');
                });
            });
        }

        addquantitybuttons();

        jQuery('.slides li').on('click', 'a.basket', function(e) {
            e.preventDefault();
            if (window.innerWidth < 768) {
                jQuery('.optnes-bg').fadeIn();
            }
            if (jQuery(this).next('.optnes-quantity-group').hasClass('optnes-active')) {
                jQuery(this).next('.optnes-quantity-group').hide();
                jQuery(this).next('.optnes-quantity-group').removeClass('optnes-active');
            } else {
                jQuery('.optnes-quantity-group').removeClass('optnes-active');
                jQuery('.optnes-quantity-group').hide();
                jQuery(this).next('.optnes-quantity-group').show();
                jQuery(this).next('.optnes-quantity-group').addClass('optnes-active');
            }
        });

        jQuery('.slides li').on('click', '.optnes-quantity-item button', function() {
            var product = jQuery(this).closest('.buttons').find('a.basket').attr('data-sku');
            var qty = jQuery(this).attr('data-value');
            addtocart(product, qty);
            hidequantities(jQuery(this));
        });

        jQuery('.slides li').on('click', '.optnes-quantity-ok', function() {
            jQuery(this).parent().parent().find('.optnes-quantity-error').hide();
            jQuery(this).parent().parent().find('.optnes-quantity-list').show();
            var product = jQuery(this).closest('.buttons').find('a.basket').attr('data-sku');
            var qty = jQuery(this).parent().find('input').val();

            if (qty !== 0) {
                if (jQuery(this).parent().parent().find('.optnes-quantity-list .optnes-quantity-item:eq(-1) button').attr('data-value') == '300') {
                    if ((qty % 10) == 0) {
                        addtocart(product, qty);
                        hidequantities(jQuery(this));
                    } else {
                        var newvalue = (Math.round(qty / 10) * 10);
                        jQuery(this).parent().parent().find('.optnes-quantity-error').show();
                        jQuery(this).parent().parent().find('.optnes-quantity-list').hide();
                        jQuery(this).parent().parent().find('.optnes-quantity-error span').text(newvalue);
                        jQuery(this).parent().find('input').val(newvalue);
                    }
                } else {
                        addtocart(product, qty);
                        hidequantities(jQuery(this));
                }
            }
        });

        jQuery('.slides li').on('click', '.optnes-quantity-input', function() {
            jQuery(this).addClass('optnes-input-active');
        });

        jQuery('.slides li').on('keypress', '.optnes-quantity-input input', function(e) {
            if(e.which == 13) {
                jQuery(this).parent().parent().find('.optnes-quantity-ok').click();
            }
        });

        jQuery('.optnes-bg').click(function() {
            jQuery(this).fadeOut();
            jQuery('.optnes-quantity-group').hide();
            jQuery('.optnes-quantity-group').removeClass('optnes-active');
        });


    }
    /**************************************
        LAYOUT
    **************************************/ 
    jQuery(".opt #main-container #main").remove();
    jQuery('<div id="main"><div id="home-container" /></div>').insertAfter(jQuery(".opt #main-container #top"));
    jQuery("#home-container").append('<div id="home-section-1" class="home-section"><div class="home-content" /></div>')
    jQuery("#home-container").append('<div id="home-section-2" class="home-section"><div class="home-content" /></div>')
    jQuery("#home-container").append('<div id="home-section-3" class="home-section"><div class="home-content" /></div>')
    jQuery("#home-container").append('<div id="home-section-4" class="home-section"><div class="home-content" /></div>')



    /**************************************
        Data
    **************************************/
    data = {
        
        /***** Section 1 *****/
        section_1: {
            'title'   : "A french touch to your gift inspiration",
            'content' : "Get inspired by our gifting ideas and bring joy with a perfect Nespresso moment.",
        },
        
        
        /***** Section 2 *****/
        section_2: {
            'title'    : "Machines",
            'products' : [
                {
                    'name'   : 'Creatista Plus Matte White',
                    'copy'   : 'Creatista Plus enables you to easily create an authentic top-quality Latte Art coffee at home.',
                    'price'  : '$799.00',
                    'colors' : [
                        {
                            'image' : 'https://www.nespresso.com/ecom/medias/sys_master/public/11969371013150/8XSNTiG.png',
                            'color' : '#ffffff',
                            'title' : 'Matte White',
                            'sku'   : 'BNE800SST',
                            'link'  : 'https://www.nespresso.com/au/en/order/machines/original/creatista-plus-matte-white-coffee-machine',
                            'type'  : 'machine'
                        },
                    ]
                },
                {
                    'name'   : 'Vertuo Plus',
                    'copy'   : 'Nespresso, introduces the VertuoPlus with an all-new design and colours for the ultimate brewing experience.',
                    'price'  : '$299.00',
                    'colors' : [
                        {
                            'image' : 'https://www.nespresso.com/ecom/medias/sys_master/public/11969371471902/EH9t7T2.png',
                            'color' : '#000000',
                            'title' : 'Black (Round Top)',
                            'sku'   : 'GCB2-AU-BK-NE',
                            'link'  : 'https://www.nespresso.com/au/en/order/machines/vertuo/vertuo-plus-coffee-machine-deluxe-black-c',
                            'type'  : 'machine'
                        }
                    ]
                },
                {
                    'name'   : 'Essenza Mini',
                    'copy'   : 'Essenza Mini, the small machine for big coffee moments.',
                    'price'  : '$159.00',
                    'colors' : [
                        {
                            'image' : 'https://www.nespresso.com/ecom/medias/sys_master/public/11969371865118/i1eQpky.png',
                            'color' : '#000000',
                            'title' : 'Breville Pure Black',
                            'sku'   : 'BEC220BLK',
                            'link'  : 'https://www.nespresso.com/au/en/order/machines/original/essenza-mini-breville-pure-black-solo',
                            'type'  : 'machine'
                        },
                    ]
                },
            ]
        },
        
        
        /***** Section 3 *****/
        section_3: {
            'products' : [
                {
                    'name'      : 'Variations Paris Praliné',
                    'copy'      : 'A harmonious hazelnut flavoured blend reminiscent of a Praliné dessert.',
                    'category'  : 'original',
                    'image'     : 'https://www.nespresso.com/ecom/medias/sys_master/public/11969371078686/BCgre2L.png',
                    'intensity' : '6',
                    'price'     : '$0.95',
                    'sku'       : '7706.30',
                    'link'      : 'https://www.nespresso.com/au/en/order/capsules/original/praline-coffee-capsules',
                    'type'      : 'capsule'
                },
                {
                    'name'      : 'Variations Paris Macaron',
                    'copy'      : 'An almond flavoured coffee emblematic of the Macaron pastry.',
                    'category'  : 'original',
                    'image'     : 'https://www.nespresso.com/ecom/medias/sys_master/public/11969372880926/pYrDMc8.png',
                    'intensity' : '6',
                    'price'     : '$0.95',
                    'sku'       : '7705.30',
                    'link'      : 'https://www.nespresso.com/au/en/order/capsules/original/coffee-macaron-capsules',
                    'type'      : 'capsule'
                },
                {
                    'name'      : 'Paris Black',
                    'copy'      : 'A true Parisian experience, worthy of the best traditional coffee places in Paris.',
                    'category'  : 'original',
                    'image'     : 'https://www.nespresso.com/ecom/medias/sys_master/public/11969373077534/v3EdSmr.png',
                    'intensity' : '9',
                    'price'     : '$0.95',
                    'sku'       : '7707.30',
                    'link'      : 'https://www.nespresso.com/au/en/order/capsules/original/paris-black-coffee-capsules',
                    'type'      : 'capsule'
                },
                {
                    'name'      : 'Variations Paris Madeleine',
                    'copy'      : 'An almond flavoured coffee pleasantly reminiscent of a Madeleine.',
                    'category'  : 'vertuo',
                    'image'     : 'https://www.nespresso.com/ecom/medias/sys_master/public/11969371734046/HorLWCx.png',
                    'intensity' : '5',
                    'price'     : '$1.00',
                    'sku'       : '7346.10',
                    'link'      : 'https://www.nespresso.com/au/en/order/capsules/vertuo/madeleine-coffee-vertuo-pods',
                    'type'      : 'capsule'
                },
                {
                    'name'      : 'Variations Paris Exotic Macaron',
                    'copy'      : 'A delicately coconut flavoured blend reminiscent of an Exotic Macaron.',
                    'category'  : 'vertuo',
                    'image'     : 'https://www.nespresso.com/ecom/medias/sys_master/public/11969373011998/s7cJ2fh.png',
                    'intensity' : '5',
                    'price'     : '$1.00',
                    'sku'       : '7345.10',
                    'link'      : 'https://www.nespresso.com/au/en/order/capsules/vertuo/exotic-macaron-vertuo-pods',
                    'type'      : 'capsule'
                },
                {
                    'name'      : 'Paris Black',
                    'copy'      : 'A true Parisian experience, worthy of the best traditional coffee places in Paris.',
                    'category'  : 'vertuo',
                    'image'     : 'https://www.nespresso.com/ecom/medias/sys_master/public/11969372258334/X6LYykv.png',
                    'intensity' : '11',
                    'price'     : '$1.00',
                    'sku'       : '7347.10',
                    'link'      : 'https://www.nespresso.com/au/en/order/capsules/vertuo/paris-black-vertuo-coffee-pods',
                    'type'      : 'capsule'
                }
            ]
        },
        
        
        /***** Section 4 *****/
        section_4: {
            'title'    : "Accessories",
            'products' : [
                {
                    'name'   : 'Touch Travel Mug',
                    'copy'   : 'Enjoy your Nespresso coffee outside of home thanks to the Touch Travel Mug.',
                    'image'  : 'https://www.nespresso.com/ecom/medias/sys_master/public/11969372749854/EIyxVkt.png',
                    'price'  : '$25.00',
                    'sku'    : '3692',
                    'link'   : 'https://www.nespresso.com/au/en/order/accessories/original/touch-travel-mug-coffee-cup',
                    'type'   : 'accessories'
                },
                {
                    'name'   : 'VIEW Cappuccino Cup Set',
                    'copy'   : 'Set of 2 Cappuccino cups (ca. 180ml) in tempered glass and 2 saucers in stainless steel (18/10) with a shiny and brushed finish.',
                    'image'  : 'https://www.nespresso.com/ecom/medias/sys_master/public/11969371602974/FQQa2SI.png',
                    'price'  : '$34.00',
                    'sku'    : '3732/2',
                    'link'   : 'https://www.nespresso.com/au/en/order/accessories/original/glass-cappuccino-cup-saucer',
                    'type'   : 'accessories'
                },
                {
                    'name'   : 'Aeroccino3 Black Milk Frother',
                    'copy'   : 'Be your own barista with the Retro line of Aeroccino milk frothers, available in 2 colours: white or black.',
                    'image'  : 'https://www.nespresso.com/ecom/medias/sys_master/public/11969371668510/H7NIwJm.png',
                    'price'  : '$99.00',
                    'sku'    : '3594-AU-BK',
                    'link'   : 'https://www.nespresso.com/au/en/order/accessories/original/aeroccino-3-black',
                    'type'   : 'accessories'
                }
            ]
        }
    };




    /***** Section 1 Data *****/
    jQuery("#home-section-1 .home-content").append('<h1>'+data.section_1.title+'</h1>');
    jQuery("#home-section-1 .home-content").append('<div class="bottom-text"><p>'+data.section_1.content+'</p></div>');



    /***** Section 2 Data *****/
    jQuery("#home-section-2 .home-content").append('<h2>'+data.section_2.title+'</h2>');
    jQuery("#home-section-2 .home-content").append('<div class="products flexslider"><ul class="slides" /></div>');

    data.section_2.products.forEach(function(product, i){
        jQuery("#home-section-2 .home-content .products .slides").append(`
            <li>
                <h3>`+product.name+`</h3>
                <div class="product-image"></div>
                <p>`+product.copy+`</p>
                <span class="colors">
                    <p>Available Colours:</p>
                </span>
                <span class="price">`+product.price+`</span>
                <span class="buttons">
                    <a href="#" class="button more">Discover more</a>
                    <a href="#" class="button basket"><i aria-hidden="true" class="Glyph Glyph--basket MiniBasketButton__basketIcon"></i>Add to basket<i aria-hidden="true" class="Glyph Glyph--plus AddToBagButtonLarge__plusIcon"></i></a>
                </span>
            </li>
        `);   
        
        product.colors.forEach(function(color,j){
        jQuery("#home-section-2 .home-content .products .slides li:last .product-image").append('<img src="'+color.image+'" />');
        jQuery("#home-section-2 .home-content .products .slides li:last .colors").append('<a href="#" data-href="'+color.link+'" data-sku="'+color.sku+'" data-type="'+color.type+'" title="'+color.title+'"><span style="background-color:'+color.color+'"></span></a>');
        });
        
        jQuery("#home-section-2 .home-content .products .slides li .product-image img:first-of-type").addClass('active');
        jQuery("#home-section-2 .home-content .products .slides li .colors a:first-of-type").addClass('active');
    });

    jQuery("#home-section-2 .home-content .products .slides li").each(function(){
        $this = jQuery(this);
        $this.find('a.button.more').attr('href',$this.find('.colors a:first-of-type').data('href'));
        $this.find('a.button.basket').attr('data-sku',$this.find('.colors a:first-of-type').data('sku'));
        $this.find('a.button.basket').attr('data-type',$this.find('.colors a:first-of-type').data('type'));
    });

    jQuery("#home-section-2 .home-content .products .slides li .colors a").click(function(e){
        e.preventDefault();
        $this = jQuery(this);
        jQuery("#home-section-2 .home-content .products .slides li .colors a").removeClass('active');
        $this.addClass('active');
        $this.parent().parent().find('a.button.more').attr('href',$this.data('href'))
        $this.parent().parent().find('a.button.basket').attr('data-sku',$this.data('sku'))
        $this.parent().parent().find('a.button.basket').attr('data-type',$this.data('type'))
        
        $this.parent().parent().find('.product-image').animate({opacity:0},200,function(){
            $this.parent().parent().find('.product-image img').removeClass('active');
            $this.parent().parent().find('.product-image img:nth-child('+$this.index()+')').addClass('active');
            $this.parent().parent().find('.product-image').animate({opacity:1},200);    
        });
    });



    /***** Section 3 Data *****/
    jQuery("#home-section-3 .home-content").append('<div class="products"><div id="original-range" class="left"><h2>Original Capsules</h2><div class="flexslider"><ul class="slides" /></div></div><div id="vertuo-range" class="right"><h2>Vertuo Capsules</h2><div class="flexslider"><ul class="slides" /></div></div></div>');

    data.section_3.products.forEach(function(product, i){
        if(product.category == 'original'){
            jQuery("#home-section-3 .home-content .left .slides").append(`
                <li>
                    <h3>`+product.name+`</h3>
                    <img src="`+product.image+`" />
                    <span class="intensity"><span>Intensity</span></span>
                    <p>`+product.copy+`</p>
                    <span class="price">`+product.price+`</span>
                    <span class="buttons">
                        <a href="`+product.link+`" class="button more">Discover more</a>
                        <a href="#" class="button basket" data-sku="`+product.sku+`" data-type="`+product.type+`"><i aria-hidden="true" class="Glyph Glyph--basket MiniBasketButton__basketIcon"></i>Add to basket<i aria-hidden="true" class="Glyph Glyph--plus AddToBagButtonLarge__plusIcon"></i></a>
                    </span>
                </li>
            `)
            
            for (j = 1; j <= product.intensity; j++) {
                jQuery("#home-section-3 .home-content .left .slides li:last .intensity").append('<span class="intensity-block filled" />');
            };
            jQuery("#home-section-3 .home-content .left .slides li:last .intensity").append('<span class="intensity-number">'+product.intensity+'</span>')
            for (k = product.intensity; k < 12; k++) {
                jQuery("#home-section-3 .home-content .left .slides li:last .intensity").append('<span class="intensity-block" />');
            };
        } else {
            jQuery("#home-section-3 .home-content .right .slides").append(`
                <li>
                    <h3>`+product.name+`</h3>
                    <img src="`+product.image+`" />
                    <span class="intensity"></span>
                    <p>`+product.copy+`</p>
                    <span class="price">`+product.price+`</span>
                    <span class="buttons">
                        <a href="`+product.link+`" class="button more">Discover more</a>
                        <a href="#" class="button basket" data-sku="`+product.sku+`" data-type="`+product.type+`"><i aria-hidden="true" class="Glyph Glyph--basket MiniBasketButton__basketIcon"></i>Add to basket<i aria-hidden="true" class="Glyph Glyph--plus AddToBagButtonLarge__plusIcon"></i></a>
                    </span>
                </li>
            `)
            
            for (j = 1; j <= product.intensity; j++) {
                jQuery("#home-section-3 .home-content .right .slides li:last .intensity").append('<span class="intensity-block filled" />');
            };
            jQuery("#home-section-3 .home-content .right .slides li:last .intensity").append('<span class="intensity-number">'+product.intensity+'</span>')
            for (k = product.intensity; k < 12; k++) {
                jQuery("#home-section-3 .home-content .right .slides li:last .intensity").append('<span class="intensity-block" />');
            };
        };
    });

    jQuery("#home-section-3 .home-content").prepend('<div class="mobile-stuff" />');
    jQuery("#home-section-3 .mobile-stuff").append('<h2>Capsules</h2>');
    jQuery("#home-section-3 .mobile-stuff").append('<div class="capsules-navigation"><a href="#original-range" class="active">Original Range</a><a href="#vertuo-range">Vertuo Range</a></div>');


    jQuery("body").on('click','.capsules-navigation a',function(e){
        e.preventDefault();
        $this = jQuery(this);
        $href = $this.attr('href');
        jQuery(".capsules-navigation a").removeClass('active');
        $this.addClass('active');
        jQuery("#home-section-3 .products > div").hide();
        jQuery($href).css('display','block');
    });



    /***** Section 4 Data *****/
    jQuery("#home-section-4 .home-content").append('<h2>'+data.section_4.title+'</h2>');
    jQuery("#home-section-4 .home-content").append('<div class="products flexslider"><ul class="slides" /></div>');

    data.section_4.products.forEach(function(product, i){
        jQuery("#home-section-4 .home-content .products .slides").append(`
            <li>
                <h3>`+product.name+`</h3>
                <img src="`+product.image+`" />
                <p>`+product.copy+`</p>
                <span class="price">`+product.price+`</span>
                <span class="buttons">
                    <a href="`+product.link+`" class="button more">Discover more</a>
                    <a href="#" class="button basket" data-sku="`+product.sku+`" data-type="`+product.type+`"><i aria-hidden="true" class="Glyph Glyph--basket MiniBasketButton__basketIcon"></i>Add to basket<i aria-hidden="true" class="Glyph Glyph--plus AddToBagButtonLarge__plusIcon"></i></a>
                </span>
            </li>
        `);   
    });

    jQuery("#home-section-4").append('<img class="section-image" src="https://www.nespresso.com/ecom/medias/sys_master/public/11969371340830/cIu0R9w.jpg" />');


    /**************************************
        FONTS
    **************************************/
    jQuery('head').append(`
    <style type="text/css">
        @font-face {
            font-family: "NespressoLucas";
            src: url("https://www.nespresso.com/shared_res/mos/free_html/br/inbetween/fonts/NespressoLucas-Regular.woff2") format("woff2"), url("https://www.nespresso.com/shared_res/mos/free_html/br/inbetween/fonts/NespressoLucas-Regular.woff") format("woff"), url("https://www.nespresso.com/shared_res/mos/free_html/br/inbetween/fonts/NespressoLucas-Regular.ttf") format("truetype");
            font-style: normal;
            font-weight: 400
        }
        @font-face {
        font-family: "NespressoLucas";
        src: url("https://www.nespresso.com/shared_res/agility/commons/fonts/NespressoLucas-Medium.woff") format("woff");
        font-style: normal;
        font-weight: 500
        }
        @font-face {
            font-family: "NespressoLucas";
            src: url("https://www.nespresso.com/shared_res/mos/free_html/br/inbetween/fonts/NespressoLucas-SemiBd.woff2") format("woff2"), url("https://www.nespresso.com/shared_res/mos/free_html/br/inbetween/fonts/NespressoLucas-SemiBd.woff") format("woff"), url("https://www.nespresso.com/shared_res/mos/free_html/br/inbetween/fonts/NespressoLucas-SemiBd.ttf") format("truetype");
            font-style: normal;
            font-weight: 600
        }
        @font-face {
            font-family: "NespressoLucas";
            src: url("https://www.nespresso.com/shared_res/mos/free_html/br/inbetween/fonts/NespressoLucas-Bold.woff2") format("woff2"), url("https://www.nespresso.com/shared_res/mos/free_html/br/inbetween/fonts/NespressoLucas-Bold.woff") format("woff"), url("https://www.nespresso.com/shared_res/mos/free_html/br/inbetween/fonts/NespressoLucas-Bold.ttf") format("truetype");
            font-style: normal;
            font-weight: 700
        }
    </style>
    `);



    /**************************************
        STYLES
    **************************************/
    jQuery('head').append(`
    <style type="text/css">
    .opt .HeaderNavigationBar__nav {margin-bottom:0;}
        
        
    /***** General *****/
    .home-section {text-align:center; color:#ffffff; font-size:15px; line-height:19px; letter-spacing:0.03px; font-family:NespressoLucas; font-weight:600; position:relative; border-top:5px solid #ffffff; max-width:none; margin:0 auto;}
    .home-section:before {display:block; content:""; position:absolute; left:0; top:0; width:100%; height:40px;}
    
    .home-content {padding:72px 15px 50px 15px; max-width:996px; margin:0 auto;}
    .home-content h1 {text-transform:uppercase; color:#ffffff; font-size:38px; line-height:42px; letter-spacing:11.52px; font-family:NespressoLucas; font-weight:700; padding:5px 0 0 0; margin:0;}
    .home-content h2 {text-transform:uppercase; color:#ffffff; font-size:34px; line-height:38px; letter-spacing:1.47px; font-family:NespressoLucas; font-weight:700; padding:0; margin:0;}
    .home-content h3 {color:#ffffff; font-size:21px; line-height:27px; letter-spacing:0.91px; font-family:NespressoLucas; font-weight:600; padding:0 0 15px 0; margin:0;}
    .home-content p {padding:0; margin:0;} 
    
    .home-content .products .slides li .buttons {max-width:212px; margin:0 auto;}
    .home-content .products .slides li .buttons a {display:block; font-weight:normal; font-size:14px; line-height:20px; letter-spacing:0; text-decoration:none; text-transform:uppercase; border-radius:3px; background-color:#b16183; margin:0 0 10px 0; padding:8px 5px 7px 5px; position:relative; -moz-transition:background 0.2s ease; -webkit-transition:background 0.2s ease; -o-transition:background 0.2s ease;}
    .home-content .products .slides li .buttons a:hover {background-color:#893d5f;}
    .home-content .products .slides li .buttons a.basket {background-color:#518426;}
    .home-content .products .slides li .buttons a:hover.basket {background-color:#2a5e00;}
    .home-content .products .slides li .buttons a i.Glyph--basket {position:absolute; left:5px; top:50%; font-size:24px; margin-top:-12px;}
    .home-content .products .slides li .buttons a i.Glyph--plus {position:absolute; right:10px; top:50%; font-size:14px; margin-top:-7px;}
    
    .slides li .buttons {
        position: relative;   
    }

    .optnes-quantity-group {
        display: none;
        position: absolute;
        top: -200px;
        left: 8%;
        width: 84%;
        padding: 10px 10px;
        background-color: #f9f9f9;
        border: 1px solid #d5d5d5;
        border-radius: 2px;
        text-align: center;
    }

    .optnes-quantity-group:before {
        bottom: -11px;
        border-style: solid;
        border-bottom-width: 0;
        border-left-width: 10px;
        border-right-width: 10px;
        border-top-width: 10px;
        border-right-color: transparent;
        border-left-color: transparent;
        border-top-color: #d5d5d5;
        border-bottom-color: #d5d5d5;
        left: -webkit-calc(50% - 10px);
        left: calc(50% - 10px);
        content: '';
        position: absolute;
        width: 0;
        height: 0;
    }


    .optnes-quantity-group:after {
        bottom: -10px;
        border-style: solid;
        border-bottom-width: 0;
        border-left-width: 10px;
        border-right-width: 10px;
        border-top-width: 10px;
        border-color: #f9f9f9;
        border-right-color: transparent;
        border-left-color: transparent;
        left: -webkit-calc(50% - 10px);
        left: calc(50% - 10px);
        content: '';
        position: absolute;
        width: 0;
        height: 0;
    }

    .optnes-quantity-item {
        width: 20%;
        padding: 5px 0px;
        display: inline-block;
        vertical-align: top;
        border-left: .0625rem solid #d5d5d5;
        
    }

    .optnes-quantity-item button {
        border: none;
        background: none;
        font-size: 12px;
        padding: 6px 0px;
        border: 1px solid transparent;
        width: 85%;
        text-align: center;
        border-radius: 4px;
    }

    .optnes-quantity-item:hover button {
        border: 1px solid #000000;
    }

    .optnes-quantity-item:active button, .optnes-quantity-item:focus button {
        border: 1px solid #000000;
        background-color: #000000;
        color: #ffffff;
    }

    .optnes-quantity-item:first-of-type, .optnes-quantity-item:nth-of-type(5n)+.optnes-quantity-item {
        border-left: 0 none;
    }

    .optnes-quantity-error {
        display: none;
        padding: 10px 10px;
    }

    .optnes-quantity-error .optnes-quantity-text {

    }

    .optnes-quantity-error .optnes-quantity-text:last-child {
        font-weight: bold;
        padding-top: 15px;
    }

    .optnes-quantity-input-group {
        padding: 8px 0 0 0;
    }

    .optnes-quantity-input {
        position: relative;
        width: 80%;
        display: inline-block;
        vertical-align: top;
        background-color: #ffffff;
        border: 1px solid #d5d5d5;
        height: 40px;
        line-height: 40px;
        -webkit-border-top-left-radius: 4px;
        -webkit-border-bottom-left-radius: 4px;
        -moz-border-radius-topleft: 4px;
        -moz-border-radius-bottomleft: 4px;
        border-top-left-radius: 4px;
        border-bottom-left-radius: 4px;
        margin: 0;
    }

    .optnes-quantity-input input {
        border: none;
        background: none;
        -webkit-appearance: none;
        -moz-appearance: none;
            appearance: none;
        width: 100%;
        height: 40px;
        font-size: 16px;
        padding: 10px 0 0 6px;
    }

    .optnes-quantity-input input::-webkit-inner-spin-button, .optnes-quantity-input input::-webkit-outer-spin-button {
        -webkit-appearance: none;
        appearance: none;
    }

    .optnes-input-label {
        position: absolute;
        color: #656565;
        top: 0px;
        left: 10px;
        font-size: 12px;
        -webkit-transition: 0.3s all ease;
        transition: 0.3s all ease;
        pointer-events: none;
    }

    .optnes-input-active .optnes-input-label {
        position: absolute;
        color: #986f38;
        left: 5px;
        top: -10px;
        font-size: 10px;
    }

    .optnes-quantity-ok {
        width: 20%;
        display: inline-block;
        vertical-align: top;
        background-color: green;
        color: #ffffff;
        border: none;
        -webkit-border-top-right-radius: 4px;
        -webkit-border-bottom-right-radius: 4px;
        -moz-border-radius-topright: 4px;
        -moz-border-radius-bottomright: 4px;
        border-top-right-radius: 4px;
        border-bottom-right-radius: 4px;
        height: 40px;
        line-height: 40px;
        font-size: 13px;
    }

    .optnes-bg {
        display: none;
    }
    
    
    /***** Section 1 *****/
    #home-section-1 {background:#000000 url("https://www.nespresso.com/ecom/medias/sys_master/public/11969371275294/C9Zh8Bg.jpg") no-repeat center 70%; min-height:870px; -webkit-background-size:100% auto; background-size:100% auto;}
    #home-section-1:before {background:transparent url("https://www.nespresso.com/ecom/medias/sys_master/public/11969372061726/mCk6SaE.png") repeat-x left top; -webkit-background-size:auto 40px; background-size:auto 40px;} 
    
    #home-section-1 h1 {max-width:780px; margin:0 auto;}
    #home-section-1 p {text-align: center;font-size:26px; line-height:32px; letter-spacing:0.84px; font-family:NespressoLucas; font-weight:400; max-width:996px; margin:0 auto; display:block;}
    #home-section-1 .bottom-text {position:absolute; left:0; bottom:22px; width:100%;}
    
    
    
    /***** Section 2 *****/
    #home-section-2 {background:#000000 url("https://www.nespresso.com/ecom/medias/sys_master/public/11969371406366/CM8MgRi.png") repeat-x left top;}
    #home-section-2:before {background:transparent url("https://www.nespresso.com/ecom/medias/sys_master/public/11969371799582/HpB7yzf.png") repeat-x left top; -webkit-background-size:auto 40px; background-size:auto 40px;} 
    
    #home-section-2 .products {max-width:996px; margin:0 auto; padding:26px 0 0 0;}
    #home-section-2 .products .slides {padding:0; margin:0; list-style:none;}
    #home-section-2 .products .slides li {float:left; width:29.52%; margin-right:5.72%;}
    #home-section-2 .products .slides li:nth-child(3n) {margin-right:0;}
    #home-section-2 .products:after {content:""; display:table; clear:both;}
    
    #home-section-2 .products .slides li img {max-width:none; width:100%; height:auto; display:none;}
    #home-section-2 .products .slides li img.active {display:block;}
    
    #home-section-2 .products .slides li span {display:block;}
    #home-section-2 .products .slides li > p {max-width:260px; margin:0 auto; padding:16px 0 13px 0; display:block; font-weight:500;}
    
    #home-section-2 .products .slides li .colors p {display:inline-block; vertical-align:middle; font-size:16px; letter-spacing:0.69px;}
    #home-section-2 .products .slides li .colors a {display:inline-block; vertical-align:middle; margin:0 6px 0 7px;}
    #home-section-2 .products .slides li .colors a span {display:block; width:20px; height:20px; border:2px solid #ffffff; border-radius:50%; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; -moz-transition:border 0.2s ease; -webkit-transition:border 0.2s ease; -o-transition:border 0.2s ease;}
    #home-section-2 .products .slides li .colors a:hover span, #home-section-2 .products .slides li .colors a.active span {border-width:5px;}
    #home-section-2 .products .slides li .colors a.active {cursor:default;}
    
    #home-section-2 .products .slides li .price {font-size:20px; line-height:24px; padding:10px 0 14px 0;}
    
    
    
    /***** Section 3 *****/
    #home-section-3 {background:#000000 url("https://www.nespresso.com/ecom/medias/sys_master/public/11969371930654/JefVQ15.png") repeat-x left top;}
    #home-section-3:before {background:transparent url("https://www.nespresso.com/ecom/medias/sys_master/public/11969372061726/mCk6SaE.png") repeat-x left top; -webkit-background-size:auto 40px; background-size:auto 40px;} 
    
    #home-section-3 .products {max-width:996px; margin:10px auto 0 auto; padding:0;}
    #home-section-3 .products .left {float:left; width:50%; border-right:1px solid rgba(216, 216, 216, 0.6); -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;}
    #home-section-3 .products .right {float:left; width:50%; border-left:1px solid rgba(216, 216, 216, 0.6); -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;}
    #home-section-3 .products .flexslider {max-width:300px; margin:0 auto; overflow:hidden;}
    
    #home-section-3 .products .slides {padding:0; margin:0; list-style:none;}
    #home-section-3 .products .slides li {display:block; padding:26px 0 17px 0;}
    #home-section-3 .products .slides li:last-child {padding-bottom:0;}
    #home-section-3 .products:after {content:""; display:table; clear:both;}

    #home-section-3 .products .slides li h3 {padding-bottom:8px;} 
    #home-section-3 .products .slides li img {max-width:none; width:100%; height:auto;}
    
    #home-section-3 .products .slides li span {display:block;}
    #home-section-3 .products .slides li > p {margin:0 auto; padding:3px 0 0 0; display:block; font-weight:500; font-size:16px; line-height:19px; letter-spacing:0.03px;}
    
    #home-section-3 .products .slides li .intensity {margin:-4px 0 0 0;}
    #home-section-3 .products .slides li .intensity span {display:inline-block; font-size:16px; line-height:19px; font-weight:600; vertical-align:middle;}
    #home-section-3 .products .slides li .intensity span.intensity-block {width:7px; height:7px; border:1px solid #ffffff; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; margin:0 5px 0 0;}
    #home-section-3 .products .slides li .intensity span.intensity-block.filled {background-color:#ffffff;}
    #home-section-3 .products .slides li .intensity span.intensity-number {padding:0 7px 0 2px;}
    #home-section-3 .products .slides li .intensity span:first-of-type {margin-right:8px;}
    
    #home-section-3 .products .slides li .price {font-size:20px; line-height:24px; padding:7px 0 12px 0;}
    
    #home-section-3 .home-content .mobile-stuff {display:none;}
    
    
    
    /***** Section 4 *****/
    #home-section-4 {background:#000000 url("https://www.nespresso.com/ecom/medias/sys_master/public/11969372323870/XM1mUAp.png") repeat-x left top;}
    #home-section-4:before {background:transparent url("https://www.nespresso.com/ecom/medias/sys_master/public/11969371996190/JXb8tZa.png") repeat-x left top; -webkit-background-size:auto 40px; background-size:auto 40px;} 
    
    #home-section-4 .products {max-width:996px; margin:0 auto; padding:26px 0 0 0;}
    #home-section-4 .products .slides {padding:0; margin:0; list-style:none;}
    #home-section-4 .products .slides li {float:left; width:29.52%; margin-right:5.72%;}
    #home-section-4 .products .slides li:nth-child(3n) {margin-right:0;}
    #home-section-4 .products:after {content:""; display:table; clear:both;}
    
    #home-section-4 .products .slides li img {max-width:none; width:100%; height:auto; display:block;}
    
    #home-section-4 .products .slides li span {display:block;}
    #home-section-4 .products .slides li > p {max-width:260px; margin:0 auto; padding:16px 0 13px 0; display:block; font-weight:500;}
    
    #home-section-4 .products .slides li .colors p {display:inline-block; vertical-align:middle; font-size:16px; letter-spacing:0.69px;}
    #home-section-4 .products .slides li .colors a {display:inline-block; vertical-align:middle; margin:0 6px 0 7px;}
    #home-section-4 .products .slides li .colors a span {display:block; width:20px; height:20px; border:2px solid #ffffff; border-radius:50%; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; -moz-transition:border 0.2s ease; -webkit-transition:border 0.2s ease; -o-transition:border 0.2s ease;}
    #home-section-4 .products .slides li .colors a:hover span, #home-section-2 .products .slides li .colors a.active span {border-width:5px;}
    #home-section-4 .products .slides li .colors a.active {cursor:default;}
    
    #home-section-4 .products .slides li .price {font-size:20px; line-height:24px; padding:7px 0 13px 0;}
    
    #home-section-4 .section-image {display:block; max-width:100%; height:auto; margin:0;}
    
    
    
    /***** Media Queries *****/
    @media screen and (max-width: 1380px){
        /***** General *****/
        .home-section:before {height:32px; -webkit-background-size:auto 32px !important; background-size:auto 32px !important}
        
        .home-content {padding:64px 15px 50px 15px;}
        .home-content h1 {font-size:34px; line-height:38px; letter-spacing:7px; padding-top:0;}
        .home-content h2 {font-size:30px; line-height:34px; letter-spacing:1px;}
        
        
        /***** Section 1 *****/
        #home-section-1 p {font-size:22px; line-height:28px; letter-spacing:0.6px;}
    }
    
    
    @media screen and (max-width: 1200px){
        /***** Section 1 *****/
        #home-section-1 p {font-size:20px; line-height:26px; letter-spacing:0.4px; max-width:860px;}
    }
    
    
    @media screen and (max-width: 960px){
        /***** Section 1 *****/
        #home-section-1 {-webkit-background-size:cover !important;background-size:cover !important; min-height:560px !important;}
        
        /***** Section 2 *****/
        #home-section-2 .products .slides li .colors p {display:block;}
        #home-section-2 .products .slides li .colors a {margin:0 5px 0 5px;}
        #home-section-2 .products .slides li .colors a span {width:16px; height:16px; border:1px solid #ffffff;}
        #home-section-2 .products .slides li .colors a:hover span, #home-section-2 .products .slides li .colors a.active span {border-width:4px;}
        
        .opt .OrderLinksBanner {display:none;}
    } 
    
    
    @media screen and (max-width: 767px){
        /***** General *****/
        .home-section {font-size:13px; line-height:18px; letter-spacing:0.02px; border-top:2px solid #ffffff;}
        .home-section:before {height:21px; -webkit-background-size:auto 21px !important; background-size:auto 21px !important}
        
        .home-content {padding:26px 15px 50px 15px;}
        .home-content h1 {font-size:20px; line-height:24px; letter-spacing:5.46px; padding:0; text-align:left;}
        .home-content h2 {font-size:20px; line-height:24px; letter-spacing:0.86px; padding-top:8px;}
        .home-content h3 {font-size:18px; line-height:22px; letter-spacing:0.78px; padding:0 0 10px 0;}
        .home-content p {padding:0; margin:0;} 
        
        .home-content .flexslider {padding:0; margin:0; border:none; -webkit-box-shadow:none; box-shadow:none; background-color:transparent;}
        .home-content .flexslider .flex-direction-nav a {text-align:left; text-indent:-9999px; width:7px; height:13px; -webkit-background-size:7px auto !important; background-size:7px auto !important; position:absolute; top:42px; left:auto; right:auto;}
        .home-content .flexslider .flex-direction-nav a:before {display:none;}
        .home-content .flexslider .flex-direction-nav a.flex-prev {left:15px; background:transparent url("https://www.nespresso.com/ecom/medias/sys_master/public/11969372684318/eCAHElJ.png") no-repeat left top;}
        .home-content .flexslider .flex-direction-nav a.flex-next {right:15px; background:transparent url("https://www.nespresso.com/ecom/medias/sys_master/public/11969372946462/R5K3wRa.png") no-repeat left top;}
        
        
        /***** Section 1 *****/
        #home-section-1 {min-height:414px !important; background:transparent url("https://www.nespresso.com/ecom/medias/sys_master/public/11969372815390/LnXPoP2.jpg") no-repeat center top; -webkit-background-size:cover !important; background-size:cover !important;}
        #home-section-1 h1 {max-width:260px; margin:0;}
        #home-section-1 p {font-size:16px; line-height:18px; letter-spacing:0.21px; max-width:none; text-align:left;}
        #home-section-1 .bottom-text {bottom:23px; padding:0 15px; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;}
        
        
        /***** Section 2 *****/
        #home-section-2 .home-content {padding-left:0; padding-right:0;}
        #home-section-2 .products {padding-top:18px;}
        #home-section-2 .products .slides li img {max-width:294px; margin:0 auto;}
        #home-section-2 .products .slides li > p {padding:15px 0 18px 0;}
        #home-section-2 .products .slides li .colors p {display:inline-block; font-size:14px; letter-spacing:0.6px;}
        #home-section-2 .products .slides li .colors a {margin:0 5px 0 7px;}
        #home-section-2 .products .slides li .colors a span {width:20px; height:20px;}
        #home-section-2 .products .slides li .price {padding:8px 0 10px 0;}
        
        
        /***** Section 3 *****/
        #home-section-3 .home-content {padding-left:0; padding-right:0;}
        #home-section-3 .home-content .mobile-stuff {display:block;}
        #home-section-3 .home-content .mobile-stuff .capsules-navigation {background-color:#f8f8f8; border-radius:22px; padding:0; margin:15px 18px 0 18px;}
        #home-section-3 .home-content .mobile-stuff .capsules-navigation:after {content:""; display:table; clear:both;}
        #home-section-3 .home-content .mobile-stuff .capsules-navigation a {display:block; float:left; width:50%; height:44px; line-height:44px; text-decoration:none; background-color:#fbfbfb; border-radius:22px; text-align:center; font-size:16px; font-weight:600; color:#B1B1B1; position:relative; z-index:1;}
        #home-section-3 .home-content .mobile-stuff .capsules-navigation a.active {color:#ffffff; background: #bb7b99; background: -webkit-linear-gradient(top, #bb7b99 0%,#914e6a 100%); background: -webkit-gradient(linear, left top, left bottom, from(#bb7b99), to(#914e6a)); background: -webkit-linear-gradient(top, #bb7b99 0%, #914e6a 100%); background: linear-gradient(to bottom, #bb7b99 0%,#914e6a 100%); filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#bb7b99', endColorstr='#914e6a',GradientType=0 ); -webkit-box-shadow: 0px 2px 4px 0px rgba(0, 0, 0, 0.5); box-shadow: 0px 2px 4px 0px rgba(0, 0, 0, 0.5); z-index:2;}
        
        #home-section-3 .home-content .mobile-stuff .capsules-navigation a:first-of-type {left:-3px;}
        #home-section-3 .home-content .mobile-stuff .capsules-navigation a:last-of-type {right:-3px;}                
                
        #home-section-3 .products {margin-top:0;}
        #home-section-3 .products .left {float:none; width:auto; border:none;}
        #home-section-3 .products .left h2 {display:none;}
        #home-section-3 .products .right {float:none; width:auto; border:none;}
        #home-section-3 .products .right h2 {display:none;}
        
        #home-section-3 .products .slides li h3 {padding-bottom:10px;}
        #home-section-3 .products .slides li img {max-width:260px; margin:0 auto;}
        #home-section-3 .products .slides li > p {font-size:13px; line-height:18px;}
        #home-section-3 .products .slides li .intensity {margin:11px 0 2px 0;}
        
        #home-section-3 .home-content .flexslider .flex-direction-nav {position:absolute; left:0; top:117px; width:100%; padding:0; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;}
        #home-section-3 .home-content .flexslider .flex-direction-nav a {top:50px;}
        #home-section-3 .home-content .flexslider .flex-direction-nav a.flex-prev {left:15px;}
        #home-section-3 .home-content .flexslider .flex-direction-nav a.flex-next {right:15px;}
        
        #home-section-3 .home-content .flexslider {width:240px; position:static;}
        #home-section-3 .home-content .flexslider .slides li {position:relative; -webkit-transform:scale(0.8); -ms-transform:scale(0.8); transform:scale(0.8); -moz-transition: all 0.2s ease; -webkit-transition: all 0.2s ease; -o-transition: all 0.2s ease; padding-bottom:0;}
        #home-section-3 .home-content .flexslider .slides li img {position:relative;}
        #home-section-3 .home-content .flexslider .slides li.prev img {right:-60px !important;}
        #home-section-3 .home-content .flexslider .slides li.next img {left:-60px !important;}
        #home-section-3 .home-content .flexslider .slides li.flex-active-slide {left:auto !important; right:auto !important; -webkit-transform:scale(1); -ms-transform:scale(1); transform:scale(1);}
        #home-section-3 .home-content .flexslider .slides li.flex-active-slide img {left:auto !important; right:auto !important;}
        #home-section-3 .home-content .flexslider .slides li:not(.flex-active-slide) {margin:0;}
        #home-section-3 .home-content .flexslider .slides li:not(.flex-active-slide) * {opacity:0;}
        #home-section-3 .home-content .flexslider .slides li:not(.flex-active-slide) img {opacity:0.32;} 
        #home-section-3 .home-content .flex-viewport {overflow: visible !important}
        
        
        /***** Section 4 *****/
        #home-section-4 .home-content {padding-left:0; padding-right:0;}
        #home-section-4 .products {padding-top:18px;}
        #home-section-4 .products .slides li img {max-width:294px; margin:0 auto;}
        #home-section-4 .products .slides li > p {padding:15px 0 13px 0;}
        #home-section-4 .products .slides li .colors p {display:inline-block; font-size:14px; letter-spacing:0.6px;}
        #home-section-4 .products .slides li .colors a {margin:0 5px 0 7px;}
        #home-section-4 .products .slides li .colors a span {width:20px; height:20px;}
        #home-section-4 .products .slides li .price {padding:0px 0 10px 0;}
    } 
    </style>
    `);


    function capsuleResize () {
        jQuery('#home-section-3 .products li > p').adjustHeight();
        jQuery('#home-section-3 .products h3').adjustHeight();
        jQuery('#home-section-2 .products li > p').adjustHeight();
        jQuery('#home-section-2 .products h3').adjustHeight();
        jQuery('#home-section-4 .products li > p').adjustHeight();
        jQuery('#home-section-4 .products h3').adjustHeight();
    }

    var currentWidth = jQuery(window).width();
    jQuery(window).resize(function () {
        capsuleResize();
        if (currentWidth <= 767) {
            if (jQuery(window).width() > 767) {
                window.location.reload();
            }
        } else if (currentWidth > 767) {
            if (jQuery(window).width() <= 767) {
                window.location.reload();
            }
        }
    });

    if(jQuery(window).width() > 767){
        jQuery("#home-section-1").css('min-height',jQuery(window).height() * 0.7);
        capsuleResize();
        jQuery(window).resize(function(){
            jQuery("#home-section-1").css('min-height',jQuery(window).height() * 0.7);    
        });    
    } else {
        
        /***** LOAD EXTERNAL SCRIPTS *****/
        function getScript(src, callback) {
            var s = document.createElement('script');
            s.src = src;
            s.async = true;
            s.onreadystatechange = s.onload = function() {
                if (!callback.done && (!s.readyState || /loaded|complete/.test(s.readyState))) {
                    callback.done = true;
                    callback();
                }
            };
            document.querySelector('head').appendChild(s);
        };
        
        
        /** Flexslider **/
        jQuery('head').append('<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/flexslider/2.7.1/flexslider.min.css" />')
        getScript("https://cdnjs.cloudflare.com/ajax/libs/flexslider/2.7.1/jquery.flexslider-min.js",start_flexslider);
        
        function start_flexslider(){
            jQuery('.flexslider').flexslider({
                directionNav: true,
                controlNav: false,
                animation: "slide",
                slideshow: false,
                start:function(slider){
                    jQuery(slider).find('.flex-active-slide').prev().addClass('prev');
                    jQuery(slider).find('.flex-active-slide').next().addClass('next');
                    jQuery("#home-section-3 .products .right").hide();
                },
                before:function(slider){
                    jQuery(slider).find('li').removeClass('prev next');
                },
                after:function(slider){
                    jQuery(slider).find('li').removeClass('prev next');
                    jQuery(slider).find('.flex-active-slide').prev().addClass('prev');
                    jQuery(slider).find('.flex-active-slide').next().addClass('next');
                    capsuleResize();
                }
            });
        };
    };
    startAddtoCart();
}

initLandingPage();