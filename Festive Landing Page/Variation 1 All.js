console.clear();
$ = jQuery.noConflict();
$('body').addClass('opt');


/**************************************
    LAYOUT
**************************************/ 
$(".opt #main-container #main").remove();
$('<div id="home-container" />').insertAfter($(".opt #main-container #top"));
$("#home-container").append('<div id="home-section-1" class="home-section"><div class="home-content" /></div>')
$("#home-container").append('<div id="home-section-2" class="home-section"><div class="home-content" /></div>')
$("#home-container").append('<div id="home-section-3" class="home-section"><div class="home-content" /></div>')
$("#home-container").append('<div id="home-section-4" class="home-section"><div class="home-content" /></div>')



/**************************************
    Data
**************************************/
data = {
    
    /***** Section 1 *****/
    section_1: {
        'title'   : "A french touch to your gift inspiration",
        'content' : "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.",
    },
    
    
    /***** Section 2 *****/
    section_2: {
        'title'    : "Machines",
        'products' : [
            {
                'name'   : 'Creatista Plus Matte White',
                'copy'   : 'Creatista Plus enables you to easily create an authentic top-quality Latte Art coffee at home.',
                'price'  : '$799.00',
                'colors' : [
                    {
                        'image' : 'https://i.imgur.com/ix9GOln.png',
                        'color' : '#000000',
                        'title' : 'Black',
                        'sku'   : '#sku1',
                        'link'  : 'https://www.nespresso.com/au/en/order/machines/original/creatista-plus-matte-white-coffee-machine'
                    },
                    {
                        'image' : 'https://i.imgur.com/ccLGWwJ.png',
                        'color' : '#cccccc',
                        'title' : 'Green',
                        'sku'   : '#sku2',
                        'link'  : '#link2'
                    },
                    {
                        'image' : 'https://i.imgur.com/ix9GOln.png',
                        'color' : '#191919',
                        'title' : 'Red',
                        'sku'   : '#sku3',
                        'link'  : '#link3'
                    },
                    {
                        'image' : 'https://i.imgur.com/ccLGWwJ.png',
                        'color' : '#b5afaa',
                        'title' : 'Yellow',
                        'sku'   : '#sku4',
                        'link'  : '#link4'
                    }
                ]
            },
            {
                'name'   : 'Pixie Electric',
                'copy'   : 'Nespresso, introduces the VertuoPlus with an all-new design and colours for the ultimate brewing experience.',
                'price'  : '$399.95',
                'colors' : [
                    {
                        'image' : 'https://i.imgur.com/ix9GOln.png',
                        'color' : '#000000',
                        'title' : 'Black',
                        'sku'   : '#sku5',
                        'link'  : '#link5'
                    },
                    {
                        'image' : 'https://i.imgur.com/ccLGWwJ.png',
                        'color' : '#cccccc',
                        'title' : 'Green',
                        'sku'   : '#sku6',
                        'link'  : '#link6'
                    },
                    {
                        'image' : 'https://i.imgur.com/ix9GOln.png',
                        'color' : '#191919',
                        'title' : 'Red',
                        'sku'   : '#sku7',
                        'link'  : '#link7'
                    },
                    {
                        'image' : 'https://i.imgur.com/ccLGWwJ.png',
                        'color' : '#b5afaa',
                        'title' : 'Yellow',
                        'sku'   : '#sku8',
                        'link'  : '#link8'
                    }
                ]
            },
            {
                'name'   : 'CitiZ&Milk',
                'copy'   : 'Nespresso, introduces the VertuoPlus with an all-new design and colours for the ultimate brewing experience.',
                'price'  : '$499.95',
                'colors' : [
                    {
                        'image' : 'https://i.imgur.com/ix9GOln.png',
                        'color' : '#000000',
                        'title' : 'Black',
                        'sku'   : '#sku9',
                        'link'  : '#link9'
                    },
                    {
                        'image' : 'https://i.imgur.com/ccLGWwJ.png',
                        'color' : '#cccccc',
                        'title' : 'Green',
                        'sku'   : '#sku10',
                        'link'  : '#link10'
                    },
                    {
                        'image' : 'https://i.imgur.com/ix9GOln.png',
                        'color' : '#191919',
                        'title' : 'Red',
                        'sku'   : '#sku11',
                        'link'  : '#link11'
                    },
                    {
                        'image' : 'https://i.imgur.com/ccLGWwJ.png',
                        'color' : '#b5afaa',
                        'title' : 'Yellow',
                        'sku'   : '#sku12',
                        'link'  : '#link12'
                    }
                ]
            }
        ]
    },
    
    
    /***** Section 3 *****/
    section_3: {
        'products' : [
            {
                'name'      : 'Variations Macaron',
                'copy'      : 'A harmonious combination of Livantoâ€™s cereal notes and a subtle and sweet almond flavour, emblematic of the macaron pastry.',
                'category'  : 'original',
                'image'     : 'https://i.imgur.com/BCgre2L.png',
                'intensity' : '6',
                'price'     : '$0.79',
                'sku'       : '#sku1',
                'link'      : '#link1'
            },
            {
                'name'      : 'Variations Macaron',
                'copy'      : 'A harmonious combination of Livantoâ€™s cereal notes and a subtle and sweet almond flavour, emblematic of the macaron pastry.',
                'category'  : 'original',
                'image'     : 'https://i.imgur.com/pYrDMc8.png',
                'intensity' : '7',
                'price'     : '$0.79',
                'sku'       : '#sku2',
                'link'      : '#link2'
            },
            {
                'name'      : 'Variations Macaron',
                'copy'      : 'A harmonious combination of Livantoâ€™s cereal notes and a subtle and sweet almond flavour, emblematic of the macaron pastry.',
                'category'  : 'original',
                'image'     : 'https://i.imgur.com/v3EdSmr.png',
                'intensity' : '8',
                'price'     : '$0.79',
                'sku'       : '#sku3',
                'link'      : '#link3'
            },
            {
                'name'      : 'Variations Macaron',
                'copy'      : 'A harmonious combination of Livantoâ€™s cereal notes and a subtle and sweet almond flavour, emblematic of the macaron pastry.',
                'category'  : 'vertuo',
                'image'     : 'https://i.imgur.com/HorLWCx.png',
                'intensity' : '9',
                'price'     : '$0.79',
                'sku'       : '#sku4',
                'link'      : '#link4'
            },
            {
                'name'      : 'Variations Macaron',
                'copy'      : 'A harmonious combination of Livantoâ€™s cereal notes and a subtle and sweet almond flavour, emblematic of the macaron pastry.',
                'category'  : 'vertuo',
                'image'     : 'https://i.imgur.com/s7cJ2fh.png',
                'intensity' : '10',
                'price'     : '$0.79',
                'sku'       : '#sku5',
                'link'      : '#link5'
            },
            {
                'name'      : 'Variations Macaron',
                'copy'      : 'A harmonious combination of Livantoâ€™s cereal notes and a subtle and sweet almond flavour, emblematic of the macaron pastry.',
                'category'  : 'vertuo',
                'image'     : 'https://i.imgur.com/X6LYykv.png',
                'intensity' : '11',
                'price'     : '$0.79',
                'sku'       : '#sku6',
                'link'      : '#link6'
            }
        ]
    },
    
    
    /***** Section 4 *****/
    section_4: {
        'title'    : "Accessories",
        'products' : [
            {
                'name'   : 'Barista',
                'copy'   : 'Nespresso, introduces the VertuoPlus with an all-new design and colours for the ultimate brewing expereience.',
                'image'  : 'https://i.imgur.com/5uryd8W.png',
                'price'  : '$65.99',
                'sku'    : '#sku1',
                'link'   : '#link1'
            },
            {
                'name'   : 'Travel Mug',
                'copy'   : 'Nespresso, introduces the VertuoPlus with an all-new design and colours for the ultimate brewing experience.',
                'image'  : 'https://i.imgur.com/BOQ1016.png',
                'price'  : '$75.99',
                'sku'    : '#sku2',
                'link'   : '#link2'
            },
            {
                'name'   : 'Barista',
                'copy'   : 'Nespresso, introduces the VertuoPlus with an all-new design and colours for the ultimate brewing experience.',
                'image'  : 'https://i.imgur.com/5uryd8W.png',
                'price'  : '$85.99',
                'sku'    : '#sku3',
                'link'   : '#link3'
            }
        ]
    }
};




/***** Section 1 Data *****/
$("#home-section-1 .home-content").append('<h1>'+data.section_1.title+'</h1>');
$("#home-section-1 .home-content").append('<div class="bottom-text"><p>'+data.section_1.content+'</p></div>');



/***** Section 2 Data *****/
$("#home-section-2 .home-content").append('<h2>'+data.section_2.title+'</h2>');
$("#home-section-2 .home-content").append('<div class="products flexslider"><ul class="slides" /></div>');

data.section_2.products.forEach(function(product, i){
    $("#home-section-2 .home-content .products .slides").append(`
        <li>
            <h3>`+product.name+`</h3>
            <div class="product-image"></div>
            <p>`+product.copy+`</p>
            <span class="colors">
                <p>Available Colours:</p>
            </span>
            <span class="price">`+product.price+`</span>
            <span class="buttons">
                <a href="#" class="button more">Discover more</a>
                <a href="#" class="button basket"><i aria-hidden="true" class="Glyph Glyph--basket MiniBasketButton__basketIcon"></i>Add to basket<i aria-hidden="true" class="Glyph Glyph--plus AddToBagButtonLarge__plusIcon"></i></a>
            </span>
        </li>
    `);   
    
    product.colors.forEach(function(color,j){
       $("#home-section-2 .home-content .products .slides li:last .product-image").append('<img src="'+color.image+'" />');
       $("#home-section-2 .home-content .products .slides li:last .colors").append('<a href="#" data-href="'+color.link+'" data-sku="'+color.sku+'" title="'+color.title+'"><span style="background-color:'+color.color+'"></span></a>');
    });   
    
    $("#home-section-2 .home-content .products .slides li .product-image img:first-of-type").addClass('active');
    $("#home-section-2 .home-content .products .slides li .colors a:first-of-type").addClass('active');
});

$("#home-section-2 .home-content .products .slides li").each(function(){
    $this = $(this);
    $this.find('a.button.more').attr('href',$this.find('.colors a:first-of-type').data('href'));
    $this.find('a.button.basket').attr('data-sku',$this.find('.colors a:first-of-type').data('sku'));
});

$("#home-section-2 .home-content .products .slides li .colors a").click(function(e){
    e.preventDefault();
    $this = $(this);
    $("#home-section-2 .home-content .products .slides li .colors a").removeClass('active');
    $this.addClass('active');
    $this.parent().parent().find('a.button.more').attr('href',$this.data('href'))
    $this.parent().parent().find('a.button.basket').attr('data-sku',$this.data('sku'))
    
    $this.parent().parent().find('.product-image').animate({opacity:0},200,function(){
        $this.parent().parent().find('.product-image img').removeClass('active');
        $this.parent().parent().find('.product-image img:nth-child('+$this.index()+')').addClass('active');
        $this.parent().parent().find('.product-image').animate({opacity:1},200);    
    });
});



/***** Section 3 Data *****/
$("#home-section-3 .home-content").append('<div class="products"><div id="original-range" class="left"><h2>Original Capsules</h2><div class="flexslider"><ul class="slides" /></div></div><div id="vertuo-range" class="right"><h2>Vertuo Capsules</h2><div class="flexslider"><ul class="slides" /></div></div></div>');

data.section_3.products.forEach(function(product, i){
    if(product.category == 'original'){
        $("#home-section-3 .home-content .left .slides").append(`
            <li>
                <h3>`+product.name+`</h3>
                <img src="`+product.image+`" />
                <span class="intensity"><span>Intensity</span></span>
                <p>`+product.copy+`</p>
                <span class="price">`+product.price+`</span>
                <span class="buttons">
                    <a href="`+product.link+`" class="button more">Discover more</a>
                    <a href="#" class="button basket" data-sku="`+product.sku+`"><i aria-hidden="true" class="Glyph Glyph--basket MiniBasketButton__basketIcon"></i>Add to basket<i aria-hidden="true" class="Glyph Glyph--plus AddToBagButtonLarge__plusIcon"></i></a>
                </span>
            </li>
        `)
        
        for (j = 1; j <= product.intensity; j++) {
            $("#home-section-3 .home-content .left .slides li:last .intensity").append('<span class="intensity-block filled" />');
        };
        $("#home-section-3 .home-content .left .slides li:last .intensity").append('<span class="intensity-number">'+product.intensity+'</span>')
        for (k = product.intensity; k < 12; k++) {
            $("#home-section-3 .home-content .left .slides li:last .intensity").append('<span class="intensity-block" />');
        };
    } else {
        $("#home-section-3 .home-content .right .slides").append(`
            <li>
                <h3>`+product.name+`</h3>
                <img src="`+product.image+`" />
                <span class="intensity"></span>
                <p>`+product.copy+`</p>
                <span class="price">`+product.price+`</span>
                <span class="buttons">
                    <a href="`+product.link+`" class="button more">Discover more</a>
                    <a href="#" class="button basket" data-sku="`+product.sku+`"><i aria-hidden="true" class="Glyph Glyph--basket MiniBasketButton__basketIcon"></i>Add to basket<i aria-hidden="true" class="Glyph Glyph--plus AddToBagButtonLarge__plusIcon"></i></a>
                </span>
            </li>
        `)
        
        for (j = 1; j <= product.intensity; j++) {
            $("#home-section-3 .home-content .right .slides li:last .intensity").append('<span class="intensity-block filled" />');
        };
        $("#home-section-3 .home-content .right .slides li:last .intensity").append('<span class="intensity-number">'+product.intensity+'</span>')
        for (k = product.intensity; k < 12; k++) {
            $("#home-section-3 .home-content .right .slides li:last .intensity").append('<span class="intensity-block" />');
        };
    };
});

$("#home-section-3 .home-content").prepend('<div class="mobile-stuff" />');
$("#home-section-3 .mobile-stuff").append('<h2>Capsules</h2>');
$("#home-section-3 .mobile-stuff").append('<div class="capsules-navigation"><a href="#original-range" class="active">Original Range</a><a href="#vertuo-range">Vertuo Range</a></div>');


$("body").on('click','.capsules-navigation a',function(e){
    e.preventDefault();
    $this = $(this);
    $href = $this.attr('href');
    $(".capsules-navigation a").removeClass('active');
    $this.addClass('active');
    $("#home-section-3 .products > div").hide();
    $($href).css('display','block');
});



/***** Section 4 Data *****/
$("#home-section-4 .home-content").append('<h2>'+data.section_4.title+'</h2>');
$("#home-section-4 .home-content").append('<div class="products flexslider"><ul class="slides" /></div>');

data.section_4.products.forEach(function(product, i){
    $("#home-section-4 .home-content .products .slides").append(`
        <li>
            <h3>`+product.name+`</h3>
            <img src="`+product.image+`" />
            <p>`+product.copy+`</p>
            <span class="price">`+product.price+`</span>
            <span class="buttons">
                <a href="`+product.link+`" class="button more">Discover more</a>
                <a href="#" class="button basket" data-sku="`+product.sku+`"><i aria-hidden="true" class="Glyph Glyph--basket MiniBasketButton__basketIcon"></i>Add to basket<i aria-hidden="true" class="Glyph Glyph--plus AddToBagButtonLarge__plusIcon"></i></a>
            </span>
        </li>
    `);   
});

$("#home-section-4").append('<img class="section-image" src="https://i.imgur.com/cIu0R9w.jpg" />');




/**************************************
    FONTS
**************************************/
$('head').append(`
<style type="text/css">
    @font-face {
        font-family: "NespressoLucas";
        src: url("https://www.nespresso.com/shared_res/mos/free_html/br/inbetween/fonts/NespressoLucas-Regular.woff2") format("woff2"), url("https://www.nespresso.com/shared_res/mos/free_html/br/inbetween/fonts/NespressoLucas-Regular.woff") format("woff"), url("https://www.nespresso.com/shared_res/mos/free_html/br/inbetween/fonts/NespressoLucas-Regular.ttf") format("truetype");
        font-style: normal;
        font-weight: 400
    }
    @font-face {
       font-family: "NespressoLucas";
       src: url("https://www.nespresso.com/shared_res/agility/commons/fonts/NespressoLucas-Medium.woff") format("woff");
       font-style: normal;
       font-weight: 500
    }
    @font-face {
        font-family: "NespressoLucas";
        src: url("https://www.nespresso.com/shared_res/mos/free_html/br/inbetween/fonts/NespressoLucas-SemiBd.woff2") format("woff2"), url("https://www.nespresso.com/shared_res/mos/free_html/br/inbetween/fonts/NespressoLucas-SemiBd.woff") format("woff"), url("https://www.nespresso.com/shared_res/mos/free_html/br/inbetween/fonts/NespressoLucas-SemiBd.ttf") format("truetype");
        font-style: normal;
        font-weight: 600
    }
    @font-face {
        font-family: "NespressoLucas";
        src: url("https://www.nespresso.com/shared_res/mos/free_html/br/inbetween/fonts/NespressoLucas-Bold.woff2") format("woff2"), url("https://www.nespresso.com/shared_res/mos/free_html/br/inbetween/fonts/NespressoLucas-Bold.woff") format("woff"), url("https://www.nespresso.com/shared_res/mos/free_html/br/inbetween/fonts/NespressoLucas-Bold.ttf") format("truetype");
        font-style: normal;
        font-weight: 700
    }
</style>
`);



/**************************************
    STYLES
**************************************/
$('head').append(`
<style type="text/css">
    .opt .HeaderNavigationBar__nav {margin-bottom:0;}
    
    
    /***** General *****/
    .home-section {text-align:center; color:#ffffff; font-size:15px; line-height:19px; letter-spacing:0.03px; font-family:NespressoLucas; font-weight:600; position:relative; border-top:5px solid #ffffff; max-width:none; margin:0 auto;}
    .home-section:before {display:block; content:""; position:absolute; left:0; top:0; width:100%; height:40px;}
    
    .home-content {padding:72px 15px 50px 15px; max-width:996px; margin:0 auto;}
    .home-content h1 {text-transform:uppercase; color:#ffffff; font-size:38px; line-height:42px; letter-spacing:11.52px; font-family:NespressoLucas; font-weight:700; padding:5px 0 0 0; margin:0;}
    .home-content h2 {text-transform:uppercase; color:#ffffff; font-size:34px; line-height:38px; letter-spacing:1.47px; font-family:NespressoLucas; font-weight:700; padding:0; margin:0;}
    .home-content h3 {color:#ffffff; font-size:21px; line-height:27px; letter-spacing:0.91px; font-family:NespressoLucas; font-weight:600; padding:0 0 15px 0; margin:0;}
    .home-content p {padding:0; margin:0;} 
    
    .home-content .products .slides li .buttons {max-width:212px; margin:0 auto;}
    .home-content .products .slides li .buttons a {display:block; font-weight:normal; font-size:14px; line-height:20px; letter-spacing:0; text-decoration:none; text-transform:uppercase; border-radius:3px; background-color:#b16183; margin:0 0 10px 0; padding:8px 5px 7px 5px; position:relative; -moz-transition:background 0.2s ease; -webkit-transition:background 0.2s ease; -o-transition:background 0.2s ease;}
    .home-content .products .slides li .buttons a:hover {background-color:#893d5f;}
    .home-content .products .slides li .buttons a.basket {background-color:#518426;}
    .home-content .products .slides li .buttons a:hover.basket {background-color:#2a5e00;}
    .home-content .products .slides li .buttons a i.Glyph--basket {position:absolute; left:5px; top:50%; font-size:24px; margin-top:-12px;}
    .home-content .products .slides li .buttons a i.Glyph--plus {position:absolute; right:10px; top:50%; font-size:14px; margin-top:-7px;}
    
    
    
    /***** Section 1 *****/
    #home-section-1 {background:#000000 url("https://i.imgur.com/C9Zh8Bg.jpg") no-repeat center bottom; min-height:870px; background-size:100% auto;}
    #home-section-1:before {background:transparent url("https://i.imgur.com/mCk6SaE.png") repeat-x left top; background-size:auto 40px;} 
    
    #home-section-1 h1 {max-width:780px; margin:0 auto;}
    #home-section-1 p {font-size:26px; line-height:32px; letter-spacing:0.84px; font-family:NespressoLucas; font-weight:400; max-width:996px; margin:0 auto; display:block;}
    #home-section-1 .bottom-text {position:absolute; left:0; bottom:22px; width:100%;}
    
    
    
    /***** Section 2 *****/
    #home-section-2 {background:#000000 url("https://i.imgur.com/CM8MgRi.png") repeat-x left top;}
    #home-section-2:before {background:transparent url("https://i.imgur.com/HpB7yzf.png") repeat-x left top; background-size:auto 40px;} 
    
    #home-section-2 .products {max-width:996px; margin:0 auto; padding:26px 0 0 0;}
    #home-section-2 .products .slides {padding:0; margin:0; list-style:none;}
    #home-section-2 .products .slides li {float:left; width:29.52%; margin-right:5.72%;}
    #home-section-2 .products .slides li:nth-child(3n) {margin-right:0;}
    #home-section-2 .products:after {content:""; display:table; clear:both;}
    
    #home-section-2 .products .slides li img {max-width:none; width:100%; height:auto; display:none;}
    #home-section-2 .products .slides li img.active {display:block;}
    
    #home-section-2 .products .slides li span {display:block;}
    #home-section-2 .products .slides li > p {max-width:260px; margin:0 auto; padding:16px 0 13px 0; display:block; font-weight:500;}
    
    #home-section-2 .products .slides li .colors p {display:inline-block; vertical-align:middle; font-size:16px; letter-spacing:0.69px;}
    #home-section-2 .products .slides li .colors a {display:inline-block; vertical-align:middle; margin:0 6px 0 7px;}
    #home-section-2 .products .slides li .colors a span {display:block; width:20px; height:20px; border:2px solid #ffffff; border-radius:50%; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; -moz-transition:border 0.2s ease; -webkit-transition:border 0.2s ease; -o-transition:border 0.2s ease;}
    #home-section-2 .products .slides li .colors a:hover span, #home-section-2 .products .slides li .colors a.active span {border-width:5px;}
    #home-section-2 .products .slides li .colors a.active {cursor:default;}
    
    #home-section-2 .products .slides li .price {font-size:20px; line-height:24px; padding:10px 0 14px 0;}
    
    
    
    /***** Section 3 *****/
    #home-section-3 {background:#000000 url("https://i.imgur.com/JefVQ15.png") repeat-x left top;}
    #home-section-3:before {background:transparent url("https://i.imgur.com/mCk6SaE.png") repeat-x left top; background-size:auto 40px;} 
    
    #home-section-3 .products {max-width:996px; margin:10px auto 0 auto; padding:0;}
    #home-section-3 .products .left {float:left; width:50%; border-right:1px solid rgba(216, 216, 216, 0.6); -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;}
    #home-section-3 .products .right {float:left; width:50%; border-left:1px solid rgba(216, 216, 216, 0.6); -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;}
    #home-section-3 .products .flexslider {max-width:300px; margin:0 auto;}
    
    #home-section-3 .products .slides {padding:0; margin:0; list-style:none;}
    #home-section-3 .products .slides li {display:block; padding:26px 0 17px 0;}
    #home-section-3 .products .slides li:last-child {padding-bottom:0;}
    #home-section-3 .products:after {content:""; display:table; clear:both;}
   
    #home-section-3 .products .slides li h3 {padding-bottom:8px;} 
    #home-section-3 .products .slides li img {max-width:none; width:100%; height:auto;}
    
    #home-section-3 .products .slides li span {display:block;}
    #home-section-3 .products .slides li > p {margin:0 auto; padding:3px 0 0 0; display:block; font-weight:500; font-size:16px; line-height:19px; letter-spacing:0.03px;}
    
    #home-section-3 .products .slides li .intensity {margin:-4px 0 0 0;}
    #home-section-3 .products .slides li .intensity span {display:inline-block; font-size:16px; line-height:19px; font-weight:600; vertical-align:middle;}
    #home-section-3 .products .slides li .intensity span.intensity-block {width:7px; height:7px; border:1px solid #ffffff; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; margin:0 5px 0 0;}
    #home-section-3 .products .slides li .intensity span.intensity-block.filled {background-color:#ffffff;}
    #home-section-3 .products .slides li .intensity span.intensity-number {padding:0 7px 0 2px;}
    #home-section-3 .products .slides li .intensity span:first-of-type {margin-right:8px;}
    
    #home-section-3 .products .slides li .price {font-size:20px; line-height:24px; padding:7px 0 12px 0;}
    
    #home-section-3 .home-content .mobile-stuff {display:none;}
    
    
    
    /***** Section 4 *****/
    #home-section-4 {background:#000000 url("https://i.imgur.com/XM1mUAp.png") repeat-x left top;}
    #home-section-4:before {background:transparent url("https://i.imgur.com/JXb8tZa.png") repeat-x left top; background-size:auto 40px;} 
    
    #home-section-4 .products {max-width:996px; margin:0 auto; padding:26px 0 0 0;}
    #home-section-4 .products .slides {padding:0; margin:0; list-style:none;}
    #home-section-4 .products .slides li {float:left; width:29.52%; margin-right:5.72%;}
    #home-section-4 .products .slides li:nth-child(3n) {margin-right:0;}
    #home-section-4 .products:after {content:""; display:table; clear:both;}
    
    #home-section-4 .products .slides li img {max-width:none; width:100%; height:auto; display:block;}
    
    #home-section-4 .products .slides li span {display:block;}
    #home-section-4 .products .slides li > p {max-width:260px; margin:0 auto; padding:16px 0 13px 0; display:block; font-weight:500;}
    
    #home-section-4 .products .slides li .colors p {display:inline-block; vertical-align:middle; font-size:16px; letter-spacing:0.69px;}
    #home-section-4 .products .slides li .colors a {display:inline-block; vertical-align:middle; margin:0 6px 0 7px;}
    #home-section-4 .products .slides li .colors a span {display:block; width:20px; height:20px; border:2px solid #ffffff; border-radius:50%; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; -moz-transition:border 0.2s ease; -webkit-transition:border 0.2s ease; -o-transition:border 0.2s ease;}
    #home-section-4 .products .slides li .colors a:hover span, #home-section-2 .products .slides li .colors a.active span {border-width:5px;}
    #home-section-4 .products .slides li .colors a.active {cursor:default;}
    
    #home-section-4 .products .slides li .price {font-size:20px; line-height:24px; padding:7px 0 13px 0;}
    
    #home-section-4 .section-image {display:block; max-width:100%; height:auto; margin:0;}
    
    
    
    /***** Media Queries *****/
    @media screen and (max-width: 1380px){
        /***** General *****/
        .home-section:before {height:32px; background-size:auto 32px !important}
        
        .home-content {padding:64px 15px 50px 15px;}
        .home-content h1 {font-size:34px; line-height:38px; letter-spacing:7px; padding-top:0;}
        .home-content h2 {font-size:30px; line-height:34px; letter-spacing:1px;}
        
        
        /***** Section 1 *****/
        #home-section-1 p {font-size:22px; line-height:28px; letter-spacing:0.6px;}
    }
    
    
    @media screen and (max-width: 1200px){
        /***** Section 1 *****/
        #home-section-1 p {font-size:20px; line-height:26px; letter-spacing:0.4px; max-width:860px;}
    }
    
    
    @media screen and (max-width: 960px){
        /***** Section 1 *****/
        #home-section-1 {background-size:cover !important; min-height:560px !important;}
        
        /***** Section 2 *****/
        #home-section-2 .products .slides li .colors p {display:block;}
        #home-section-2 .products .slides li .colors a {margin:0 5px 0 5px;}
        #home-section-2 .products .slides li .colors a span {width:16px; height:16px; border:1px solid #ffffff;}
        #home-section-2 .products .slides li .colors a:hover span, #home-section-2 .products .slides li .colors a.active span {border-width:4px;}
        
        .opt .OrderLinksBanner {display:none;}
    } 
    
    
    @media screen and (max-width: 767px){
        /***** General *****/
        .home-section {font-size:13px; line-height:18px; letter-spacing:0.02px; border-top:2px solid #ffffff;}
        .home-section:before {height:21px; background-size:auto 21px !important}
        
        .home-content {padding:26px 15px 50px 15px;}
        .home-content h1 {font-size:20px; line-height:24px; letter-spacing:5.46px; padding:0; text-align:left;}
        .home-content h2 {font-size:20px; line-height:24px; letter-spacing:0.86px; padding-top:8px;}
        .home-content h3 {font-size:18px; line-height:22px; letter-spacing:0.78px; padding:0 0 10px 0;}
        .home-content p {padding:0; margin:0;} 
        
        .home-content .flexslider {padding:0; margin:0; border:none; box-shadow:none; background-color:transparent;}
        .home-content .flexslider .flex-direction-nav a {text-align:left; text-indent:-9999px; width:7px; height:13px; background-size:7px auto !important; position:absolute; top:42px; left:auto; right:auto;}
        .home-content .flexslider .flex-direction-nav a:before {display:none;}
        .home-content .flexslider .flex-direction-nav a.flex-prev {left:15px; background:transparent url("https://i.imgur.com/eCAHElJ.png") no-repeat left top;}
        .home-content .flexslider .flex-direction-nav a.flex-next {right:15px; background:transparent url("https://i.imgur.com/R5K3wRa.png") no-repeat left top;}
        
        
        /***** Section 1 *****/
        #home-section-1 {min-height:414px !important; background:transparent url("https://i.imgur.com/LnXPoP2.jpg") no-repeat center top; background-size:cover !important;}
        #home-section-1 h1 {max-width:260px; margin:0;}
        #home-section-1 p {font-size:16px; line-height:18px; letter-spacing:0.21px; max-width:none; text-align:left;}
        #home-section-1 .bottom-text {bottom:23px; padding:0 15px; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;}
        
        
        /***** Section 2 *****/
        #home-section-2 .home-content {padding-left:0; padding-right:0;}
        #home-section-2 .products {padding-top:18px;}
        #home-section-2 .products .slides li img {max-width:294px; margin:0 auto;}
        #home-section-2 .products .slides li > p {padding:15px 0 18px 0;}
        #home-section-2 .products .slides li .colors p {display:inline-block; font-size:14px; letter-spacing:0.6px;}
        #home-section-2 .products .slides li .colors a {margin:0 5px 0 7px;}
        #home-section-2 .products .slides li .colors a span {width:20px; height:20px;}
        #home-section-2 .products .slides li .price {padding:8px 0 10px 0;}
        
        
        /***** Section 3 *****/
        #home-section-3 .home-content {padding-left:0; padding-right:0;}
        #home-section-3 .home-content .mobile-stuff {display:block;}
        #home-section-3 .home-content .mobile-stuff .capsules-navigation {background-color:#f8f8f8; border-radius:22px; padding:0; margin:15px 18px 0 18px;}
        #home-section-3 .home-content .mobile-stuff .capsules-navigation:after {content:""; display:table; clear:both;}
        #home-section-3 .home-content .mobile-stuff .capsules-navigation a {display:block; float:left; width:50%; height:44px; line-height:44px; text-decoration:none; background-color:#fbfbfb; border-radius:22px; text-align:center; font-size:16px; font-weight:600; color:#B1B1B1; position:relative; z-index:1;}
        #home-section-3 .home-content .mobile-stuff .capsules-navigation a.active {color:#ffffff; background: #bb7b99; background: -moz-linear-gradient(top, #bb7b99 0%, #914e6a 100%); background: -webkit-linear-gradient(top, #bb7b99 0%,#914e6a 100%); background: linear-gradient(to bottom, #bb7b99 0%,#914e6a 100%); filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#bb7b99', endColorstr='#914e6a',GradientType=0 ); -webkit-box-shadow: 0px 2px 4px 0px rgba(0, 0, 0, 0.5); -moz-box-shadow: 0px 2px 4px 0px rgba(0, 0, 0, 0.5); box-shadow: 0px 2px 4px 0px rgba(0, 0, 0, 0.5); z-index:2;}
        
        #home-section-3 .home-content .mobile-stuff .capsules-navigation a:first-of-type {left:-3px;}
        #home-section-3 .home-content .mobile-stuff .capsules-navigation a:last-of-type {right:-3px;}                
                
        #home-section-3 .products {margin-top:0;}
        #home-section-3 .products .left {float:none; width:auto; border:none;}
        #home-section-3 .products .left h2 {display:none;}
        #home-section-3 .products .right {float:none; width:auto; border:none;}
        #home-section-3 .products .right h2 {display:none;}
        
        #home-section-3 .products .slides li h3 {padding-bottom:10px;}
        #home-section-3 .products .slides li img {max-width:260px; margin:0 auto;}
        #home-section-3 .products .slides li > p {font-size:13px; line-height:18px;}
        #home-section-3 .products .slides li .intensity {margin:11px 0 2px 0;}
        
        #home-section-3 .home-content .flexslider .flex-direction-nav {position:absolute; left:0; top:117px; width:100%; padding:0; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;}
        #home-section-3 .home-content .flexslider .flex-direction-nav a {top:50px;}
        #home-section-3 .home-content .flexslider .flex-direction-nav a.flex-prev {left:15px;}
        #home-section-3 .home-content .flexslider .flex-direction-nav a.flex-next {right:15px;}
        
        #home-section-3 .home-content .flexslider {width:240px; position:static;}
        #home-section-3 .home-content .flexslider .slides li {position:relative; -webkit-transform:scale(0.8); -moz-transform:scale(0.8); -ms-transform:scale(0.8); -o-transform:scale(0.8); transform:scale(0.8); -moz-transition: all 0.2s ease; -webkit-transition: all 0.2s ease; -o-transition: all 0.2s ease; padding-bottom:0;}
        #home-section-3 .home-content .flexslider .slides li img {position:relative;}
        #home-section-3 .home-content .flexslider .slides li.prev img {right:-60px !important;}
        #home-section-3 .home-content .flexslider .slides li.next img {left:-60px !important;}
        #home-section-3 .home-content .flexslider .slides li.flex-active-slide {left:auto !important; right:auto !important; -webkit-transform:scale(1); -moz-transform:scale(1); -ms-transform:scale(1); -o-transform:scale(1); transform:scale(1);}
        #home-section-3 .home-content .flexslider .slides li.flex-active-slide img {left:auto !important; right:auto !important;}
        #home-section-3 .home-content .flexslider .slides li:not(.flex-active-slide) {margin:0;}
        #home-section-3 .home-content .flexslider .slides li:not(.flex-active-slide) * {opacity:0;}
        #home-section-3 .home-content .flexslider .slides li:not(.flex-active-slide) img {opacity:0.32;} 
        #home-section-3 .home-content .flex-viewport {overflow: visible !important}
        
        
        /***** Section 4 *****/
        #home-section-4 .home-content {padding-left:0; padding-right:0;}
        #home-section-4 .products {padding-top:18px;}
        #home-section-4 .products .slides li img {max-width:294px; margin:0 auto;}
        #home-section-4 .products .slides li > p {padding:15px 0 13px 0;}
        #home-section-4 .products .slides li .colors p {display:inline-block; font-size:14px; letter-spacing:0.6px;}
        #home-section-4 .products .slides li .colors a {margin:0 5px 0 7px;}
        #home-section-4 .products .slides li .colors a span {width:20px; height:20px;}
        #home-section-4 .products .slides li .price {padding:0px 0 10px 0;}
    } 
</style>
`);




if($(window).width() > 767){
    $("#home-section-1").css('min-height',$(window).height() * 0.7);
    $(window).resize(function(){
        $("#home-section-1").css('min-height',$(window).height() * 0.7);    
    });    
} else {
    
    /***** LOAD EXTERNAL SCRIPTS *****/
    function getScript(src, callback) {
        var s = document.createElement('script');
        s.src = src;
        s.async = true;
        s.onreadystatechange = s.onload = function() {
            if (!callback.done && (!s.readyState || /loaded|complete/.test(s.readyState))) {
                callback.done = true;
                callback();
            }
        };
        document.querySelector('head').appendChild(s);
    };
    
    
    /** Flexslider **/
    $('head').append('<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/flexslider/2.7.1/flexslider.min.css" />')
    getScript("https://cdnjs.cloudflare.com/ajax/libs/flexslider/2.7.1/jquery.flexslider-min.js",start_flexslider);
    
    function start_flexslider(){
        $('.flexslider').flexslider({
            directionNav: true,
            controlNav: false,
            animation: "slide",
            slideshow: false,
            start:function(slider){
                $(slider).find('.flex-active-slide').prev().addClass('prev');
                $(slider).find('.flex-active-slide').next().addClass('next');
                $("#home-section-3 .products .right").hide();
            },
            before:function(slider){
                $(slider).find('li').removeClass('prev next');
            },
            after:function(slider){
                $(slider).find('li').removeClass('prev next');
                $(slider).find('.flex-active-slide').prev().addClass('prev');
                $(slider).find('.flex-active-slide').next().addClass('next');
            }
        });
    };
};