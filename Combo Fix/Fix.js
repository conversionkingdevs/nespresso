function defer(method, selector) {
    //console.log(count);
    if (count >= 50) {
        //console.log('Finished');
    } else if (window.jQuery) {
        if (jQuery(selector).length > 0 && count < 50){
            method();
        } else {
            count++
            setTimeout(function() { defer(method, selector); }, 50);
        }  
    } else {
         setTimeout(function() { defer(method, selector); }, 50);
    }    
}

//console.log('Running bundle Fix');
  var newname = "Nespresso On Ice Limited Edition Bundle",
    newprice = "$49.50",
    oldname = "Festive Touch Travel Mug Set",
    oldprice = "$50.00",
    sku = "103279",
    count = 0;

function homepage() {
  // Homepage
  // Change main tile
  jQuery('#prod-push-'+sku).find('.prod-info').html(newname+"<br><em>"+newprice+"<em>"); 
  // Change popup
  jQuery('#prod-push-'+sku).click(function () {
      setTimeout(function () {
          if (jQuery('#product-popin .track-product-click').attr('data-product-code') == sku){
              jQuery('#product-popin .prod-name').text(newname);
              jQuery('#product-popin .priceValue').text(newprice);
          }
      },5);
  });
  }

defer(homepage, '#prod-push-'+sku);

function catalogPage () {
  // Catalog Page
  //console.log('Running catalog')
  var mainTitle = jQuery('#ta-add_plus_button[data-product-id="'+sku+'"], #ta-add-to-cart-'+sku+'[data-product-id="'+sku+'"]').closest('.product-title');
  
        mainTitle.find('.title').find('h2, span').text(newname);
        mainTitle.find('.price strong').text(newprice);

}

function sidebar () {
var sideBar = jQuery('.products-list .product-item[data-product-code="'+sku+'"]');

        sideBar.find('.title a').text(newname);
        sideBar.find('.price span').text(newprice);
}

function sidebarcode () {
    setTimeout(function () {
        sidebar()
        sidebarupdate();
    },10);
}

function sidebarupdate () {
      jQuery('form[name="filterForm"] div').change(function () {
        sidebarcode();
    });
        jQuery('.reset-filters').click(function () {
            sidebarcode();
        });
}
function ajax() {
    jQuery(window).ajaxComplete(function () {
        setTimeout(function () {
            count = 0;
            defer(catalogPage, '#ta-add_plus_button[data-product-id="'+sku+'"], #ta-add-to-cart-'+sku+'[data-product-id="'+sku+'"]')
            defer(sidebar, '.products-list .product-item[data-product-code="'+sku+'"]')
            defer(suggestionlink, '.prod-suggest-link[data-product-code="'+sku+'"]')
            defer(sidebarupdate, 'form[name="filterForm"] div')
        },50);
    });
}

defer(ajax, 'body')



function suggestionlink () {
  var suggestionlink = jQuery('.prod-suggest-link[data-product-code="'+sku+'"]');

      suggestionlink.find('.suggestion-info-title').text(newname);
      suggestionlink.find('.suggestion-price .price').text(newprice);
}

defer(catalogPage, '#ta-add_plus_button[data-product-id="'+sku+'"],#ta-add-to-cart-'+sku+'[data-product-id="'+sku+'"]')
defer(sidebar, '.products-list .product-item[data-product-code="'+sku+'"]')
defer(suggestionlink, '.prod-suggest-link[data-product-code="'+sku+'"]')
defer(sidebarupdate, 'form[name="filterForm"] div')

var url = document.location.href

function mobileCatItem() {
  // Mobile Catalog
  var mobileCatItem = jQuery('.list-items.products #li-'+sku);

      mobileCatItem.find('.product-title').text(newname);
      mobileCatItem.find('.exact-price').text(newprice);

}

defer(mobileCatItem, '.list-items.products #li-'+sku)

function Mobilepdp () {
  // Mobile PDP
  var mobilePDP = jQuery('.lbt-product[data-product-code="'+sku+'"]');

      mobilePDP.find('#product-view-title h1').text(newname);
      mobilePDP.find('.exact-price').text(newprice);
    }

defer(Mobilepdp, '.lbt-product[data-product-code="'+sku+'"] #product-view-title h1')

function mobileAjax() {
    jQuery(window).ajaxComplete(function () {
        console.log('running mobile ajaxcomplete')
        setTimeout(function () {
            defer(mobileCatItem, '.list-items.products #li-'+sku)
            defer(Mobilepdp, '.lbt-product[data-product-code="'+sku+'"]')
            defer(mobileCartBanner, '#basket #promotionBanners img');
        }, 50)
    })
};

if (url.indexOf('/mobile/') >= 0 ) {
    console.log('attaching mobile defer')
    defer(mobileAjax, 'body')
} 

function mobileCartBanner () {
    if (jQuery('#listviewid button[data-product-code="'+sku+'"]').length >= 1) {
		jQuery('#basket #promotionBanners img').attr('src', '');
        jQuery('#basket #promotionBanners img').attr('src', '//service.maxymiser.net/cm/images-eu/1/1/1/CBC27B72641B2C8925AF79AE53348B102FD1151622BC47199F73D19E74F4A22E/nespresso-au/Bundle-Fix/Offer-2_Iced_Basket-Banner_640x200.png');
    }
}

defer(mobileCartBanner, '#basket #promotionBanners img');