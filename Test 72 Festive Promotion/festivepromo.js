function defer(method, selector) {
    if (window.jQuery) {
        if (jQuery(selector).length > 0){
            method();
        } else {
            setTimeout(function() { defer(method, selector) }, 50);
        }  
    } else {
         setTimeout(function() { defer(method, selector) }, 50);
    }    
}

var _pathname = window.location.pathname;

function isMobile(){
    if(_pathname.indexOf("mobile") >= 0){
        return true;
    } else {
        return false;
    }
}

function getCountryCode(){
    var pa = _pathname.split("/");
    if(isMobile()){
        return pa[2];
    } else {
        return pa[1];
    }
}


var elementsToRemove = [
    {
        "attr": "id",
        "value": "li-10681"
    },
    {
        "attr": "id",
        "value": "li-10685"
    },
    {
        "attr": "id",
        "value": "li-102851"
    },
    {
        "attr": "data-product-id",
        "value": "10681"
    },
    {
        "attr": "data-product-id",
        "value": "10685" 
    },
    {
        "attr": "data-product-id",
        "value": "102851" 
    }
];


function reverseElements(parentElement){
    var children = jQuery(parentElement).children();
    for(var i = children.length;  i >= 0 ; i--){
        jQuery(parentElement).append(jQuery(children[i]));
    }
}

function start(){
    jQuery("body").addClass("opt-72");
    removeElements();
    if(getCountryCode() == "au"){
        reverseElements("#a11y-product-list .products-group:eq(1) ul");
    }
}

function removeElements(){
    console.log("running");
    for(var i = 0; i < elementsToRemove.length; i++){
        jQuery("li["+elementsToRemove[i]["attr"]+"='"+elementsToRemove[i]["value"]+"']").remove();
    }
}

function mRun(){
    defer(function(){
        var cc = getCountryCode();
        removeElements();
        if(getCountryCode() == "au"){
            reverseElements(".list-items ul:eq(1)");
        }
    }, ".list-items ul li");  
}

function attachOnload(callback){
    jQuery("body").addClass("opt-72m opt-72");
    (function(open) {
        XMLHttpRequest.prototype.open = function(m, u, a, us, p) {
            this.addEventListener('readystatechange', function() {
                callback();
            }, false);
            open.call(this, m, u, a, us, p);
        };
    })(XMLHttpRequest.prototype.open);  
}

function mStart(){
    jQuery("body").addClass("opt-72m");
    mRun();
    attachOnload(mRun);
}

if(isMobile()){
    defer(function(){
        setTimeout(function(){
        mStart();
        },200);
    }, ".list-items ul li");
} else {
    defer(function(){
        setTimeout(function(){
            start();
        },200);
    }, "#a11y-product-list ul.unstyled li");
}
