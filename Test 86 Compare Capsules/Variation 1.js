
var html = 
`
<div class="opt_CompareContent g">
        <div class="opt_MobileToggle">
            <a href="#" id="opt_OriginalButton" class="opt_Selected">ORIGINAL</a>
            <a href="#" id="opt_VertuoButton">VERTUO</a>
        </div>
        <div class="opt_HalfCol opt_Show" id="opt_OriginalColumn">
            <div id="opt_First" class="opt_Section">
                <img src="//service.maxymiser.net/cm/images-eu/1/1/1/49FC53648FA90C430E42187DA587CC51B5250AE195095D22B169381D5BBFDEFF/nespresso-au/Test-86-Compare-Capsule/original_top_image.png" />
                <h2 class="opt_h2 g_h2">
                    <strong>Original</strong>
                    <span>The Classic<br>Espresso Experience</span>
                </h2>
                <p class="opt_P">
                    Short or long, strong or decaffeinated coffee, enjoyed black or topped with milk foam. Classic in its own way.
                </p>
            </div>
            <div class="opt_SectionBreak"></div>
            <div id="opt_Second" class="opt_Section">
                <img src="//service.maxymiser.net/cm/images-eu/1/1/1/99F4E059177B80E72574AF440B4585B71CE8AB5E1A82BBE6D689865501DF743B/nespresso-au/Test-86-Compare-Capsule/orginal_capsule_reflection.png" />
                <p class="opt_Title">DISCOVER NESPRESSO <strong>ORIGINAL</strong> CAPSULES</p>
                <p class="opt_P">
                    Enjoy your authentic Espresso experience with a wide range of capsules. Each has its own distinctive character and aroma, and can be enjoyed with or without milk.
                </p>
            </div>
            <div class="opt_SectionBreak"></div>
            <div id="opt_Third" class="opt_Section">
                <p class="opt_Title">How Much <strong>Espresso</strong> Do you Get</p>
                <div class="opt_HowMuchDoYouGet">
                    <div class="opt_Item">
                        <img src="//service.maxymiser.net/cm/images-eu/1/1/1/438B3C0189AA1D03122139E0C2EB0A5709E8C01139A46A99A70D3716F24B8F39/nespresso-au/Test-86-Compare-Capsule/ristretto25ml.png" />
                        <h2 class="opt_h2 g_h2"><strong>Ristretto</strong></h2>
                        <p class="opt_P">25 ml</p>
                    </div>
                    <div class="opt_Item">
                        <img src="//service.maxymiser.net/cm/images-eu/1/1/1/3DF9FEDA316802859635D71D81D5682B97DA7542549BC672553AF75B8274C00F/nespresso-au/Test-86-Compare-Capsule/espresso.png" />
                        <h2 class="opt_h2 g_h2"><strong>Espresso</strong></h2>
                        <p class="opt_P">40 ml</p>
                    </div>
                    <div class="opt_Item">
                        <img src="//service.maxymiser.net/cm/images-eu/1/1/1/22D94B0D0D2B08E3C610ACA60F7ABDAF7E6A0868967087DFCF613264C008FCD5/nespresso-au/Test-86-Compare-Capsule/lungo.png" />
                        <h2 class="opt_h2 g_h2"><strong>LUNGO</strong></h2>
                        <p class="opt_P">110 ml</p>
                    </div>
                </div>
            </div>
            <div class="opt_SectionBreak"></div>
            <div id="opt_Fourth" class="opt_Section">
                <div class="opt_BenefitItem">
                    <img src="//service.maxymiser.net/cm/images-eu/1/1/1/3676E5C6D73FAF5FB79E9C47D385E8B614CD3E4BAD91757D3EEEAB357BDCEAA8/nespresso-au/Test-86-Compare-Capsule/exceptionalchoice.png" />
                    <h2 class="opt_h2 g_h2"><strong>Exceptional<br>Coffee Choice</strong></h2>
                </div>
                <div class="opt_BenefitItem">
                    <img src="//service.maxymiser.net/cm/images-eu/1/1/1/72E30AF1018B4770855C2BB9344DC52E9BFB1E1D8AD2829F4AF652F725D1B906/nespresso-au/Test-86-Compare-Capsule/densecrema.png" />
                    <h2 class="opt_h2 g_h2"><strong>Creamy And<br>Dense Crema</strong></h2>
                </div>
                <div class="opt_BenefitItem">
                    <img src="//service.maxymiser.net/cm/images-eu/1/1/1/8FEC65DEAE675975748A559B370F241565CD5512B472BEB96ACA30D3B380D977/nespresso-au/Test-86-Compare-Capsule/pressureextraction.png" />
                    <h2 class="opt_h2 g_h2"><strong>Pressure<br>Extraction System</strong></h2>
                </div>
                <div class="opt_BenefitItem">
                    <img src="//service.maxymiser.net/cm/images-eu/1/1/1/EF0EAE053028A6E43DEB42676DC4141BAA0E97652BFDB940667171305CD5375D/nespresso-au/Test-86-Compare-Capsule/milkrecipe.png" />
                    <h2 class="opt_h2 g_h2"><strong>Espresso-Based<br>And Milk Recipes</strong></h2>
                </div>
            </div>
            <div class="opt_SectionBreak opt_ClearBreak"></div>
            <div id="opt_Fifth" class="opt_Section">
            <p class="opt_Title opt_MobileShow">DISCOVER NESPRESSO<br><strong>ORIGINAL</strong> MACHINES</p>
                <div class="opt_Machine">
                    <img src="//service.maxymiser.net/cm/images-eu/1/1/1/5F9F53C28BEC504EB74462CA587E091D4718DEC7BCBEEA4BC250B6F79BBCD71A/nespresso-au/Test-86-Compare-Capsule/originalmachine.png" />
                    <div class="opt_DiscoverContent">
                        <p class="opt_Title opt_MobileHide">DISCOVER NESPRESSO<br><strong>ORIGINAL</strong> MACHINES</p>
                        <button class="g_btn g_btnBuy" data-link="https://www.nespresso.com/au/en/original-classic-espresso-experience"><span>Buy A Machine</span></button>
                    </div>
                </div>
            </div>
        </div>
        <div class="opt_HalfCol opt_Hide" id="opt_VertuoColumn">
            <div id="opt_First" class="opt_Section">
                <img src="//service.maxymiser.net/cm/images-eu/1/1/1/84101A50F13FA9044079E5D753A775B9DF098E7415A0BCB1367AE8E451B4A697/nespresso-au/Test-86-Compare-Capsule/vertuo_top_image.png" />
                <h2 class="opt_h2 g_h2">
                    <strong>Vertuo</strong>
                    <span>THE FULL RANGE<br>OF COFFEE STYLES</span>
                </h2>
                <p class="opt_P">
                    From small cups to large mugs, from hot to cold recipes. It’s a universe of infinite possibilities, tailored to your every taste. Every style, your style.
                </p>
            </div>
            <div class="opt_SectionBreak"></div>
            <div id="opt_Second" class="opt_Section">
                <img src="//service.maxymiser.net/cm/images-eu/1/1/1/769BA8864FAA4E92A2A673A310919CBE42E402D4E66FF04370DE506D2500C2A7/nespresso-au/Test-86-Compare-Capsule/Vertuo_capsule_reflection.png" />
                <p class="opt_Title">DISCOVER NESPRESSO <strong>VERTUO</strong> CAPSULES</p>
                <p class="opt_P">
                    Enjoy your authentic Espresso experience with a wide range of capsules. Each has its own distinctive character and aroma, and can be enjoyed with or without milk.
                </p>
            </div>
            <div class="opt_SectionBreak"></div>
            <div id="opt_Third" class="opt_Section">
                <p class="opt_Title">How Much <strong>Espresso</strong> Do you Get</p>
                <div class="opt_HowMuchDoYouGet">
                    <div class="opt_Item">
                        <img src="/shared_res/agility/define/vertuoCoffeeHub/img/cups/alto_L.png" />
                        <h2 class="opt_h2 g_h2"><strong>Alto</strong></h2>
                        <p class="opt_P">414 ml</p>
                    </div>
                    <div class="opt_Item">
                        <img src="/shared_res/agility/define/vertuoCoffeeHub/img/cups/mug_L.png" />
                        <h2 class="opt_h2 g_h2"><strong>Mug</strong></h2>
                        <p class="opt_P">230 ml</p>
                    </div>
                    <div class="opt_Item">
                        <img src="/shared_res/agility/define/vertuoCoffeeHub/img/cups/granlungo_L.png" />
                        <h2 class="opt_h2 g_h2"><strong>Gran<br> Lungo</strong></h2>
                        <p class="opt_P">150 ml</p>
                    </div>
                    <div class="opt_Item">
                        <img src="/shared_res/agility/define/vertuoCoffeeHub/img/cups/doubleespresso_L.png" />
                        <h2 class="opt_h2 g_h2"><strong>Double<br> Espresso</strong></h2>
                        <p class="opt_P">80 ml</p>
                    </div>
                    <div class="opt_Item">
                        <img src="/shared_res/agility/define/vertuoCoffeeHub/img/cups/espresso_L.png" />
                        <h2 class="opt_h2 g_h2"><strong>Espresso</strong></h2>
                        <p class="opt_P">40 ml</p>
                    </div>
                </div>
            </div>
            <div class="opt_SectionBreak"></div>
            <div id="opt_Fourth" class="opt_Section">
                <div class="opt_BenefitItem">
                    <img src="//service.maxymiser.net/cm/images-eu/1/1/1/11E49D22F480C4BCFE42187ABDA8A8FE6FFC56E033D813B867EA77610B5D30D3/nespresso-au/Test-86-Compare-Capsule/coffeequality.png" />
                    <h2 class="opt_h2 g_h2"><strong>Exceptional<br>Coffee Quality</strong></h2>
                </div>
                <div class="opt_BenefitItem">
                    <img src="//service.maxymiser.net/cm/images-eu/1/1/1/7F6C84FA6BE6BC9CF561D4768FA5473D55A0B82EB45620F8A1D15FE129727E94/nespresso-au/Test-86-Compare-Capsule/smoothcrema.png" />
                    <h2 class="opt_h2 g_h2"><strong>Generous And<br>Smooth Crema</strong></h2>
                </div>
                <div class="opt_BenefitItem">
                    <img src="//service.maxymiser.net/cm/images-eu/1/1/1/6BAFF5C7219FAE95F0CCD2247CE7E135A6229DDFAEB2F247459B7F599BD7225B/nespresso-au/Test-86-Compare-Capsule/innovativesystem.png" />
                    <h2 class="opt_h2 g_h2"><strong>Innovative<br>Coffee System</strong></h2>
                </div>
                <div class="opt_BenefitItem">
                    <img src="//service.maxymiser.net/cm/images-eu/1/1/1/DEAD21D717E70726FA03C1B31C30B9968183498CB78DF0C00E7627614E9949A1/nespresso-au/Test-86-Compare-Capsule/centrifusion.png" />
                    <h2 class="opt_h2 g_h2"><strong>Extraction By<br>Centrifusion</strong></h2>
                </div>
            </div>
            <div class="opt_SectionBreak opt_ClearBreak"></div>
            <div id="opt_Fifth" class="opt_Section">
            <p class="opt_Title opt_MobileShow">DISCOVER NESPRESSO<br><strong>VERTUO</strong> MACHINES</p>
                <div class="opt_Machine">
                    <img src="//service.maxymiser.net/cm/images-eu/1/1/1/357719EB2F06E0F22B90DED56A2F7D9964C1735EB39974317529A25DC7F3B83F/nespresso-au/Test-86-Compare-Capsule/vertuomachine.png" />
                    <div class="opt_DiscoverContent">
                        <p class="opt_Title opt_MobileHide">DISCOVER NESPRESSO<br><strong>VERTUO</strong> MACHINES</p>
                        <button class="g_btn g_btnBuy" data-link="https://www.nespresso.com/au/en/vertuo-coffee-redefined"><span>Buy A Machine</span></button>
                    </div>
                </div>
            </div>
        </div>
    </div>
`
function addHtml () {
    jQuery('.g_bothOverflow').after(html);
}

function setHeights () {
    // Section one Heights
    jQuery('#opt_First .opt_h2.g_h2').adjustHeight(true);
    jQuery('#opt_First .opt_P').adjustHeight(true);
    // Section two Heights
    jQuery('#opt_Second .opt_Title').adjustHeight(true);
    jQuery('#opt_Second .opt_P').adjustHeight(true);
    // Section three Heights
    jQuery('#opt_Third .opt_Item .opt_h2.g_h2').adjustHeight(true);
    jQuery('#opt_Third .opt_HowMuchDoYouGet').adjustHeight(true);
    jQuery('.opt_CompareContent .opt_HowMuchDoYouGet img').adjustHeightMin(true);

}

function addButtonLinking () {
    jQuery('#opt_Fifth button').each(function () {
        jQuery(this).click(function (e) {
            e.preventDefault();
            var link = jQuery(this).attr('data-link');
            window.location = link;
        });
    });
}

function addMobileToggleFunctionality() {
    jQuery('#opt_OriginalButton').click(function (e) {
        e.preventDefault();
        jQuery('.opt_Selected').removeClass('opt_Selected');
        jQuery(this).addClass('opt_Selected');
        jQuery('#opt_OriginalColumn').addClass('opt_Show').removeClass('opt_Hide');
        jQuery('#opt_VertuoColumn').addClass('opt_Hide').removeClass('opt_Show');
      	setHeights();
    });

    jQuery('#opt_VertuoButton').click(function (e) {
        e.preventDefault();
        jQuery('.opt_Selected').removeClass('opt_Selected');
        jQuery(this).addClass('opt_Selected');
        jQuery('#opt_VertuoColumn').addClass('opt_Show').removeClass('opt_Hide');
        jQuery('#opt_OriginalColumn').addClass('opt_Hide').removeClass('opt_Show');
		setHeights();
    });
}

function defer(method, selector) {
    if (window.jQuery) {
        if (jQuery(selector).length > 0){
            method();
        } else {
            setTimeout(function() { defer(method, selector); }, 50);
        }  
    } else {
         setTimeout(function() { defer(method, selector); }, 50);
    }    
}

defer(function () {
  jQuery.fn.adjustHeight = function(set) {
    if (jQuery(window).width() < 768 && set == true) {
        this.css('min-height', '1px');
    } else {
        var maxHeightFound = 0;
        this.css('min-height','1px');
        this.each(function() {
            if( jQuery(this).height() > maxHeightFound ) {
                maxHeightFound = jQuery(this).outerHeight();
            }
        });
        this.css('min-height', maxHeightFound);
    }
};

jQuery.fn.adjustHeightMin = function(set) {
    if (jQuery(window).width() < 768 && set == true) {
        var minHeightFound = 0;
        this.css('max-height','initial');
        this.each(function() {
            if( jQuery(this).height() < minHeightFound || minHeightFound == 0) {
                if (jQuery(this).outerHeight() != 0) {
                    minHeightFound = jQuery(this).outerHeight();
                }
            }
        });
        this.css('max-height', minHeightFound);
    } else {
        this.css('max-height','initial');
    }
};

    addHtml();
    
    addButtonLinking();
    addMobileToggleFunctionality();
	setHeights();
    jQuery(window).resize(function () {
      setHeights();
    });
    setTimeout(function () {

      setHeights();
    },500);
}, '.g_bothOverflow')