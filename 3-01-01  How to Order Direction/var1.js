var prod_url,
    prod_name,
    prod_qty,
    num_capsules,
    capsules_left,
    capsules_needed;

$(window).on("load",function(){
   prod_url = localStorage.getItem("opt-url");
   prod_name = localStorage.getItem("opt-name");
});

function addModal(){
  
   prod_url = localStorage.getItem("opt-url");
   prod_name = localStorage.getItem("opt-name");

  $("body").append("<div class='opt-overlay'><div class='opt-modal'><img class='opt-close' src='https://service.maxymiser.net/cm/images-eu/1/1/1/22F6BC6DD520867E7C8298379BF9ABED1804DFAE28A428E66CB0887CD2FBF0D7/nespresso-au/Test-3-01-01-How-to-Order-Direction/cross-1.png' /></div></div>");

  $(".opt-modal").append("<h2>Review your Basket");

  $(".opt-modal").append("<hr />");

  $("#mini-cart-dropdown").clone().appendTo(".opt-modal").show();

  $(".opt-modal .cart-details caption").hide();
  $(".opt-modal").append("<div class='opt-upsell'></div>");
  $(".opt-upsell").append("<h3> You'll need another <span class='opt-left'>20</span> pods to complete this order. </h3>");
  $(".opt-upsell").append("<div class='upsell-area'> </div>");
  $(".upsell-area").append("<p> To validate your basket, your order should consist of a minimum 50 capsules and be in multiples of {4} (i.e. 50, 100, 150, etc.) </p>");
  $(".upsell-area").append("<h4> WOULD YOU LIKE <span class='opt-left'>-</span> MORE OF THESE PODS? </h4>");

  $(".upsell-area").append("<div class='opt-item'> <span class='item-name'>"+prod_name+"</span> <span class='item-price opt-left'> 20 </span> </div>");

  $(".upsell-area .opt-item").append("<img src='http://i.imgur.com/Iemquuu.png' />");
  $(".upsell-area").append("<h4> OR </h4>");
  $(".upsell-area").append("<h4> WANT TO CHOOSE ANOTHER TYPE? </h4>");
  $(".upsell-area").append("<button class='opt-continue'> CONTINUE SHOPPING </div>");
  
  $(".opt-modal .cart-open").removeAttr("style");

  num_capsules = $(".opt-modal th:contains('Capsules')").next().text().trim();

  $(".opt-left").text(50 - num_capsules);

  if (num_capsules < 50) {
    $(".opt-modal .button-green").addClass("disabled");

    $(".opt-modal .button-green.disabled").click(function(e){
      e.preventDefault();
    });

  } else {
    $(".opt-modal").addClass("enough");
  }

  $(".opt-modal").click(function(e){
    e.stopPropagation();
  });

  $(".opt-close, .opt-overlay").click(function() {
      $(".opt-overlay").remove(); 
      $("body").css({"height":"auto","overflow":"inherit"});
    });
  
  
  capsules_left = 50 - num_capsules; 
  prod_qty = parseInt($(".opt-iframe").contents().find("#ta-add_plus_button .quantity:eq(0)").text());
  capsules_needed = capsules_left + prod_qty;

  $(".opt-item img").click(function(){
    $(".opt-iframe").contents().find("#ta-add_plus_button").click();
    $(".opt-iframe").contents().find("#quantity-selector-options button[data-quantity="+capsules_needed+"]").click();
    
//     $(".opt-iframe").on("load", function(){
      location.reload();
//     });
    
//     location.reload();
  });
  
}

$("button.add-to-cart").click(function(){
  prod_url = $(this).parents(".popin").find("a.track-product-click").attr("href");
  prod_name = $(this).attr("data-product-name");
  
  localStorage.setItem("opt-url",prod_url)
  localStorage.setItem("opt-name",prod_name);

  console.log(prod_url);
  console.log(prod_name);
  $(".opt-iframe").remove();
  $("body").append("<iframe class='opt-iframe' src='"+localStorage.getItem("opt-url")+"'></iframe>");
  
  $(".opt-iframe").on("load", function(){
    prod_qty = parseInt($(".opt-iframe").contents().find("#ta-add_plus_button .quantity:eq(0)").text());
  });
});

$(window).on("load", function(){
  if ($(".opt-iframe").length <= 0 && localStorage.getItem("opt-url") != null){
     $("body").append("<iframe class='opt-iframe' src='"+localStorage.getItem("opt-url")+"'></iframe>");
  }
});

$("#mini-cart").click(function(e){
  e.preventDefault();
  addModal();
});