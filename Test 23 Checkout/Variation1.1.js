
function getItemQuantityFromName(name){
    if(name.indexOf("3") >= 0 || name.indexOf("Trio") >= 0 || name.indexOf("TRIO") >= 0){
        return 3 * 10;
    } else if(name.indexOf("150") >= 0){
        return 150;
    } else if(name.indexOf("250") >= 0){    
        return 250;
    } else if(name.indexOf("5") >= 0){
        return 5 * 10;
    } else if(name.indexOf("10") >= 0){
        return 10 * 10;
    } 
    return 1;
}

function getCapsuleCartQuantity(){
    var cart = nestms.DataLayer.cart.products;
    var sum = 0;
    for(var product in cart){
        var name = cart[product].product.name;
        var quantity = cart[product].quantity;
        console.log(quantity);
        sum = sum + (getItemQuantityFromName(name) * quantity);
    }
    return sum;
}

function isCheckoutAvailable(){
    var sum = getCapsuleCartQuantity();
    if(sum % 50 == 0 && sum != 0){
        return true;
    } else {
        return false;
    }
}

function setCheckoutButton(){
    var valid = "opt-disabled";
    var href = '';
    if(isCheckoutAvailable()){
        valid = "";
        href = 'href="/au/en/checkout"';
    } 
    if (jQuery('.opt23Button').length == 0) {
        jQuery('.product-title .price').parent().find('.add-to-cart').wrapAll('<div class="optWrapper pull-right opt23Button" style="    float: right;    display: flex;    flex-direction: row;"></div>');
    } else {
        jQuery('.optWrapper a').remove(); 
    }
    
    if (jQuery('.opt23Button').length > 0) {
        jQuery('.optWrapper').append(`
        <a  id="ta-order-proceed" data-ng-if="state === 'filled' || state === 'add'" class="ng-scope ` + valid + `" style="">
            <div id="opt-hover-details"><p>To validate your basket, your order should consist of a minimum 50 capsules and be in multiples of 50 (i.e. 50, 100, 150, etc.). You can mix the variety of capsules as you choose, as long as the total number is a multiple of 50. </p></div>
            <span class="btn button-primary button-green"> Checkout</span>
        </a>
        `);
    }

    jQuery(".opt-disabled").hover(
       function () {
          $("#opt-hover-details").show();
       }, 
       function () {
          $("#opt-hover-details").hide();
       }
    );
}

// USeful for Nespresso mobile to check whether your script has run.
function start () {
    setTimeout(function () {
        jQuery('body').addClass('opt23');
        setCheckoutButton();
        jQuery("button.add-to-cart").click(function(){
            setTimeout(function(){
            jQuery(".quantity-selection, .qty-btn").click(function(){
                    setTimeout(function(){
                        setCheckoutButton();
                    },2000);
                });
            },200);
        },1000);
    });
}

//example wait for jQuery
function defer(method, selector, checkadded) {
    if (window.jQuery) {
        if (jQuery(selector).length > 0){
        method();
        jQuery(window).ajaxComplete(function () {
                start();
            });
        } else {
            setTimeout(function() { defer(method, selector) }, 50);
        }  
    } else {
        setTimeout(function() { defer(method, selector) }, 50);
    }    
}
  
defer(start, '.product-title .button-primary.pull-right.button-green.two-parts.add-to-cart');
// USeful for Nespresso mobile to check whether your script has run.
