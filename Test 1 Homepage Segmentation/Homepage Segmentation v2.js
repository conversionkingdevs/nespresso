

var products = [{
    "title":"Intenso",
    "url":"https://www.nespresso.com/au/en/intenso-coffees",
    "image":"#$(ContentManager:intenso.png)!"
},
{
    "title":"Decaffinato",
    "url":"https://www.nespresso.com/au/en/decaffeinato-coffees",
    "image":"#$(ContentManager:decaf_tile.png)!"
},
{
    "title":"Espresso",
    "url":"https://www.nespresso.com/au/en/espresso-coffees",
    "image":"#$(ContentManager:espresso_tile.png)!"
},
{
    "title":"Pure Origin",
    "url":"https://www.nespresso.com/au/en/pure-origin-coffees",
    "image":"#$(ContentManager:pureorigin_tile.png)!"
},
{
    "title":"Lungo",
    "url":"https://www.nespresso.com/au/en/lungo-coffees",
    "image":"#$(ContentManager:lungo_tile.png)!"
},
{
    "title":"Variations",
    "url":"https://www.nespresso.com/au/en/variations-coffees",
    "image":"#$(ContentManager:variations_tile.png)!"
}]

$('#slider-contentCarrouselBlock').parent().after('<div class="opt1Container"></div>');

for (x=0; x < products.length; x++) {
    var title = products[x].title,
        url = products[x].url,
        image = products[x].image;

    $('.opt1Container').append('<div class="opt-cat-cont" id="'+title+'"><a href="'+url+'"><p class="opt-cat-title">'+title+'</p><img src="'+image+'" /></a></div>')

}
