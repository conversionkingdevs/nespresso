<script>

var products = [{
    "title":"Capsules",
    "url":"https://www.nespresso.com/au/en/order/capsules",
    "image":"#$(ContentManager:capsules_tile.png)!"
},
{
    "title":"Machines",
    "url":"https://www.nespresso.com/au/en/order/machines",
    "image":"#$(ContentManager:machines_tiles.png)!"
},
{
    "title":"Accessories",
    "url":"https://www.nespresso.com/au/en/order/accessories",
    "image":"#$(ContentManager:accessories_tile.png)!"
}]

$('#slider-contentCarrouselBlock').parent().after('<div class="opt1Container"></div>');

for (x=0; x < products.length; x++) {
    var title = products[x].title,
        url = products[x].url,
        image = products[x].image;

    $('.opt1Container').append('<div class="opt-cat-cont" id="'+title+'"><a href="'+url+'"><p class="opt-cat-title">'+title+'</p><img src="'+image+'" /></a></div>')

}
</script>