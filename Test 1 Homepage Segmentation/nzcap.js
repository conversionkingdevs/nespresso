var products = [{
    "title":"Intenso",
    "url":"https://www.nespresso.com/au/en/intenso-coffees",
    "image":"#$(ContentManager:1intenso.png)!"
},
{
    "title":"Pure Origin",
    "url":"https://www.nespresso.com/au/en/pure-origin-coffees",
    "image":"#$(ContentManager:1pureorigin.png)!"
},
{
    "title":"Espresso",
    "url":"https://www.nespresso.com/au/en/espresso-coffees",
    "image":"#$(ContentManager:1espresso.png)!"
},
{
    "title":"Lungo",
    "url":"https://www.nespresso.com/au/en/lungo-coffees",
    "image":"#$(ContentManager:1lungo.png)!"
},
{
    "title":"Variations",
    "url":"https://www.nespresso.com/au/en/variations-coffees",
    "image":"#$(ContentManager:1variations.png)!"
},
{
    "title":"Decaffeinato",
    "url":"https://www.nespresso.com/au/en/decaffeinato-coffees",
    "image":"#$(ContentManager:1decaf.png)!"
}];

jQuery('#slider-contentCarrouselBlock').parent().after('<div class="opt1Container"></div>');

for (var x=0; x < products.length; x++) {
    var title = products[x].title,
        url = products[x].url,
        image = products[x].image;

    jQuery('.opt1Container').append('<div class="opt-cat-cont" id="'+title+'"><a href="'+url+'"><p class="opt-cat-title">'+title+'</p></a></div>')

}

actions.trackClicks("#Intenso > a", {name:"Segmentation", value:"1", attribute:""}, "627e6a77-f278-ea9e-0508-673405643c94");
	actions.trackClicks("#PureOrigin > a", {name:"Segmentation", value:"1", attribute:""}, "7f79edee-af7c-ceb7-99b4-82970ad61e2d");
	actions.trackClicks("#Espresso > a", {name:"Segmentation", value:"1", attribute:""}, "8866c56c-b116-08ba-bc22-068518751b34");
	actions.trackClicks("#Lungo > a", {name:"Segmentation", value:"1", attribute:""}, "b9c90f2d-2f9b-4ee2-e009-001330b7ab55");
	actions.trackClicks("#Variations > a", {name:"Segmentation", value:"1", attribute:""}, "b09e4842-e88c-9096-2bb5-5be6cac7add8");
	actions.trackClicks("#Decaffeinato > a", {name:"Segmentation", value:"1", attribute:""}, "2a753bc6-533e-dcda-a7cc-8f30c01ba1be");
	actions.trackClicks("#Intenso > a", {name:"Intenso Click", value:"1", attribute:""}, "cc8354d4-9056-c003-f75b-e6da43a40563");
	actions.trackClicks("#PureOrigin > a", {name:"Segmentation", value:"1", attribute:""}, "c70ae20d-6814-73fe-56ca-87ec0ef74025");
	actions.trackClicks("#Espresso > a", {name:"Espresso Click", value:"1", attribute:""}, "6e4d5a4b-b647-8590-052b-8b875e66ce6a");
	actions.trackClicks("#Lungo > a", {name:"Lungo Click", value:"1", attribute:""}, "a967dcd1-97b8-002f-644d-396a6b8bb768");
	actions.trackClicks("#Variations > a", {name:"Variations Click", value:"1", attribute:""}, "a4aaba61-e57a-90ad-4094-dc37e8dc610f");
	actions.trackClicks("#PureOrigin > a", {name:"Segmentation", value:"1", attribute:""}, "e1ec8358-f6ce-14f5-b6ce-228462e9bb9a");