
var products = [{
    "title":"Capsules",
    "url":"https://www.nespresso.com/au/en/order/capsules",
    "image":"#$(ContentManager:capsules_tile.png)!"
},
{
    "title":"Machines",
    "url":"https://www.nespresso.com/au/en/order/machines",
    "image":"#$(ContentManager:machines_tiles.png)!"
},
{
    "title":"Accessories",
    "url":"https://www.nespresso.com/au/en/order/accessories",
    "image":"#$(ContentManager:accessories_tile.png)!"
}]

$('#slider-contentCarrouselBlock').parent().after('<div class="opt1Container"></div>');

for (var x=0; x < products.length; x++) {
    var title = products[x].title,
        url = products[x].url,
        image = products[x].image;

    jQuery('.opt1Container').append('<div class="opt-cat-cont" id="'+title+'"><a href="'+url+'"><p class="opt-cat-title">'+title+'</p></a></div>')

}
  
actions.trackClicks("#Capsules > a", {name:"Segmentation", value:"1", attribute:""}, "d2c32006-25c2-9a34-52a8-744346ef1394");
	actions.trackClicks("#Machines > a", {name:"Segmentation", value:"1", attribute:""}, "cc7fd99a-2c3f-564e-7523-058d37b65979");
	actions.trackClicks("#Accessories > a", {name:"Segmentation", value:"1", attribute:""}, "1c1c7755-12f2-1061-6cb7-e1926882c34b");
	actions.trackClicks("#Capsules > a", {name:"Capsules Click", value:"1", attribute:""}, "c23a9099-030d-4f12-f1d1-c9833b3adbc7");
	actions.trackClicks("#Machines > a", {name:"Machine Click", value:"1", attribute:""}, "cdcf9490-0f7e-f32f-3edc-a7a8037f39f1");
	actions.trackClicks("#Accessories > a", {name:"Accessory Click", value:"1", attribute:""}, "513d8116-d602-3642-43a4-18d584024494");