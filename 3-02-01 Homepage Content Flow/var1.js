$(".articles.pull-left").removeClass("pull-left").insertAfter($("#push-group-element"));
$("#push-group-element li:eq(3)").hide();
$(".prod-ctn img").attr("width","244").attr("height","217");
$("#push-group-element li:eq(6) img").attr("width","488px");

$(".horizontal-separator").insertBefore(".articles");
$(".recipes-table li:eq(1)").hide();

$(".articles").append("<div class='focus-block-col opt-recycling'></div>");
$(".opt-recycling").append("<h2><i>NESPRESSO</i> RECYCLING</h2>");
$(".opt-recycling").append("<img src='https://service.maxymiser.net/cm/images-eu/1/1/1/69D8E9657438FACD7F49E238B5051DD0BDBDB9CE566DB2E8DF7D36D057A06684/nespresso-au/Test-3-02-01-Homepage-Content-Flow/new_capsules_tile.png' />");

$(".opt-recycling").append("<p class='title'>Give your <i>Nespresso</i> capsules a second life</p>");
$(".opt-recycling").append("<p><i>Nespresso</i> coffee capsules are made from aluminium which is infinitely recyclable. In Australia, 100% of <i>Nespresso</i> Club Members now have access to a recycling solution. Once you have finished enjoying your Grand Cru, make sure you return your used <i>Nespresso</i> capsules by using one of our convenient recycing options. Find out how to recycle your <i>Nespresso</i> capsules </p>");

$(".articles li img").attr("src","https://service.maxymiser.net/cm/images-eu/1/1/1/715DA5CD7CEF41DA042145835334DA70C0808A060934355471C3112DB95781C7/nespresso-au/Test-3-02-01-Homepage-Content-Flow/nespressoimage.png");