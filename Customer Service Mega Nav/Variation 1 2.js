function start() {
    
    // Initial Changes
    jQuery('body').addClass('optNav');
    jQuery('li.menu-top:nth-child(8) > ul > li.push.wrapper-push-banner').remove();
    jQuery('#ta-menu-7-2').replaceWith('<div>'+jQuery("#ta-menu-7-2")[0].innerHTML+'</div>');
    jQuery('li.menu-top:nth-child(8) > a > .item-name').text('FAQ');
    var info = [
        {
            "tile": 2,
            "image": 'https://service.maxymiser.net/cm/images-eu/1/1/1/AB51B586ED8E901E3B894F51BFF94DE9B3A1F1C158B9154997DBA08CB7274788/nespresso-au/Customer-Service-Mega-Nav/faq3.png',
            "title": 'FAQ Categories',
            "href": 'https://www.nespresso.com/au/en/service-faq'
        },
        {
            "tile": 3,
            "image": 'https://service.maxymiser.net/cm/images-eu/1/1/1/00B27DC59887DA11C84B3011210D7E51B5CA55263EE2226A38F9E9A0500167D1/nespresso-au/Customer-Service-Mega-Nav/top5faq.png',
            "title": 'Top 5 FAQ\'s'
        }
    ]
    
    function tileChanges(tile, image, title) {
          
        tile.find('img').attr('src', image);
        tile.find('.item-title').text(title).addClass('optTitleFont');
        tile.find('.info p').html('<div class="optContent"></div>');
    }
    
    function tile2(data) {
        var tile = jQuery('li.menu-top:nth-child(8) > ul > li.item'+data.tile+'.row1.medium');
    
        tileChanges(tile, data.image, data.title);
        var html = `
            <ul class="optTile2List">
                <li>Product Information <span class="optOrange">(16)</span></li>
                <li>Brand & Promotion <span class="optOrange">(1)</span></li>
                <li>Orders & Delivery <span class="optOrange">(5)</span></li>
                <li>Machine Support <span class="optOrange">(6)</span></li>
                <li>Product Issues <span class="optOrange">(1)</span></li>
                <li>Recycling <span class="optOrange">(29)</span></li>
                <li>Other <span class="optOrange">(4)</span></li>
            </ul>
        `
    
        tile.find('.optContent').html(html);
        tile.find('a').attr('href', data.href);
    }
    
    function tile3(data) {
        var tile = jQuery('li.menu-top:nth-child(8) > ul > li.item'+data.tile+'.row1.medium');
        
        tileChanges(tile, data.image, data.title)
        var html = `
        <ul class="optTile3List">
            <li id="chocolate">Does Nespresso offer tea, hot chocolate or chai capsules?</li>
            <li id="compCap">Can I use non-Nespresso/compatible capsules in my Nespresso Unit?</li>
            <li id="bleDiff">What is the difference between an 'Espresso' and a 'Lungo' blend?</li>
            <li id="offerOrd">What is the welcome offer, and how do I order this?</li>
            <li id="capCost">How much do Nespresso Capsules cost?</li>
        </ul>
    `
    
    tile.find('.optContent').html(html);
    }
    
    
    for (var x = 0; x < info.length; x++) {  
        switch (info[x].tile) {
            case 2:
                tile2(info[x]);
                break;
            case 3:
                tile3(info[x]);
                break;
        }
    }
    
    var modals = {
        "compCap":{
            "content":'<div class="answer"><p>The original <em>Nespresso</em> capsules and the genuine <em>Nespresso</em> machines&nbsp;were designed to work together as a single system to deliver the perfect cup of coffee time after time. Only the interaction of our genuine Grands Crus and the genuine <em>Nespresso</em> machine’s brewing unit can guarantee the in-cup quality that <em>Nespresso</em> is known for.</p><p>While we cannot comment on non-<em>Nespresso</em> products as a matter of policy, allow us to say that the <em>Nespresso</em> aluminium capsule and the extraction through the <em>Nespresso</em> machine brewing unit assures the pressure, water temperature and quantity needed to consistently create the perfect cup, time after time.</p></div>',
            "title":'Other',
            "question":'Can I use non-Nespresso/compatible capsules in my Nespresso Unit?'
        },
        "bleDiff":{
            "content":'<div class="answer"><p><em>Nespresso</em> Grands Crus capsules are divided into 2 different types: Espresso and Lungo.</p><p>An Espresso is a 40 mL shot of coffee (the small cup button). The Espresso capsules contain 5g of coffee of a very fine grind.</p><p>A Lungo is a 110 mL shot of coffee (the large cup button). The Lungo capsules contain 7g of coffee of a courser grind, meaning that more water can be passed through the capsules before you begin to over extract.</p></div>',
            "title":'Product Information',
            "question":"What is the difference between an 'Espresso' and a 'Lungo' blend?"
        },
        "offerOrd":{
            "content":'<div class="answer"><p>We offer new <em>Nespresso</em> Club Members the chance to benefit from a Welcome Offer on their first capsule order.&nbsp;We have three offers available containing either 150 or 250 capsules, designed to introduce you to the world of <em>Nespresso</em> and our Grands Crus.</p><p>The Welcome Offers can be purchased online, in Boutiques or by calling the <em>Nespresso</em> Club on toll-free 1800 623 033.&nbsp;The offers contain a pre-selected assortment of our Grands Crus and a complementary gift.</p><p><strong>150 capsule Discovery Selection Welcome Offer contents:</strong><br><strong>10 capsules each of:</strong> Kazaar, Dharkan, Ristretto, Arpeggio, Roma, Volluto, Livanto, Indriya from India, Dulsao Do Brasil, Fortissio Lungo, Envivo Lungo, Arpeggio Decaffeinato, Ciocattino, Caramelito and Vanilio.</p><p><strong>150 capsule Morning Selection Welcome Offer contents:</strong><br><strong>10 capsules each of:</strong> Kazaar, Dharkan, Indriya from India, Rosabaya de Colombia, Volluto, Envivo Lungo&nbsp;and Linizio Lungo.&nbsp;<br><strong>20 capsules each of:</strong>&nbsp;Ristretto, Arpeggio, Roma and Livanto</p><p><strong>Both 150 Welcome Offers come with your choice of one of the below complementary gifts:</strong></p><ol type="A"><li><em>Nespresso</em> Bonbonniere Dispenser* (valued at $35)</li><li><em>Nespresso</em> Glass Cappuccino Cup Set (valued at $30)</li><li><em>Nespresso</em> Recycling Canister (valued at $29)</li></ol><p><strong>250 capsule Ultimate Selection Welcome Offer contents:</strong><br><strong>10 capsules each of:</strong> Kazaar, Livanto, Capriccio, Volluto, Indriya from India, Rosabaya de Colombia, Dulsao do Brasil, Bukeela ka Ethiopia, Envivo Lungo, Fortissio Lungo, Vivalto Lungo, Linizio Lungo, Arpeggio Decaffeinato, Volluto Decaffeinato, Caramelito and Vanilio<br><strong>20 capsules each of:</strong> Ristretto, Arpeggio, Dharkan and Roma.</p><p><strong>The 250 Welcome Offer comes with your choice of one of the below complementary gifts:</strong></p><ol type="A"><li><em>Nespresso</em> Totem 2 in 1* (valued at $60)</li><li><em>Nespresso</em> View Glass Espresso &amp; Lungo Set (valued at $50)</li><li><em>Nespresso</em> Versilo Dispenser* (valued at $50)</li></ol><p>The Welcome Offer and included complementary gift are only redeemable once per Club Member.&nbsp;</p><p><em>* All capsule dispensers are supplied without capsules.</em></p><p><em>Please note, the Welcome Offer is available on the Club Member’s first or second coffee order only. Club Members can only redeem the Welcome Offer once per Club Member account number. Welcome Offer coffee selections and gifts subject to stock availability. Capsule dispensers are supplied without capsules. Selection of complimentary accessories will be determined by <em>Nespresso</em> and will not include the full accessories range.</em></p></div>',
            "title":'Orders & order follow up',
            "question":'What is the welcome offer, and how do i order this?'
        },
        "capCost":{
            "content":'<div class="answer"><p class="MsoNormal"><span>The <em>Nespresso</em> system operates on hermetically sealed coffee capsules, guaranteeing that each single-portioned espresso is as fresh as the day it was ground. Our Grands Crus are packaged in what we refer to as sleeves, which contain 10 capsules of a particular blend.</span></p><p class="MsoNormal"><span>Prices are shown on the website per capsule and vary depending on the blend. Prices of coffee assortment packs are shown per pack and are reflective of the Grands Crus within each assortment pack. Click <a title="https://www.nespresso.com/au/en/order/capsules/" href="https://www.nespresso.com/au/en/order/capsules/" target="_blank">here</a> to see the current capsule prices.<br></span></p><p class="MsoNormal">Capsules are available for purchase in any of our Boutiques locations or directly through the <em>Nespresso</em> Club via our toll-free number, online at <a title="https://www.nespresso.com/au/en/home" href="https://www.nespresso.com/au/en/home" target="_blank">www.nespresso.com</a>, mobile and iPad apps, fax or post order.</p></div>',
            "title":'Product Information',
            "question":'How much do Nespresso Capsules cost?'
        },
        "chocolate":{
            "content":'<div class="answer"><p><em><em>Nespresso</em>\'s</em> core business is the production of premium portioned coffee, providing perfect in-cup quality each and every time. We do not have any plans to introduce tea, hot chocolate or chai capsules to our Grands Crus selection. We do offer Limited Edition and flavoured Variations throughout the year. Explore our&nbsp;<a title="https://www.nespresso.com/ultimate-coffee-creations/int/en/search/all.html" href="https://www.nespresso.com/ultimate-coffee-creations/int/en/search/all.html" target="_blank">Ultimate Coffee Creations</a> site for ideas on serving suggestions and creative coffee styling.</p></div>',
            "title": "Product Information",
            "question":'Does Nespresso offer tea, hot chocolate or chai capsules?',
        }
    }
    
    
    function addModal (data) {
        var html = `
        <div class="optModalMain">
            <div class="optOverlay"></div>
            <div class="optModal">
                <div class="optClose"></div>
                <div class="optModalContainer">
                    <div class="optHeader">
                        <h2>`+data.title+`</h2>
                    </div>
                <div class="optSep"></div>
                    <div class="optModalContent">
                        <p class="optbolder">`+data.question+`</p>
                        <div>`+data.content+`</div>
                    </div>
                </div>
            </div>
        </div>
        `
        jQuery('li.menu-top:nth-child(8)').append(html);
        jQuery('.optClose').click(function () {
            jQuery('.optModalMain').remove();
        });
        jQuery('.optOverlay').click(function () {
            jQuery('.optModalMain').remove();
            jQuery('li.menu-top:nth-child(8)').mouseout()
        });
    }
        
    jQuery('.optTile3List > li:not(#georgeModal)').click(function () {
        var id = jQuery(this).attr('id');
        addModal(modals[id]);
    })
}
    
    function defer(method, selector) {
        if (window.jQuery) {
            if (jQuery(selector).length > 0){
                method();
            } else {
                setTimeout(function() { defer(method, selector) }, 50);
            }  
        } else {
             setTimeout(function() { defer(method, selector) }, 50);
        }    
    }
    
    defer(start, 'li.menu-top:nth-child(8) > ul > li.push.wrapper-push-banner')