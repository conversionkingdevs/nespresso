
$("body").addClass("opt-2-01-03");


/** This function waits x amount of INTERVALTIME waiting for given selector.
*   If the selector is found run function f.
*   If the function has reached x times INTERVALTIME it will timeout.
**/

//interval time is set to 1/10 of a second.
var INTERVALTIME = 100;
var stop = false;
function ckOnFindElement(selector, f, x) {
  var count = 0;
  stop = false;
  //console.log(x);
  var checkExist = setInterval(function() {
    console.log(count);
    try{
      if ($(selector).length > 0) {
        f();
        clearInterval(checkExist);
        //console.log("found:" + selector);
        //show nothing
      } else {
        if(count >= x || stop){
          //when timeout show modal
          clearInterval(checkExist);
          //console.log("activationTimeOut");
        }
        count++;
      }
    } catch(e){
      if(count >= x){
        //when timeout show modal
        clearInterval(checkExist);
        //console.log("activationTimeOut");
      }
      count++;
    }
  }, INTERVALTIME);
}

function intense(url, callback) {
    $.get( url, function( data ) {
        var html = $.parseHTML(data),
        intensity = $(html).find('#product-description-title span').html();
        if (intensity == NaN || intensity == undefined ) {
            description = "";
        } else {
            description = $(html).find('#product-description > div[itemprop="description"]').html().split("<br>")[0];
        }
        //console.log(intensity)
        if (callback) {
            callback(intensity, description)
        } else {
            return intensity;
        }
    });
}

function load(){
    $("body").toggleClass("opt-viewMore");
     $(".product").not(".opt-active-test").each(function(){
        $(this).find('.qtt-price').children().wrap('<div class="opt-pricing"></div>');
        $(this).find('.qtt-price').append('<div class="opt-container"></div>');
        $(this).children().find('.list-items.products li.product-container').each(function () {
            var href = $(this).find('a').attr('href');
            var ref = $(this)
            var temp = ""
            intense(href, function (a,b) {
                if (a == NaN || a == undefined ) {
                    $(ref).find('.opt-container').html('<span class="exact-price">Assortment</span><br><span>'+b+'</span>')
                } else {
                    temp = '<div class="opt-first-container"><span class="exact-price">Intensity: <span>'+a+'</span></span><div class="opt-intense-container">';
                    $intensity = parseInt(a);
                    for(var i = 1; i < 12; i++){
                        temp += '<div class="';
                        if(i < $intensity){
                            temp += 'opt-full';
                        }
                        temp += '"></div>';
                    }
                    temp += '</div></div><span>'+b+'</span>';
                    $(ref).find('.opt-container').html(temp);
                }
            });
            $(ref).find('div.right-item').children().wrap('<div class="opt-pricing"></div>');
            $(ref).find('div.right-item').append('<a class="optButton right-item" href='+href+' style="text-align: center; border: 1px solid lightgrey; padding: 2px; color: black; margin-left: 5px;">View More</a>');
        });
        $(this).addClass("opt-active-test");

        $(this).find("#productListForm > input").after('<div class="opt-continue-shopping"><a href="#" class="opt-continue-button">Continue Shopping?</a></div>');
        $(this).find("#productListForm > input").before('<div class="opt-content"><h2>Finished Shopping</h2></div>');
        $(this).find(".opt-continue-shopping").click(function(){    
            $(".back.menu-button.retina").click();
        });
     });
}

function catagoryLoad(){
    if($("#main-product-title").text() == "Capsules")
    $("body").toggleClass("opt-viewMore");
        $("#promotionBanners").after(`<div class="opt-banner-container">
            <div class="opt-first">
                <div><h1>Know what you want?</h1></div>
                <div><a href="#" class="opt-quick-order">QUICK ORDER</a></div>
            </div>
            <div class="opt-second">
                <div><a href="#" class="opt-quick-order">Back to listing</a></div>
            </div>
        </div>`);

        $(".opt-second").click(function(){
            $("body").addClass("opt-viewMore");
        });

        $(".opt-first").click(function(){
            $("body").removeClass("opt-viewMore");
        });
        

        $('.qtt-price').children().wrap('<div class="opt-pricing"></div>');
        $('.qtt-price').append('<div class="opt-container"></div>');
        $('.list-items.products li.product-container').each(function () {
            var href = $(this).find('a').attr('href');
            var ref = $(this);
            var temp = "";
            intense(href, function (a,b) {
                if (a == NaN || a == undefined ) {
                    $(ref).find('.opt-container').html('<span class="exact-price">Assortment</span><br><span>'+b+'</span>')
                } else {
                    temp = '<div class="opt-first-container"><span class="exact-price">Intensity: <span>'+a+'</span></span><div class="opt-intense-container">';
                    $intensity = parseInt(a);
                    for(var i = 1; i < 12; i++){
                        temp += '<div class="';
                        if(i < $intensity){
                            temp += 'opt-full';
                        }
                        temp += '"></div>';
                    }
                    temp += '</div></div><span>'+b+'</span>';
                    $(ref).find('.opt-container').html(temp);
                }
            });
            $(ref).find('div.right-item').children().wrap('<div class="opt-pricing"></div>');
            $(ref).find('div.right-item').append('<a class="optButton right-item" href='+href+' style="text-align: center; border: 1px solid lightgrey; padding: 2px; color: black; margin-left: 5px;">View More</a>');
        });
}
if(window.location.pathname.indexOf("/p/") > 0 || window.location.pathname.indexOf("/products/") > 0 ){
    $(".swipe-wrap").on('DOMNodeRemoved', function(e) {
        setTimeout(function(){
            load();
        },200);
    });
    load();
}


if(window.location.href.indexOf("/catalog#capsules") > 0 || $("#main-product-title").text() == "Capsules"){

    $("#navbar a").not().click(function(){
        stop = true;
        $("body").removeClass("opt-viewMore");
    });
    $("#link-capsules").click(function(){
        ckOnFindElement("#productListForm",catagoryLoad,500);
    });
    catagoryLoad();
}