
$("body").addClass("opt-2-01-03");


/** This function waits x amount of INTERVALTIME waiting for given selector.
*   If the selector is found run function f.
*   If the function has reached x times INTERVALTIME it will timeout.
**/

//interval time is set to 1/10 of a second.
var INTERVALTIME = 100;
function ckOnFindElement(selector, f, x) {
  var count = 0;
  //console.log(x);
  var checkExist = setInterval(function() {
    
        console.log(count);
            try{
            if ($(selector).length > 0) {
                f();
                
                clearInterval(checkExist);
                //console.log("found:" + selector);
                //show nothing
            } else {
                if(count >= x){
                //when timeout show modal
                clearInterval(checkExist);
                //console.log("activationTimeOut");
                }
                count++;
            }
            } catch(e){
            if(count >= x){
                //when timeout show modal
                clearInterval(checkExist);
                //console.log("activationTimeOut");
            }
            count++;
        }
    
  }, INTERVALTIME);
}

function ckOnFindCapsules(f, x) {
  var count = 0;
  //console.log(x);
  var checkExist = setInterval(function() {

    if(!$("body").hasClass("opt-found")){
        console.log(count);
        try{
        if ($("#main-product-title").length > 0) {

            if($("#main-product-title").text() == "Capsules"){
                    f();
                    clearInterval(checkExist);
                    $("body").addClass("opt-found");
                    $("body").addClass("opt-viewMore");
            }
        } else {
            if(count >= x){
            clearInterval(checkExist);
            }
            count++;
        }
        } catch(e){
        if(count >= x){
            //when timeout show modal
            clearInterval(checkExist);
            //console.log("activationTimeOut");
        }
        count++;
        }
    } else {
           clearInterval(checkExist);
    }
  }, INTERVALTIME);
}

function intense(url, callback) {
    $.get( url, function( data ) {
        var html = $.parseHTML(data),
        intensity = $(html).find('#product-description-title span').html(), description = "";
        if (intensity == NaN || intensity == undefined ) {
            description = "";
        } else {
            description = $(html).find('#product-description > div[itemprop="description"]').html().split("<br>")[0];
        }
        //console.log(intensity)
        if (callback) {
            callback(intensity, description)
        } else {
            return intensity;
        }
    });
}

function load(){
    $("body").addClass("opt-viewMore");
     $(".product").not(".opt-active-test").each(function(){
        $(this).find('.qtt-price').children().wrap('<div class="opt-pricing"></div>');
        $(this).find('.qtt-price').append('<div class="opt-container"></div>');
            $(this).children().find('.list-items.products li.product-container').each(function () {
                var href = $(this).find('a').attr('href');
                var ref = $(this);
                var temp = "";
                var intensity = "";
                intense(href, function (a,b) {
                    if (a == NaN || a == undefined ) {
                        $(ref).find('.opt-container').html('<span class="exact-price">Assortment</span><br><span>'+b+'</span>')
                    } else {
                        temp = '<div class="opt-first-container"><span class="exact-price">Intensity: <span>'+a+'</span></span><div class="opt-intense-container">';
                        intensity = parseInt(a);
                        for(var i = 1; i < 12; i++){
                            temp += '<div class="';
                            if(i < intensity){
                                temp += 'opt-full';
                            }
                            temp += '"></div>';
                        }
                        temp += '</div></div><span>'+b+'</span>';
                        $(ref).find('.opt-container').html(temp);
                    }
                });
                $(ref).find('div.right-item').children().wrap('<div class="opt-pricing"></div>');
                $(ref).find('div.right-item').append('<a class="optButton right-item" href='+href+' style="text-align:center;">View More</a>');
            });
           
            $(this).find("#productListForm > input").after('<div class="opt-continue-shopping"><a href="#" class="opt-continue-button">Continue Shopping?</a></div>');
            $(this).find("#productListForm > input").before('<div class="opt-content"><h2>Finished Shopping</h2></div>');
            $(this).find(".opt-continue-shopping").click(function(){    
                $(".back.menu-button.retina").click();
            });

            $(this).addClass("opt-active-test");
    });
}

function catagoryLoad(){
    $("body").toggleClass("opt-viewMore");
        $("#promotionBanners").after(`<div class="opt-banner-container">
            <div class="opt-first">
                <div><h1>Know what you want?</h1></div>
                <div><a href="#" class="opt-quick-order">QUICK ORDER</a></div>
            </div>
            <div class="opt-second">
                <div><h1>Want more information?</h1></div>
                <div><a href="#" class="opt-quick-order">DETAILED VIEW</a></div>
            </div>
        </div>`);

        $(".opt-second").click(function(){
            $("body").addClass("opt-viewMore");
        });

        $(".opt-first").click(function(){
            $("body").removeClass("opt-viewMore");
        });
        

        $('.qtt-price').children().wrap('<div class="opt-pricing"></div>');
        $('.qtt-price').append('<div class="opt-container"></div>');
        $('.list-items.products li.product-container').each(function () {
            var href = $(this).find('a').attr('href');
            var ref = $(this);
            var temp = "";
            var intensity = "";
            intense(href, function (a,b) {
                if (a == NaN || a == undefined ) {
                    $(ref).find('.opt-container').html('<span class="exact-price">Assortment</span><br><span>'+b+'</span>')
                } else {
                    temp = '<div class="opt-first-container"><span class="exact-price">Intensity: <span>'+a+'</span></span><div class="opt-intense-container">';
                    intensity = parseInt(a);
                    for(var i = 1; i < 12; i++){
                        temp += '<div class="';
                        if(i < intensity){
                            temp += 'opt-full';
                        }
                        temp += '"></div>';
                    }
                    temp += '</div></div><span>'+b+'</span>';
                    $(ref).find('.opt-container').html(temp);
                }
            });
            $(ref).find('div.right-item').children().wrap('<div class="opt-pricing"></div>');
          $(ref).find('div.right-item').append('<a class="optButton right-item" href='+href+' style="text-align:center;">View More</a>');
        });
}

if(window.location.pathname.indexOf("/p/") > 0 || window.location.pathname.indexOf("/products/") > 0 ){
    $(".swipe-wrap").on('DOMNodeRemoved', function(e) {
        setTimeout(function(){
            load();
        },400);
    });
    load();
}

//change the check to a capsule 
if(window.location.href.indexOf("/catalog") > 0){
    //poll  to see if its a capsule page
    $("#navbar a").click(function(){
        $("body").removeClass("opt-found");
        ckOnFindCapsules(catagoryLoad,50);
    });
    ckOnFindCapsules(catagoryLoad,50);
}  