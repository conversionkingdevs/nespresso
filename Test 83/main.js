

var variation = 3;


var config = { attributes: true, childList: true, characterData: true, subtree: true, attributeOldValue: true, characterDataOldValue: true };

var text = [
    "Discover EasyOrder",
    "Never run out of coffee",
    "Free delivery with EasyOrder",
    "Setup Recurring Coffee order"
]

var country = window.location.pathname.split("/")[1];

var link = "/au/en/easyorder-coffee-pod-orders";


var menuObserver = new MutationObserver(function(mutations) {
    mutations.forEach(function(mutation) {
        if(jQuery(window).width() > 996){
            var element = jQuery(".HeaderNavigationBar__nav.ResponsiveContainer li:nth-child(1) > div");
            if(checkElement(element)){
                addDesktop();
            }
        } else {
            var element = jQuery(".HeaderNavigationBar__nav.ResponsiveContainer li:nth-child(2) > div");
            if(checkElement(element)){
                addMobile();
            }
        }
    });
});

function addDesktop(){
    jQuery(".HeaderNavigationBar__nav.ResponsiveContainer li:nth-child(1) .HeaderNavigationBarDropdown__medium-links").append('<li class="HeaderNavigationBarDropdown__medium-links-item"><a class="AccessibleLink HeaderNavigationBarDropdown__medium-link" href="'+link+'" style="color: rgb(65, 130, 0);">'+text[variation]+'</a></li>');
}

function addMobile(){
    jQuery(".HeaderNavigationBar__nav.ResponsiveContainer li:nth-child(2) .HeaderNavigationBarDropdown__medium-links").append('<li class="HeaderNavigationBarDropdown__medium-links-item"><a class="AccessibleLink HeaderNavigationBarDropdown__medium-link" href="'+link+'" style="color: rgb(65, 130, 0);">'+text[variation]+'</a></li>');
}

function checkElement(element){
    if(jQuery(element).children().length > 0){
        if(!jQuery(element).children(0).hasClass("opt-added")){
            jQuery(element).children(0).addClass("opt-added");
            return true;
        }
    }
    return false;
}

function addObservers(){
    var menuTarget = jQuery(".HeaderNavigationBar > div")[0];
    menuObserver.disconnect();
    menuObserver.observe(menuTarget, config);
}

function loadopt() {
    jQuery("body").addClass("opt-83");
    addObservers();
}

function defer(method, selector) {
    if (window.jQuery) {
        if (jQuery(selector).length > 0){
            method();
           
        } else {
            setTimeout(function() { defer(method, selector); }, 50);
        }  
    } else {
         setTimeout(function() { defer(method, selector); }, 50);
    }    
}

defer(loadopt, '.HeaderNavigationBar > div');
