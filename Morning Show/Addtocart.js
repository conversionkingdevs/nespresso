function watcher(loadingIdentifier, callback){
    (function(open) {
        XMLHttpRequest.prototype.open = function(m, u, a, us, p) {
            this.addEventListener('readystatechange', function() {
                try{
                    if(this.responseURL.indexOf(loadingIdentifier) >= 0 && this.readyState == 4) {
                        console.log("Callback",this.response);
                        callback();
                    }
                } catch(e){
                }
            }, false);
            open.call(this, m, u, a, us, p);
        };
    })(XMLHttpRequest.prototype.open);
}

watcher('update', function () {
    setTimeout(function () {
    window.newCart = parseInt(jQuery('.MiniBasketButton').text().split('(')[1].split(')')[0])
    console.log('here',window.optCartCheckSet, '2', window.newCart);
    if (window.newCart && window.optCartCheckSet && window.newCart > window.optCartCheckSet) {
        window.location = '/au/en/checkout'
    }
    },100);
});


defer(function () {
    jQuery('.add-to-bag').click(function () {
      if (parseInt(jQuery(this).find('.AddToBagButtonLarge__quantity').text()) > 0) {
            window.location = '/au/en/checkout';
      }
      window.optCartCheckSet = parseInt(jQuery('.MiniBasketButton').text().split('(')[1].split(')')[0])
      window.optAddeditem = jQuery(this).attr('data-product-id');
      window.CartManager.updateItem('erp.au.b2c/prod/' + optAddeditem, 1);
    });
  },'.add-to-bag');

  