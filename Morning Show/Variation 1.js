function moveAddToCarts () {
    window.count = 0;
      jQuery('.add-to-bag').each(function () {
        console.log(count)
        count ++;
          var id = jQuery(this).attr('data-product-id');
          jQuery(this).appendTo(jQuery('.'+id))
      })
  }
  
  
  function addSelectFunctionality () {
      jQuery('.opt_ProductInformation .opt_Colour').click(function () {
          // Selected Class
          var container = jQuery(this).closest('.opt_ProductInformation');
          container.find('.opt_Selected').removeClass('opt_Selected');
          container.closest('.opt_Content').find('.opt_Show').removeClass('opt_Show');
          jQuery(this).addClass('opt_Selected');
  
          // Show Right Add to Cart
          var id = jQuery(this).attr('data-id');
          container.find('.opt_Show').removeClass('opt_Show');
          container.closest('.opt_Content').find('img[data-image="'+id+'"]').addClass('opt_Show');
          jQuery('div[data-product-id="'+id+'"]').addClass('opt_Show');
          
      })
  }
  
  function initialiseSelected() {
      jQuery('.opt_Selected').each(function () {
          var container = jQuery(this).closest('.opt_ProductInformation');
          var id = jQuery(this).attr('data-id');
          container.find('.opt_Show').removeClass('opt_Show');
          container.closest('.opt_Content').find('img[data-image="'+id+'"]').addClass('opt_Show');
          jQuery('div[data-product-id="'+id+'"]').addClass('opt_Show');
      })
  }
    
  function defer(method, selector) {
      if (window.jQuery) {
        if (jQuery(selector).length > 0){
          method();
        } else {
          setTimeout(function() { defer(method, selector); }, 50);
        }  
      } else {
        setTimeout(function() { defer(method, selector); }, 50);
      }    
    }
    
  function start () {
    var MorningShowOfferImage = jQuery('img[alt="Vertuo Range Discovery Banner"]').attr('src'),
      MorningShowOfferLink = jQuery('img[alt="Vertuo Range Discovery Banner"]').parent().attr('href'),
      TANDCImage = jQuery('img[alt="Terms and Conditions Banner"]').attr('src'),
      TANDCLink = jQuery('img[alt="Terms and Conditions Banner"]').parent().attr('href');
  
  jQuery('#main').append(`<div class="opt_NewContainer responsive">
      <div class="opt_TopBanner">
          <img src="//service.maxymiser.net/cm/images-eu/1/1/1/7A60ED787C5DB8FC8BE89CBBF163148AE79D80D21A0962DD932D30D6D5A264FE/nespresso-au/Good-Morning-Monday/desktop-top-1.png" />
      </div>
      <div id="opt_SecondBanner" class="opt_TextBanner">
          <h1 class="opt_h1">The VertuoPlus<br>Morning Show Offer</h1>
          <p class="opt_P opt_SmallText">For a limited time only, <strong>Nespresso</strong> are offering fantastic value on selected <strong>VertuoPlus machines</strong>. Choose from 3 machines, and receive your complimentary gifts and coffee today.</p>
          <p class="opt_P opt_NormalText opt_Bold">RRP $375, save yourself <span class="opt_LargerText">$176</span> today only!</p>
      </div>
      <div id="opt_ThirdBanner" class="opt_WhiteBanner opt_ProductBanner">
          <div class="opt_Divide"></div>
          <div class="opt_Content">
              <div class="opt_ImageContainer">
                  <img src="//service.maxymiser.net/cm/images-eu/1/1/1/DBE0DDDA9D78E416E82445BC46D7FDF109FEC0A3F1072E26E0D92BD0BC487BFF/nespresso-au/Good-Morning-Monday/black-2.png" data-image="105208" />
                  <img src="//service.maxymiser.net/cm/images-eu/1/1/1/18480DF06B0088BB79DA62EC1CBCF7AD806B7866B49CADCF7B2E65B23C84466C/nespresso-au/Good-Morning-Monday/silver-1.png" data-image="105209"/>
              </div>
              <div class="opt_ProductInformation">
                  <p class="opt_Heading">VERTUOPLUS ROUND TOP</p>
                  <p class="opt_WasPricing">Was <span class="opt_Gold opt_Strikout">$375</span> Now <span class="opt_Green opt_Larger">$199</span> Today Only!</p>
                  <p class="opt_Includes">Includes:</p>
                  <ul class="opt_IncludeList">
                      <li>+ VertuoPlus Machine <span class="opt_Bold">(RRP $299)</span></li>
                      <li>+ Vertuo Coffee Mug Set <span class="opt_Bold">(RRP $39)</span></li>
                      <li>+ 12 Capsule Sample Set <span class="opt_Bold"></span></li>
                      <li>+ Vertuo 4 Sleeve Assortment <span class="opt_Bold">(RRP $39)</span></li>
                      <li>+ FREE Courier Delivery <span class="opt_Bold"></span></li>
                  </ul>
                  <div class="opt_Selector">
                      <p class="opt_Bold">Select Colour:</p>
                      <div class="opt_Colour opt_Black opt_Selected" data-id="105208">
                          Black
                      </div>
                      <div class="opt_Colour opt_Silver" data-id="105209">
                          Silver
                      </div>
                  </div>
                  <div class="opt_Buttons 105208 105209">
                  </div>
              </div>
          </div>
      </div>
      <div id="opt_FourthBanner" class="opt_WhiteBanner opt_ProductBanner">
          <div class="opt_Divide"></div>
          <div class="opt_Content">
              <div class="opt_ImageContainer">
                  <img src="//service.maxymiser.net/cm/images-eu/1/1/1/34C95156458E8930CDEB6DD29AA63F0F2F9B511CFAF72910D61E0580DBEA7B7F/nespresso-au/Good-Morning-Monday/black-3.png" data-image="105210" />
              </div>
              <div class="opt_ProductInformation">
                  <p class="opt_Heading">VERTUOPLUS FLAT TOP</p>
                  <p class="opt_WasPricing">Was <span class="opt_Gold opt_Strikout">$375</span> Now <span class="opt_Green opt_Larger">$199</span> Today Only!</p>
                  <p class="opt_Includes">Includes:</p>
                  <ul class="opt_IncludeList">
                      <li>+ VertuoPlus Machine <span class="opt_Bold">(RRP $299)</span></li>
                      <li>+ Vertuo Coffee Mug Set <span class="opt_Bold">(RRP $39)</span></li>
                      <li>+ 12 Capsule Sample Set <span class="opt_Bold"></span></li>
                      <li>+ Vertuo 4 Sleeve Assortment <span class="opt_Bold">(RRP $39)</span></li>
                      <li>+ FREE Courier Delivery <span class="opt_Bold"></span></li>
                  </ul>
                  <div class="opt_Selector">
                      <p class="opt_Bold">Select Colour:</p>
                      <div class="opt_Colour opt_Black opt_Selected" data-id="105210">
                          Black
                      </div>
                  </div>
                  <div class="opt_Buttons 105210">
                  </div>
              </div>
          </div>
      </div>
      <div id="opt_FifthBanner" class="opt_BottomBanner">
          <div class="opt_Header">
              <p>As Seen on the morning show</p>
          </div>
          <div class="opt_VideoContent">
              <div class="opt_left opt_Half">
                  <iframe src="https://www.youtube.com/embed/Ydwau1d9_yA?rel=0&amp;autoplay=1&amp;mute=1&amp;enablejsapi=1&amp;origin=https%3A%2F%2Fwww.nespresso.com" frameborder="0"; encrypted-media" allowfullscreen="" data-gtm-yt-inspected-2202016_370="true" id="814108724"></iframe>
              </div>
              <div class="opt_right opt_Half">
                  <div class="opt_top opt_HalfHeight">
                      <a href="`+TANDCLink+`" style="background-image:url(`+TANDCImage+`)">
                      </a>
                  </div>
                  <div class="opt_bottom opt_HalfHeight">
                      <a href="`+MorningShowOfferLink+`" style="background-image:url(`+MorningShowOfferImage+`)">
                      </a>
                  </div>
              </div>
          </div>
      </div>
  </div>`);
    
    setTimeout(function () {
      moveAddToCarts();
        addSelectFunctionality();
        initialiseSelected();
      jQuery('.Footer__bar').before('<div class="opt_Divide opt_Grey"></div>');
    },100);
  }
    
    defer(start, '.add-to-bag[data-product-id="105208"]')
    defer(function () {
          jQuery('.Footer__bar').before('<div class="opt_Divide opt_Grey"></div>');
    }, '.AccessibleLink.FooterLink');