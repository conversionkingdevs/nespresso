
function defer(method, selector) {
    if (window.jQuery) {
        if (jQuery(selector).length > 0){
            method();
        } else {
            setTimeout(function() { defer(method, selector); }, 50);
        }  
    } else {
         setTimeout(function() { defer(method, selector); }, 50);
    }    
}
defer(function () {
    var url = window.location.pathname.split('/').pop();
    var content = {
        "original": {
            "dimage": '//service.maxymiser.net/cm/images-eu/1/1/1/18152994B93A04B170C6CD6B03F1B06701D7627168AAC8CBCBD9F34B08906C0E/nespresso-au/Test-84-Vertuo-Machine-Direction/V2_Desktop.png',
            "mimage": '//service.maxymiser.net/cm/images-eu/1/1/1/4EE1B8B3711EF2E0A99DDBC7C6DAC17B3FD31D5BAFA699AD6A30EA3C165917D7/nespresso-au/Test-84-Vertuo-Machine-Direction/V2_Mobile.png',
            "link": ''
        }
    }

    function updateBanner () {
        jQuery('.PromotionBanner .Banner .AccessibleLink.Banner__link').attr('href', content[url].link);
        jQuery('.PromotionBanner .Banner .AccessibleLink.Banner__link img').attr('src', content[url].image);
    }

    if (url == "original" || url == "vertuo") {
        updateBanner ()
    }
}, '.PromotionBanner .Banner .AccessibleLink.Banner__link > img');
