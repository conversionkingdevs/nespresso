function defer(method, selector) {
    if (window.jQuery) {
        if (jQuery(selector).length > 0){
            method();
        } else {
            setTimeout(function() { defer(method, selector); }, 50);
        }  
    } else {
         setTimeout(function() { defer(method, selector); }, 50);
    }    
}
  defer(function () {
    var url = window.location.pathname.split('/').pop();
    var content = {
      "original": {
        "dimage": '//service.maxymiser.net/cm/images-eu/1/1/1/DDEE0E53D3202F05EC8481E66C0B79540EFD8EF9C15D753BE74A7F4F5F6DC444/nespresso-au/Test-84-Vertuo-Machine-Direction/V1_Desktop.png',
        "mimage": '//service.maxymiser.net/cm/images-eu/1/1/1/2E3A24B2162974CFECE21E9746C0B06B7A4BCEABD6372E8D82F6B06AAABD8298/nespresso-au/Test-84-Vertuo-Machine-Direction/V1_Mobile.png',
        "link": '/au/en/order/machines/vertuo'
      }
    }

    function updateBanner () {
      jQuery('.PromotionBanner .Banner .AccessibleLink.Banner__link').attr('href', content["original"].link)
      jQuery('.PromotionBanner .Banner .AccessibleLink.Banner__link > img').addClass('opt_oldImage');
      jQuery('.PromotionBanner .Banner .AccessibleLink.Banner__link').append('<img class="opt_dimage" src="'+content["original"].dimage+'"/>');
      jQuery('.PromotionBanner .Banner .AccessibleLink.Banner__link').append('<img class="opt_mimage" src="'+content["original"].mimage+'"/>');
    }

    updateBanner ()
  }, '.PromotionBanner .Banner .AccessibleLink.Banner__link > img');