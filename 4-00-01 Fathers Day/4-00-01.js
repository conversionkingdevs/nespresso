
// USeful for Nespresso mobile to check whether your script has run.

 function run(x) {
    try {
        Start();
    }
    catch (err) {
        setTimeout(function () {
            run(x);
        },x)
    }
   }
   run(50)

function Start() {
var html= '<div class="optContent"> <div class="optBackground"> <div class="optSection" id="optSection1"> <h1 class="optUpper"> Dual Offer </h1> </div><div class="optSection" id="optSection2"> <div class="optTopRow"> <div class="optLeft"> <img src="https://image.ibb.co/jb2OfQ/creatistalogo.png"> <p class="optGoldy"> Why make coffee when you can create art? </p></div><div class="optRight"> <p class="optLarger"> Purchase Any Nespresso machine before 17 September and recieve $60 off your coffee order AND save $40 </p><p class="optRightSmaller"> Coffee must be purchased in the same transaction as the machine. <a class="optBlack" href="//nespresso.com">T&Cs apply</a> </p></div></div><div class="optBottomRow"> <div class="optLeft"> <img src="https://image.ibb.co/bzALS5/milk_drop.png"> </div><div class="optRight"> <img src="https://image.ibb.co/mYjEn5/milkdrop2.png"> </div></div></div></div><div class="optSection" id="optSection3"><div class="optStepContentText"><p class="optText optSectionTitle">How to benefit from your order</p><p class="optBolderText">Redeem your offer online in three simple steps</p><div class="optTextCont"><span class="optCircle">1</span><p class="optStepTitle">Step 1 - Select your machine</p></div><p class="optSmallest">All of our machines qualify for this offer. <a href="nespresso.com" class="optOrange">Shop the entire range.</a><br>Here are some of our best sellers:</p></div><div class="optSwiper optMachines"></div></div><div class="optSection" id="optSection4"><div class="optStepContentText"><div class="optTextCont"><span class="optCircle">2</span><p class="optStepTitle">Step 2 - Select your coffee</p></div><p class="optSmallest">A minimum of 100 capsules are required to redeem your coffee discount. All of our capsules qualify for this offer. <br><a href="nesspress.com" class="optOrange">Shop the entire range.</a> <br>We recommend our <i>Tribute to Milano</i> assortment packs:</p></div><div class="optSwiper optCapsules"></div></div><div class="optSection" id="optSection4"><div class="optStepContentText"><div class="optTextCont"><span class="optCircle">3</span><p class="optStepTitle">Step 3 - Checkout</p></div><p class="optSmallest"> Your discounts will automatically be applied.</p><button class="optGreen" id="optCheckout">CHECKOUT</button></div></div></div>'

jQuery('#freeHTML').html(html);

function loadSwiper () {
    loadMachines();
    loadCapsules();

}

function loadMachines(){
  var slidesMachines = "";
  var dataArray = [{
      img:"https://image.ibb.co/gZ7Zn5/creatistaplus.png",
      title:"Creatista Plus",
      price:"799.00",
      button:'<button type="button" id="ta-add-to-cart-BNE800BSS" aria-labelledby="aria-BNE800BSS" class="button-primary pull-right button-green two-parts add-to-cart" data-ng-class="{\'button-green two-parts add-to-cart\': currentProduct.addToCartButton.activated, \'add-to-cart disabled two-parts\': !currentProduct.addToCartButton.activated}" data-product-id="BNE800BSS" data-min="0" data-step="1" data-max="9990" data-orientation="top" data-use-custom-third-line="false" data-force-page-reload="" data-product-name="Creatista Plus Breville Stainless Steel" data-number-of-units="1" aria-disabled="false" data-defaultcontent=" <img src=&quot;/mosaic/_ui/img/Elements/cross_plus.png&quot; alt=&quot;&quot; width=&quot;16&quot;> "> <span data-ng-if="currentProduct.addToCartButton.activated" class="visually-hidden ng-binding ng-scope" id="aria-BNE800BSS" tabindex="-1"> You have <span class="quantity">0</span> of Creatista Plus Breville Stainless Steel in your basket, click on this button to <span class="label">Add to basket</span>.</span> <span data-ng-if="currentProduct.addToCartButton.activated" aria-hidden="true" class="left-part quantity ng-scope" data-product-id="BNE800BSS"> <img src="/mosaic/_ui/img/Elements/cross_plus.png" alt="" width="16"> </span> <span data-ng-if="currentProduct.addToCartButton.activated" aria-hidden="true" class="label right-part ng-scope">Add to basket</span> </button>'
  },{
      img:"https://image.ibb.co/gZ7Zn5/creatistaplus.png",
      title:"Creatista Plus",
      price:"799.00",
      button:'<button type="button" id="ta-add-to-cart-BNE800BSS" aria-labelledby="aria-BNE800BSS" class="button-primary pull-right button-green two-parts add-to-cart" data-ng-class="{\'button-green two-parts add-to-cart\': currentProduct.addToCartButton.activated, \'add-to-cart disabled two-parts\': !currentProduct.addToCartButton.activated}" data-product-id="BNE800BSS" data-min="0" data-step="1" data-max="9990" data-orientation="top" data-use-custom-third-line="false" data-force-page-reload="" data-product-name="Creatista Plus Breville Stainless Steel" data-number-of-units="1" aria-disabled="false" data-defaultcontent=" <img src=&quot;/mosaic/_ui/img/Elements/cross_plus.png&quot; alt=&quot;&quot; width=&quot;16&quot;> "> <span data-ng-if="currentProduct.addToCartButton.activated" class="visually-hidden ng-binding ng-scope" id="aria-BNE800BSS" tabindex="-1"> You have <span class="quantity">0</span> of Creatista Plus Breville Stainless Steel in your basket, click on this button to <span class="label">Add to basket</span>.</span> <span data-ng-if="currentProduct.addToCartButton.activated" aria-hidden="true" class="left-part quantity ng-scope" data-product-id="BNE800BSS"> <img src="/mosaic/_ui/img/Elements/cross_plus.png" alt="" width="16"> </span> <span data-ng-if="currentProduct.addToCartButton.activated" aria-hidden="true" class="label right-part ng-scope">Add to basket</span> </button>'
  },{
      img:"https://image.ibb.co/gZ7Zn5/creatistaplus.png",
      title:"Creatista Plus",
      price:"799.00",
      button:'<button type="button" id="ta-add-to-cart-BNE800BSS" aria-labelledby="aria-BNE800BSS" class="button-primary pull-right button-green two-parts add-to-cart" data-ng-class="{\'button-green two-parts add-to-cart\': currentProduct.addToCartButton.activated, \'add-to-cart disabled two-parts\': !currentProduct.addToCartButton.activated}" data-product-id="BNE800BSS" data-min="0" data-step="1" data-max="9990" data-orientation="top" data-use-custom-third-line="false" data-force-page-reload="" data-product-name="Creatista Plus Breville Stainless Steel" data-number-of-units="1" aria-disabled="false" data-defaultcontent=" <img src=&quot;/mosaic/_ui/img/Elements/cross_plus.png&quot; alt=&quot;&quot; width=&quot;16&quot;> "> <span data-ng-if="currentProduct.addToCartButton.activated" class="visually-hidden ng-binding ng-scope" id="aria-BNE800BSS" tabindex="-1"> You have <span class="quantity">0</span> of Creatista Plus Breville Stainless Steel in your basket, click on this button to <span class="label">Add to basket</span>.</span> <span data-ng-if="currentProduct.addToCartButton.activated" aria-hidden="true" class="left-part quantity ng-scope" data-product-id="BNE800BSS"> <img src="/mosaic/_ui/img/Elements/cross_plus.png" alt="" width="16"> </span> <span data-ng-if="currentProduct.addToCartButton.activated" aria-hidden="true" class="label right-part ng-scope">Add to basket</span> </button>'
  },{
      img:"https://image.ibb.co/gZ7Zn5/creatistaplus.png",
      title:"Creatista Plus",
      price:"799.00",
      button:'<button type="button" id="ta-add-to-cart-BNE800BSS" aria-labelledby="aria-BNE800BSS" class="button-primary pull-right button-green two-parts add-to-cart" data-ng-class="{\'button-green two-parts add-to-cart\': currentProduct.addToCartButton.activated, \'add-to-cart disabled two-parts\': !currentProduct.addToCartButton.activated}" data-product-id="BNE800BSS" data-min="0" data-step="1" data-max="9990" data-orientation="top" data-use-custom-third-line="false" data-force-page-reload="" data-product-name="Creatista Plus Breville Stainless Steel" data-number-of-units="1" aria-disabled="false" data-defaultcontent=" <img src=&quot;/mosaic/_ui/img/Elements/cross_plus.png&quot; alt=&quot;&quot; width=&quot;16&quot;> "> <span data-ng-if="currentProduct.addToCartButton.activated" class="visually-hidden ng-binding ng-scope" id="aria-BNE800BSS" tabindex="-1"> You have <span class="quantity">0</span> of Creatista Plus Breville Stainless Steel in your basket, click on this button to <span class="label">Add to basket</span>.</span> <span data-ng-if="currentProduct.addToCartButton.activated" aria-hidden="true" class="left-part quantity ng-scope" data-product-id="BNE800BSS"> <img src="/mosaic/_ui/img/Elements/cross_plus.png" alt="" width="16"> </span> <span data-ng-if="currentProduct.addToCartButton.activated" aria-hidden="true" class="label right-part ng-scope">Add to basket</span> </button>'
  }]


  for(var i = 0; i < dataArray.length; i++){
    var slide = '<div class="swiper-slide">';
            slide += '<img class="optImage" src="'+dataArray[i].img+'">';
            slide += '<div class="optContentS">';
                slide += '<p class="optSlideTitle">'+dataArray[i].title+'</p>';
                slide += '<p class="optOrange">'+dataArray[i].price+'</p>';
            slide += '</div>';
            slide += ''+dataArray[i].button+'';
        slide += '</div>';
    slidesMachines += slide;
  }

  jQuery(".optSwiper.optMachines").append(`
    <div class="swiper-container">
    <!-- Additional required wrapper -->
        <div class="swiper-wrapper">
            `+slidesMachines+`
        </div>
    <!-- If we need pagination -->
        <div class="swiper-pagination"></div>
        </div>
    <!-- If we need navigation buttons -->
        <div class="swiper-button-prev"></div>
        <div class="swiper-button-next"></div>
  `);

    var machineSwiper = new Swiper ('.optMachines .swiper-container', {
    // Optional parameters
    direction: 'horizontal',
    pagination: '.swiper-pagination',
    paginationClickable: true,
    loop: true,
     // Navigation arrows
    nextButton: '.optMachines .swiper-button-next',
    prevButton: '.optMachines .swiper-button-prev',
    slidesPerView: 4
  });
}

  function loadCapsules(){
  var slidesCapsules = "";
  var dataArray = [{
      img:"https://image.ibb.co/gZ7Zn5/creatistaplus.png",
      title:"Creatista CAPSULE",
      price:"799.00",
      button:'<button type="button" id="ta-add-to-cart-BNE800BSS" aria-labelledby="aria-BNE800BSS" class="button-primary pull-right button-green two-parts add-to-cart" data-ng-class="{\'button-green two-parts add-to-cart\': currentProduct.addToCartButton.activated, \'add-to-cart disabled two-parts\': !currentProduct.addToCartButton.activated}" data-product-id="BNE800BSS" data-min="0" data-step="1" data-max="9990" data-orientation="top" data-use-custom-third-line="false" data-force-page-reload="" data-product-name="Creatista Plus Breville Stainless Steel" data-number-of-units="1" aria-disabled="false" data-defaultcontent=" <img src=&quot;/mosaic/_ui/img/Elements/cross_plus.png&quot; alt=&quot;&quot; width=&quot;16&quot;> "> <span data-ng-if="currentProduct.addToCartButton.activated" class="visually-hidden ng-binding ng-scope" id="aria-BNE800BSS" tabindex="-1"> You have <span class="quantity">0</span> of Creatista Plus Breville Stainless Steel in your basket, click on this button to <span class="label">Add to basket</span>.</span> <span data-ng-if="currentProduct.addToCartButton.activated" aria-hidden="true" class="left-part quantity ng-scope" data-product-id="BNE800BSS"> <img src="/mosaic/_ui/img/Elements/cross_plus.png" alt="" width="16"> </span> <span data-ng-if="currentProduct.addToCartButton.activated" aria-hidden="true" class="label right-part ng-scope">Add to basket</span> </button>'
  },{
      img:"https://image.ibb.co/gZ7Zn5/creatistaplus.png",
      title:"Creatista CAPSULE",
      price:"799.00",
      button:'<button type="button" id="ta-add-to-cart-BNE800BSS" aria-labelledby="aria-BNE800BSS" class="button-primary pull-right button-green two-parts add-to-cart" data-ng-class="{\'button-green two-parts add-to-cart\': currentProduct.addToCartButton.activated, \'add-to-cart disabled two-parts\': !currentProduct.addToCartButton.activated}" data-product-id="BNE800BSS" data-min="0" data-step="1" data-max="9990" data-orientation="top" data-use-custom-third-line="false" data-force-page-reload="" data-product-name="Creatista Plus Breville Stainless Steel" data-number-of-units="1" aria-disabled="false" data-defaultcontent=" <img src=&quot;/mosaic/_ui/img/Elements/cross_plus.png&quot; alt=&quot;&quot; width=&quot;16&quot;> "> <span data-ng-if="currentProduct.addToCartButton.activated" class="visually-hidden ng-binding ng-scope" id="aria-BNE800BSS" tabindex="-1"> You have <span class="quantity">0</span> of Creatista Plus Breville Stainless Steel in your basket, click on this button to <span class="label">Add to basket</span>.</span> <span data-ng-if="currentProduct.addToCartButton.activated" aria-hidden="true" class="left-part quantity ng-scope" data-product-id="BNE800BSS"> <img src="/mosaic/_ui/img/Elements/cross_plus.png" alt="" width="16"> </span> <span data-ng-if="currentProduct.addToCartButton.activated" aria-hidden="true" class="label right-part ng-scope">Add to basket</span> </button>'
  },{
      img:"https://image.ibb.co/gZ7Zn5/creatistaplus.png",
      title:"Creatista CAPSULE",
      price:"799.00",
      button:'<button type="button" id="ta-add-to-cart-BNE800BSS" aria-labelledby="aria-BNE800BSS" class="button-primary pull-right button-green two-parts add-to-cart" data-ng-class="{\'button-green two-parts add-to-cart\': currentProduct.addToCartButton.activated, \'add-to-cart disabled two-parts\': !currentProduct.addToCartButton.activated}" data-product-id="BNE800BSS" data-min="0" data-step="1" data-max="9990" data-orientation="top" data-use-custom-third-line="false" data-force-page-reload="" data-product-name="Creatista Plus Breville Stainless Steel" data-number-of-units="1" aria-disabled="false" data-defaultcontent=" <img src=&quot;/mosaic/_ui/img/Elements/cross_plus.png&quot; alt=&quot;&quot; width=&quot;16&quot;> "> <span data-ng-if="currentProduct.addToCartButton.activated" class="visually-hidden ng-binding ng-scope" id="aria-BNE800BSS" tabindex="-1"> You have <span class="quantity">0</span> of Creatista Plus Breville Stainless Steel in your basket, click on this button to <span class="label">Add to basket</span>.</span> <span data-ng-if="currentProduct.addToCartButton.activated" aria-hidden="true" class="left-part quantity ng-scope" data-product-id="BNE800BSS"> <img src="/mosaic/_ui/img/Elements/cross_plus.png" alt="" width="16"> </span> <span data-ng-if="currentProduct.addToCartButton.activated" aria-hidden="true" class="label right-part ng-scope">Add to basket</span> </button>'
  },{
      img:"https://image.ibb.co/gZ7Zn5/creatistaplus.png",
      title:"Creatista CAPSULE",
      price:"799.00",
      button:'<button type="button" id="ta-add-to-cart-BNE800BSS" aria-labelledby="aria-BNE800BSS" class="button-primary pull-right button-green two-parts add-to-cart" data-ng-class="{\'button-green two-parts add-to-cart\': currentProduct.addToCartButton.activated, \'add-to-cart disabled two-parts\': !currentProduct.addToCartButton.activated}" data-product-id="BNE800BSS" data-min="0" data-step="1" data-max="9990" data-orientation="top" data-use-custom-third-line="false" data-force-page-reload="" data-product-name="Creatista Plus Breville Stainless Steel" data-number-of-units="1" aria-disabled="false" data-defaultcontent=" <img src=&quot;/mosaic/_ui/img/Elements/cross_plus.png&quot; alt=&quot;&quot; width=&quot;16&quot;> "> <span data-ng-if="currentProduct.addToCartButton.activated" class="visually-hidden ng-binding ng-scope" id="aria-BNE800BSS" tabindex="-1"> You have <span class="quantity">0</span> of Creatista Plus Breville Stainless Steel in your basket, click on this button to <span class="label">Add to basket</span>.</span> <span data-ng-if="currentProduct.addToCartButton.activated" aria-hidden="true" class="left-part quantity ng-scope" data-product-id="BNE800BSS"> <img src="/mosaic/_ui/img/Elements/cross_plus.png" alt="" width="16"> </span> <span data-ng-if="currentProduct.addToCartButton.activated" aria-hidden="true" class="label right-part ng-scope">Add to basket</span> </button>'
  }];


  for(var i = 0; i < dataArray.length; i++){
    var slide = '<div class="swiper-slide">';
            slide += '<img class="optImage" src="'+dataArray[i].img+'">';
            slide += '<div class="optContentS">';
                slide += '<p class="optSlideTitle">'+dataArray[i].title+'</p>';
                slide += '<p class="optOrange">'+dataArray[i].price+'</p>';
            slide += '</div>';
            slide += ''+dataArray[i].button+'';
        slide += '</div>';
    slidesCapsules += slide;
  }

    jQuery(".optSwiper.optCapsules").append(`
    <div class="swiper-container">
        <div class="swiper-wrapper">
            `+slidesCapsules+`
        </div>
        <div class="swiper-pagination"></div>
        </div>
        <div class="swiper-button-prev"></div>
        <div class="swiper-button-next"></div>
  `);

  var capsuleSwiper = new Swiper ('.optCapsules .swiper-container', {
    // Optional parameters
    direction: 'horizontal',
    pagination: '.swiper-pagination',
    paginationClickable: true,
    loop: true,
     // Navigation arrows
    nextButton: '.optCapsules .swiper-button-next',
    prevButton: '.optCapsules .swiper-button-prev',
    slidesPerView: 4
  });
}


function getScript(src, callback) {
  var s = document.createElement('script');
  s.src = src;
  s.async = true;
  s.onreadystatechange = s.onload = function() {
    if (!callback.done && (!s.readyState || /loaded|complete/.test(s.readyState))) {
      callback.done = true;
      callback();
    }
  };
  document.querySelector('head').appendChild(s);
}

jQuery("head link[rel='stylesheet']").last().after("<link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/Swiper/3.4.2/css/swiper.min.css' type='text/css'>");

getScript("https://cdnjs.cloudflare.com/ajax/libs/Swiper/3.4.2/js/swiper.min.js", loadSwiper);
}

