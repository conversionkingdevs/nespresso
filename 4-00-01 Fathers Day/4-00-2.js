// Useful for Nespresso mobile to check whether your script has run.
    function getPath(obj, key, value, path) {

    if(typeof obj !== 'object') {
        return;
    }

    for(var k in obj) {
        if(obj.hasOwnProperty(k)) {
            var t = path;
            var v = obj[k];
            if(!path) {
                path = k;
            }
            else {
                path = path + '.' + k;
            }
            if(v === value) {
                if(key === k) {
                    return path;
                }
                else {
                    path = t;
                }
            }
            else if(typeof v !== 'object'){
                path = t;
            }
            var res = getPath(v, key, value, path);
            if(res) {
                return res;
            } 
        }
    }

}


function defer(method, selector) {
    if (window.jQuery) {
        if (jQuery(selector).length > 0){
            method();
        } else {
            setTimeout(function() { defer(method, selector) }, 50);
        }  
    } else {
         setTimeout(function() { defer(method, selector) }, 50);
    }    
}

 window.dataArrayMachines = [
	{
		"name": "Essenza Mini & Aero",
		"colors": [
			{
				"img": "/ecom/medias/sys_master/public/10280164032542/Delonghi-Red-8-Essenza-Mini-Main-684x378.jpg",
				"title": "Essenza Mini De\\'Longhi Ruby Red ",
				"price": "$249.00",
				"html": "<button type=\"button\" id=\"ta-add-to-cart-EN85RAE\" aria-labelledby=\"aria-EN85RAE\" class=\"button-primary pull-right button-green two-parts add-to-cart\" data-ng-class=\"{\\'button-green two-parts add-to-cart\': currentProduct.addToCartButton.activated, \'add-to-cart disabled two-parts\': !currentProduct.addToCartButton.activated}\" data-product-id=\"EN85RAE\" data-min=\"0\" data-step=\"1\" data-max=\"9990\" data-orientation=\"top\" data-use-custom-third-line=\"false\" data-force-page-reload=\"\" data-product-name=\"Essenza Mini De\'Longhi Ruby Red \" data-number-of-units=\"1\" aria-disabled=\"false\" data-defaultcontent=\"\n        <img src=&quot;/mosaic/_ui/img/Elements/cross_plus.png&quot; alt=&quot;&quot; width=&quot;16&quot;>\n    \">\n\n    <span data-ng-if=\"currentProduct.addToCartButton.activated\" class=\"visually-hidden ng-binding ng-scope\" id=\"aria-EN85RAE\" tabindex=\"-1\">\n        You have <span class=\"quantity\">0</span> of  Essenza Mini De\'Longhi Ruby Red  in your basket, click on this button to <span class=\"label\">Add to basket</span>.</span>\n    <span data-ng-if=\"currentProduct.addToCartButton.activated\" aria-hidden=\"true\" class=\"left-part quantity ng-scope\" data-product-id=\"EN85RAE\">\n        <img src=\"/mosaic/_ui/img/Elements/cross_plus.png\" alt=\"\" width=\"16\">\n    </span>\n    <span data-ng-if=\"currentProduct.addToCartButton.activated\" aria-hidden=\"true\" class=\"label right-part ng-scope\">Add to basket</span></button>",
				"url": "https://www.nespresso.com/au/en/order/machines/essenza-mini-delonghi-ruby-red",
				"colorrgba": "background-color: rgb(255, 0, 0);"
			},
			{
				"img": "/ecom/medias/sys_master/public/10280174223390/Delonghi-White-8-Essenza-Mini-Main-684x378.jpg",
				"title": "Essenza Mini De\'Longhi Pure White ",
				"price": "$249.00",
				"html": "<button type=\"button\" id=\"ta-add-to-cart-EN85WAE\" aria-labelledby=\"aria-EN85WAE\" class=\"button-primary pull-right button-green two-parts add-to-cart\" data-ng-class=\"{\'button-green two-parts add-to-cart\': currentProduct.addToCartButton.activated, \'add-to-cart disabled two-parts\': !currentProduct.addToCartButton.activated}\" data-product-id=\"EN85WAE\" data-min=\"0\" data-step=\"1\" data-max=\"9990\" data-orientation=\"top\" data-use-custom-third-line=\"false\" data-force-page-reload=\"\" data-product-name=\"Essenza Mini De\'Longhi Pure White \" data-number-of-units=\"1\" aria-disabled=\"false\" data-defaultcontent=\"\n        <img src=&quot;/mosaic/_ui/img/Elements/cross_plus.png&quot; alt=&quot;&quot; width=&quot;16&quot;>\n    \">\n\n    <span data-ng-if=\"currentProduct.addToCartButton.activated\" class=\"visually-hidden ng-binding ng-scope\" id=\"aria-EN85WAE\" tabindex=\"-1\">\n        You have <span class=\"quantity\">0</span> of  Essenza Mini De\'Longhi Pure White  in your basket, click on this button to <span class=\"label\">Add to basket</span>.</span>\n    <span data-ng-if=\"currentProduct.addToCartButton.activated\" aria-hidden=\"true\" class=\"left-part quantity ng-scope\" data-product-id=\"EN85WAE\">\n        <img src=\"/mosaic/_ui/img/Elements/cross_plus.png\" alt=\"\" width=\"16\">\n    </span>\n    <span data-ng-if=\"currentProduct.addToCartButton.activated\" aria-hidden=\"true\" class=\"label right-part ng-scope\">Add to basket</span></button>",
				"url": "https://www.nespresso.com/au/en/order/machines/essenza-mini-delonghi-pure-white",
				"colorrgba": "background-color: rgb(255, 255, 255);"
			},
			{
				"img": "/ecom/medias/sys_master/public/10280062812190/Breville-Grey-8-Essenza-Mini-Main-684x378.jpg",
				"title": "Essenza Mini Breville Intense Grey",
				"price": "$249.00",
				"html": "<button type=\"button\" id=\"ta-add-to-cart-BEC250GRY\" aria-labelledby=\"aria-BEC250GRY\" class=\"button-primary pull-right button-green two-parts add-to-cart\" data-ng-class=\"{\'button-green two-parts add-to-cart\': currentProduct.addToCartButton.activated, \'add-to-cart disabled two-parts\': !currentProduct.addToCartButton.activated}\" data-product-id=\"BEC250GRY\" data-min=\"0\" data-step=\"1\" data-max=\"9990\" data-orientation=\"top\" data-use-custom-third-line=\"false\" data-force-page-reload=\"\" data-product-name=\"Essenza Mini Breville Intense Grey\" data-number-of-units=\"1\" aria-disabled=\"false\" data-defaultcontent=\"\n        <img src=&quot;/mosaic/_ui/img/Elements/cross_plus.png&quot; alt=&quot;&quot; width=&quot;16&quot;>\n    \">\n\n    <span data-ng-if=\"currentProduct.addToCartButton.activated\" class=\"visually-hidden ng-binding ng-scope\" id=\"aria-BEC250GRY\" tabindex=\"-1\">\n        You have <span class=\"quantity\">0</span> of  Essenza Mini Breville Intense Grey in your basket, click on this button to <span class=\"label\">Add to basket</span>.</span>\n    <span data-ng-if=\"currentProduct.addToCartButton.activated\" aria-hidden=\"true\" class=\"left-part quantity ng-scope\" data-product-id=\"BEC250GRY\">\n        <img src=\"/mosaic/_ui/img/Elements/cross_plus.png\" alt=\"\" width=\"16\">\n    </span>\n    <span data-ng-if=\"currentProduct.addToCartButton.activated\" aria-hidden=\"true\" class=\"label right-part ng-scope\">Add to basket</span></button>",
				"url": "https://www.nespresso.com/au/en/order/machines/essenza-mini-breville-intense-grey",
				"colorrgba": "background-color: rgb(128, 128, 128);"
			},
			{
				"img": "/ecom/medias/sys_master/public/10280068907038/Breville-White-8-Essenza-Mini-Main-684x378.jpg",
				"title": "Essenza Mini Breville Pure White ",
				"price": "$249.00",
				"html": "<button type=\"button\" id=\"ta-add-to-cart-BEC250WHT\" aria-labelledby=\"aria-BEC250WHT\" class=\"button-primary pull-right button-green two-parts add-to-cart\" data-ng-class=\"{\'button-green two-parts add-to-cart\': currentProduct.addToCartButton.activated, \'add-to-cart disabled two-parts\': !currentProduct.addToCartButton.activated}\" data-product-id=\"BEC250WHT\" data-min=\"0\" data-step=\"1\" data-max=\"9990\" data-orientation=\"top\" data-use-custom-third-line=\"false\" data-force-page-reload=\"\" data-product-name=\"Essenza Mini Breville Pure White \" data-number-of-units=\"1\" aria-disabled=\"false\" data-defaultcontent=\"\n        <img src=&quot;/mosaic/_ui/img/Elements/cross_plus.png&quot; alt=&quot;&quot; width=&quot;16&quot;>\n    \">\n\n    <span data-ng-if=\"currentProduct.addToCartButton.activated\" class=\"visually-hidden ng-binding ng-scope\" id=\"aria-BEC250WHT\" tabindex=\"-1\">\n        You have <span class=\"quantity\">0</span> of  Essenza Mini Breville Pure White  in your basket, click on this button to <span class=\"label\">Add to basket</span>.</span>\n    <span data-ng-if=\"currentProduct.addToCartButton.activated\" aria-hidden=\"true\" class=\"left-part quantity ng-scope\" data-product-id=\"BEC250WHT\">\n        <img src=\"/mosaic/_ui/img/Elements/cross_plus.png\" alt=\"\" width=\"16\">\n    </span>\n    <span data-ng-if=\"currentProduct.addToCartButton.activated\" aria-hidden=\"true\" class=\"label right-part ng-scope\">Add to basket</span></button>",
				"url": "https://www.nespresso.com/au/en/order/machines/essenza-mini-breville-pure-white",
				"colorrgba": "background-color: rgb(255, 255, 255);"
			},
			{
				"img": "/ecom/medias/sys_master/public/10280158298142/Delonghi-Lime-8-Essenza-Mini-Main-684x378.jpg",
				"title": "Essenza Mini De\'Longhi Lime Green ",
				"price": "$249.00",
				"html": "<button type=\"button\" id=\"ta-add-to-cart-EN85LAE\" aria-labelledby=\"aria-EN85LAE\" class=\"button-primary pull-right button-green two-parts add-to-cart\" data-ng-class=\"{\'button-green two-parts add-to-cart\': currentProduct.addToCartButton.activated, \'add-to-cart disabled two-parts\': !currentProduct.addToCartButton.activated}\" data-product-id=\"EN85LAE\" data-min=\"0\" data-step=\"1\" data-max=\"9990\" data-orientation=\"top\" data-use-custom-third-line=\"false\" data-force-page-reload=\"\" data-product-name=\"Essenza Mini De\'Longhi Lime Green \" data-number-of-units=\"1\" aria-disabled=\"false\" data-defaultcontent=\"\n        <img src=&quot;/mosaic/_ui/img/Elements/cross_plus.png&quot; alt=&quot;&quot; width=&quot;16&quot;>\n    \">\n\n    <span data-ng-if=\"currentProduct.addToCartButton.activated\" class=\"visually-hidden ng-binding ng-scope\" id=\"aria-EN85LAE\" tabindex=\"-1\">\n        You have <span class=\"quantity\">0</span> of  Essenza Mini De\'Longhi Lime Green  in your basket, click on this button to <span class=\"label\">Add to basket</span>.</span>\n    <span data-ng-if=\"currentProduct.addToCartButton.activated\" aria-hidden=\"true\" class=\"left-part quantity ng-scope\" data-product-id=\"EN85LAE\">\n        <img src=\"/mosaic/_ui/img/Elements/cross_plus.png\" alt=\"\" width=\"16\">\n    </span>\n    <span data-ng-if=\"currentProduct.addToCartButton.activated\" aria-hidden=\"true\" class=\"label right-part ng-scope\">Add to basket</span></button>",
				"url": "https://www.nespresso.com/au/en/order/machines/essenza-mini-delonghi-lime-green",
				"colorrgba": "background-color: rgb(0, 128, 0);"
			},
			{
				"img": "/ecom/medias/sys_master/public/10279917584414/Breville-Black-8-Essenza-Mini-Main-684x378.jpg",
				"title": "Essenza Mini Breville Pure Black",
				"price": "$249.00",
				"html": "<button type=\"button\" id=\"ta-add-to-cart-BEC250BLK\" aria-labelledby=\"aria-BEC250BLK\" class=\"button-primary pull-right button-green two-parts add-to-cart\" data-ng-class=\"{\'button-green two-parts add-to-cart\': currentProduct.addToCartButton.activated, \'add-to-cart disabled two-parts\': !currentProduct.addToCartButton.activated}\" data-product-id=\"BEC250BLK\" data-min=\"0\" data-step=\"1\" data-max=\"9990\" data-orientation=\"top\" data-use-custom-third-line=\"false\" data-force-page-reload=\"\" data-product-name=\"Essenza Mini Breville Pure Black\" data-number-of-units=\"1\" aria-disabled=\"false\" data-defaultcontent=\"\n        <img src=&quot;/mosaic/_ui/img/Elements/cross_plus.png&quot; alt=&quot;&quot; width=&quot;16&quot;>\n    \">\n\n    <span data-ng-if=\"currentProduct.addToCartButton.activated\" class=\"visually-hidden ng-binding ng-scope\" id=\"aria-BEC250BLK\" tabindex=\"-1\">\n        You have <span class=\"quantity\">0</span> of  Essenza Mini Breville Pure Black in your basket, click on this button to <span class=\"label\">Add to basket</span>.</span>\n    <span data-ng-if=\"currentProduct.addToCartButton.activated\" aria-hidden=\"true\" class=\"left-part quantity ng-scope\" data-product-id=\"BEC250BLK\">\n        <img src=\"/mosaic/_ui/img/Elements/cross_plus.png\" alt=\"\" width=\"16\">\n    </span>\n    <span data-ng-if=\"currentProduct.addToCartButton.activated\" aria-hidden=\"true\" class=\"label right-part ng-scope\">Add to basket</span></button>",
				"url": "https://www.nespresso.com/au/en/order/machines/essenza-mini-breville-pure-black",
				"colorrgba": "background-color: rgb(0, 0, 0);"
			}
		]
	},
	{
		"name": "Essenza Mini",
		"colors": [
			{
				"img": "/ecom/medias/sys_master/public/10259171115038/M-0432-main-684x378.jpg",
				"title": "Essenza Mini Solo Breville Pure Black",
				"price": "$159.00",
				"html": "<button type=\"button\" id=\"ta-add-to-cart-BEC220BLK\" aria-labelledby=\"aria-BEC220BLK\" class=\"button-primary pull-right button-green two-parts add-to-cart\" data-ng-class=\"{\'button-green two-parts add-to-cart\': currentProduct.addToCartButton.activated, \'add-to-cart disabled two-parts\': !currentProduct.addToCartButton.activated}\" data-product-id=\"BEC220BLK\" data-min=\"0\" data-step=\"1\" data-max=\"9990\" data-orientation=\"top\" data-use-custom-third-line=\"false\" data-force-page-reload=\"\" data-product-name=\"Essenza Mini Solo Breville Pure Black\" data-number-of-units=\"1\" aria-disabled=\"false\" data-defaultcontent=\"\n        <img src=&quot;/mosaic/_ui/img/Elements/cross_plus.png&quot; alt=&quot;&quot; width=&quot;16&quot;>\n    \">\n\n    <span data-ng-if=\"currentProduct.addToCartButton.activated\" class=\"visually-hidden ng-binding ng-scope\" id=\"aria-BEC220BLK\" tabindex=\"-1\">\n        You have <span class=\"quantity\">0</span> of  Essenza Mini Solo Breville Pure Black in your basket, click on this button to <span class=\"label\">Add to basket</span>.</span>\n    <span data-ng-if=\"currentProduct.addToCartButton.activated\" aria-hidden=\"true\" class=\"left-part quantity ng-scope\" data-product-id=\"BEC220BLK\">\n        <img src=\"/mosaic/_ui/img/Elements/cross_plus.png\" alt=\"\" width=\"16\">\n    </span>\n    <span data-ng-if=\"currentProduct.addToCartButton.activated\" aria-hidden=\"true\" class=\"label right-part ng-scope\">Add to basket</span></button>",
				"url": "https://www.nespresso.com/au/en/order/machines/essenza-mini-breville-pure-black-solo",
				"colorrgba": "background-color: rgb(0, 0, 0);"
			},
			{
				"img": "/ecom/medias/sys_master/public/10259177078814/M-0435-main-684x378.jpg",
				"title": "Essenza Mini Solo De\'Longhi Pure White",
				"price": "$159.00",
				"html": "<button type=\"button\" id=\"ta-add-to-cart-EN85WSOLO\" aria-labelledby=\"aria-EN85WSOLO\" class=\"button-primary pull-right button-green two-parts add-to-cart\" data-ng-class=\"{\'button-green two-parts add-to-cart\': currentProduct.addToCartButton.activated, \'add-to-cart disabled two-parts\': !currentProduct.addToCartButton.activated}\" data-product-id=\"EN85WSOLO\" data-min=\"0\" data-step=\"1\" data-max=\"9990\" data-orientation=\"top\" data-use-custom-third-line=\"false\" data-force-page-reload=\"\" data-product-name=\"Essenza Mini Solo De\'Longhi Pure White\" data-number-of-units=\"1\" aria-disabled=\"false\" data-defaultcontent=\"\n        <img src=&quot;/mosaic/_ui/img/Elements/cross_plus.png&quot; alt=&quot;&quot; width=&quot;16&quot;>\n    \">\n\n    <span data-ng-if=\"currentProduct.addToCartButton.activated\" class=\"visually-hidden ng-binding ng-scope\" id=\"aria-EN85WSOLO\" tabindex=\"-1\">\n        You have <span class=\"quantity\">0</span> of  Essenza Mini Solo De\'Longhi Pure White in your basket, click on this button to <span class=\"label\">Add to basket</span>.</span>\n    <span data-ng-if=\"currentProduct.addToCartButton.activated\" aria-hidden=\"true\" class=\"left-part quantity ng-scope\" data-product-id=\"EN85WSOLO\">\n        <img src=\"/mosaic/_ui/img/Elements/cross_plus.png\" alt=\"\" width=\"16\">\n    </span>\n    <span data-ng-if=\"currentProduct.addToCartButton.activated\" aria-hidden=\"true\" class=\"label right-part ng-scope\">Add to basket</span></button>",
				"url": "https://www.nespresso.com/au/en/order/machines/essenza-mini-delonghi-pure-white-solo",
				"colorrgba": "background-color: rgb(255, 255, 255);"
			}
		]
	},
	{
		"name": "UMilk",
		"colors": [
			{
				"img": "/ecom/medias/sys_master/public/9952376979486/5-UMilk-Black-Main-684x378.jpg",
				"title": "UMilk DeLonghi Black",
				"price": "$299.00",
				"html": "<button type=\"button\" id=\"ta-add-to-cart-EN210BAE\" aria-labelledby=\"aria-EN210BAE\" class=\"button-primary pull-right button-green two-parts add-to-cart\" data-ng-class=\"{\'button-green two-parts add-to-cart\': currentProduct.addToCartButton.activated, \'add-to-cart disabled two-parts\': !currentProduct.addToCartButton.activated}\" data-product-id=\"EN210BAE\" data-min=\"0\" data-step=\"1\" data-max=\"9990\" data-orientation=\"top\" data-use-custom-third-line=\"false\" data-force-page-reload=\"\" data-product-name=\"UMilk DeLonghi Black\" data-number-of-units=\"1\" aria-disabled=\"false\" data-defaultcontent=\"\n        <img src=&quot;/mosaic/_ui/img/Elements/cross_plus.png&quot; alt=&quot;&quot; width=&quot;16&quot;>\n    \">\n\n    <span data-ng-if=\"currentProduct.addToCartButton.activated\" class=\"visually-hidden ng-binding ng-scope\" id=\"aria-EN210BAE\" tabindex=\"-1\">\n        You have <span class=\"quantity\">0</span> of  UMilk DeLonghi Black in your basket, click on this button to <span class=\"label\">Add to basket</span>.</span>\n    <span data-ng-if=\"currentProduct.addToCartButton.activated\" aria-hidden=\"true\" class=\"left-part quantity ng-scope\" data-product-id=\"EN210BAE\">\n        <img src=\"/mosaic/_ui/img/Elements/cross_plus.png\" alt=\"\" width=\"16\">\n    </span>\n    <span data-ng-if=\"currentProduct.addToCartButton.activated\" aria-hidden=\"true\" class=\"label right-part ng-scope\">Add to basket</span></button>",
				"url": "https://www.nespresso.com/au/en/order/machines/delonghi-umilk-pure-black-coffee-machine",
				"colorrgba": "background-color: rgb(0, 0, 0);"
			}
		]
	},
	{
		"name": "Inissia & Aeroccino",
		"colors": [
			{
				"img": "/ecom/medias/sys_master/public/9952346013726/5-Inissia-Black-Main-684x378.jpg",
				"title": "Inissia DeLonghi Black",
				"price": "$249.00",
				"html": "<button type=\"button\" id=\"ta-add-to-cart-EN80BAE\" aria-labelledby=\"aria-EN80BAE\" class=\"button-primary pull-right button-green two-parts add-to-cart\" data-ng-class=\"{\'button-green two-parts add-to-cart\': currentProduct.addToCartButton.activated, \'add-to-cart disabled two-parts\': !currentProduct.addToCartButton.activated}\" data-product-id=\"EN80BAE\" data-min=\"0\" data-step=\"1\" data-max=\"9990\" data-orientation=\"top\" data-use-custom-third-line=\"false\" data-force-page-reload=\"\" data-product-name=\"Inissia DeLonghi Black\" data-number-of-units=\"1\" aria-disabled=\"false\" data-defaultcontent=\"\n        <img src=&quot;/mosaic/_ui/img/Elements/cross_plus.png&quot; alt=&quot;&quot; width=&quot;16&quot;>\n    \">\n\n    <span data-ng-if=\"currentProduct.addToCartButton.activated\" class=\"visually-hidden ng-binding ng-scope\" id=\"aria-EN80BAE\" tabindex=\"-1\">\n        You have <span class=\"quantity\">0</span> of  Inissia DeLonghi Black in your basket, click on this button to <span class=\"label\">Add to basket</span>.</span>\n    <span data-ng-if=\"currentProduct.addToCartButton.activated\" aria-hidden=\"true\" class=\"left-part quantity ng-scope\" data-product-id=\"EN80BAE\">\n        <img src=\"/mosaic/_ui/img/Elements/cross_plus.png\" alt=\"\" width=\"16\">\n    </span>\n    <span data-ng-if=\"currentProduct.addToCartButton.activated\" aria-hidden=\"true\" class=\"label right-part ng-scope\">Add to basket</span></button>",
				"url": "https://www.nespresso.com/au/en/order/machines/delonghi-inissia-and-aeroccino3-black-coffee-machine",
				"colorrgba": "background-color: rgb(0, 0, 0);"
			}
		]
	},
	{
		"name": "Citiz",
		"colors": [
			{
				"img": "/ecom/medias/sys_master/public/10036009599006/M-Main-684x378black.jpg",
				"title": "CitiZ DeLonghi Black",
				"price": "$299.00",
				"html": "<button type=\"button\" id=\"ta-add-to-cart-EN167B\" aria-labelledby=\"aria-EN167B\" class=\"button-primary pull-right button-green two-parts add-to-cart\" data-ng-class=\"{\'button-green two-parts add-to-cart\': currentProduct.addToCartButton.activated, \'add-to-cart disabled two-parts\': !currentProduct.addToCartButton.activated}\" data-product-id=\"EN167B\" data-min=\"0\" data-step=\"1\" data-max=\"9990\" data-orientation=\"top\" data-use-custom-third-line=\"false\" data-force-page-reload=\"\" data-product-name=\"CitiZ DeLonghi Black\" data-number-of-units=\"1\" aria-disabled=\"false\" data-defaultcontent=\"\n        <img src=&quot;/mosaic/_ui/img/Elements/cross_plus.png&quot; alt=&quot;&quot; width=&quot;16&quot;>\n    \">\n\n    <span data-ng-if=\"currentProduct.addToCartButton.activated\" class=\"visually-hidden ng-binding ng-scope\" id=\"aria-EN167B\" tabindex=\"-1\">\n        You have <span class=\"quantity\">0</span> of  CitiZ DeLonghi Black in your basket, click on this button to <span class=\"label\">Add to basket</span>.</span>\n    <span data-ng-if=\"currentProduct.addToCartButton.activated\" aria-hidden=\"true\" class=\"left-part quantity ng-scope\" data-product-id=\"EN167B\">\n        <img src=\"/mosaic/_ui/img/Elements/cross_plus.png\" alt=\"\" width=\"16\">\n    </span>\n    <span data-ng-if=\"currentProduct.addToCartButton.activated\" aria-hidden=\"true\" class=\"label right-part ng-scope\">Add to basket</span></button>",
				"url": "https://www.nespresso.com/au/en/order/machines/citiz-delonghi-black",
				"colorrgba": "background-color: rgb(0, 0, 0);"
			}
		]
	},
	{
		"name": "Citiz & Milk",
		"colors": [
			{
				"img": "/ecom/medias/sys_master/public/10046738333726/8-Citiz-Milk-Black-Main-684x378.jpg",
				"title": "CitiZ&milk DeLonghi Black",
				"price": "$399.00",
				"html": "<button type=\"button\" id=\"ta-add-to-cart-EN267BAE\" aria-labelledby=\"aria-EN267BAE\" class=\"button-primary pull-right button-green two-parts add-to-cart\" data-ng-class=\"{\'button-green two-parts add-to-cart\': currentProduct.addToCartButton.activated, \'add-to-cart disabled two-parts\': !currentProduct.addToCartButton.activated}\" data-product-id=\"EN267BAE\" data-min=\"0\" data-step=\"1\" data-max=\"9990\" data-orientation=\"top\" data-use-custom-third-line=\"false\" data-force-page-reload=\"\" data-product-name=\"CitiZ&amp;milk DeLonghi Black\" data-number-of-units=\"1\" aria-disabled=\"false\" data-defaultcontent=\"\n        <img src=&quot;/mosaic/_ui/img/Elements/cross_plus.png&quot; alt=&quot;&quot; width=&quot;16&quot;>\n    \">\n\n    <span data-ng-if=\"currentProduct.addToCartButton.activated\" class=\"visually-hidden ng-binding ng-scope\" id=\"aria-EN267BAE\" tabindex=\"-1\">\n        You have <span class=\"quantity\">0</span> of  CitiZ&amp;milk DeLonghi Black in your basket, click on this button to <span class=\"label\">Add to basket</span>.</span>\n    <span data-ng-if=\"currentProduct.addToCartButton.activated\" aria-hidden=\"true\" class=\"left-part quantity ng-scope\" data-product-id=\"EN267BAE\">\n        <img src=\"/mosaic/_ui/img/Elements/cross_plus.png\" alt=\"\" width=\"16\">\n    </span>\n    <span data-ng-if=\"currentProduct.addToCartButton.activated\" aria-hidden=\"true\" class=\"label right-part ng-scope\">Add to basket</span></button>",
				"url": "https://www.nespresso.com/au/en/order/machines/delonghi-citiz-milk-black-coffee-machine",
				"colorrgba": "background-color: rgb(0, 0, 0);"
			},
			{
				"img": "/ecom/medias/sys_master/public/10047695159326/8-Citiz-Milk-Chrome-Main-684x378.jpg",
				"title": "CitiZ&milk Breville Chrome",
				"price": "$399.00",
				"html": "<button type=\"button\" id=\"ta-add-to-cart-BEC650MC\" aria-labelledby=\"aria-BEC650MC\" class=\"button-primary pull-right button-green two-parts add-to-cart\" data-ng-class=\"{\'button-green two-parts add-to-cart\': currentProduct.addToCartButton.activated, \'add-to-cart disabled two-parts\': !currentProduct.addToCartButton.activated}\" data-product-id=\"BEC650MC\" data-min=\"0\" data-step=\"1\" data-max=\"9990\" data-orientation=\"top\" data-use-custom-third-line=\"false\" data-force-page-reload=\"\" data-product-name=\"CitiZ&amp;milk Breville Chrome\" data-number-of-units=\"1\" aria-disabled=\"false\" data-defaultcontent=\"\n        <img src=&quot;/mosaic/_ui/img/Elements/cross_plus.png&quot; alt=&quot;&quot; width=&quot;16&quot;>\n    \">\n\n    <span data-ng-if=\"currentProduct.addToCartButton.activated\" class=\"visually-hidden ng-binding ng-scope\" id=\"aria-BEC650MC\" tabindex=\"-1\">\n        You have <span class=\"quantity\">0</span> of  CitiZ&amp;milk Breville Chrome in your basket, click on this button to <span class=\"label\">Add to basket</span>.</span>\n    <span data-ng-if=\"currentProduct.addToCartButton.activated\" aria-hidden=\"true\" class=\"left-part quantity ng-scope\" data-product-id=\"BEC650MC\">\n        <img src=\"/mosaic/_ui/img/Elements/cross_plus.png\" alt=\"\" width=\"16\">\n    </span>\n    <span data-ng-if=\"currentProduct.addToCartButton.activated\" aria-hidden=\"true\" class=\"label right-part ng-scope\">Add to basket</span></button>",
				"url": "https://www.nespresso.com/au/en/order/machines/breville-citiz-milk-chrome-coffee-machine",
				"colorrgba": "background-color: rgb(232, 241, 212);"
			},
			{
				"img": "/ecom/medias/sys_master/public/10047934857246/8-Citiz-Milk-White-Main-684x378.jpg",
				"title": "CitiZ&milk DeLonghi White",
				"price": "$399.00",
				"html": "<button type=\"button\" id=\"ta-add-to-cart-EN267WAE\" aria-labelledby=\"aria-EN267WAE\" class=\"button-primary pull-right button-green two-parts add-to-cart\" data-ng-class=\"{\'button-green two-parts add-to-cart\': currentProduct.addToCartButton.activated, \'add-to-cart disabled two-parts\': !currentProduct.addToCartButton.activated}\" data-product-id=\"EN267WAE\" data-min=\"0\" data-step=\"1\" data-max=\"9990\" data-orientation=\"top\" data-use-custom-third-line=\"false\" data-force-page-reload=\"\" data-product-name=\"CitiZ&amp;milk DeLonghi White\" data-number-of-units=\"1\" aria-disabled=\"false\" data-defaultcontent=\"\n        <img src=&quot;/mosaic/_ui/img/Elements/cross_plus.png&quot; alt=&quot;&quot; width=&quot;16&quot;>\n    \">\n\n    <span data-ng-if=\"currentProduct.addToCartButton.activated\" class=\"visually-hidden ng-binding ng-scope\" id=\"aria-EN267WAE\" tabindex=\"-1\">\n        You have <span class=\"quantity\">0</span> of  CitiZ&amp;milk DeLonghi White in your basket, click on this button to <span class=\"label\">Add to basket</span>.</span>\n    <span data-ng-if=\"currentProduct.addToCartButton.activated\" aria-hidden=\"true\" class=\"left-part quantity ng-scope\" data-product-id=\"EN267WAE\">\n        <img src=\"/mosaic/_ui/img/Elements/cross_plus.png\" alt=\"\" width=\"16\">\n    </span>\n    <span data-ng-if=\"currentProduct.addToCartButton.activated\" aria-hidden=\"true\" class=\"label right-part ng-scope\">Add to basket</span></button>",
				"url": "https://www.nespresso.com/au/en/order/machines/delonghi-citiz-milk-white-coffee-machine",
				"colorrgba": "background-color: rgb(255, 255, 204);"
			}
		]
	},
	{
		"name": "Latissima Touch",
		"colors": [
			{
				"img": "/ecom/medias/sys_master/public/9939483426846/5-LattTouch-Black-Main-684x378.jpg",
				"title": "Lattissima Touch Glam Black",
				"price": "$649.00",
				"html": "<button type=\"button\" id=\"ta-add-to-cart-EN550B\" aria-labelledby=\"aria-EN550B\" class=\"button-primary pull-right button-green two-parts add-to-cart\" data-ng-class=\"{\'button-green two-parts add-to-cart\': currentProduct.addToCartButton.activated, \'add-to-cart disabled two-parts\': !currentProduct.addToCartButton.activated}\" data-product-id=\"EN550B\" data-min=\"0\" data-step=\"1\" data-max=\"9990\" data-orientation=\"top\" data-use-custom-third-line=\"false\" data-force-page-reload=\"\" data-product-name=\"Lattissima Touch Glam Black\" data-number-of-units=\"1\" aria-disabled=\"false\" data-defaultcontent=\"\n        <img src=&quot;/mosaic/_ui/img/Elements/cross_plus.png&quot; alt=&quot;&quot; width=&quot;16&quot;>\n    \">\n\n    <span data-ng-if=\"currentProduct.addToCartButton.activated\" class=\"visually-hidden ng-binding ng-scope\" id=\"aria-EN550B\" tabindex=\"-1\">\n        You have <span class=\"quantity\">0</span> of  Lattissima Touch Glam Black in your basket, click on this button to <span class=\"label\">Add to basket</span>.</span>\n    <span data-ng-if=\"currentProduct.addToCartButton.activated\" aria-hidden=\"true\" class=\"left-part quantity ng-scope\" data-product-id=\"EN550B\">\n        <img src=\"/mosaic/_ui/img/Elements/cross_plus.png\" alt=\"\" width=\"16\">\n    </span>\n    <span data-ng-if=\"currentProduct.addToCartButton.activated\" aria-hidden=\"true\" class=\"label right-part ng-scope\">Add to basket</span></button>",
				"url": "https://www.nespresso.com/au/en/order/machines/lattissima-touch-glam-black-coffee-machine",
				"colorrgba": "background-color: rgb(0, 0, 0);"
			},
			{
				"img": "/ecom/medias/sys_master/public/9940639907870/5-LattTouch-GlamWhite-Main-684x378.jpg",
				"title": "Lattissima Touch Glam White",
				"price": "$649.00",
				"html": "<button type=\"button\" id=\"ta-add-to-cart-EN550W\" aria-labelledby=\"aria-EN550W\" class=\"button-primary pull-right button-green two-parts add-to-cart\" data-ng-class=\"{\'button-green two-parts add-to-cart\': currentProduct.addToCartButton.activated, \'add-to-cart disabled two-parts\': !currentProduct.addToCartButton.activated}\" data-product-id=\"EN550W\" data-min=\"0\" data-step=\"1\" data-max=\"9990\" data-orientation=\"top\" data-use-custom-third-line=\"false\" data-force-page-reload=\"\" data-product-name=\"Lattissima Touch Glam White\" data-number-of-units=\"1\" aria-disabled=\"false\" data-defaultcontent=\"\n        <img src=&quot;/mosaic/_ui/img/Elements/cross_plus.png&quot; alt=&quot;&quot; width=&quot;16&quot;>\n    \">\n\n    <span data-ng-if=\"currentProduct.addToCartButton.activated\" class=\"visually-hidden ng-binding ng-scope\" id=\"aria-EN550W\" tabindex=\"-1\">\n        You have <span class=\"quantity\">0</span> of  Lattissima Touch Glam White in your basket, click on this button to <span class=\"label\">Add to basket</span>.</span>\n    <span data-ng-if=\"currentProduct.addToCartButton.activated\" aria-hidden=\"true\" class=\"left-part quantity ng-scope\" data-product-id=\"EN550W\">\n        <img src=\"/mosaic/_ui/img/Elements/cross_plus.png\" alt=\"\" width=\"16\">\n    </span>\n    <span data-ng-if=\"currentProduct.addToCartButton.activated\" aria-hidden=\"true\" class=\"label right-part ng-scope\">Add to basket</span></button>",
				"url": "https://www.nespresso.com/au/en/order/machines/lattissima-touch-glam-white-coffee-machine",
				"colorrgba": "background-color: rgb(255, 255, 255);"
			},
			{
				"img": "/ecom/medias/sys_master/public/9940267991070/5-LattTouch-GlamRed-Main-684x378.jpg",
				"title": "Lattissima Touch Glam Red",
				"price": "$649.00",
				"html": "<button type=\"button\" id=\"ta-add-to-cart-EN550R\" aria-labelledby=\"aria-EN550R\" class=\"button-primary pull-right button-green two-parts add-to-cart\" data-ng-class=\"{\'button-green two-parts add-to-cart\': currentProduct.addToCartButton.activated, \'add-to-cart disabled two-parts\': !currentProduct.addToCartButton.activated}\" data-product-id=\"EN550R\" data-min=\"0\" data-step=\"1\" data-max=\"9990\" data-orientation=\"top\" data-use-custom-third-line=\"false\" data-force-page-reload=\"\" data-product-name=\"Lattissima Touch Glam Red\" data-number-of-units=\"1\" aria-disabled=\"false\" data-defaultcontent=\"\n        <img src=&quot;/mosaic/_ui/img/Elements/cross_plus.png&quot; alt=&quot;&quot; width=&quot;16&quot;>\n    \">\n\n    <span data-ng-if=\"currentProduct.addToCartButton.activated\" class=\"visually-hidden ng-binding ng-scope\" id=\"aria-EN550R\" tabindex=\"-1\">\n        You have <span class=\"quantity\">0</span> of  Lattissima Touch Glam Red in your basket, click on this button to <span class=\"label\">Add to basket</span>.</span>\n    <span data-ng-if=\"currentProduct.addToCartButton.activated\" aria-hidden=\"true\" class=\"left-part quantity ng-scope\" data-product-id=\"EN550R\">\n        <img src=\"/mosaic/_ui/img/Elements/cross_plus.png\" alt=\"\" width=\"16\">\n    </span>\n    <span data-ng-if=\"currentProduct.addToCartButton.activated\" aria-hidden=\"true\" class=\"label right-part ng-scope\">Add to basket</span></button>",
				"url": "https://www.nespresso.com/au/en/order/machines/lattissima-touch-glam-red-coffee-machine",
				"colorrgba": "background-color: rgb(255, 0, 0);"
			},
			{
				"img": "/ecom/medias/sys_master/public/9940884750366/5-LattTouch-BlackTitanium-Main-684x378.jpg",
				"title": "Lattissima Touch DeLonghi Black Titanium",
				"price": "$649.00",
				"html": "<button type=\"button\" id=\"ta-add-to-cart-EN550BM\" aria-labelledby=\"aria-EN550BM\" class=\"button-primary pull-right button-green two-parts add-to-cart\" data-ng-class=\"{\'button-green two-parts add-to-cart\': currentProduct.addToCartButton.activated, \'add-to-cart disabled two-parts\': !currentProduct.addToCartButton.activated}\" data-product-id=\"EN550BM\" data-min=\"0\" data-step=\"1\" data-max=\"9990\" data-orientation=\"top\" data-use-custom-third-line=\"false\" data-force-page-reload=\"\" data-product-name=\"Lattissima Touch DeLonghi Black Titanium\" data-number-of-units=\"1\" aria-disabled=\"false\" data-defaultcontent=\"\n        <img src=&quot;/mosaic/_ui/img/Elements/cross_plus.png&quot; alt=&quot;&quot; width=&quot;16&quot;>\n    \">\n\n    <span data-ng-if=\"currentProduct.addToCartButton.activated\" class=\"visually-hidden ng-binding ng-scope\" id=\"aria-EN550BM\" tabindex=\"-1\">\n        You have <span class=\"quantity\">0</span> of  Lattissima Touch DeLonghi Black Titanium in your basket, click on this button to <span class=\"label\">Add to basket</span>.</span>\n    <span data-ng-if=\"currentProduct.addToCartButton.activated\" aria-hidden=\"true\" class=\"left-part quantity ng-scope\" data-product-id=\"EN550BM\">\n        <img src=\"/mosaic/_ui/img/Elements/cross_plus.png\" alt=\"\" width=\"16\">\n    </span>\n    <span data-ng-if=\"currentProduct.addToCartButton.activated\" aria-hidden=\"true\" class=\"label right-part ng-scope\">Add to basket</span></button>",
				"url": "https://www.nespresso.com/au/en/order/machines/lattissima-touch-black-titanium-coffee-machine",
				"colorrgba": "background-color: rgb(0, 0, 0);"
			},
			{
				"img": "/ecom/medias/sys_master/public/9940884029470/5-LattTouch-PalladiumSilver-Main-684x378.jpg",
				"title": "Lattissima Touch DeLonghi Palladium Silver",
				"price": "$649.00",
				"html": "<button type=\"button\" id=\"ta-add-to-cart-EN550S\" aria-labelledby=\"aria-EN550S\" class=\"button-primary pull-right button-green two-parts add-to-cart\" data-ng-class=\"{\'button-green two-parts add-to-cart\': currentProduct.addToCartButton.activated, \'add-to-cart disabled two-parts\': !currentProduct.addToCartButton.activated}\" data-product-id=\"EN550S\" data-min=\"0\" data-step=\"1\" data-max=\"9990\" data-orientation=\"top\" data-use-custom-third-line=\"false\" data-force-page-reload=\"\" data-product-name=\"Lattissima Touch DeLonghi Palladium Silver\" data-number-of-units=\"1\" aria-disabled=\"false\" data-defaultcontent=\"\n        <img src=&quot;/mosaic/_ui/img/Elements/cross_plus.png&quot; alt=&quot;&quot; width=&quot;16&quot;>\n    \">\n\n    <span data-ng-if=\"currentProduct.addToCartButton.activated\" class=\"visually-hidden ng-binding ng-scope\" id=\"aria-EN550S\" tabindex=\"-1\">\n        You have <span class=\"quantity\">0</span> of  Lattissima Touch DeLonghi Palladium Silver in your basket, click on this button to <span class=\"label\">Add to basket</span>.</span>\n    <span data-ng-if=\"currentProduct.addToCartButton.activated\" aria-hidden=\"true\" class=\"left-part quantity ng-scope\" data-product-id=\"EN550S\">\n        <img src=\"/mosaic/_ui/img/Elements/cross_plus.png\" alt=\"\" width=\"16\">\n    </span>\n    <span data-ng-if=\"currentProduct.addToCartButton.activated\" aria-hidden=\"true\" class=\"label right-part ng-scope\">Add to basket</span></button>",
				"url": "https://www.nespresso.com/au/en/order/machines/lattissima-touch-palladium-silver-coffee-machine",
				"colorrgba": "background-color: rgb(128, 128, 128);"
			}
		]
	},
	{
		"name": "Expert & Milk",
		"colors": [
			{
				"img": "/ecom/medias/sys_master/public/10296043405342/8-Expert-Milk-Black-Main-684x378.jpg",
				"title": "Expert&Milk Breville Black",
				"price": "$599.00",
				"html": "<button type=\"button\" id=\"ta-add-to-cart-BEC780BLK\" aria-labelledby=\"aria-BEC780BLK\" class=\"button-primary pull-right button-green two-parts add-to-cart\" data-ng-class=\"{\'button-green two-parts add-to-cart\': currentProduct.addToCartButton.activated, \'add-to-cart disabled two-parts\': !currentProduct.addToCartButton.activated}\" data-product-id=\"BEC780BLK\" data-min=\"0\" data-step=\"1\" data-max=\"9990\" data-orientation=\"top\" data-use-custom-third-line=\"false\" data-force-page-reload=\"\" data-product-name=\"Expert&amp;Milk Breville Black\" data-number-of-units=\"1\" aria-disabled=\"false\" data-defaultcontent=\"\n        <img src=&quot;/mosaic/_ui/img/Elements/cross_plus.png&quot; alt=&quot;&quot; width=&quot;16&quot;>\n    \">\n\n    <span data-ng-if=\"currentProduct.addToCartButton.activated\" class=\"visually-hidden ng-binding ng-scope\" id=\"aria-BEC780BLK\" tabindex=\"-1\">\n        You have <span class=\"quantity\">0</span> of  Expert&amp;Milk Breville Black in your basket, click on this button to <span class=\"label\">Add to basket</span>.</span>\n    <span data-ng-if=\"currentProduct.addToCartButton.activated\" aria-hidden=\"true\" class=\"left-part quantity ng-scope\" data-product-id=\"BEC780BLK\">\n        <img src=\"/mosaic/_ui/img/Elements/cross_plus.png\" alt=\"\" width=\"16\">\n    </span>\n    <span data-ng-if=\"currentProduct.addToCartButton.activated\" aria-hidden=\"true\" class=\"label right-part ng-scope\">Add to basket</span></button>",
				"url": "https://www.nespresso.com/au/en/order/machines/expert-milk-breville-black-smart-coffee-machine",
				"colorrgba": "background-color: rgb(0, 0, 0);"
			},
			{
				"img": "/ecom/medias/sys_master/public/10284030525470/8-Expert-Milk-Grey-Main-684x378.jpg",
				"title": "Expert&Milk DeLonghi Grey",
				"price": "$599.00",
				"html": "<button type=\"button\" id=\"ta-add-to-cart-EN355GAE\" aria-labelledby=\"aria-EN355GAE\" class=\"button-primary pull-right button-green two-parts add-to-cart\" data-ng-class=\"{\'button-green two-parts add-to-cart\': currentProduct.addToCartButton.activated, \'add-to-cart disabled two-parts\': !currentProduct.addToCartButton.activated}\" data-product-id=\"EN355GAE\" data-min=\"0\" data-step=\"1\" data-max=\"9990\" data-orientation=\"top\" data-use-custom-third-line=\"false\" data-force-page-reload=\"\" data-product-name=\"Expert&amp;Milk DeLonghi Grey\" data-number-of-units=\"1\" aria-disabled=\"false\" data-defaultcontent=\"\n        <img src=&quot;/mosaic/_ui/img/Elements/cross_plus.png&quot; alt=&quot;&quot; width=&quot;16&quot;>\n    \">\n\n    <span data-ng-if=\"currentProduct.addToCartButton.activated\" class=\"visually-hidden ng-binding ng-scope\" id=\"aria-EN355GAE\" tabindex=\"-1\">\n        You have <span class=\"quantity\">0</span> of  Expert&amp;Milk DeLonghi Grey in your basket, click on this button to <span class=\"label\">Add to basket</span>.</span>\n    <span data-ng-if=\"currentProduct.addToCartButton.activated\" aria-hidden=\"true\" class=\"left-part quantity ng-scope\" data-product-id=\"EN355GAE\">\n        <img src=\"/mosaic/_ui/img/Elements/cross_plus.png\" alt=\"\" width=\"16\">\n    </span>\n    <span data-ng-if=\"currentProduct.addToCartButton.activated\" aria-hidden=\"true\" class=\"label right-part ng-scope\">Add to basket</span></button>",
				"url": "https://www.nespresso.com/au/en/order/machines/expert-milk-delonghi-grey-smart-coffee-machine",
				"colorrgba": "background-color: rgb(128, 128, 128);"
			}
		]
	},
	{
		"name": "Creatista Plus",
		"colors": [
			{
				"img": "/ecom/medias/sys_master/public/10194213470238/8-Creatista-Steel-Front-Jug-Main-684x378.jpg",
				"title": "Creatista Plus Breville Stainless Steel",
				"price": "$799.00",
				"html": "<button type=\"button\" id=\"ta-add-to-cart-BNE800BSS\" aria-labelledby=\"aria-BNE800BSS\" class=\"button-primary pull-right button-green two-parts add-to-cart\" data-ng-class=\"{\'button-green two-parts add-to-cart\': currentProduct.addToCartButton.activated, \'add-to-cart disabled two-parts\': !currentProduct.addToCartButton.activated}\" data-product-id=\"BNE800BSS\" data-min=\"0\" data-step=\"1\" data-max=\"9990\" data-orientation=\"top\" data-use-custom-third-line=\"false\" data-force-page-reload=\"\" data-product-name=\"Creatista Plus Breville Stainless Steel\" data-number-of-units=\"1\" aria-disabled=\"false\" data-defaultcontent=\"\n        <img src=&quot;/mosaic/_ui/img/Elements/cross_plus.png&quot; alt=&quot;&quot; width=&quot;16&quot;>\n    \">\n\n    <span data-ng-if=\"currentProduct.addToCartButton.activated\" class=\"visually-hidden ng-binding ng-scope\" id=\"aria-BNE800BSS\" tabindex=\"-1\">\n        You have <span class=\"quantity\">0</span> of  Creatista Plus Breville Stainless Steel in your basket, click on this button to <span class=\"label\">Add to basket</span>.</span>\n    <span data-ng-if=\"currentProduct.addToCartButton.activated\" aria-hidden=\"true\" class=\"left-part quantity ng-scope\" data-product-id=\"BNE800BSS\">\n        <img src=\"/mosaic/_ui/img/Elements/cross_plus.png\" alt=\"\" width=\"16\">\n    </span>\n    <span data-ng-if=\"currentProduct.addToCartButton.activated\" aria-hidden=\"true\" class=\"label right-part ng-scope\">Add to basket</span></button>",
				"url": "https://www.nespresso.com/au/en/order/machines/creatista-plus-breville-stainless-steel-coffee-machine",
				"colorrgba": "background-color: rgb(224, 223, 219);"
			}
		]
	},
	{
		"name": "Creatista",
		"colors": [
			{
				"img": "/ecom/medias/sys_master/public/10143393218590/8-Creatista-Champagne-Front-Jug-Main-684x378.jpg",
				"title": "Creatista Breville Royal Champagne",
				"price": "$699.00",
				"html": "<button type=\"button\" id=\"ta-add-to-cart-BNE600RCH\" aria-labelledby=\"aria-BNE600RCH\" class=\"button-primary pull-right button-green two-parts add-to-cart\" data-ng-class=\"{\'button-green two-parts add-to-cart\': currentProduct.addToCartButton.activated, \'add-to-cart disabled two-parts\': !currentProduct.addToCartButton.activated}\" data-product-id=\"BNE600RCH\" data-min=\"0\" data-step=\"1\" data-max=\"9990\" data-orientation=\"top\" data-use-custom-third-line=\"false\" data-force-page-reload=\"\" data-product-name=\"Creatista Breville Royal Champagne\" data-number-of-units=\"1\" aria-disabled=\"false\" data-defaultcontent=\"\n        <img src=&quot;/mosaic/_ui/img/Elements/cross_plus.png&quot; alt=&quot;&quot; width=&quot;16&quot;>\n    \">\n\n    <span data-ng-if=\"currentProduct.addToCartButton.activated\" class=\"visually-hidden ng-binding ng-scope\" id=\"aria-BNE600RCH\" tabindex=\"-1\">\n        You have <span class=\"quantity\">0</span> of  Creatista Breville Royal Champagne in your basket, click on this button to <span class=\"label\">Add to basket</span>.</span>\n    <span data-ng-if=\"currentProduct.addToCartButton.activated\" aria-hidden=\"true\" class=\"left-part quantity ng-scope\" data-product-id=\"BNE600RCH\">\n        <img src=\"/mosaic/_ui/img/Elements/cross_plus.png\" alt=\"\" width=\"16\">\n    </span>\n    <span data-ng-if=\"currentProduct.addToCartButton.activated\" aria-hidden=\"true\" class=\"label right-part ng-scope\">Add to basket</span></button>",
				"url": "https://www.nespresso.com/au/en/order/machines/creatista-breville-royal-champagne-coffee-machine",
				"colorrgba": "background-color: rgb(255, 255, 204);"
			},
			{
				"img": "/ecom/medias/sys_master/public/10142810341406/8-Creatista-Black-Front-Jug-Main-684x378.jpg",
				"title": "Creatista Breville Black",
				"price": "$699.00",
				"html": "<button type=\"button\" id=\"ta-add-to-cart-BNE600SLQ\" aria-labelledby=\"aria-BNE600SLQ\" class=\"button-primary pull-right button-green two-parts add-to-cart\" data-ng-class=\"{\'button-green two-parts add-to-cart\': currentProduct.addToCartButton.activated, \'add-to-cart disabled two-parts\': !currentProduct.addToCartButton.activated}\" data-product-id=\"BNE600SLQ\" data-min=\"0\" data-step=\"1\" data-max=\"9990\" data-orientation=\"top\" data-use-custom-third-line=\"false\" data-force-page-reload=\"\" data-product-name=\"Creatista Breville Black\" data-number-of-units=\"1\" aria-disabled=\"false\" data-defaultcontent=\"\n        <img src=&quot;/mosaic/_ui/img/Elements/cross_plus.png&quot; alt=&quot;&quot; width=&quot;16&quot;>\n    \">\n\n    <span data-ng-if=\"currentProduct.addToCartButton.activated\" class=\"visually-hidden ng-binding ng-scope\" id=\"aria-BNE600SLQ\" tabindex=\"-1\">\n        You have <span class=\"quantity\">0</span> of  Creatista Breville Black in your basket, click on this button to <span class=\"label\">Add to basket</span>.</span>\n    <span data-ng-if=\"currentProduct.addToCartButton.activated\" aria-hidden=\"true\" class=\"left-part quantity ng-scope\" data-product-id=\"BNE600SLQ\">\n        <img src=\"/mosaic/_ui/img/Elements/cross_plus.png\" alt=\"\" width=\"16\">\n    </span>\n    <span data-ng-if=\"currentProduct.addToCartButton.activated\" aria-hidden=\"true\" class=\"label right-part ng-scope\">Add to basket</span></button>",
				"url": "https://www.nespresso.com/au/en/order/machines/creatista-breville-black-coffee-machine",
				"colorrgba": "background-color: rgb(0, 0, 0);"
			},
			{
				"img": "/ecom/medias/sys_master/public/10266631831582/8-Creatista-Blueberry-Main-684x378-frontview.jpg",
				"title": "Creatista Breville Blueberry Granita",
				"price": "$699.00",
				"html": "<button type=\"button\" id=\"ta-add-to-cart-BNE600BBG\" aria-labelledby=\"aria-BNE600BBG\" class=\"button-primary pull-right button-green two-parts add-to-cart\" data-ng-class=\"{\'button-green two-parts add-to-cart\': currentProduct.addToCartButton.activated, \'add-to-cart disabled two-parts\': !currentProduct.addToCartButton.activated}\" data-product-id=\"BNE600BBG\" data-min=\"0\" data-step=\"1\" data-max=\"9990\" data-orientation=\"top\" data-use-custom-third-line=\"false\" data-force-page-reload=\"\" data-product-name=\"Creatista Breville Blueberry Granita\" data-number-of-units=\"1\" aria-disabled=\"false\" data-defaultcontent=\"\n        <img src=&quot;/mosaic/_ui/img/Elements/cross_plus.png&quot; alt=&quot;&quot; width=&quot;16&quot;>\n    \">\n\n    <span data-ng-if=\"currentProduct.addToCartButton.activated\" class=\"visually-hidden ng-binding ng-scope\" id=\"aria-BNE600BBG\" tabindex=\"-1\">\n        You have <span class=\"quantity\">0</span> of  Creatista Breville Blueberry Granita in your basket, click on this button to <span class=\"label\">Add to basket</span>.</span>\n    <span data-ng-if=\"currentProduct.addToCartButton.activated\" aria-hidden=\"true\" class=\"left-part quantity ng-scope\" data-product-id=\"BNE600BBG\">\n        <img src=\"/mosaic/_ui/img/Elements/cross_plus.png\" alt=\"\" width=\"16\">\n    </span>\n    <span data-ng-if=\"currentProduct.addToCartButton.activated\" aria-hidden=\"true\" class=\"label right-part ng-scope\">Add to basket</span></button>",
				"url": "https://www.nespresso.com/au/en/order/machines/creatista-breville-blueberry-granita-coffee-machine",
				"colorrgba": "background-color: rgb(120, 146, 163);"
			}
		]
	},
	{
		"name": "Kitchenaid",
		"colors": [
			{
				"img": "/ecom/medias/sys_master/public/9554744410142/M-0296-KitchenAid-Nespresso-Apple-Candy-Red-Latte-Bundle-main-684x378.jpg",
				"title": "KitchenAid  Candy Apple Red & Aeroccino3",
				"price": "$599.00",
				"html": "<button type=\"button\" id=\"ta-add-to-cart-98915B\" aria-labelledby=\"aria-98915B\" class=\"button-primary pull-right button-green two-parts add-to-cart\" data-ng-class=\"{\'button-green two-parts add-to-cart\': currentProduct.addToCartButton.activated, \'add-to-cart disabled two-parts\': !currentProduct.addToCartButton.activated}\" data-product-id=\"98915B\" data-min=\"0\" data-step=\"1\" data-max=\"9990\" data-orientation=\"top\" data-use-custom-third-line=\"false\" data-force-page-reload=\"\" data-product-name=\"KitchenAid  Candy Apple Red &amp; Aeroccino3\" data-number-of-units=\"1\" aria-disabled=\"false\" data-defaultcontent=\"\n        <img src=&quot;/mosaic/_ui/img/Elements/cross_plus.png&quot; alt=&quot;&quot; width=&quot;16&quot;>\n    \">\n\n    <span data-ng-if=\"currentProduct.addToCartButton.activated\" class=\"visually-hidden ng-binding ng-scope\" id=\"aria-98915B\" tabindex=\"-1\">\n        You have <span class=\"quantity\">0</span> of  KitchenAid  Candy Apple Red &amp; Aeroccino3 in your basket, click on this button to <span class=\"label\">Add to basket</span>.</span>\n    <span data-ng-if=\"currentProduct.addToCartButton.activated\" aria-hidden=\"true\" class=\"left-part quantity ng-scope\" data-product-id=\"98915B\">\n        <img src=\"/mosaic/_ui/img/Elements/cross_plus.png\" alt=\"\" width=\"16\">\n    </span>\n    <span data-ng-if=\"currentProduct.addToCartButton.activated\" aria-hidden=\"true\" class=\"label right-part ng-scope\">Add to basket</span></button>",
				"url": "https://www.nespresso.com/au/en/order/machines/kitchenaid-candy-apple-red-aeroccino3-coffee-machine",
				"colorrgba": "background-color: rgb(255, 0, 0);"
			},
			{
				"img": "/ecom/medias/sys_master/public/9896455274526/KitchenAidAlmondCream-Aeroccino3-main-684x378.jpg",
				"title": "KitchenAid Almond Cream & Aeroccino3",
				"price": "$599.00",
				"html": "<button type=\"button\" id=\"ta-add-to-cart-98905B\" aria-labelledby=\"aria-98905B\" class=\"button-primary pull-right button-green two-parts add-to-cart\" data-ng-class=\"{\'button-green two-parts add-to-cart\': currentProduct.addToCartButton.activated, \'add-to-cart disabled two-parts\': !currentProduct.addToCartButton.activated}\" data-product-id=\"98905B\" data-min=\"0\" data-step=\"1\" data-max=\"9990\" data-orientation=\"top\" data-use-custom-third-line=\"false\" data-force-page-reload=\"\" data-product-name=\"KitchenAid Almond Cream &amp; Aeroccino3\" data-number-of-units=\"1\" aria-disabled=\"false\" data-defaultcontent=\"\n        <img src=&quot;/mosaic/_ui/img/Elements/cross_plus.png&quot; alt=&quot;&quot; width=&quot;16&quot;>\n    \">\n\n    <span data-ng-if=\"currentProduct.addToCartButton.activated\" class=\"visually-hidden ng-binding ng-scope\" id=\"aria-98905B\" tabindex=\"-1\">\n        You have <span class=\"quantity\">0</span> of  KitchenAid Almond Cream &amp; Aeroccino3 in your basket, click on this button to <span class=\"label\">Add to basket</span>.</span>\n    <span data-ng-if=\"currentProduct.addToCartButton.activated\" aria-hidden=\"true\" class=\"left-part quantity ng-scope\" data-product-id=\"98905B\">\n        <img src=\"/mosaic/_ui/img/Elements/cross_plus.png\" alt=\"\" width=\"16\">\n    </span>\n    <span data-ng-if=\"currentProduct.addToCartButton.activated\" aria-hidden=\"true\" class=\"label right-part ng-scope\">Add to basket</span></button>",
				"url": "https://www.nespresso.com/au/en/order/machines/kitchenaid-almond-cream-aeroccino3-coffee-machine",
				"colorrgba": "background-color: rgb(255, 255, 204);"
			},
			{
				"img": "/ecom/medias/sys_master/public/9534807441438/KitchenAid-aeroccino3-empirered-main-648x378.jpg",
				"title": "KitchenAid Empire Red & Aeroccino3",
				"price": "$599.00",
				"html": "<button type=\"button\" id=\"ta-add-to-cart-98910B\" aria-labelledby=\"aria-98910B\" class=\"button-primary pull-right button-green two-parts add-to-cart\" data-ng-class=\"{\'button-green two-parts add-to-cart\': currentProduct.addToCartButton.activated, \'add-to-cart disabled two-parts\': !currentProduct.addToCartButton.activated}\" data-product-id=\"98910B\" data-min=\"0\" data-step=\"1\" data-max=\"9990\" data-orientation=\"top\" data-use-custom-third-line=\"false\" data-force-page-reload=\"\" data-product-name=\"KitchenAid Empire Red &amp; Aeroccino3\" data-number-of-units=\"1\" aria-disabled=\"false\" data-defaultcontent=\"\n        <img src=&quot;/mosaic/_ui/img/Elements/cross_plus.png&quot; alt=&quot;&quot; width=&quot;16&quot;>\n    \">\n\n    <span data-ng-if=\"currentProduct.addToCartButton.activated\" class=\"visually-hidden ng-binding ng-scope\" id=\"aria-98910B\" tabindex=\"-1\">\n        You have <span class=\"quantity\">0</span> of  KitchenAid Empire Red &amp; Aeroccino3 in your basket, click on this button to <span class=\"label\">Add to basket</span>.</span>\n    <span data-ng-if=\"currentProduct.addToCartButton.activated\" aria-hidden=\"true\" class=\"left-part quantity ng-scope\" data-product-id=\"98910B\">\n        <img src=\"/mosaic/_ui/img/Elements/cross_plus.png\" alt=\"\" width=\"16\">\n    </span>\n    <span data-ng-if=\"currentProduct.addToCartButton.activated\" aria-hidden=\"true\" class=\"label right-part ng-scope\">Add to basket</span></button>",
				"url": "https://www.nespresso.com/au/en/order/machines/kitchen-aid-empire-red-aeroccino3-coffee-machine",
				"colorrgba": "background-color: rgb(140, 0, 26);"
			}
		]
	},
	{
		"name": "Lattissima Pro",
		"colors": [
			{
				"img": "/ecom/medias/sys_master/public/9960785182750/5-LattissimaPro-Main-684x378.jpg",
				"title": "Lattissima Pro DeLonghi Brushed Aluminium",
				"price": "$899.00",
				"html": "<button type=\"button\" id=\"ta-add-to-cart-EN750MB\" aria-labelledby=\"aria-EN750MB\" class=\"button-primary pull-right button-green two-parts add-to-cart\" data-ng-class=\"{\'button-green two-parts add-to-cart\': currentProduct.addToCartButton.activated, \'add-to-cart disabled two-parts\': !currentProduct.addToCartButton.activated}\" data-product-id=\"EN750MB\" data-min=\"0\" data-step=\"1\" data-max=\"9990\" data-orientation=\"top\" data-use-custom-third-line=\"false\" data-force-page-reload=\"\" data-product-name=\"Lattissima Pro DeLonghi Brushed Aluminium\" data-number-of-units=\"1\" aria-disabled=\"false\" data-defaultcontent=\"\n        <img src=&quot;/mosaic/_ui/img/Elements/cross_plus.png&quot; alt=&quot;&quot; width=&quot;16&quot;>\n    \">\n\n    <span data-ng-if=\"currentProduct.addToCartButton.activated\" class=\"visually-hidden ng-binding ng-scope\" id=\"aria-EN750MB\" tabindex=\"-1\">\n        You have <span class=\"quantity\">0</span> of  Lattissima Pro DeLonghi Brushed Aluminium in your basket, click on this button to <span class=\"label\">Add to basket</span>.</span>\n    <span data-ng-if=\"currentProduct.addToCartButton.activated\" aria-hidden=\"true\" class=\"left-part quantity ng-scope\" data-product-id=\"EN750MB\">\n        <img src=\"/mosaic/_ui/img/Elements/cross_plus.png\" alt=\"\" width=\"16\">\n    </span>\n    <span data-ng-if=\"currentProduct.addToCartButton.activated\" aria-hidden=\"true\" class=\"label right-part ng-scope\">Add to basket</span></button>",
				"url": "https://www.nespresso.com/au/en/order/machines/delonghi-lattissima-pro-brushed-aluminium-coffee-machine",
				"colorrgba": "background-color: rgb(204, 204, 204);"
			}
		]
	}
];

function testStart() {
    
	if (document.location.href.indexOf('mobile')) {
		jQuery('body').addClass('optMobile');
		mobileIt()
	}

    var html= '<div class="optContent"> <div class="optBackground"> <div class="optSection" id="optSection1"> <h1 class="optUpper"> Dual Offer </h1> </div><div class="optSection" id="optSection2"> <div class="optTopRow"> <div class="optLeft"> <img src="https://image.ibb.co/jb2OfQ/creatistalogo.png"> <p class="optGoldy"> Why make coffee when you can create art? </p></div><div class="optRight"> <p class="optLarger"> Purchase Any Nespresso machine before 17 September and recieve $60 off your coffee order AND save $40 </p><p class="optRightSmaller"> Coffee must be purchased in the same transaction as the machine. <a class="optBlack" href="//nespresso.com">T&Cs apply</a> </p></div></div><div class="optBottomRow"> <div class="optLeft"> <img src="https://image.ibb.co/bzALS5/milk_drop.png"> </div><div class="optRight"> <img src="https://image.ibb.co/mYjEn5/milkdrop2.png"> </div></div></div></div><div class="optSection" id="optSection3"><div class="optStepContentText"><p class="optText optSectionTitle">How to benefit from your order</p><p class="optBolderText">Redeem your offer online in three simple steps</p><div class="optTextCont"><span class="optCircle">1</span><p class="optStepTitle">Step 1 - Select your machine</p></div><p class="optSmallest">All of our machines qualify for this offer. <a href="https://www.nespresso.com/au/en/order/machines/essenza-mini-delonghi-ruby-red?icid=B2C_Auen_Homepage_MachinesMegaMenu_2016_03_OrderMachine_1A" class="optOrange">Shop the entire range.</a><br>Here are some of our best sellers:</p></div><div class="optSwiper optMachines"></div></div><div class="optSection" id="optSection4"><div class="optStepContentText"><div class="optTextCont"><span class="optCircle">2</span><p class="optStepTitle">Step 2 - Select your coffee</p></div><p class="optSmallest">A minimum of 100 capsules are required to redeem your coffee discount. All of our capsules qualify for this offer. <br><a href="https://www.nespresso.com/au/en/order/capsules/tribute-to-milano-single-sleeve?icid=B2C_Auen_Homepage_CoffeeMegaMenu_2016_03_OrderCapsules_1A" class="optOrange">Shop the entire range.</a> <br>We recommend our <i>Tribute to Milano</i> assortment packs:</p></div><div class="optSwiper optCapsules"></div></div><div class="optSection" id="optSection5"><div class="optStepContentText"><div class="optTextCont"><span class="optCircle">3</span><p class="optStepTitle">Step 3 - Checkout</p></div><p class="optSmallest"> Your discounts will automatically be applied.</p><a href="/au/en/checkout" class="optGreen" id="optCheckout">CHECKOUT</a></div></div></div>',
        dataArrayCapsules = [{
            img:"https://www.nespresso.com/ecom/medias/sys_master/public/10367190335518/A-XXXX-crossPush-214x187-10sleeve.jpg",
            title:"10 Pack",
            price:"$74.00",
            button:'<button type="button" id="ta-add-to-cart-100915" aria-labelledby="aria-100915" class="button-primary pull-right button-green two-parts add-to-cart" data-ng-class="{\'button-green two-parts add-to-cart\': currentProduct.addToCartButton.activated, \'add-to-cart disabled two-parts\': !currentProduct.addToCartButton.activated}" data-product-id="100915" data-min="0" data-step="1" data-max="9990" data-orientation="top" data-use-custom-third-line="false" data-force-page-reload="" data-product-name="BREAKFAST 10 SLEEVE ASSORTMENT, TRIBUTE TO MILANO" data-number-of-units="100" aria-disabled="false" data-defaultcontent=" <img src=&quot;/mosaic/_ui/img/Elements/cross_plus.png&quot; alt=&quot;&quot; width=&quot;16&quot;> "> <span data-ng-if="currentProduct.addToCartButton.activated" class="visually-hidden ng-binding ng-scope" id="aria-100915" tabindex="-1"> You have <span class="quantity">0</span> of BREAKFAST 10 SLEEVE ASSORTMENT, TRIBUTE TO MILANO in your basket, click on this button to <span class="label">Add to basket</span>.</span> <span data-ng-if="currentProduct.addToCartButton.activated" aria-hidden="true" class="left-part quantity ng-scope" data-product-id="100915"> <img src="/mosaic/_ui/img/Elements/cross_plus.png" alt="" width="16"> </span> <span data-ng-if="currentProduct.addToCartButton.activated" aria-hidden="true" class="label right-part ng-scope">Add to basket</span> </button>'
        },{
            img:"https://www.nespresso.com/ecom/medias/sys_master/public/10367190597662/A-XXXX-bigPush-429x375.jpg",
            title:"5 Sleeve Milano Pack",
            price:"$46.50",
            button:'<button type="button" id="ta-add-to-cart-100917" aria-labelledby="aria-100917" class="button-primary pull-right button-green two-parts add-to-cart" data-ng-class="{\'button-green two-parts add-to-cart\': currentProduct.addToCartButton.activated, \'add-to-cart disabled two-parts\': !currentProduct.addToCartButton.activated}" data-product-id="100917" data-min="0" data-step="1" data-max="9990" data-orientation="top" data-use-custom-third-line="false" data-force-page-reload="" data-product-name="5 SLEEVE PACK, TRIBUTE TO MILANO" data-number-of-units="50" aria-disabled="false" data-defaultcontent=" <img src=&quot;/mosaic/_ui/img/Elements/cross_plus.png&quot; alt=&quot;&quot; width=&quot;16&quot;> "> <span data-ng-if="currentProduct.addToCartButton.activated" class="visually-hidden ng-binding ng-scope" id="aria-100917" tabindex="-1"> You have <span class="quantity">0</span> of 5 SLEEVE PACK, TRIBUTE TO MILANO in your basket, click on this button to <span class="label">Add to basket</span>.</span> <span data-ng-if="currentProduct.addToCartButton.activated" aria-hidden="true" class="left-part quantity ng-scope" data-product-id="100917"> <img src="/mosaic/_ui/img/Elements/cross_plus.png" alt="" width="16"> </span> <span data-ng-if="currentProduct.addToCartButton.activated" aria-hidden="true" class="label right-part ng-scope">Add to basket</span> </button>'
        },{
            img:"https://www.nespresso.com/ecom/medias/sys_master/public/10367190204446/A-XXXX-crossPush-214x187-5sleeve-breakfast.jpg",
            title:"5 Sleeve Milano Pack - Breakfast",
            price:"$39.00",
            button:'<button type="button" id="ta-add-to-cart-100913" aria-labelledby="aria-100913" class="button-primary pull-right button-green two-parts add-to-cart" data-ng-class="{\'button-green two-parts add-to-cart\': currentProduct.addToCartButton.activated, \'add-to-cart disabled two-parts\': !currentProduct.addToCartButton.activated}" data-product-id="100913" data-min="0" data-step="1" data-max="9990" data-orientation="top" data-use-custom-third-line="false" data-force-page-reload="" data-product-name="BREAKFAST 5 SLEEVE PACK, TRIBUTE TO MILANO" data-number-of-units="50" aria-disabled="false" data-defaultcontent=" <img src=&quot;/mosaic/_ui/img/Elements/cross_plus.png&quot; alt=&quot;&quot; width=&quot;16&quot;> "> <span data-ng-if="currentProduct.addToCartButton.activated" class="visually-hidden ng-binding ng-scope" id="aria-100913" tabindex="-1"> You have <span class="quantity">0</span> of BREAKFAST 5 SLEEVE PACK, TRIBUTE TO MILANO in your basket, click on this button to <span class="label">Add to basket</span>.</span> <span data-ng-if="currentProduct.addToCartButton.activated" aria-hidden="true" class="left-part quantity ng-scope" data-product-id="100913"> <img src="/mosaic/_ui/img/Elements/cross_plus.png" alt="" width="16"> </span> <span data-ng-if="currentProduct.addToCartButton.activated" aria-hidden="true" class="label right-part ng-scope">Add to basket</span> </button>'
        }];

    jQuery('#freeHTML').html(html);

    function loadSwiper() {
        loadSwipers(dataArrayCapsules, 1, '.optCapsules', 3, "capsule");
        loadSwipers(dataArrayMachines, 2, '.optMachines', 3, "machine");
        
    jQuery('.optColorCir').click(function () {

        var name = jQuery(this).attr('data-name'),
            datapath = getPath(dataArrayMachines, "title", name),
            datapath1 = datapath.split('.colors'),
            firstpath = datapath1[0].split('.').pop(),
            datapath2 = datapath.split('.title'),
            lastpath = datapath2[0].split('.').pop(),
            objpath = dataArrayMachines[firstpath].colors[lastpath],
            slide = jQuery(this).closest('.swiper-slide');
            if (name == objpath.title) {
            slide.find('.optImage').attr("style","background:url("+objpath.img+")");
            slide.find('.optSlideTitle').text(objpath.title);
            slide.find('.optContentS .optOrange').text(objpath.price);
            slide.find('button').replaceWith(objpath.html);
            }

        });
    }

    function loadSwipers(dataArray, num, location, tiles, type){
    var slides = "";
        if (type == "capsule") {
            for(var i = 0; i < dataArray.length; i++){
                var slide = '<div class="swiper-slide">';
                        slide += '<img class="optImage" src="'+dataArray[i].img+'">';
                        slide += '<div class="optContentS">';
                            slide += '<p class="optSlideTitle">'+dataArray[i].title+'</p>';
                            slide += '<p class="optOrange">'+dataArray[i].price+'</p>';
                        slide += '</div>';
                        slide += ''+dataArray[i].button+'';
                    slide += '</div>';
                slides += slide;
            }
        } else if (type == "machine") {
            for(var i = 0; i < dataArray.length; i++){
                var colorshtml = "";

                for (var c = 0; c < dataArray[i].colors.length; c++) {
                    var color = '<div class="optColorCir" style="'+dataArray[i].colors[c].colorrgba+'" data-name="'+dataArray[i].colors[c].title+'"></div>';
                    colorshtml += color;
                }

                var slide = '<div class="swiper-slide">';
                        slide += '<div class="optImage" style="background:url('+dataArray[i].colors[0].img+')"></div>';
                        slide += '<div class="optContentS">';
                            slide += '<p class="optSlideTitle">'+dataArray[i].colors[0].title+'</p>';
                            slide += '<p class="optOrange">'+dataArray[i].colors[0].price+'</p>';
                            slide += '<div class="optColorCont">'+colorshtml+'</div>';
                        slide += '</div>';
                        slide += ''+dataArray[i].colors[0].html+'';
                    slide += '</div>';
                slides += slide;
            }

        }

        var nextprev = `
            <div class="swiper-button-prev"></div>
            <div class="swiper-button-next"></div>
            `

            if (type == "capsule") {
                nextprev = ''
            }

    jQuery(".optSwiper"+location+"").append(`
        <div class="swiper-container">
            <div class="swiper-wrapper">
                `+slides+`
            </div>
            <div class="swiper-pagination"></div>
            </div>
            `+nextprev+`
    `);

        var machineSwiper = new Swiper (''+location+' .swiper-container', {
        // Optional parameters
        direction: 'horizontal',
        pagination: '.swiper-pagination',
        paginationClickable: true,
        // Navigation arrows
        nextButton: '.optMachines .swiper-button-next',
        prevButton: '.optMachines .swiper-button-prev',
        slidesPerView: tiles
    });
    }

    function getScript(src, callback) {
    var s = document.createElement('script');
    s.src = src;
    s.async = true;
    s.onreadystatechange = s.onload = function() {
        if (!callback.done && (!s.readyState || /loaded|complete/.test(s.readyState))) {
        callback.done = true;
        callback();
        }
    };
    document.querySelector('head').appendChild(s);
    }

    jQuery("head link[rel='stylesheet']").last().after("<link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/Swiper/3.4.2/css/swiper.min.css' type='text/css'>");

    getScript("https://cdnjs.cloudflare.com/ajax/libs/Swiper/3.4.2/js/swiper.min.js", loadSwiper);
   
}

defer(testStart, '#freeHTML');

