var opt_cvp = [
	"Free Delivery on orders of 200 capsules or more",
	"Free Delivery with any machine purchase",
	"Re-order your last order",
	"Track your order",
	"Participate in our recycling program",
	"Same day delivery"
];


$("head").append('<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">');

// tick icon
// <i class="fa fa-check" aria-hidden="true"></i>

$(".col.right .img").hide();

$(".col.right .img").after("<div class='opt-list'><ul class='fa-ul'></ul></div>");

$.each(opt_cvp, function(){
	var this_cvp = this;

	$(".opt-list ul").append("<li><i class='fa-li fa fa-check'></i>"+this_cvp+"</li>");
});