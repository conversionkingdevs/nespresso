
/**JS**/
/********************** 
Campaign Name: Test 3-01-02 Shipping Messaging
Author: Conversion Kings
Version: 2
***********************/
/**JS**/

/**Helper Functions**/
function defer(method, selector) {
    if (window.jQuery) {
        if (jQuery(selector).length > 0){
            method();
        } else {
            setTimeout(function() { defer(method, selector) }, 50);
        }  
    } else {
         setTimeout(function() { defer(method, selector) }, 50);
    }    
}

//update the highlight values
function highlight(num){
    $(".opt-item").removeClass("opt-active");
    var value = num/10;
    if(value >= 20){
        value = 20;
        var remaining = 0;
        $(".opt-content").hide();
        $(".opt-content-two").show();
    } else {
        var remaining = 200 - (value*10);
        $(".opt-content").show();
        $(".opt-content-two").hide();
        $('.opt-content #opt-capsules-left').text(remaining);
    }
    for(var i = 0; i < value; i++){
        if(i < value){
            $(".opt-item:eq("+i+")").addClass("opt-active");
        }
    }
}

function getItemQuantityFromName(name){
    if(name.indexOf("Trio") >= 0 || name.indexOf("TRIO") >= 0){
        return 30;
    } else if(name.indexOf("150") >= 0){
        return 150;
    } else if(name.indexOf("250") >= 0){    
        return 250;
    } else if(name.indexOf("15") >= 0){
      return 15 * 10;
    } else if(name.indexOf("5") >= 0){
        return 5 * 10;
    } else if(name.indexOf("10") >= 0){
        return 10 * 10;
    }             
    return 1;
}

// Gets capsule quantity in cart. Assumes nestms is updated correctly.
function getCapsuleQuantity(){
    var quantity = 0;
    var products = nestms.DataLayer.cart.products;
    for (var product in products) {
        switch(products[product].product.type){
            case "capsule":
                quantity += parseInt(products[product].quantity) * getItemQuantityFromName(products[product].product.name);
                break;
            default:
                break;
        }
    }
    return quantity;
}

function getQuantityByName(value){
    var type = "nameLocale";
    var products = nestms.DataLayer.cart.products;
    for (var product in products) {
        if(products[product].product[type].toLowerCase().trim() == value.toLowerCase().trim()){
        return parseInt(products[product].quantity);
        }
    }
    return 0;
}

function start () {
    $(".opt-container").remove();
    $("body").addClass("opt3-01-02");
    if($(window).width() <= 768){
        addMobile();
    } else {
        addDesktop();
    }
}

function addMobile(){
    $("#header").after('<div class="opt-container opt-mobile"></div>');
    buildHTML();
    $value = $("#cartProductsCount").text().trim();
    if($value != ""){
        highlight( $("#cartProductsCount").text().trim());
    } else {
        highlight("0");
    }
    (function(open) {
        XMLHttpRequest.prototype.open = function(m, u, a, us, p) {
            this.addEventListener('readystatechange', function() {
                if(this.response.indexOf('partialUpdates') >= 0) {
                    //wait for nestms to be updated
                    setTimeout(function(){
                        highlight($("#header .basket_number").text().trim());
                    },800);
                }
            }, false);
            open.call(this, m, u, a, us, p);
        };
    })(XMLHttpRequest.prototype.open);
}

function addDesktop(){
    $("#header").append('<div class="opt-container"></div>');
    //build html
    buildHTML();
    
    //set init value for highlight
    var value = getCapsuleQuantity();
    if(value  != undefined){
        highlight(value);
    } else {
        highlight("0");
    }
    
    (function(open) {
        XMLHttpRequest.prototype.open = function(m, u, a, us, p) {
            this.addEventListener('readystatechange', function() {
                if(this.response.indexOf('cartLines') >= 0) {
                    //wait for nestms to be updated
                    setTimeout(function(){
                        highlight(getCapsuleQuantity());
                    },500);
                }
            }, false);
            open.call(this, m, u, a, us, p);
        };
    })(XMLHttpRequest.prototype.open);
}

function buildHTML(){
    $(".opt-container").append('<div class="opt-top"></div><div class="opt-bottom"></div>');
    $(".opt-container .opt-top").append('<span class="opt-content">Add <span id="opt-capsules-left">200</span> more capsules to receive <span>Free Delivery</span></span>');
    $(".opt-container .opt-top").append('<span class="opt-content opt-content-two">You can now apply for <span>Free Delivery</span></span>');
    $(".opt-content-two").hide();
    $(".opt-container .opt-bottom").append('<div class="opt-capsule-container"></div>');
    for(var i = 0; i < 20; i++){
        if(i < 5){
            if(i == 4){
                $(".opt-container .opt-bottom .opt-capsule-container").append('<div class="opt-item opt-red"><span class="opt-top-text">Minimum</span><div class="opt-button" data-value='+(i+1)*10+'></div><span class="opt-bottom-text">50</span></div>');
            } else{
                $(".opt-container .opt-bottom .opt-capsule-container").append('<div class="opt-item opt-red"><span class="opt-top-text"></span><div class="opt-button" data-value='+(i+1)*10+'></div><span class="opt-bottom-text"></span></div>');
            }
        } else {
            if(i == 19){
                $(".opt-container .opt-bottom .opt-capsule-container").append('<div class="opt-item opt-green"><span class="opt-top-text">Free Delivery</span><div class="opt-button" data-value='+(i+1)*10+'></div><span class="opt-bottom-text">200</span></div>');
            } else {
                $(".opt-container .opt-bottom .opt-capsule-container").append('<div class="opt-item opt-green"><span class="opt-top-text"></span><div class="opt-button" data-value='+(i+1)*10+'></div><span class="opt-bottom-text"></span></div>');
            }
        }
    }
}

defer(function(){
    start();
}, "#header");